import React from 'react';
import { Alert } from 'react-native';
import { getCourseOrderId, setCourseOrderId, removeCourseOrderId, getProductOrderId, setProductOrderId, removeProductOrderId, HTTP_NO_CONTENT } from '../src/assets/constants';
import { getSelectedCourses, loadUserOrder } from '../src/services/ServiceWorker';
import context from './context';
import OrderContext from './OrderContext';

class OrderState extends React.Component {
    constructor() {
        super()

        this.state = {
            //#region Order Product
            productOrder: {
                orderDetailList: [],
            },
            isProductOrderLoaded: false,
            //#endregion

            //#region Order Course
            courseOrder: {
                orderDetailList: [],
            },
            isCourseOrderLoaded: false,

            selectedCourses: [],
            isSelectedLoaded: false
            //#endregion
        }
    }

    static contextType = context

    async componentDidMount() {
        const {userInfo, isLoggedIn} = this.context.state;
        if(isLoggedIn && userInfo && userInfo.id) {
            await this.handleSetCourseOrder();
            await this.handleSetProductOrder();
        }
    }

    async componentDidUpdate(prevProps, prevState) {
        const {userInfo, isAppLoaded, isLoggedIn} = this.context.state;
        if(isAppLoaded && isLoggedIn && userInfo && userInfo.id && !this.state.isCourseOrderLoaded && !this.state.isProductOrderLoaded) {
            await this.handleSetCourseOrder();
            await this.handleSetProductOrder();
        }
    }

    //#region Courses Order
    handleSetCourseOrder = async() => {
        const courseData = await getCourseOrderId();
        const courseOrderId = parseInt(courseData);

        if(courseOrderId && courseOrderId !== 0) {
            await setCourseOrderId(courseOrderId)
            await this.loadSelectedCourses(courseOrderId);
            await this.loadCourseOrder(courseOrderId);
        } else {
            await setCourseOrderId(0)
            this.setState({
                isSelectedLoaded: true,
                isCourseOrderLoaded: true
            })
        }
    }

    loadSelectedCourses = async(courseOrderId) => {
        const selectedCourses = await getSelectedCourses(this.context.state.userInfo?.id, this.context.state.version, courseOrderId);
        if(selectedCourses && Array.isArray(selectedCourses)) {
            this.setState({
                selectedCourses: selectedCourses,
                isSelectedLoaded: true,
            })
        } else {
            this.setState({
                isSelectedLoaded: true,
                isCourseOrderLoaded: true
            })
        }
    }

    loadCourseOrder = async(courseOrderId) => {
        if(courseOrderId) {
            const {state} = this.context;
            const courseOrder = await loadUserOrder(state.userInfo?.id, state.version, courseOrderId, false);
            if(courseOrder && courseOrder.id) {
                this.setState({
                    courseOrder: courseOrder,
                    isCourseOrderLoaded: true
                })
            } else {
                if(courseOrder === HTTP_NO_CONTENT) {
                    await setCourseOrderId(0);
                }

                this.setState({
                    isCourseOrderLoaded: true
                })
            }
        }
    }

    setCourseOrder = (courseOrder) => {
        this.setState({
            courseOrder: courseOrder
        })
    }

    handleReloadCourseOrder = async(courseOrderId) => {
        await this.loadSelectedCourses(courseOrderId);
        await this.loadCourseOrder(courseOrderId)
    }

    removeCourseOrderId = async () => {
        await removeCourseOrderId()
    }
    //#endregion

     //#region Product Order
    handleSetProductOrder = async() => {
        const productData = await getProductOrderId();
        const productOrderId = parseInt(productData);

        if(productOrderId && productOrderId !== 0) {
            await setProductOrderId(productOrderId)
            await this.loadProductOrder(productOrderId);
        } else {
            await setProductOrderId(0);
            this.setState({
                isProductOrderLoaded: true
            })
        }
    }

    loadProductOrder = async(productOrderId) => {
        if(productOrderId) {
            const {state} = this.context;
            const productOrder = await loadUserOrder(state.userInfo?.id, state.version, productOrderId, false);
            if(productOrder && productOrder.id) {
                this.setState({
                    productOrder: productOrder,
                    isProductOrderLoaded: true
                })
            } else {
                if(courseOrder === HTTP_NO_CONTENT) {
                    await setProductOrderId(0);
                }

                this.setState({
                    isProductOrderLoaded : true
                })
            }
        }
    }

    setProductOrder = (productOrder) => {
        this.setState({
            productOrder : productOrder
        })
    }

    handleReloadProductOrder = async(productOrderId) => {
        await this.loadProductOrder(productOrderId)
    }

    removeProductOrderId = async() => {
        await removeProductOrderId();
    }
    //#endregion

    //#region Clear Order
    clearOrderState = async() => {
        await setCourseOrderId(0);
        await setProductOrderId(0);

        this.setState({
            productOrder: {
                orderDetailList: []
            },

            courseOrder: {
                orderDetailList: []
            },

            selectedCourses: []
        })
    }

    clearCourseOrder = async() => {
        await setCourseOrderId(0);

        this.setState({
            courseOrder: {
                orderDetailList: []
            },

            selectedCourses: []
        })
    }

    clearProductOrder = async() => {
        await setProductOrderId(0);

        this.setState({
            productOrder: {
                orderDetailList: []
            }
        })
    }
    //#endregion

    render() {
        return (
            <OrderContext.Provider
                value={{
                    orderState: this.state,
                    orderActions: {
                        reloadCourseOrder: this.handleReloadCourseOrder,
                        reloadProductOrder: this.handleReloadProductOrder,
                        setCourseOrder: this.setCourseOrder,
                        setProductOrder: this.setProductOrder,
                        clearOrderState: this.clearOrderState,
                        clearCourseOrder: this.clearCourseOrder,
                        clearProductOrder:  this.clearProductOrder
                    }
                }}
            >
                {this.props.children}
            </OrderContext.Provider>
        )
    }
}

export default OrderState
