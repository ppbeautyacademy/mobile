import React from 'react'

export type DefaultPopupProps = {
    isVisible: false,
    title: '',
    message: '',
    okText: null,
    cancelText: null,
    onOkHandler: () => {},
    onCancelHandler: () => {},
    onDismiss: () => {},
}

export default React.createContext({
    // STATE VALUES
    theme: 'light',
    spinnerVisible: false,
    popupProps: {},
    version: '',
    token: '',
    userAuth: {
        token: '',
        refreshToken: '',
    },
    userInfo: null,

    // STATE ACTIONS
    setVersion: () => {},
    setToken: token => {},
    setRefreshToken: refreshToken => {},
    toggleTheme: () => {},
    showAppSpinner: show => {},
    showAppPopup: ({}: DefaultPopupProps) => {},
    setIsLoggedIn: isLoggedIn => {},
    setIsAppLoaded: isLoaded => {},
    setAppCurrency: curreny => {},
    setUserInfo: userInfo => {},
    resetUserInfo: () => {}
})
