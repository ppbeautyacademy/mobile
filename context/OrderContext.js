import React from 'react'

export default React.createContext({ 
    //#region Order State
    productOrder: {
        orderDetailList: []
    },
    courseOrder: {
        orderDetailList: []
    },
    selectedCoures: [],
    //#endregion

    //#region Order Actions
    reloadCourseOrder: orderId => {},
    reloadProductOrder: orderId => {}
    //#endregion
})