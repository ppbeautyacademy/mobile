import React, {useContext, useMemo} from 'react'
import context from './context'
import axiosInstance from '../config/AxiosConfig'
import {
    BACKEND_TRANSLATION,
    getVersion,
    HTTP_STATUS_BAD_REQUEST,
    HTTP_STATUS_OK,
    LANGUAGE_CODES,
    whitelist,
} from '../src/assets/constants'
import {translate} from '../config/i18n'
import {clearAuthState, setUserAuth} from '../src/services/AuthService'
import {getNewToken} from '../src/services/ServiceWorker'
import logService from '../src/services/logService'
import {rootNavigationRef} from '../src/Navigation'
import {ScreenIds} from '../src/ScreenIds'
import {getUserCredentials, removeUserCredentials} from '../src/services/secureData'
import { Toast } from 'popup-ui'
import { Keyboard } from 'react-native'
import { COLORS } from '../src/assets/theme/colors'

const AppWithAxios = ({children}) => {
    const {state, actions} = useContext(context)

    useMemo(async () => {
        if (state.userAuth.token) {
            axiosInstance.defaults.headers.common[
                'Authorization'
            ] = `Bearer ${state.userAuth.token}`
        } else {
            let userAuthInfo
            await getUserCredentials()
                .then(authInfo => {
                    userAuthInfo = authInfo
                })
                .catch(err => logService.log(err))
            if (!userAuthInfo?.token) {
                delete axiosInstance.defaults.headers.common['Authorization']
            } else {
                axiosInstance.defaults.headers.common[
                    'Authorization'
                ] = `Bearer ${userAuthInfo?.token}`
            }
        }
    }, [state.userAuth.token])

    useMemo(() => {
        axiosInstance.interceptors.request.use(function (config) {
            // Do something before request is sent
            Keyboard.dismiss();

            console.log('request', JSON.stringify(config))

            return config;
          }, function (error) {
            // Do something with request error
            return Promise.reject(error);
          });

        axiosInstance.interceptors.response.use(
            async function (response) {
                // Any status code that lie within the range of 2xx cause this function to trigger
                // Do something with response data
                return response
            },
            async function (error) {
                // Any status codes that falls outside the range of 2xx cause this function to trigger
                // Do something with response error
                let res = error.response;
                let originalRequest = error.config;
                
                if (res) {
                    switch (res.status) {
                        case 500:
                            const version = getVersion()
                            if (version === LANGUAGE_CODES.vi.value) {
                                Toast.show({
                                    title: translate('common.errorTitle'),
                                    text: 'Lỗi hệ thống. Vui lòng thử lại sau',
                                    color: COLORS.ERROR_NOTIFY,
                                    timing: 3500,
                                })
                            } else {
                                Toast.show({
                                    title: translate('common.errorTitle'),
                                    text: 'Problem occurs. Please try again',
                                    color: COLORS.ERROR_NOTIFY,
                                    timing: 3500,
                                })
                            }

                            break
                        case 400:
                            if (
                                res.data &&
                                typeof res.data === 'string' &&
                                res.data.includes(BACKEND_TRANSLATION)
                            ) {
                                Toast.show({
                                    title: translate('common.errorTitle'),
                                    text: translate(res.data),
                                    color: COLORS.ERROR_NOTIFY,
                                    timing: 3500,
                                })
                            } else if(
                                res.data && 
                                typeof res.data === 'string' &&
                                res.data !== HTTP_STATUS_BAD_REQUEST
                            ) {
                                Toast.show({
                                    title: translate('common.errorTitle'),
                                    text: translate(res.data),
                                    color: COLORS.ERROR_NOTIFY,
                                    timing: 3500,
                                })
                            } else {
                                Toast.show({
                                    title: translate('common.errorTitle'),
                                    text: "Fail to excecute your request !",
                                    color: COLORS.ERROR_NOTIFY,
                                    timing: 3500,
                                })
                            }
                            logService.log(res)
                            break
                        case 404:
                            Toast.show({
                                title: translate('common.errorTitle'),
                                text: "Data not found !",
                                color: COLORS.ERROR_NOTIFY,
                                timing: 3500,
                            })
                            break
                        case 401:
                            if (
                                res.config &&
                                !whitelist.includes(res.config.url)
                            ) {
                                if(
                                    !originalRequest._retry
                                ) {
                                    originalRequest._retry = true;

                                    delete axiosInstance.defaults.headers.common['Authorization']

                                    let userAuthInfo
                                    await getUserCredentials()
                                        .then(authInfo => {
                                            userAuthInfo = authInfo
                                        })
                                        .catch(err => logService.log(err))
    
                                    await getNewToken(userAuthInfo?.refreshToken)
                                        .then(async response => {
                                            if (response && response.token) {
                                                axiosInstance.defaults.headers.common[
                                                    'Authorization'
                                                ] = `Bearer ${response.token}`
    
                                                const {token, refreshToken, tokenType} = response
                                                actions.setToken(token);
                                                actions.setRefreshToken(refreshToken);
                                                
                                                await setUserAuth({token, refreshToken, tokenType})

                                                originalRequest.headers.Authorization = `Bearer ${response.token}`
    
                                                error = await Promise.resolve(await axiosInstance.request(originalRequest));
                                            } else {
                                                await clearAuthState()
                                                actions.resetUserAuth()
    
                                                actions.setIsLoggedIn(false)
                                                rootNavigationRef.current?.navigate(
                                                    ScreenIds.Login,
                                                )
                                            }
                                        })
                                        .catch(err => {
                                            logService.log(err)
                                        })
                                    }
                            } 
                            else {
                                Toast.show({
                                    title: translate('common.errorTitle'),
                                    text: 'Thông tài không chính xác',
                                    color: '#DB646A',
                                    timing: 3500,
                                })
                            }
                            break
                        case 403:
                            await removeUserCredentials();
                        default:
                    }
                }
                
                if(originalRequest._retry) {
                    return Promise.resolve(error);
                }
                return Promise.reject(error)
            },
        )
    }, [])

    return children
}

export default AppWithAxios
