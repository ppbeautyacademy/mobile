import React from 'react'
import Spinner from 'react-native-loading-spinner-overlay'
import {SafeAreaProvider} from 'react-native-safe-area-context'
import {
    APP_CURRENCY,
    getVersion,
    LANGUAGE_CODES,
    setVersion,
} from '../src/assets/constants'
import { COLORS } from '../src/assets/theme/colors'
import Popup from '../src/component/Popup'
import Context, {defaultPopupProps} from './context'

export default class GlobalState extends React.Component {
    state = {
        //#region User
        version: null,
        userAuth: {
            token: '',
            refreshToken: '',
        },
        userInfo: null,
        //#endregion

        //#region App
        appCurrency: APP_CURRENCY.CAD.value,
        theme: 'light',
        spinnerVisible: false,
        isLoggedIn: false,
        isAppLoaded: false,
        popupProps: defaultPopupProps,
        //#endregion
    }

    async componentDidMount() {
        this.handleSetVersion();
        this.setIsAppLoaded(true);
    }  

    //#region APP
    handleSetVersion = async () => {
        const version = await getVersion()
        if (!version) {
            setVersion(LANGUAGE_CODES.en.value)

            this.setVersion(LANGUAGE_CODES.en.value)
        } else {
            this.setVersion(version)
        }
    }

    setToken = token => {
        this.setState({
            userAuth: {
                ...this.state.userAuth,
                token,
            },
        })
    }

    setRefreshToken = refreshToken => {
        this.setState({
            userAuth: {
                ...this.state.userAuth,
                refreshToken,
            },
        })
    }

    setAppCurrency = appCurrency => {
        this.setState({
            appCurrency,
        })
    }

    setVersion = version => {
        this.setState({
            version: version,
        })
    }

    toggleTheme = () => {
        this.setState({
            theme: this.state.theme === 'light' ? 'dark' : 'light',
        })
    }

    showAppSpinner = (show: Boolean = false) => {
        this.setState({
            spinnerVisible: show,
        })
    }

    showWhiteAppSpinner = (show: Boolen = false) => {
        this.setState({
            whiteSpinnerVisible: show
        })
    }

    onDismissPopup = () => {
        this.setState({
            popupProps: {
                ...this.state.popupProps,
                isVisible: false,
            },
        })
    }

    showAppPopup = props => {
        this.setState({
            popupProps: {
                ...defaultPopupProps,
                ...props,
                onDismiss: this.onDismissPopup,
            },
        })
    }

    setIsLoggedIn = isLoggedIn => {
        this.setState({
            isLoggedIn,
        })
    }

    setIsAppLoaded = isLoaded => {
        this.setState({
            isAppLoaded: isLoaded,
        })
    }

    setUserInfo = userInfo => {
        this.setState({
            userInfo,
        })
    }

    resetUserInfo = () => {
        this.setState({
            userInfo: null,
        })
    }
    //#endregion

    resetUserAuth = () => {
        this.setState({
            userAuth: {
                token: '',
                refreshToken: '',
            },
        })
    }

    render() {
        return (
            <Context.Provider
                value={{
                    state: this.state,
                    actions: {
                        toggleTheme: this.toggleTheme,
                        showAppSpinner: this.showAppSpinner,
                        showWhiteAppSpinner: this.showWhiteAppSpinner,
                        showAppPopup: this.showAppPopup,
                        setIsAppLoaded: this.setIsAppLoaded,
                        setIsLoggedIn: this.setIsLoggedIn,
                        setVersion: this.setVersion,
                        setToken: this.setToken,
                        setRefreshToken: this.setRefreshToken,
                        setAppCurrency: this.setAppCurrency,
                        setUserInfo: this.setUserInfo,
                        resetUserInfo: this.resetUserInfo,
                        resetUserAuth: this.resetUserAuth,
                    },
                }}>
                <SafeAreaProvider>
                    <Popup {...this.state.popupProps} />
                    <Spinner
                        visible={this.state.spinnerVisible}
                        color={COLORS.SUB}
                        overlayColor={COLORS.TRANSPARENT}
                        animation="fade"
                    />
                    <Spinner
                        visible={this.state.whiteSpinnerVisible}
                        color={COLORS.SUB}
                        overlayColor={COLORS.WHITE}
                        animation="fade"
                    />
                    {this.props.children}
                </SafeAreaProvider>
            </Context.Provider>
        )
    }
}
