import { Button, Icon, Layout } from '@ui-kitten/components';
import React from 'react';
import { Text, View, TouchableHighlight, Image, TouchableOpacity  } from 'react-native';
import Context from '../context/context';
import { StyleSheet } from 'react-native';
import { withInAppNotification } from 'react-native-in-app-notification';
import { Root, Popup } from 'popup-ui'

function Home(props) {
    const customToast = () => {
        return(
            <View style={{padding: 15, backgroundColor: 'grey'}}>
                <Text>hi</Text>
            </View>
        )
    }

    return (
        <Context.Consumer>
            {(context) => (
                <Root>
                    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View>
                            {/* <Text>{context.state.quantity}</Text> */}
                            <Button appearance="outline" onPress={() => toast.show(
                                "Product item added"
                            )}>Toast Test</Button>
                        </View>

                        <View>
                            <Button onPress={() => context.actions.toggleTheme()}>ădawd</Button>
                            <Text>This is my app</Text>
                            <TouchableHighlight
                                onPress={() => {
                                    props.showNotification({
                                    icon: {uri:'https://th.bing.com/th/id/R.7e19dcff0fb3cdee770ff4b202df7710?rik=Io5Ur61v3ClQZw&riu=http%3a%2f%2fwww.designbust.com%2fdownload%2f636%2fthumb%2ftwitter_logo_transparent_png_thum.png&ehk=Etr%2bt8oZ3t1VzTZEijp272N28ppO%2b4WmO9k89WdX0Wg%3d&risl=&pid=ImgRaw&r=0'},
                                    title: 'You pressed it!',
                                    message: 'The notification has been triggered',
                                    // onPress: () => Alert.alert('Alert', 'You clicked the notification!'),
                                    vibrate: true
                                })}}
                            >
                                <Text>Click me to trigger a notification</Text>
                            </TouchableHighlight>
                        </View>

                    
                        <View>
                            <TouchableOpacity
                                onPress={() =>
                                    Popup.show({
                                        type: 'Danger',
                                        title: 'Upload complete',
                                        button: true,
                                        textBody: 'Congrats! Your upload successfully done',
                                        buttontext: 'Ok',
                                        autoClose: true,
                                        callback: () => Popup.hide()
                                    })
                                }
                            >
                                <Text>Open Popup</Text>
                            </TouchableOpacity>
                        </View>
                    </Layout>
                </Root>
            )}
        </Context.Consumer>
    )
}

export default withInAppNotification(Home);

const styles = StyleSheet.create({
    icon: {
      width: 32,
      height: 32,
    },
  });