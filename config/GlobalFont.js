import React from "react"
import wrap from 'lodash.wrap'
import { Button as KittenButton, Input as KittenInput, Text as KittenText} from "@ui-kitten/components"
import { Text, Button, TextInput, StyleSheet } from 'react-native';

let _applyed = false
export default class GlobalFont {
    static applyGlobal() {
        if (_applyed) { return }
        KittenText.render = wrap(KittenText.render, function (func, ...args) {
            let originText = func.apply(this, args)
            return (
                React.cloneElement(originText, {
                    style: [
                        styles.mainFont,
                        originText.props.style
                    ]
                })
            )
        })
        KittenInput.render = wrap(KittenInput.render, function (func, ...args) {
          let originTextInput = func.apply(this, args)
          return React.cloneElement(originTextInput, {
            style: [
                styles.mainFont,
                originTextInput.props.style
            ]
          })
        })
        Text.render = wrap(Text.render, function (func, ...args) {
            let originText = func.apply(this, args)
            return (
                React.cloneElement(originText, {
                    style: [
                        styles.mainFont,
                        originText.props.style
                    ]
                })
            )
        })
        TextInput.render = wrap(TextInput.render, function (func, ...args) {
          let originTextInput = func.apply(this, args)
          return React.cloneElement(originTextInput, {
            style: [
                styles.mainFont,
                originTextInput.props.style
            ]
          })
        })
        Button.render = wrap(Button.render, function (func, ...args) {
          let originTextInput = func.apply(this, args)
          return React.cloneElement(originTextInput, {
            style: [
                styles.mainFont,
                originTextInput.props.style
            ]
          })
        })
        _applyed = true
  }
}

const styles = StyleSheet.create({
    mainFont: {
      fontFamily: 'Prata-Regular'
    },
  });