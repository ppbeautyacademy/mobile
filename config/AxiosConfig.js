import axios from 'axios'
import _ from 'denodeify'
const axiosInstance = axios.create()

axiosInstance.defaults.headers.post['Content-Type'] = 'application/json'
export default axiosInstance
