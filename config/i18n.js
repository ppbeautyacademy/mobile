import i18n from 'i18next'
import {initReactI18next} from 'react-i18next'
import {getLocales} from 'react-native-localize'

import en from '../src/assets/localize/en.json'
import vi from '../src/assets/localize/vi.json'
import logService from '../src/services/logService'

const resources = {
    vi: {
        translation: vi,
    },
    en: {
        translation: en,
    },
}

export const languages = [
    {
        label: 'vietnamese',
        abbr: 'vn',
        lang: 'vi, vi-VN',
        code: 'vi',
    },
    {
        label: 'english',
        abbr: 'en',
        lang: 'en, en-GB',
        code: 'en',
    },
]
const DEFAULT_LANGUAGE = languages[1]

i18n.use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        fallbackLng: 'en',

        keySeparator: '.', // we do not use keys in form messages.welcome

        interpolation: {
            escapeValue: false, // react already safes from xss
        },
    })

export function getLanguageInfo(language) {
    if (!language) {
        return DEFAULT_LANGUAGE
    }
    const langCodes = languages.filter(item => item.code === language)
    if (langCodes && langCodes.length) {
        return langCodes[0]
    }

    return DEFAULT_LANGUAGE
}

export function getAppLanguage() {
    return i18n.language
}

export async function setMainLocaleLanguage(language: string, cb = () => {}) {
    try {
        await i18n.changeLanguage(language, cb)
    } catch (error) {
        logService.log(error)
    }
}

export function loadDeviceLocale() {
    const locales = getLocales()
    if (!isEmpty(locales)) {
        return locales[0].languageCode
    }
    return null
}

export function getDeviceLocale() {
    const deviceLocale = loadDeviceLocale()
    const languageInfo = getLanguageInfo(deviceLocale)
    const locale = languageInfo.code
    setMainLocaleLanguage(locale)
    return locale
}

export const getLocale = language => {
    return getLanguageInfo(language).code
}

export function translate(name: string, params = {defaultValue: name}) {
    return i18n.t(name, params)
}

export default i18n
