import AsyncStorage from '@react-native-async-storage/async-storage'
import {getUserCredentials} from '../../services/secureData'

//#region CONSTANTS DECLARATION
export const API_BASE_URL = 'http://192.168.1.152:8082'
// export const API_BASE_URL = 'https://api.ppbeautyacademy.com'
export const WEBSOCKET_BASE_URL = `${API_BASE_URL}/websocket`
export const WEBSOCKET_PREFIX = '/ws/app'

export const BACKEND_TRANSLATION = 'translation:backend'

export const API_IMAGE_KEY = '16d286000f98538beb846d1995d99319'
export const API_IMAGE_URL =
    'https://api.imgbb.com/1/upload?key=' + API_IMAGE_KEY

export const HTTP_STATUS_INTERNAL_SERVER = 'INTERNAL_SERVER_ERROR'
export const HTTP_STATUS_CONFLICT = 'CONFLICT'
export const HTTP_STATUS_BAD_REQUEST = 'BAD_REQUEST'
export const HTTP_STATUS_OK = 'OK'
export const HTTP_STATUS_NOT_FOUND = 'NOT_FOUND'
export const HTTP_NO_CONTENT = 'NO_CONTENT'

export const MAX_BADGE_COUNT = 99

export const NAVIGATION_DELAY_DURATION = 300

export const MAX_UPLOAD_IMAGE_SIZE = 512

export const GALLERY_PICKER_OPTION = {
    waitAnimationEnd: false,
    cropping: false,
    width: MAX_UPLOAD_IMAGE_SIZE,
    height: MAX_UPLOAD_IMAGE_SIZE,
    compressImageMaxWidth: MAX_UPLOAD_IMAGE_SIZE,
    compressImageMaxHeight: MAX_UPLOAD_IMAGE_SIZE,
    compressImageQuality: 0.7,
    mediaType: 'photo',
}

export const CAMERA_PICKER_OPTION = {
    waitAnimationEnd: false,
    cropping: true,
    width: MAX_UPLOAD_IMAGE_SIZE,
    height: MAX_UPLOAD_IMAGE_SIZE,
    compressImageMaxWidth: MAX_UPLOAD_IMAGE_SIZE,
    compressImageMaxHeight: MAX_UPLOAD_IMAGE_SIZE,
    compressImageQuality: 0.7,
}

export const VN_ADDRESS =
    '306/25, Nguyễn Thị Minh Khai, P.5, Q.3, TP. Hồ Chí Minh'
export const EN_ADDRESS = '80 Bloor St W, 401 Suite, Toronto , ON , Canada. M5S2V1'

export function numberWithCommas(n) {
    var parts = n.toString().split('.')
    return (
        parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',') +
        (parts[1] ? '.' + parts[1] : '')
    )
}

export function capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export function getMoneyFormat(amount, currency) {
    if (!amount) {
        return 0
    }

    if (currency === APP_CURRENCY.CAD.value) {
        return `${APP_CURRENCY.CAD.label} ${numberWithCommas(amount)}`
    }

    return `${numberWithCommas(amount)} ${APP_CURRENCY.VND.label}`
}

export async function getCurrencySign() {
    const version = await getVersion()
    if (version) {
        if (version === LANGUAGE_CODES.en.value) return APP_CURRENCY.CAD.value
        else return APP_CURRENCY.VND.value
    }

    return APP_CURRENCY.CAD.value
}

export const LANGUAGE_CODES = {
    en: {label: 'Canada (CA)', value: 'en'},
    vi: {label: 'Vietnam (VN)', value: 'vi'},
}

export const INPUT_HEIGHT = 45

export const LOCAL_STORAGE_KEY = {
    LANGUAGE: 'LANGUAGE',
    CURRENCY: 'CURRENCY',
}

export const APP_CURRENCY = {
    VND: {label: 'Đ', value: 'vnd'},
    CAD: {label: 'CA$', value: 'cad'},
}

export const orderTypes = {
    product: {label: "Product Order", value: "product"},
    course: {label: "Course Order", value: "register"}
}

export const orderStatuses = {
    NEW: {value: 'NEW', label: 'Mới'},
    CONFIRMED: {value: 'CONFIRMED', label: 'Xác nhận'},
    PREPARED: {value: 'PREPARED', label: 'Đã chuẩn bị'},
    DONE: {value: 'DONE', label: 'Hoàn tất'},
    CANCELLED: {value: 'CANCELLED', label: 'Đã hủy'}
}

export const checkoutStates = {
    pending: {value: 'pending', label: 'Chưa thanh toán'},
    completed: {value: 'completed', label: 'Đã thanh toán'},
}

export const CHECKOUT_TYPE = {
    COD : {label: "Cash on delivery", value: 'cod'},
    PICKUP : {label: "Cash", value: "pickup"},
    CREDITCARD : {label: 'Credit Card', value: 'credit_card'}
}

export const cartActionTypes = {
    order: {label: "Order Screen", value: "order"},
    cart: {label: "Cart Screen", value: "cart"},
    product: {label: "Product Screen", value: "product"},
    course: {label: "Course Screen", value: "course"},
    productDetail: {label: "Product Detail Screen", value: "productDetail"},
    courseDetail: {label: "Course Detail Screen", value: "courseDetail"},
    checkout: {label: "Check Out Screen", value: "checkOutScreen"},
    billing: {label: "Billing Screen", value: "billing"},
    assignmentDetail: {label: "Assginment Detail Screen", value: "assignmentDetail"},
    submissionDetail: {label: "Submission Detail Screen", value: "submissionDetail"}
}

export const messageSubjectTypes = {
    guestSupport: "Hỗ trợ khách hàng",
    orderSupport: "Hỗ trợ xử lí cho đơn hàng"
}

export const messageTypes = {
    question: "question",
    ticket: "ticket"
}

export const COMMENT_REF_TYPE = {
    productDetail: {value: 'product_detail', label: 'Sản Phẩm'},
    courseDetail: {value: 'course_detail', label: 'Khóa Học'},
    serviceDetail: {value: 'service_detail', label: 'Dịch Vụ'},
    blogDetail: {value: 'blog_detail', label: 'Bài Viết'},
}

export const COMMENT_MODES = {
    self: {label: 'Self Comment', value: 'self'},
    reply: {label: 'Reply Comment', value: 'reply'},
}

export const POLICY_URL =
    'https://www.ppbeautyacademy.com/vi/chinh-sach-bao-mat'

export const MAX_INPUT_LENGTH = 500

export const uploadImageTypes = {
    submission: {label: "Course Submission", value: "courseSubmission"}
}
//#endregion

//#region AUTHENTICATION
export const whitelist = [
    `${API_BASE_URL}/api/authenticate/getToken`,
    `${API_BASE_URL}/api/user/change-password`,
]

export const GOOGLE_SIGN_IN_CLIENT_ID = "587944565175-tntni6h7a1e9099647td6v81ej7a5vm9.apps.googleusercontent.com";
export const FACEBOOK_SIGN_IN_APP_ID = "1076883603164840";
export const STRIPE_APPLE_PAY_MERCHANT = "merchant.com.ppbeautyacademy.elearning";

export async function clearStorage() {
    try {
        const keys = [
            'access_token',
            'id',
            'username',
            'role',
            'email',
            'birthdate',
            'fullname',
            'avatar',
            'address',
            'phone',
            'phoneExtension',
            'note',
            'createdDate',
            'active',
        ]
        await AsyncStorage.multiRemove(keys)
    } catch (error) {
        console.log(error)
    }
}

export async function isAuthenticated() {
    let userAuthInfo
    await getUserCredentials()
        .then(authInfo => {
            userAuthInfo = authInfo
        })
        .catch(err => logService.log(err))
    if (!userAuthInfo.token) {
        return false
    }

    return true
}

export function isActivated() {
    if (!getActivationStatus()) {
        return false
    }

    return true
}
//#endregion

//#region SET USER INFORMATION
export async function setUserInfo({userInfo}) {
    try {
        await AsyncStorage.multiSet(
            [
                ['id', userInfo.id ? JSON.stringify(userInfo.id) : ''],
                ['username', userInfo.username ?? ''],
                ['role', userInfo.role ?? ''],
                ['email', userInfo.email ?? ''],
                [
                    'birthdate',
                    userInfo.birthdate
                        ? JSON.stringify(userInfo.birthdate)
                        : '',
                ],
                ['fullname', userInfo.fullname ?? ''],
                ['avatar', userInfo.avatar ?? ''],
                ['address', userInfo.address ?? ''],
                ['phone', userInfo.phone ?? ''],
                // ['phoneExtension', userInfo.phoneExtension ? JSON.stringify(userInfo.phoneExtension) : '{}']
                ['note', userInfo.note ?? ''],
                [
                    'createdDate',
                    userInfo.createdDate
                        ? JSON.stringify(userInfo.birthdate)
                        : '',
                ],
                [
                    'active',
                    userInfo.active ? JSON.stringify(userInfo.active) : '',
                ],
                [
                    'profileSetUp',
                    userInfo.profileSetUp ? JSON.stringify(userInfo.profileSetUp) : '',
                ]
            ],
            () => {
                console.log('Finish')
            },
        )
    } catch (error) {
        console.log(error)
    }
}

export async function setUserUpdateInfo(userInfo) {
    try {
        await AsyncStorage.multiSet([
            [
                'birthdate',
                userInfo.birthdate ? JSON.stringify(userInfo.birthdate) : '',
            ],
            ['fullname', userInfo.fullname ? userInfo.fullname : ''],
            ['address', userInfo.address ? userInfo.address : ''],
            ['phone', userInfo.phone ? userInfo.phone : ''],
            // ['phoneExtension', userInfo.phoneExtension ? JSON.stringify(userInfo.phoneExtension) : '{}']
            ['avatar', userInfo.avatar ? userInfo.avatar : ''],
        ])
    } catch (error) {
        console.log(error)
    }
}

export async function setVersion(value) {
    try {
        await AsyncStorage.setItem('version', value)
    } catch (error) {
        console.log(error)
    }
}

export async function setUserId(userId) {
    try {
        await AsyncStorage.setItem('userId', JSON.stringify(userId))
    } catch (error) {
        console.log(error)
    }
}

export async function setUsername(username) {
    try {
        await AsyncStorage.setItem('username', JSON.stringify(username))
    } catch (error) {
        console.log(error)
    }
}
//#endregion

//#region GET USER INFORMATION
export async function getUsername() {
    const username = await AsyncStorage.getItem('username')
    return username
}

export async function getUserId() {
    const userId = await AsyncStorage.getItem('userId')
    return userId
}

export async function getVersion() {
    const version = await AsyncStorage.getItem('version')
    return version !== null ? version : ''
}

export async function getActivationStatus() {
    const activationStatus = await AsyncStorage.getItem('active')
    return activationStatus
}
//#endregion

//#region ORDER INFORMATION
export async function setCourseOrderId(orderId) {
    try {
        await AsyncStorage.setItem('courseOrderId', JSON.stringify(orderId))
    } catch (error) {
        console.log(error)
    }
}

export async function getCourseOrderId() {
    const courseOrderId = await AsyncStorage.getItem('courseOrderId')
    return courseOrderId
}

export async function removeCourseOrderId() {
    try {
        await AsyncStorage.removeItem('courseOrderId')
    } catch (error) {
        console.log(error)
    }
}

export async function setProductOrderId(orderId) {
    try {
        await AsyncStorage.setItem('productOrderId', JSON.stringify(orderId))
    } catch (error) {
        console.log(error)
    }
}

export async function getProductOrderId() {
    const courseOrderId = await AsyncStorage.getItem('productOrderId')
    return courseOrderId
}

export async function removeProductOrderId() {
    try {
        await AsyncStorage.removeItem('productOrderId')
    } catch (error) {
        console.log(error)
    }
}

//#endregion

//#region DATE OPTIONS
export const DATE_PATTERNS = {
    DMY: {value: "DMY", label: "Day - Month - Year"},
    MDY: {value: "MDY", label: "Month - Day - Year"},
    YMD: {value: "YMD", label: "Year - Month - Day"}
};

export function dateStringFormat(day, month, year, pattern) {
    if(!day || !month || !year) {
        return "";
    }

    let convertDay = day && day.length > 1 ? day : `0${day}`;
    let convertMonth = month && month.length > 1 ? month : `0${month}`;

    let fromPattern = pattern ? pattern : DATE_PATTERNS.DMY;

    let returnedData = "";
    switch(fromPattern) {
        case DATE_PATTERNS.DMY.value:
            returnedData = `${convertDay}-${convertMonth}-${year}`;
            break;
        case DATE_PATTERNS.MDY.value:
            returnedData = `${convertMonth}-${convertDay}-${year}`;
            break;
        case DATE_PATTERNS.YMD.value:
            returnedData = `${year}-${convertMonth}-${convertDay}`;
            break;
    }

    return returnedData;
}

export const yearOptions = [
    {value: '2022', label: '2022'},
    {value: '2021', label: '2021'},
    {value: '2020', label: '2020'},
    {value: '2019', label: '2019'},
    {value: '2018', label: '2018'},
    {value: '2017', label: '2017'},
    {value: '2016', label: '2016'},
    {value: '2015', label: '2015'},
    {value: '2014', label: '2014'},
    {value: '2013', label: '2013'},
    {value: '2012', label: '2012'},
    {value: '2011', label: '2011'},
    {value: '2010', label: '2010'},
    {value: '2009', label: '2009'},
    {value: '2008', label: '2008'},
    {value: '2007', label: '2007'},
    {value: '2006', label: '2006'},
    {value: '2005', label: '2005'},
    {value: '2004', label: '2004'},
    {value: '2003', label: '2003'},
    {value: '2002', label: '2002'},
    {value: '2001', label: '2001'},
    {value: '2000', label: '2000'},
    {value: '1999', label: '1999'},
    {value: '1998', label: '1998'},
    {value: '1997', label: '1997'},
    {value: '1996', label: '1996'},
    {value: '1995', label: '1995'},
    {value: '1994', label: '1994'},
    {value: '1993', label: '1993'},
    {value: '1992', label: '1992'},
    {value: '1991', label: '1991'},
    {value: '1990', label: '1990'},
    {value: '1989', label: '1989'},
    {value: '1988', label: '1988'},
    {value: '1987', label: '1987'},
    {value: '1986', label: '1986'},
    {value: '1985', label: '1985'},
    {value: '1984', label: '1984'},
    {value: '1983', label: '1983'},
    {value: '1982', label: '1982'},
    {value: '1981', label: '1981'},
    {value: '1980', label: '1980'},
    {value: '1979', label: '1979'},
    {value: '1978', label: '1978'},
    {value: '1977', label: '1977'},
    {value: '1976', label: '1976'},
    {value: '1975', label: '1975'},
    {value: '1974', label: '1974'},
    {value: '1973', label: '1973'},
    {value: '1972', label: '1972'},
    {value: '1971', label: '1971'},
    {value: '1970', label: '1970'},
    {value: '1969', label: '1969'},
    {value: '1968', label: '1968'},
    {value: '1967', label: '1967'},
    {value: '1966', label: '1966'},
    {value: '1965', label: '1965'},
    {value: '1964', label: '1964'},
    {value: '1963', label: '1963'},
    {value: '1962', label: '1962'},
    {value: '1961', label: '1961'},
    {value: '1960', label: '1960'},
    {value: '1959', label: '1959'},
    {value: '1958', label: '1958'},
    {value: '1957', label: '1957'},
    {value: '1956', label: '1956'},
    {value: '1955', label: '1955'},
    {value: '1954', label: '1954'},
    {value: '1953', label: '1953'},
    {value: '1952', label: '1952'},
    {value: '1951', label: '1951'},
    {value: '1950', label: '1950'},
    {value: '1949', label: '1949'},
    {value: '1948', label: '1948'},
    {value: '1947', label: '1947'},
    {value: '1946', label: '1946'},
    {value: '1945', label: '1945'},
    {value: '1944', label: '1944'},
    {value: '1943', label: '1943'},
    {value: '1942', label: '1942'},
    {value: '1941', label: '1941'},
    {value: '1940', label: '1940'},
    {value: '1939', label: '1939'},
    {value: '1938', label: '1938'},
    {value: '1937', label: '1937'},
    {value: '1936', label: '1936'},
    {value: '1935', label: '1935'},
    {value: '1934', label: '1934'},
    {value: '1933', label: '1933'},
    {value: '1932', label: '1932'},
    {value: '1931', label: '1931'},
    {value: '1930', label: '1930'},
]

export const monthOptions = [
    {value: 1, label: '1'},
    {value: 2, label: '2'},
    {value: 3, label: '3'},
    {value: 4, label: '4'},
    {value: 5, label: '5'},
    {value: 6, label: '6'},
    {value: 7, label: '7'},
    {value: 8, label: '8'},
    {value: 9, label: '9'},
    {value: 10, label: '10'},
    {value: 11, label: '11'},
    {value: 12, label: '12'},
]

export const dayOptions = [
    {value: '1', label: '1'},
    {value: '2', label: '2'},
    {value: '3', label: '3'},
    {value: '4', label: '4'},
    {value: '5', label: '5'},
    {value: '6', label: '6'},
    {value: '7', label: '7'},
    {value: '8', label: '8'},
    {value: '9', label: '9'},
    {value: '10', label: '10'},
    {value: '11', label: '11'},
    {value: '12', label: '12'},
    {value: '13', label: '13'},
    {value: '14', label: '14'},
    {value: '15', label: '15'},
    {value: '16', label: '16'},
    {value: '17', label: '17'},
    {value: '18', label: '18'},
    {value: '19', label: '19'},
    {value: '20', label: '20'},
    {value: '21', label: '21'},
    {value: '22', label: '22'},
    {value: '23', label: '23'},
    {value: '24', label: '24'},
    {value: '25', label: '25'},
    {value: '26', label: '26'},
    {value: '27', label: '27'},
    {value: '28', label: '28'},
    {value: '29', label: '29'},
    {value: '30', label: '30'},
    {value: '31', label: '31'},
]
//#endregion
