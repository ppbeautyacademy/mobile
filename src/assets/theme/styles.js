import {INPUT_HEIGHT} from '../constants'
import {COLORS} from './colors'

export const commonStyles = {
    separator: {
        column4: {
            width: 4,
        },
        column8: {
            width: 8,
        },
        column16: {
            width: 16,
        },
        row4: {
            height: 4,
        },
        row8: {
            height: 8,
        },
        row16: {
            height: 16,
        },
        row32: {
            height: 32,
        },
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    shadowApp: {
        shadowColor: COLORS.SHADOW,
        shadowOffset: {width: 3, height: 3},
        shadowOpacity: 0.12,
        shadowRadius: 10,
        elevation: 5,
    },
    absoluteFill: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    resetPadding: {
        padding: 0,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
    },
    borderedInput: {
        paddingVertical: 8,
        marginTop: 8,
        borderRadius: 5,
        paddingHorizontal: 8,
        backgroundColor: COLORS.WHITE,
        minHeight: INPUT_HEIGHT,
        borderWidth: 0.5,
    },
    rowCenter: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    fill: {
        flex: 1,
    },
    customPhoneInputContainer : {
        backgroundColor: "rgb(247, 249, 252)",
        borderColor: "rgb(228, 233, 242)",
        borderRadius: 4,
        borderWidth: 1,
        minHeight: 40,
        width: "100%",
        paddingHorizontal: 0,
        paddingVertical: 0,
        margin: 0
    },
    phoneText: {
        backgroundColor: "rgb(247, 249, 252)"
    },
    p0: {
        padding: 0,
        paddingHorizontal: 0,
        paddingVertical: 0,
    },
    m0: {
        margin: 0
    },
    mainFont: {
        fontFamily: 'Prata-Regular'
    },
    lightColor: {
        color: COLORS.WHITE
    },
    mainColor: {
        color: COLORS.SUB
    }
}
