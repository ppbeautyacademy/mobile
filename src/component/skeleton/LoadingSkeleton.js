import { Text } from '@ui-kitten/components';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { COLORS } from '../../assets/theme/colors';
import SkeletonView, { SKELETON_TYPE } from '../SkeletonView';

const LoadingSkeleton = ({listType = SKELETON_TYPE.card, length}) => {
    return(
        <React.Fragment>
            {(()=> {
                const mockList = []
                const skeletonCardView = (idx) => <SkeletonView key={`card-${idx}`} type={SKELETON_TYPE.card} />
                const skeletonCardExpandView = (idx) => (
                    <SkeletonView  key={`cardExpand-${idx}`} type={SKELETON_TYPE.cardExpand} />
                )
                for (let i = 0; i < length; i++) {
                    if (listType === SKELETON_TYPE.card) {
                        mockList.push(skeletonCardView(i))
                    } else {
                        mockList.push(skeletonCardExpandView(i))
                    }
                }
                
                return(
                    <View style={styles.container}>
                        {mockList.map((Skeleton, idx) => (
                            <View key={idx} style={listType === SKELETON_TYPE.card ? styles.card : styles.cardExpand}>
                                {Skeleton}
                            </View>
                        ))}
                    </View>
                )
            })()}
        </React.Fragment>
    )
       
}

export default LoadingSkeleton;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap'
    }, 
    /******* Card **********/ 
    card: {
        height: 225,
        backgroundColor: COLORS.LIGHT,
        width: '43%',
        marginLeft: 14,
        marginRight: 10,
        borderRadius: 10,
        marginBottom: 20,
        padding: 15,
    },
    cardExpand: {
        width: '90%',
        marginBottom: 20,
        marginHorizontal: 20,
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        padding: 15,
    },
})