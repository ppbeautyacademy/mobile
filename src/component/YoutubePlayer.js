import React from 'react';
import WebView from 'react-native-webview';

const YoutubePlayer = ({url}) => {
    return (
        <View style={{flex: 1}}>
            <WebView
                style={ {  marginTop: (Platform.OS == 'ios') ? 20 : 0,} }
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{uri: url }}
            />
        </View>
    )
}