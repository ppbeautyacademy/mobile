import {Text} from '@ui-kitten/components'
import React from 'react'
import {StyleSheet, TouchableOpacity, View} from 'react-native'
import {ScreenWidth} from 'react-native-elements/dist/helpers'
import {TabView} from 'react-native-tab-view'
import {COLORS} from '../assets/theme/colors'

const tabItemWidth = ScreenWidth / 2

const styles = StyleSheet.create({
    tabItemButton: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 8,
        borderRadius: 5,
        width: tabItemWidth,
    },
    tabItemEnabled: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 8,
        width: tabItemWidth,
        borderRadius: 5,
        backgroundColor: COLORS.MAIN,
    },
    tabTitleEnabled: {
        fontSize: 13,
        color: COLORS.SUB,
        fontWeight: '700',
    },
    tabTitleDisabled: {
        fontSize: 13,
        color: COLORS.GRAY_9A,
    },
    tabBarContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingBottom: 20
    },
})

const CustomTabView = ({
    defaultIndex,
    routes,
    renderScene,
    customTabBar,
    sceneContainerStyle,
    onIndexChange,
    isLazy = false,
    tabBarStyle = {},
}) => {
    const [state, setState] = React.useState({
        index: defaultIndex ?? 0,
        routes: routes,
    })

    function selectTab(index) {
        setState({index, routes})
    }
    const handleIndexChange = index => {
        selectTab(index)
        onIndexChange && onIndexChange({index, key: routes[index].key})
    }

    const getTabbarTitle = route => {
        return route.title
    }

    const renderTabBar = props => {
        if (customTabBar) {
            return customTabBar(props)
        }
        const TabButtons = props.navigationState.routes.map(
            (route, tabIndex) => {
                return (
                    <TouchableOpacity
                        hitSlop={{top: 20, bottom: 20, left: 0, right: 0}}
                        key={tabIndex}
                        style={
                            tabIndex === state.index
                                ? styles.tabItemEnabled
                                : styles.tabItemButton
                        }
                        onPress={() => handleIndexChange(tabIndex)}>
                        <Text
                            style={
                                tabIndex === state.index
                                    ? styles.tabTitleEnabled
                                    : styles.tabTitleDisabled
                            }>
                            {getTabbarTitle(route, tabIndex)}
                        </Text>
                    </TouchableOpacity>
                )
            },
        )
        return (
            <View style={[styles.tabBarContainer, tabBarStyle]}>
                {TabButtons}
            </View>
        )
    }

    return (
        <TabView
            sceneContainerStyle={sceneContainerStyle}
            navigationState={state}
            renderScene={renderScene}
            renderTabBar={renderTabBar}
            onIndexChange={handleIndexChange}
            swipeEnabled={false}
            lazy={isLazy}
        />
    )
}

export default CustomTabView
