import React from 'react';
import { StyleSheet } from 'react-native';
import {Text} from '@ui-kitten/components';


const FontText = () => {
    return(
        <Text style={styles.mainFont}>Hello</Text>
    )
}

export default FontText;

const styles = StyleSheet.create({
    mainFont: {
        fontFamily: 'Prata-Regular'
    }
})