import React from 'react';
import {Dimensions, Platform, StyleSheet, View} from 'react-native';
import {Modalize} from 'react-native-modalize';

const {height: screenHeight} = Dimensions.get('screen');
const modalMaxHeight = screenHeight * 0.9;

const styles = StyleSheet.create({
  modalContainer: {
    maxHeight: modalMaxHeight,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
});

const ModalWithModalize = ({
  children,
  withReactModal = false,
  getModalRef,
  onClosed = () => {},
  ...props
}) => {
  return (
    <Modalize
      {...props}
      threshold={300}
      velocity={1000}
      modalTopOffset={0}
      modalStyle={styles.modalContainer}
      adjustToContentHeight
      scrollViewProps
      withReactModal={withReactModal}
      ref={getModalRef}
      onClosed={onClosed}
      keyboardAvoidingBehavior={Platform.OS === 'ios' ? 'padding' : ''}
      keyboardAvoidingOffset={0}
      handlePosition={'inside'}
      withOverlay>
      <View>{children}</View>
    </Modalize>
  );
};

export default ModalWithModalize;
