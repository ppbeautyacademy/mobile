import React from 'react';
import {ImageBackground, StyleSheet} from 'react-native';
import {Divider, Drawer, DrawerItem, Icon} from '@ui-kitten/components';
import Context from '../../../context/context';

const PersonIcon = props => <Icon {...props} name="person-outline" />;

const BellIcon = props => <Icon {...props} name="bell-outline" />;

const ForwardIcon = props => <Icon {...props} name="arrow-ios-forward" />;

const Header = props => (
  <React.Fragment>
    <ImageBackground
      style={[props.style, styles.header]}
      source={{
        uri: 'https://images.unsplash.com/photo-1519750783826-e2420f4d687f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80',
      }}
    />
    <Divider />
  </React.Fragment>
);

export const ThemeDrawer = () => {
  const [selectedIndex, setSelectedIndex] = React.useState(null);

  return (
    <Context.Consumer>
      {value => (
        <Drawer
          header={Header}
          selectedIndex={selectedIndex}
          onSelect={index => setSelectedIndex(index)}>
          <DrawerItem
            title="Users"
            accessoryLeft={PersonIcon}
            accessoryRight={ForwardIcon}
          />
          <DrawerItem
            title="Orders"
            accessoryLeft={BellIcon}
            accessoryRight={ForwardIcon}
          />
        </Drawer>
      )}
    </Context.Consumer>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 128,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
