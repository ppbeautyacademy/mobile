import {Select, SelectItem, Text} from '@ui-kitten/components'
import React, {useState} from 'react'
import {View} from 'react-native'
import {translate} from '../../../config/i18n'
import {dayOptions, monthOptions, yearOptions} from '../../assets/constants'
import {commonStyles} from '../../assets/theme/styles'
import styles from '../../screens/Authentication/SignUpScreen/styles'
import {selectParser} from '../address/parser'

const DatePicker = props => {
    const {errors, defaultDate} = props ?? {}
    const [selectedDay, setSelectedDay] = useState(defaultDate?.day ?? null)
    const [selectedMonth, setSelectedMonth] = useState(
        defaultDate?.month ?? null,
    )
    const [selectedYear, setSelectedYear] = useState(defaultDate?.year ?? null)

    const onDayChange = day => {
        const dayData = selectParser(day.row, dayOptions)
        if (dayData) {
            props.setDay(dayData.value)

            setSelectedDay(dayData.label)
        }
    }

    const onMonthChange = month => {
        const monthData = selectParser(month.row, monthOptions)
        if (monthData) {
            props.setMonth(monthData.value)

            setSelectedMonth(monthData.label)
        }
    }

    const onYearChange = year => {
        const yearData = selectParser(year.row, yearOptions)
        if (yearData) {
            props.setYear(yearData.value)

            setSelectedYear(yearData.label)
        }
    }

    return (
        <React.Fragment>
            <View style={styles.itemPicker}>
                <Text appearance={'hint'} category={'label'}>
                    {translate('common.day')}
                </Text>
                <Select
                    placeholder={translate('common.select')}
                    value={evaProps => {
                        let withCustomFont = {
                            style: [
                                evaProps.style[0],
                                {
                                    ...evaProps.style[1],
                                    fontFamily: 'Prata-Regular',
                                    color: 'black',
                                },
                            ],
                        }
                        return <Text {...withCustomFont}>{selectedDay}</Text>
                    }}
                    onSelect={day => onDayChange(day)}
                    status={errors?.day && 'danger'}>
                    {dayOptions &&
                        dayOptions.length > 0 &&
                        dayOptions.map((item, idx) => (
                            <SelectItem
                                key={idx}
                                title={evaProps => {
                                    let withCustomFont = {
                                        style: [
                                            evaProps.style[0],
                                            {
                                                ...evaProps.style[1],
                                                fontFamily: 'Prata-Regular',
                                            },
                                        ],
                                    }
                                    return (
                                        <Text {...withCustomFont}>
                                            {item.label}
                                        </Text>
                                    )
                                }}
                            />
                        ))}
                </Select>
                {errors.day && (
                    <Text category={'label'} status="danger">
                        {translate('errors.emptyDay')}
                    </Text>
                )}
                <View style={commonStyles.separator.row16} />
            </View>
            <View style={styles.itemPicker}>
                <Text appearance={'hint'} category={'label'}>
                    {translate('common.month')}
                </Text>
                <Select
                    placeholder={translate('common.select')}
                    value={evaProps => {
                        let withCustomFont = {
                            style: [
                                evaProps.style[0],
                                {
                                    ...evaProps.style[1],
                                    fontFamily: 'Prata-Regular',
                                    color: 'black',
                                },
                            ],
                        }
                        return <Text {...withCustomFont}>{selectedMonth}</Text>
                    }}
                    onSelect={month => onMonthChange(month)}
                    status={errors?.month && 'danger'}>
                    {monthOptions &&
                        monthOptions.length > 0 &&
                        monthOptions.map((item, idx) => (
                            <SelectItem
                                key={idx}
                                title={evaProps => {
                                    let withCustomFont = {
                                        style: [
                                            evaProps.style[0],
                                            {
                                                ...evaProps.style[1],
                                                fontFamily: 'Prata-Regular',
                                            },
                                        ],
                                    }
                                    return (
                                        <Text {...withCustomFont}>
                                            {item.label}
                                        </Text>
                                    )
                                }}
                            />
                        ))}
                </Select>
                {errors.month && (
                    <Text category={'label'} status="danger">
                        {translate('errors.emptyMonth')}
                    </Text>
                )}
                <View style={commonStyles.separator.row16} />
            </View>
            <View style={styles.itemPicker}>
                <Text appearance={'hint'} category={'label'}>
                    {translate('common.year')}
                </Text>
                <Select
                    placeholder={translate('common.select')}
                    value={evaProps => {
                        let withCustomFont = {
                            style: [
                                evaProps.style[0],
                                {
                                    ...evaProps.style[1],
                                    fontFamily: 'Prata-Regular',
                                    color: 'black',
                                },
                            ],
                        }
                        return <Text {...withCustomFont}>{selectedYear}</Text>
                    }}
                    onSelect={year => onYearChange(year)}
                    status={errors?.year && 'danger'}>
                    {yearOptions &&
                        yearOptions.length > 0 &&
                        yearOptions.map((item, idx) => (
                            <SelectItem
                                key={idx}
                                title={evaProps => {
                                    let withCustomFont = {
                                        style: [
                                            evaProps.style[0],
                                            {
                                                ...evaProps.style[1],
                                                fontFamily: 'Prata-Regular',
                                            },
                                        ],
                                    }
                                    return (
                                        <Text {...withCustomFont}>
                                            {item.label}
                                        </Text>
                                    )
                                }}
                            />
                        ))}
                </Select>
                {errors.year && (
                    <Text category={'label'} status="danger">
                        {translate('errors.emptyYear')}
                    </Text>
                )}
            </View>
        </React.Fragment>
    )
}

export default DatePicker
