import {isEmpty} from 'lodash'
import PropTypes from 'prop-types'
import React from 'react'
import {
    ActivityIndicator,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ViewPropTypes,
} from 'react-native'

import {IMAGES} from '../assets/images'
import {COLORS} from '../assets/theme/colors'
import {commonStyles} from '../assets/theme/styles'
import ImageProgress from './ImageProgress'

const styles = StyleSheet.create({
    defaultNameStyle: {
        fontSize: 16,
        color: COLORS.BLACK,
    },
    avatarBorder: {
        borderWidth: 3,
        borderColor: COLORS.MAIN,
    },
})

const Avatar = ({
    url,
    size,
    name,
    containerStyle,
    avatarStyle,
    nameStyle,
    isLoading = false,
    resizeMode,
    onPressImage = null,
    disableOnPress = false,
    defaultImage,
}) => {
    const imageStyle = imageSize => {
        return {
            height: imageSize,
            backgroundColor: COLORS.TRANSPARENT,
            width: imageSize,
            borderRadius: imageSize / 2,
        }
    }

    const renderImage = (
        <>
            <ImageProgress
                url={url}
                resizeMode={resizeMode}
                defaultImage={defaultImage}
                containerStyle={[imageStyle(size), containerStyle]}
                imageStyle={{
                    ...imageStyle(size),
                    ...styles.avatarBorder,
                    ...avatarStyle,
                }}>
                <ActivityIndicator animating={isLoading} color={COLORS.SUB} />
            </ImageProgress>
            {!isEmpty(name) && (
                <Text style={[styles.defaultNameStyle, nameStyle]}>{name}</Text>
            )}
        </>
    )

    return !!onPressImage ? (
        <TouchableOpacity
            style={[commonStyles.center]}
            onPress={onPressImage}
            disabled={disableOnPress}>
            {renderImage}
        </TouchableOpacity>
    ) : (
        <View style={[commonStyles.center]}>{renderImage}</View>
    )
}

Avatar.propTypes = {
    url: PropTypes.string,
    size: PropTypes.number,
    name: PropTypes.string,
    containerStyle: ViewPropTypes.style,
}

Avatar.defaultProps = {
    name: '',
    size: 115,
    containerStyle: {},
    nameStyle: {},
}

export default Avatar
