// import { Navigation } from 'react-native-navigation';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import React, { useRef } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Modalize } from 'react-native-modalize';

// Navigation.registerComponent('App', () => gestureHandlerRootHOC(ContentModal));

const ContentModal = () => {
    const modalizeRef = useRef(null);

    const onOpen = () => {
        modalizeRef.current?.open();
    };

    const getData = () => {
        return ({heading: "fawfawef"})
    };

    const renderItem = (item) => (
        <View>
        <Text>{item.heading}</Text>
        </View>
    );

    return (
        <>
        <TouchableOpacity onPress={onOpen}>
            <Text>Open the modal</Text>
        </TouchableOpacity>

        <Modalize
            ref={modalizeRef}
            scrollViewProps={{ showsVerticalScrollIndicator: false }}
            snapPoint={100}
            HeaderComponent={
            <View>
                <Text>Header</Text>
            </View>
            }
            withHandle={false}
            // flatListProps={{
            //     data: getData(),
            //     renderItem: renderItem,
            //     keyExtractor: item => item.heading,
            //     showsVerticalScrollIndicator: false,
            // }}
        ><Text>...your content"</Text></Modalize>
        </>
    );
}

export default ContentModal;