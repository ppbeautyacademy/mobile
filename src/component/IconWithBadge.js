import React from 'react'
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native'

import {MAX_BADGE_COUNT} from '../assets/constants'
import {COLORS} from '../assets/theme/colors'
import Icon from 'react-native-vector-icons/MaterialIcons'

const styles = StyleSheet.create({
    badgeContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: -4,
        right: -8,
        borderRadius: 40,
        backgroundColor: COLORS.SUB,
        height: 16,
        width: 16,
    },
    badgeText: {
        fontSize: 8,
        color: COLORS.WHITE,
    },
})

export const Badge = ({count}) => {
    const countText = count > MAX_BADGE_COUNT ? `${MAX_BADGE_COUNT}⁺` : count
    if (count <= 0) {
        return null
    }
    return (
        <View style={styles.badgeContainer}>
            <Text style={styles.badgeText}>{countText}</Text>
        </View>
    )
}

const IconWithBadge = ({iconName, count, color, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <Icon name={iconName} size={28} color={color ?? COLORS.WHITE} />
            <Badge count={count} />
        </TouchableOpacity>
    )
}

export default IconWithBadge
