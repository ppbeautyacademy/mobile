import React, {forwardRef, useContext, useImperativeHandle, useState} from 'react';
import { GooglePayButton, useGooglePay } from '@stripe/stripe-react-native';
import { HTTP_STATUS_OK } from '../../assets/constants';
import { getStripeClientSecret,  updateMobileSuccessPayment} from '../../services/ServiceWorker';
import context from '../../../context/context';
import { Alert, StyleSheet } from 'react-native';

const GooglePayForm = ({
    orderNumber,
    amount,
    coupon,
    onSuccessPayment
}, ref) => {
    useImperativeHandle(ref, () => ({
        initialize: () => { initialize() }
    }))

    const {state, actions} = useContext(context);
    const {orderState} = useContext(context);

    const {
        initGooglePay,
        presentGooglePay,
        loading,
        createGooglePayPaymentMethod,
    } = useGooglePay();

    const [initialized, setInitialized] = useState(false);

    //#region Initialize Google Pay
    const initialize = async () => {
        const { error } = await initGooglePay({
            testEnv: true,
            merchantName: 'PP Beauty & Academy',
            countryCode: 'CA',
            billingAddressConfig: {
                format: 'FULL',
                isPhoneNumberRequired: true,
                isRequired: false,
            },
            existingPaymentMethodRequired: false,
            isEmailRequired: true,
        });

        if (error) {
            // Alert.alert(error.code, error.message);
            return;
        }
        setInitialized(true);
    };
    //#endregion

    //#region Make payment
    const pay = async () => {
        let totalCheckout = parseFloat(amount * 100).toFixed(2);
        const {clientSecret} = await getStripeClientSecret(state.userInfo?.id, parseInt(totalCheckout), orderNumber, coupon, {currency: "cad", paymentMethodType: "card"});

        // 3. Open Google Pay sheet and proceed a payment
        const { error } = await presentGooglePay({
            clientSecret,
            forSetupIntent: false,
        });

        if (error) {
            actions.showWhiteAppSpinner(false);
            Alert.alert(error.code, error.message);
            return;
        }

        setInitialized(false);

        // actions.showWhiteAppSpinner(true);
        const updateOrderPaySuccess = await updateMobileSuccessPayment(orderNumber, coupon);
        if(updateOrderPaySuccess && updateOrderPaySuccess === HTTP_STATUS_OK) {
            onSuccessPayment();
        }
    };
    //#endregion

    return(
        <GooglePayButton
            disabled={!initialized || loading}
            style={styles.payButton}
            type="pay"
            onPress={pay}
        />
    )
}

export default forwardRef(GooglePayForm);

const styles = StyleSheet.create({
    row: {
      marginTop: 30,
    },
    payButton: {
      width: "100%",
      height: 45,
    },
    standardButton: {
      width: 90,
      height: 40,
    },
  });
