import React, {forwardRef, useContext, useEffect, useState} from 'react';
import { initStripe, presentApplePay } from '@stripe/stripe-react-native';
import { STRIPE_APPLE_PAY_MERCHANT } from '../../assets/constants';
import { ApplePayButton, useApplePay } from '@stripe/stripe-react-native';
import { getStripeClientSecret, getStripePulicKey } from '../../services/ServiceWorker';
import context from '../../../context/context';
import BaseScreen from '../BaseScreen';
import { Text } from '@ui-kitten/components';
import { Alert } from 'react-native';

const ApplePayForm = ({
    orderNumber,
    amount,
    coupon,
    onSuccessPayment
}, ref) => {
    const { isApplePaySupported } = useApplePay();

    const {state, actions} = useContext(context);
    const {orderState} = useContext(context);

    //#region Load Stripe public key
    const [publishableKey, setPublishableKey] = useState('')

    const fetchPublishableKey = async () => {
        const key = await getStripePulicKey(state.userInfo?.id, state.version)
        setPublishableKey(key);

        initStripe({
            publishableKey: key,
            merchantIdentifier: STRIPE_APPLE_PAY_MERCHANT,
        });
    }

    useEffect(async() => {
        await fetchPublishableKey()
    }, [])
    //#endregion

    //#region Make payment
    const pay = async () => {
        if (!isApplePaySupported) return;

        const { error } = await presentApplePay({
            cartItems: [{ label: "Total Checkout", amount: amount }],
            country: 'CA',
            currency: 'CAD',
            shippingMethods: [],
            requiredShippingAddressFields: [],
            requiredBillingContactFields: ['phoneNumber', 'name'],
        });

        if (error) {
            // handle error
            Alert.alert(error.code, error.message);
            return;
        }

        let totalCheckout = parseFloat(amount * 100).toFixed(2);
        const {clientSecret} = await getStripeClientSecret(state.userInfo?.id, parseInt(totalCheckout), orderNumber, coupon, {currency: "cad", paymentMethodType: "card"});

        const { error: confirmApplePayError } = await confirmApplePayPayment(
            clientSecret
        );

        if (confirmApplePayError) {
            Alert.alert(confirmApplePayError.code, confirmApplePayError.message);
            return;
        } else {
            const updateOrderPaySuccess = await updateMobileSuccessPayment(orderNumber, coupon);
            if(updateOrderPaySuccess && updateOrderPaySuccess === HTTP_STATUS_OK) {
                onSuccessPayment();
            }
        }
    };
    //#endregion

    return(
        <ApplePayButton
            onPress={pay}
            type="plain"
            buttonStyle="black"
            borderRadius={4}
            style={{
                width: '100%',
                height: 45,
            }}
        />
    )
}

export default forwardRef(ApplePayForm);
