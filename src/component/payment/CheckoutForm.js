import React, {useContext, useEffect, useState} from 'react'
import {chargePayment, getStripePulicKey, getStripeToken} from '../../services/ServiceWorker'
import {getUserId, getVersion, HTTP_STATUS_OK, orderTypes} from '../../assets/constants'
import {Alert, StyleSheet, UIManager, View} from 'react-native'
import {CreditCardInput} from 'react-native-credit-card-input-view'
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler'
import {Button, Text} from '@ui-kitten/components'
import context from '../../../context/context'
import BaseScreen from '../BaseScreen'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../assets/theme/colors'
import { ScreenIds } from '../../ScreenIds'
import OrderContext from '../../../context/OrderContext'
import { Toast } from 'popup-ui'
import { translate } from '../../../config/i18n'

if (
    Platform.OS === 'android' &&
    UIManager.setLayoutAnimationEnabledExperimental
) {
    UIManager.setLayoutAnimationEnabledExperimental(true)
}

const CheckoutForm = ({navigation, route}) => {
    const {orderType} = route.params;

    const {state, actions} = useContext(context);
    const {orderState, orderActions} = useContext(OrderContext);

    //#region ORDER
    const [order, setOrder] = useState({})
    const [isLoaded, setIsLoaded] = useState(false);

    const loadUserOrder = () => {
        if(orderType === orderTypes.product.value) {
            setOrder(orderState.productOrder)
            setIsLoaded(true);
        } else {
            setOrder(orderState.courseOrder)
            setIsLoaded(true);
        }
    }

    useEffect(async() => {
        await loadUserOrder();
    }, [])
    //#endregion

    //#region CHECKOUT
    const [publishableKey, setPublishableKey] = useState('')
    const [cardInput, setCardInput] = useState({})

    const fetchPublishableKey = async () => {
        const key = await getStripePulicKey(state.userInfo?.id, state.version)
        setPublishableKey(key)
    }

    useEffect(async() => {
        await fetchPublishableKey()
    }, [])

    const handleOnChangeCardInput = input => {
        setCardInput(input)
    }

    const onPay = async () => {
        if(Object.keys(cardInput).length < 1) {
            Toast.show({
                title: translate('common.errorTitle'),
                text: 'Empty Card Information',
                color: '#DB646A',
                timing: 3500,
            })
            return
        }

        if (!cardInput.valid) {
            Toast.show({
                title: translate('common.errorTitle'),
                text: 'Invalid Card',
                color: '#DB646A',
                timing: 3500,
            })
            return
        }

        actions.showWhiteAppSpinner(true)
        let creditCardToken = {}
        try {
            creditCardToken = await getCreditCardToken(cardInput)
            if (creditCardToken.error) {
                Toast.show({
                    title: translate('common.errorTitle'),
                    text: 'Fail to get card token',
                    color: '#DB646A',
                    timing: 3500,
                })
                actions.showWhiteAppSpinner(false)

                return
            }
        } catch (e) {
            actions.showWhiteAppSpinner(false)
            return
        }

        const chargeStatus = await handleCharge(creditCardToken)
        if (chargeStatus === HTTP_STATUS_OK) {
            if(orderType === orderTypes.product.value) {
                orderActions.setProductOrder({
                    ...order,
                    paymentProceed: true
                })
            } else {
                orderActions.setCourseOrder({
                    ...order,
                    paymentProceed: true
                })
            }

            navigation.navigate(ScreenIds.OrderBillingScreen, {orderType: orderType, paymentProceed: true})
        }

        actions.showWhiteAppSpinner(false)
    }

    const getCreditCardToken = async cardInput => {
        const card = {
            'card[number]': cardInput.values.number.replace(/ /g, ''),
            'card[exp_month]': cardInput.values.expiry.split('/')[0],
            'card[exp_year]': cardInput.values.expiry.split('/')[1],
            'card[cvc]': cardInput.values.cvc,
        }

        let formatData = Object.keys(card)
            .map(key => key + '=' + card[key])
            .join('&')

        const token = await getStripeToken(formatData, publishableKey)
        if (token && token.id) {
            return token
        } else {
            let tokenError = {
                error: true
            }

            return tokenError;
        }
    }

    const handleCharge = async token => {
        const chargePaymentStatus = await chargePayment(
            state.version,
            order.orderNumber,
            order.totalCheckout * 100,
            token,
        )
        return chargePaymentStatus
    }
    //#endregion

    return (
        <React.Fragment>
            <BaseScreen title='Payment'>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    nestedScrollEnabled
                    contentContainerStyle={{
                        flex: 1
                    }}
                >
                    <View  style={styles.container}>
                        <CreditCardInput
                            cardFontFamily={"Prata-Regular"}
                            inputContainerStyle={styles.inputContainerStyle}
                            inputStyle={styles.inputStyle}
                            labelStyle={styles.labelStyle}
                            requiresName={true}
                            validColor="#fff"
                            placeholderColor="#ccc"
                            onChange={handleOnChangeCardInput}
                        />
                    </View>
                    <View style={{flex: 1, paddingHorizontal: 20}}>
                        <Button onPress={() => onPay()} appearance="outline" style={styles.buttonText}>PAY NOW</Button>
                    </View>
                    {/* <View style={{flex: 1, paddingHorizontal: 20, paddingTop: 30}}>
                        <Text></Text>
                    </View> */}
                </ScrollView>
            </BaseScreen>
        </React.Fragment>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: '50%',
        // backgroundColor: COLORS.WHITE
    },
    button: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: '10%',
    },
    buttonText: {
        fontSize: 15,
        fontWeight: '700',
        textTransform: 'uppercase',
    },
    inputContainerStyle: {
        borderRadius: 5,
    },
    inputStyle: {
        backgroundColor: '#eaa196',
        paddingLeft: 10,
        paddingRight: 5,
        borderRadius: 5,
        color: '#fff',
    },
    labelStyle: {
        marginBottom: 5,
        fontSize: 12,
    },
})

export default CheckoutForm
