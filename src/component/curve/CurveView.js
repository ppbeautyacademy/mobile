import { Text } from '@ui-kitten/components';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import { COLORS } from '../../assets/theme/colors';

const styles = StyleSheet.create({
	parent : {
        height : '80%',
        width : '100%',
        transform : [ { scaleX : 2 } ],
        borderBottomStartRadius : 200,
        borderBottomEndRadius : 200,
        overflow : 'hidden',
    },
    child : {
        flex : 1,
        transform : [ { scaleX : 0.5 } ],

        backgroundColor : COLORS.MAIN,
        alignItems : 'center',
        justifyContent : 'center'
    }
});

const MyCurvedView = () => {
	return (
		<View style={styles.parent}>
			<View style={styles.child}>
      		<Text>awdawd</Text>
      		</View>
      	</View>
    );
}

export default MyCurvedView;