import {Button, Text} from '@ui-kitten/components'
import React from 'react'
import {StyleSheet, View} from 'react-native'
import {COLORS} from '../../assets/theme/colors'

const SizePicker = ({sizes, selectedSize, setSelectedSize}) => {
    return sizes && sizes.length > 0 ? (
        <View style={styles.container}>
            {sizes.map(({productSize}) => (
                <Button
                    key={productSize.id}
                    appearance={
                        selectedSize === productSize.id ? 'filled' : 'outline'
                    }
                    style={[
                        styles.size,
                        selectedSize === productSize.id && styles.activeBtn,
                    ]}
                    size="tiny"
                    onPress={() => setSelectedSize(productSize.id)}>
                    {props => (
                        <Text style={[styles.mainFont]} {...props}>
                            {productSize.name}
                        </Text>
                    )}
                </Button>
            ))}
        </View>
    ) : null
}

export default SizePicker

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    size: {
        marginRight: 10,
        backgroundColor: COLORS.WHITE,
    },
    activeBtn: {
        backgroundColor: COLORS.SUB,
        borderColor: COLORS.WHITE,
    },
    mainFont: {
        fontFamily: 'Prata-Regular',
    },
})
