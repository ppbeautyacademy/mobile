import React from 'react';
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import ChatUI from './ChatUI';

const ChatWrapper = () => {
    return(
        <ActionSheetProvider>
            <ChatUI />
        </ActionSheetProvider>
    )
}

export default ChatWrapper;