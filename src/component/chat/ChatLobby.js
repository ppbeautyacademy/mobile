import {Avatar, Divider, List, ListItem, Text} from '@ui-kitten/components'
import React, {useState} from 'react'
import {StyleSheet, View} from 'react-native'
import {SearchBar} from 'react-native-elements'
import {COLORS} from '../../assets/theme/colors'

const ChatLobby = () => {
    const [searchValue, setSearchValue] = useState('')
    const [isSearching, setIsSearching] = useState(false)

    const handleSearch = value => {
        setSearchValue(value)
    }

    const handleClearSearch = () => {}

    const handleCancelSearch = () => {}

    const UserImage = props => (
        <Avatar
            {...props}
            style={[props.style, {tintColor: null}]}
            source={require('../../assets/logo/pp-logo.png')}
        />
    )

    const data = new Array(20).fill({
        title: 'Item',
        description: 'Some small description',
    })

    const renderItemList = ({item, idx}) => (
        <ListItem
            key={idx}
            title={evaProps => (
                <Text style={styles.title} category="h6">
                    {item.title}
                </Text>
            )}
            description={evaProps => (
                <Text
                    style={styles.description}
                    category="label"
                    appearance="hint">
                    {item.description}
                </Text>
            )}
            accessoryLeft={UserImage}
        />
    )

    return (
        <React.Fragment>
            <View style={styles.container}>
                <Text category="h4" style={styles.header}>
                    Messages
                </Text>
                <SearchBar
                    placeholder="Type Here..."
                    onChangeText={handleSearch}
                    value={searchValue}
                    round
                    lightTheme
                    showLoading={isSearching}
                    onClear={handleClearSearch}
                    onCancel={handleCancelSearch}
                    inputStyle={styles.searchBar}
                    containerStyle={styles.searchBarContainer}
                    inputContainerStyle={styles.searchBarInputContainer}
                />
                <List
                    style={styles.listContainer}
                    data={data}
                    renderItem={renderItemList}
                    ItemSeparatorComponent={Divider}
                />
            </View>
        </React.Fragment>
    )
}

export default ChatLobby

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 16,
    },
    header: {
        marginBottom: 10,
        fontWeight: '700',
        paddingLeft: 20,
    },
    listContainer: {
        marginHorizontal: 10,
    },
    /******** Search Bar *********/
    searchContainer: {
        marginTop: 30,
        flexDirection: 'row',
        paddingLeft: 20,
    },
    searchBarInputContainer: {
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        height: 45,
    },
    searchBarContainer: {
        width: '100%',
        paddingHorizontal: 20,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: COLORS.WHITE,
    },
    searchBar: {
        height: 50,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },

    title: {
        fontWeight: '500',
        marginLeft: 5,
    },
    description: {
        marginLeft: 5,
    },
})
