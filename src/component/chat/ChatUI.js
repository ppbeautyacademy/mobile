import { useActionSheet } from '@expo/react-native-action-sheet'
import { Chat, MessageType } from '@flyerhq/react-native-chat-ui'
import { PreviewData } from '@flyerhq/react-native-link-preview'
import React, { useState } from 'react'
import { launchImageLibrary } from 'react-native-image-picker'
import {uploadImageToServer } from '../../services/ServiceWorker'

// For the testing purposes, you should probably use https://github.com/uuidjs/uuid
const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = Math.floor(Math.random() * 16)
    const v = c === 'x' ? r : (r % 4) + 8
    return v.toString(16)
  })
 }

const ChatUI = () => {
  const user = { id: '06c33e8b-e835-4736-80f4-63f44b66666c' }
  const { showActionSheetWithOptions } = useActionSheet()
  const [messages, setMessages] = useState([])

  const addMessage = (message: MessageType.Any) => {
    setMessages([{ ...message, status: 'read' }, ...messages])
  }

  const handleAttachmentPress = () => {
    showActionSheetWithOptions(
      {
        options: ['Photo', 'Cancel'],
        cancelButtonIndex: 2,
      },
      (buttonIndex) => {
        switch (buttonIndex) {
          case 0:
            handleImageSelection()
            break;
        }
      }
    )
  }

  const handleImageSelection = async() => {
    launchImageLibrary(
      {
        includeBase64: true,
        maxWidth: 1440,
        mediaType: 'photo',
        quality: 0.7,
      },
      async( response ) => {
        if (response?.base64) {
          const imageUrl = await updateImageToServer(response.base64);
          if(imageUrl) {
            const imageMessage: MessageType.Image = {
              author: user,
              height: response.height,
              id: uuidv4(),
              imageName:
                response.fileName ?? response.uri?.split('/').pop() ?? '🖼',
              size: response.fileSize ?? 0,
              timestamp: Math.floor(Date.now() / 1000),
              type: 'image',
              uri: imageUrl,
              width: response.width,
            }
            addMessage(imageMessage)
          }
        }
      }
    )
  }

  const updateImageToServer = async(imageData) => {
    var base64ImageContent = imageData.replace(/^data:image\/(png|jpg);base64,/, "");

    var formData = new FormData();  
    formData.append('image', base64ImageContent)

    
    const response = await uploadImageToServer(formData);
    if(response && response.status === 200 && response.data && response.data.display_url) {
      // TODO save to backend server
      return response.data.display_url;
    }
    
  }

  const handlePreviewDataFetched = ({
    message,
    previewData,
  }: {
    message: MessageType.Text,
    previewData: PreviewData
  }) => {
    setMessages(
      messages.map<MessageType.Any>((m) =>
        m.id === message.id ? { ...m, previewData } : m
      )
    )
  }

  const handleSendPress = (message: MessageType.PartialText) => {
    const textMessage: MessageType.Text = {
      author: user,
      id: uuidv4(),
      text: message.text,
      timestamp: Math.floor(Date.now() / 1000),
      type: 'text',
    }
    addMessage(textMessage)
  }

  return (
    // Remove this provider if already registered elsewhere
    // or you have React Navigation set up
    // <SafeAreaProvider>
      <Chat
        messages={messages}
        onAttachmentPress={handleAttachmentPress}
        onPreviewDataFetched={handlePreviewDataFetched}
        onSendPress={handleSendPress}
        user={user}
      />
    // </SafeAreaProvider>
  )
}

export default ChatUI