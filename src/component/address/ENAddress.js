import {Icon, Input, Select, SelectItem, Text} from '@ui-kitten/components'
import React, {useEffect, useState} from 'react'
import {View} from 'react-native'
import { translate } from '../../../config/i18n'
import {commonStyles} from '../../assets/theme/styles'
import {getCities, getCountries, getStates} from '../../services/ServiceWorker'
import {selectParser} from './parser'

// TODO translation
const ENAddress = props => {
    const {addressBean, errors} = props
    const [countryOptions, setCountryOptions] = useState([])
    const [stateOptions, setStateOptions] = useState([])
    const [cityOptions, setCityOptions] = useState([])

    const [selectedCountry, setSelectedCountry] = useState(
        addressBean.country ?? null,
    )
    const [selectedState, setSelectedState] = useState(
        addressBean.state ?? null,
    )
    const [selectedCity, setSelectedCity] = useState(addressBean.city ?? null)

    const renderStreetIcon = props => <Icon {...props} name="book-outline" />

    const renderZipCodeIcon = props => <Icon {...props} name="pin-outline" />

    useEffect(async () => {
        await loadCountries()
        if (addressBean.country) {
            await loadStates(addressBean?.refCountry)
        }
        if (addressBean.state) {
            await loadCities(addressBean?.refState)
        }
    }, [])

    const loadCountries = async () => {
        const countries = await getCountries(true)
        setCountryOptions(countries)
    }

    const loadStates = async refCountry => {
        const states = await getStates(true, refCountry)
        setStateOptions(states)
    }

    const loadCities = async refState => {
        const cities = await getCities(true, addressBean.refCountry, refState)
        setCityOptions(cities)
    }

    const onCountryChange = refCountry => {
        const refCountryData = selectParser(refCountry.row, countryOptions)
        if (refCountryData) {
            props.setRefCountry(refCountryData.value)

            setSelectedCountry(refCountryData.label)
            setSelectedState(null)
            setSelectedCity(null)

            loadStates(refCountryData.value)
        }
    }

    const onStateChange = refState => {
        const refStateData = selectParser(refState.row, stateOptions)
        if (refStateData) {
            props.setRefState(refStateData.value)

            setSelectedState(refStateData.label)
            setSelectedCity(null)

            loadCities(refStateData.value)
        }
    }

    const onCityChange = city => {
        const cityData = selectParser(city.row, cityOptions)
        if (cityData) {
            props.setCity(cityData.value)

            setSelectedCity(cityData.label)
        }
    }

    return (
        <React.Fragment>
            <Select
                label="* Country"
                value={selectedCountry}
                onSelect={refCountry => onCountryChange(refCountry)}
                status={errors.refCountry && 'danger'}>
                {countryOptions &&
                    countryOptions.length > 0 &&
                    countryOptions.map((item, idx) => (
                        <SelectItem key={idx} title={item.label} />
                    ))}
            </Select>
            {errors.refCountry && <Text category={"label"} status={"danger"}>Empty country</Text>}
            <View style={commonStyles.separator.row16} />
            <Select
                label="* State"
                value={selectedState}
                onSelect={refState => onStateChange(refState)}
                status={errors.refState && 'danger'}>
                {stateOptions &&
                    stateOptions.length > 0 &&
                    stateOptions.map((item, idx) => (
                        <SelectItem key={idx} title={item.label} />
                    ))}
            </Select>
            {errors.refState && <Text category={"label"} status={"danger"}>Empty state</Text>}
            <View style={commonStyles.separator.row16} />
            <Select
                label="* City"
                value={selectedCity}
                onSelect={city => onCityChange(city)}
                status={errors.city && 'danger'}>
                {cityOptions &&
                    cityOptions.length > 0 &&
                    cityOptions.map((item, idx) => (
                        <SelectItem key={idx} title={item.label} />
                    ))}
            </Select>
            {errors.city && <Text category={"label"} status={"danger"}>Empty city</Text>}
            <View style={commonStyles.separator.row16} />
            <View>
                <Input
                    value={addressBean.street}
                    label="Street Number"
                    placeholder={translate('common.inputPlaceholder')}
                    accessoryRight={renderStreetIcon}
                    onChangeText={street => props.setStreet(street)}
                    status={errors.street && 'danger'}
                />
                {errors.city && <Text category={"label"} status={"danger"}>Empty street</Text>}
            </View>
            <View style={commonStyles.separator.row16} />
            <View>
                <Input
                    value={addressBean.zipCode}
                    label="Zip Code"
                    placeholder={translate('common.inputPlaceholder')}
                    accessoryRight={renderZipCodeIcon}
                    onChangeText={zipCode => props.setZipCode(zipCode)}
                    status={errors.zipCode && 'danger'}
                />
                {errors.zipCode && <Text category={"label"} status={"danger"}>Empty zip code</Text>}
            </View>
        </React.Fragment>
    )
}

export default ENAddress
