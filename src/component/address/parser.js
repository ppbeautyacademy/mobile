import { types } from "@babel/core";

export const keyTypes = {
    LABEL : 'label',
    VALUE : 'value'
}

export function selectParser(key = 0, arr = [], type = null) {
    if(arr.length < 1) {
        return null;
    }

    const KeyValue = arr[key];
    if(!KeyValue) 
        return null;

    return !type ? KeyValue : type === keyTypes.VALUE ? KeyValue.value : KeyValue.label;
}