import {Icon, Input, Select, SelectItem, Text} from '@ui-kitten/components'
import React, {useContext, useEffect, useState} from 'react'
import {View} from 'react-native'
import context from '../../../context/context'
import {commonStyles} from '../../assets/theme/styles'
import {
    getDistricts,
    getProvinces,
    getWards,
} from '../../services/ServiceWorker'
import {selectParser} from './parser'

// TODO Translation
const VNAddress = props => {
    const {
        addressBean,
        errors,
        setStreet = () => {},
        setRefProvince = () => {},
        setRefDistrict = () => {},
        setRefWard = () => {},
    } = props
    const [provinceOptions, setProvinceOptions] = useState([])
    const [districtOptions, setDistrictOptions] = useState([])
    const [wardOptions, setWardOptions] = useState([])

    const [selectedProvince, setSelectedProvince] = useState(
        addressBean?.province ?? null,
    )
    const [selectedDistrict, setSelectedDistrict] = useState(
        addressBean?.district ?? null,
    )
    const [selectedWard, setSelectedWard] = useState(addressBean?.ward ?? null)

    const renderStreetIcon = props => <Icon {...props} name="book-outline" />

    useEffect(async () => {
        await loadProvinces()
        if (addressBean.province) {
            await loadDistricts(addressBean?.refProvince)
        }
        if (addressBean.district) {
            await loadWards(addressBean?.refDistrict)
        }
    }, [])

    const loadProvinces = async () => {
        const provinces = await getProvinces(true)
        setProvinceOptions(provinces)
    }

    const loadDistricts = async refProvince => {
        const districts = await getDistricts(true, refProvince)
        setDistrictOptions(districts)
    }

    const loadWards = async refDistrict => {
        const wards = await getWards(true, refDistrict)
        setWardOptions(wards)
    }

    const onProvinceChange = refProvince => {
        const refProvinceData = selectParser(refProvince.row, provinceOptions)
        if (refProvinceData) {
            setRefProvince(refProvinceData.value)

            setSelectedProvince(refProvinceData.label)
            setSelectedDistrict(null)
            setSelectedWard(null)

            loadDistricts(refProvinceData.value)
        }
    }

    const onDistrictChange = refDistrict => {
        const refDistrictData = selectParser(refDistrict.row, districtOptions)
        if (refDistrictData) {
            setRefDistrict(refDistrictData.value)

            setSelectedDistrict(refDistrictData.label)
            setSelectedWard(null)

            loadWards(refDistrictData.value)
        }
    }

    const onWardChange = refWard => {
        const refWardData = selectParser(refWard.row, wardOptions)
        if (refWardData) {
            setRefWard(refWardData.value)

            setSelectedWard(refWardData.label)
        }
    }

    return (
        <React.Fragment>
            <Select
                label="* Tỉnh / Thành phố"
                placeholder="Lựa chọn"
                value={selectedProvince}
                onSelect={refProvince => onProvinceChange(refProvince)}
                status={errors.refProvince && 'danger'}>
                {provinceOptions &&
                    provinceOptions.length > 0 &&
                    provinceOptions.map((item, idx) => (
                        <SelectItem key={idx} title={item.label} />
                    ))}
            </Select>
            {errors.refProvince && <Text category={"label"} status={"danger"}>Empty province</Text>}
            <View style={commonStyles.separator.row16} />
            <Select
                label="* Quận / Huyện"
                placeholder="Lựa chọn"
                value={selectedDistrict}
                onSelect={refDistrict => onDistrictChange(refDistrict)}
                status={errors.refDistrict && 'danger'}>
                {districtOptions &&
                    districtOptions.length > 0 &&
                    districtOptions.map((item, idx) => (
                        <SelectItem key={idx} title={item.label} />
                    ))}
            </Select>
            {errors.refDistrict && <Text category={"label"} status={"danger"}>Empty district</Text>}
            <View style={commonStyles.separator.row16} />
            <Select
                label="* Phường / Xã"
                placeholder="Lựa chọn"
                value={selectedWard}
                onSelect={refWard => onWardChange(refWard)}
                status={errors.refWard && 'danger'}>
                {wardOptions &&
                    wardOptions.length > 0 &&
                    wardOptions.map((item, idx) => (
                        <SelectItem key={idx} title={item.label} />
                    ))}
            </Select>
            {errors.refWard && <Text category={"label"} status={"danger"}>Empty ward</Text>}
            <View style={commonStyles.separator.row16} />
            <Input
                value={addressBean.street}
                label="* Địa chỉ"
                placeholder="Nhập địa chỉ"
                accessoryRight={renderStreetIcon}
                onChangeText={street => setStreet(street)}
                status={errors.street && 'danger'}
            />
            {errors.street && <Text category={"label"} status={"danger"}>Empty street</Text>}
        </React.Fragment>
    )
}

export default VNAddress
