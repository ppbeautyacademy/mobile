import {findIndex} from 'lodash'
import React, {useEffect, useState} from 'react'
import {
    Dimensions,
    FlatList,
    Modal,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TouchableWithoutFeedback,
} from 'react-native'
import ImagePickerComp from 'react-native-image-crop-picker'
import {PERMISSIONS, request, RESULTS} from 'react-native-permissions'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {translate} from '../../config/i18n'

import {
    CAMERA_PICKER_OPTION,
    GALLERY_PICKER_OPTION,
    NAVIGATION_DELAY_DURATION,
} from '../assets/constants'
import {COLORS} from '../assets/theme/colors'
import {commonStyles} from '../assets/theme/styles'
import logService from '../services/logService'
import {callAfterInteraction} from '../utils/commonHooks'
import ImagePickerUtils from '../utils/ImagePickerUtils'
import ImageProgress from './ImageProgress'
import ModalPopup from './ModalPopup'

const screen = Dimensions.get('screen')

const ImagePickerOptions = [
    {id: 'CAMERA', label: translate('account.takePhoto'), icon: 'camera'},
    {
        id: 'GALLERY',
        label: translate('account.fromGallery'),
        icon: 'collections',
    },
]

const numberImagePerRow = 3
const styles = StyleSheet.create({
    textFileName: {
        fontSize: 14,
        color: COLORS.SUB,
        fontWeight: '700',
    },
    buttonUpload: {
        width: '100%',
        minHeight: 57,
        borderWidth: 1,
        borderRadius: 4,
        borderStyle: 'dashed',
        borderColor: COLORS.MAIN,
        ...commonStyles.center,
        paddingVertical: 8,
        // marignBottom: 16,
        flexDirection: 'row',
    },
    buttonUploadInline: {
        width: 118,
        height: 118,
        margin: 4,
        borderWidth: 1,
        borderRadius: 4,
        borderStyle: 'dashed',
        borderColor: COLORS.MAIN,
        ...commonStyles.center,
    },
    buttonUploadDisabled: {
        opacity: 0.4,
    },
    iconFile: {
        width: 20,
        height: 22,
    },
    errorText: {
        color: COLORS.ERROR,
        fontSize: 12,
    },
    uploadedImage: {
        margin: 4,
        width: (screen.width - 60) / numberImagePerRow,
        height: (screen.width - 60) / numberImagePerRow,
    },
    closeIcon: {
        marginTop: 8,
        marginEnd: 8,
        position: 'absolute',
        top: 4,
        right: 4,
        alignItems: 'center',
        zIndex: 3,
    },
    shadow: {
        width: 24,
        height: 24,
        shadowColor: COLORS.MAIN,
        shadowOpacity: 0.6,
        shadowRadius: 12,
        shadowOffset: {
            width: 1, // These can't both be 0
            height: 1, // i.e. the shadow has to be offset in some way
        },
    },
    whiteBackground: {
        backgroundColor: COLORS.WHITE,
    },
    selectableItemContainer: {
        ...commonStyles.absoluteFill,
        zIndex: 2,
    },
    selectableItemButton: {
        flex: 1,
        padding: 10,
        justifyContent: 'flex-end',
        backgroundColor: COLORS.TRANSPARENT,
    },
    selectableIndicatorContainer: {
        width: 20,
        height: 20,
        padding: 2,
        borderRadius: 10,
        backgroundColor: COLORS.MAIN,
    },
    selectableIndicatorInnerCircle1: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        padding: 2,
        borderRadius: 10, // Keep the value >= parents' width(or height)/2 to form a circle(e.g in this senario, parent's style is selectableIndicatorContainer which has width = 20)
    },
    selectableIndicatorInnerCircle2: {
        flex: 1,
        borderRadius: 10, // Keep the value >= parents' width(or height)/2 to form a circle(e.g in this senario, parent's style is selectableIndicatorInnerCircle1 which has width >= 16)
        overflow: 'hidden',
    },
    contentContainer: {
        backgroundColor: COLORS.WHITE_BACKGROUND,
        padding: 16,

        borderRadius: 10,
    },
    actionText: {
        fontSize: 14,
        color: COLORS.BLACK,
        marginLeft: 12,
    },
    cancelContainer: {
        backgroundColor: COLORS.WHITE_BACKGROUND,
        padding: 16,
        paddingVertical: 8,
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 10,
    },
})

const DEFAULT_EDITTING_ID = -1

const SingleSelectableIndicatorView = ({
    isSelected,
    disabled = true,
    onPress,
}) => {
    const highlightStyle = isSelected ? {backgroundColor: COLORS.MAIN} : {}
    return (
        <View
            style={styles.selectableItemContainer}
            pointerEvents={disabled ? 'none' : 'auto'}>
            <TouchableOpacity
                style={styles.selectableItemButton}
                onPress={onPress}>
                <View style={styles.selectableIndicatorContainer}>
                    <View style={styles.selectableIndicatorInnerCircle1}>
                        <View
                            style={[
                                styles.selectableIndicatorInnerCircle2,
                                highlightStyle,
                            ]}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const removeImageItemFromArray = (item, images) => {
    if (!images || !images?.length) {
        return []
    }

    if (!item) {
        return [...images]
    }

    const newArr = [...images]
    const itemIndex = findIndex(images, {id: item.id})
    if (itemIndex !== -1) {
        newArr.splice(itemIndex, 1)
    }
    const reIndexingNewArr = newArr.map((e, index) => {
        return {...e, id: index}
    })
    return reIndexingNewArr
}

const ImageItem = ({
    item,
    onRemove,
    onEditImage,
    onSelectSingleSelection,
    isShowOnly = false,
    isDisabled,
    isSelecting,
    disableSelecting,
}) => {
    const imageUrl = item?.uri || item?.url || ''

    return (
        <>
            <TouchableOpacity
                onPress={onEditImage}
                disabled={isShowOnly || isDisabled}
                activeOpacity={isShowOnly ? 1 : 0.2}>
                <ImageProgress
                    url={imageUrl}
                    containerStyle={styles.uploadedImage}
                />
                {isShowOnly ? (
                    <></>
                ) : (
                    <TouchableOpacity
                        style={styles.closeIcon}
                        disabled={isDisabled}
                        onPress={onRemove}>
                        <Icon
                            name="cancel"
                            size={28}
                            color={COLORS.BLACK}
                            style={styles.shadow}
                        />
                    </TouchableOpacity>
                )}
                {isSelecting && (
                    <SingleSelectableIndicatorView
                        disabled={disableSelecting}
                        onPress={() => onSelectSingleSelection(item.id)}
                        isSelected={item.checked}
                    />
                )}
            </TouchableOpacity>
        </>
    )
}

const PostImageItem = ({
    item,
    index,
    data,
    onRemoveItem,
    onEditItem,
    isShowOnly = false,
    isDisabled,
    isSelecting,
    disableSelecting,
    onSelectSingleSelection,
}) => {
    const onRemove = () => {
        onRemoveItem(item)
    }

    const onEdit = () => {
        onEditItem(item)
    }
    if (index > 0 && index === data.length - 1) {
        const ButtonUpload = item.view || null
        return ButtonUpload
    }
    return (
        <ImageItem
            item={item}
            onRemove={onRemove}
            onEditImage={onEdit}
            isShowOnly={isShowOnly}
            isDisabled={isDisabled}
            isSelecting={isSelecting}
            disableSelecting={disableSelecting}
            onSelectSingleSelection={onSelectSingleSelection}
        />
    )
}

const RenderButtonUpload = ({
    buttonUploadStyle,
    isDisabled,
    icon,
    buttonTitle,
    onPressUploadTransactionFile,
    isInlineWithFlatlist = false,
}) => (
    <TouchableOpacity
        style={[
            styles.whiteBackground,
            isInlineWithFlatlist
                ? styles.buttonUploadInline
                : styles.buttonUpload,
            buttonUploadStyle,
            isDisabled ? styles.buttonUploadDisabled : {},
        ]}
        onPress={onPressUploadTransactionFile}
        disabled={isDisabled}>
        <Icon name={icon} size={28} color={COLORS.BLACK} />
        <View style={commonStyles.separator.column8} />
        <Text style={styles.textFileName}>{buttonTitle ?? ''}</Text>
    </TouchableOpacity>
)

const ImagePicker = ({
    getShowPicker = null,
    defaultImages = [],
    onDeleteImage = () => {},
    errorCode,
    onWrongMime,
    buttonTitle = translate('account.clickToUpload'),
    icon = 'add-photo-alternate',
    isDisabled = false,
    isShowOnly = false,
    onNoPermission = () => {},
    onSelectedImages = () => {},
    onSingleSelectedImage = () => {},
    buttonUploadStyle = {},
    isMaxImageUpload = false,
    isSelectMode = false,
    disableSelectMode = false,
}) => {
    const [showPicker, setShowPicker] = useState(false)
    const onPressUploadTransactionFile = () => {
        setShowPicker(true)
    }
    useEffect(() => {
        getShowPicker && getShowPicker(setShowPicker)
    }, [])
    // <relocate upload button into flatlist when there is 1 or more images>
    const mapImages = imgs => {
        const data = [...imgs]
        if (data?.length > 0) {
            const removeExistedUploadButton = data.filter(e => !e.view)
            removeExistedUploadButton.push({
                view: (
                    <RenderButtonUpload
                        buttonUploadStyle={buttonUploadStyle}
                        isDisabled={isDisabled || isMaxImageUpload}
                        icon={icon}
                        buttonTitle={buttonTitle}
                        onPressUploadTransactionFile={
                            onPressUploadTransactionFile
                        }
                        isInlineWithFlatlist
                    />
                ),
            })
            return removeExistedUploadButton
        }
        return data
    }

    const images = mapImages(defaultImages)

    const receiveImages = (selectedImages, editingId = DEFAULT_EDITTING_ID) => {
        if (selectedImages && selectedImages.length > 0) {
            onSelectedImages(selectedImages, editingId)
        }
    }

    const editImage = item => {
        ImagePickerComp.openCropper({
            ...GALLERY_PICKER_OPTION,
            path: item.uri || item.url,
        })
            .then(selectedImage => {
                receiveImages([selectedImage], item.id)
            })
            .catch(error => {
                logService.log('user cancel pick photo', JSON.stringify(error))
            })
    }

    const removeImage = item => {
        const newArr = removeImageItemFromArray(item, [...defaultImages])
        onDeleteImage(newArr, item)
    }

    const renderItem = ({
        item,
        index,
        _isShowOnly = false,
        data,
        _isDisabled,
        isSelecting,
        disableSelecting,
        onSelectSingleSelection,
    }) => {
        return (
            <PostImageItem
                item={item}
                index={index}
                onRemoveItem={removeImage}
                onEditItem={editImage}
                isShowOnly={_isShowOnly}
                data={data}
                isDisabled={_isDisabled}
                isSelecting={isSelecting}
                disableSelecting={disableSelecting}
                onSelectSingleSelection={onSelectSingleSelection}
            />
        )
    }

    const openAction = action => {
        action
            .then(selectedImages => {
                if (!Array.isArray(selectedImages)) {
                    selectedImages = [selectedImages]
                }
                let isCorrectMime = true
                const arrayImageSource = selectedImages?.map(e => {
                    if (!ImagePickerUtils.checkMime(e)) {
                        isCorrectMime = false
                    }
                    return e
                })
                if (isCorrectMime) {
                    setTimeout(() => {
                        receiveImages(arrayImageSource)
                    }, NAVIGATION_DELAY_DURATION * 2)
                } else {
                    setTimeout(() => {
                        onWrongMime()
                    }, NAVIGATION_DELAY_DURATION * 2)
                }
            })
            .catch(error => {
                if (error.code === 'E_PERMISSION_MISSING') {
                    setTimeout(() => {
                        onNoPermission()
                    }, NAVIGATION_DELAY_DURATION * 2)
                }
            })
    }

    const openCamera = () => {
        openAction(ImagePickerComp.openCamera(CAMERA_PICKER_OPTION))
    }

    const openGallery = () => {
        openAction(
            ImagePickerComp.openPicker({
                ...GALLERY_PICKER_OPTION,
                multiple: true,
            }),
        )
    }

    const onPermissionResult = (action, result) => {
        switch (result) {
            case RESULTS.DENIED:
                setShowPicker(false)
                onNoPermission()
                break
            case RESULTS.GRANTED:
                action.id === 'CAMERA' ? openCamera() : openGallery()
                break
            case RESULTS.BLOCKED:
                setShowPicker(false)
                onNoPermission()
                break
        }
    }

    function handleActionType(action) {
        if (action.id === 'CAMERA') {
            Promise.all([
                request(
                    Platform.OS === 'ios'
                        ? PERMISSIONS.IOS.CAMERA
                        : PERMISSIONS.ANDROID.CAMERA,
                ),
            ]).then(([cameraStatus]) => {
                setTimeout(() => {
                    onPermissionResult(action, cameraStatus)
                }, NAVIGATION_DELAY_DURATION * 2)
            })
        } else {
            if (Platform.OS === 'ios') {
                setTimeout(() => {
                    openGallery()
                }, NAVIGATION_DELAY_DURATION * 2)
            } else {
                Promise.all([
                    request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE),
                ]).then(([readExternalStorageStatus]) => {
                    setTimeout(() => {
                        onPermissionResult(action, readExternalStorageStatus)
                    }, NAVIGATION_DELAY_DURATION * 2)
                })
            }
        }
    }

    const onChooseAction = action => {
        setShowPicker(false)
        callAfterInteraction(() => {
            handleActionType(action)
        })
    }

    const ModalPicker = (
        <ModalPopup
            visible={showPicker}
            onPressOutSide={() => setShowPicker(false)}>
            <View style={styles.contentContainer}>
                {ImagePickerOptions.map(item => (
                    <View key={item.id}>
                        <TouchableOpacity
                            style={styles.cancelContainer}
                            onPress={() => onChooseAction(item)}>
                            <Icon
                                name={item?.icon}
                                size={20}
                                color={COLORS.BLACK}
                            />
                            <Text style={styles.actionText}>{item.label}</Text>
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
        </ModalPopup>
    )

    return (
        <View>
            {!!getShowPicker || isShowOnly || defaultImages?.length !== 0 || (
                <RenderButtonUpload
                    buttonUploadStyle={buttonUploadStyle}
                    isDisabled={isDisabled}
                    icon={icon}
                    buttonTitle={buttonTitle}
                    onPressUploadTransactionFile={onPressUploadTransactionFile}
                />
            )}
            <FlatList
                data={images}
                renderItem={({item, index}) =>
                    renderItem({
                        item,
                        index,
                        _isShowOnly: isShowOnly,
                        data: images,
                        _isDisabled: isDisabled,
                        isSelecting: isSelectMode,
                        disableSelecting: disableSelectMode,
                        onSelectSingleSelection: onSingleSelectedImage,
                    })
                }
                keyExtractor={item => item.id}
                numColumns={numberImagePerRow}
                scrollEnabled={false}
            />
            {errorCode ? (
                <Text style={styles.errorText}>{translate(errorCode)}</Text>
            ) : null}
            {ModalPicker}
        </View>
    )
}

export default ImagePicker
