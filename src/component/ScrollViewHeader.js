import {NormalHeader} from 'react-native-spring-scrollview/NormalHeader'
import {translate} from '../../config/i18n'

export default class ScrollViewHeader extends NormalHeader {
    constructor(props) {
        super(props)
    }

    getTitle() {
        const s = this.state.status
        if (s === 'pulling' || s === 'waiting') {
            return translate('Pull down to refresh')
        } else if (s === 'pullingEnough') {
            return translate('Release to refresh')
        } else if (s === 'refreshing') {
            return translate('Refreshing')
        } else if (s === 'pullingCancel') {
            return translate('Cancelled')
        } else if (s === 'rebound') {
            return translate('Refresh completed')
        }
    }
}
