import {useNavigation} from '@react-navigation/native'
import PropTypes from 'prop-types'
import React from 'react'
import {StyleSheet, TouchableOpacity, View} from 'react-native'
import {translate} from '../../config/i18n'

import Icon from 'react-native-vector-icons/MaterialIcons'
import {Text} from '@ui-kitten/components'
import { COLORS } from '../assets/theme/colors'

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 16,
        justifyContent: 'space-between',
    },
    leftContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    rightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    backButtonContainer: {
        paddingEnd: 24,
        paddingVertical: 8,
    },
    backButtonText: {
        fontSize: 18,
        flex: 1,
    },
    hitSlop: {
        top: 20,
        bottom: 20,
        left: 20,
        right: 20,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
})

const Header = ({
    leftText,
    leftTextStyle,
    leftIconContainerStyle,
    rightComponent,
    isBackable,
    leftTextProps,
    onBackPress,
    ...headerProps
}) => {
    const navigation = useNavigation()

    const onBackPressHandler = () => {
        if (onBackPress) {
            //check custom back action and handle it
            onBackPress()
            return
        }

        //default is goback navigation
        navigation.canGoBack() && navigation.goBack()
    }

    return (
        <View style={[styles.container, headerProps.headerContainerStyle]}>
            <View style={styles.leftContainer}>
                {!!isBackable && (
                    <TouchableOpacity
                        hitSlop={styles.hitSlop}
                        style={[
                            styles.backButtonContainer,
                            leftIconContainerStyle,
                        ]}
                        onPress={onBackPressHandler}>
                            <View
                                style={[styles.navigationBackground, styles.navigationContainer]}
                            >
                                <Icon
                                    name={'chevron-left'}
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                    </TouchableOpacity>
                )}
                <Text
                    {...leftTextProps}
                    style={[styles.backButtonText, leftTextStyle]}>
                    {leftText}
                </Text>
            </View>
            <View style={styles.rightContainer}>{rightComponent}</View>
        </View>
    )
}

Header.propTypes = {
    leftText: PropTypes.string,
    rightComponent: PropTypes.element,
    isBackable: PropTypes.bool,
    leftTextProps: PropTypes.object,
    leftIcon: PropTypes.string,
    leftIconSize: PropTypes.number,
    leftTextStyle: PropTypes.object,
    leftIconStyle: PropTypes.object,
    leftIconContainerStyle: PropTypes.object,
}

Header.defaultProps = {
    leftText: translate('common.back'),
    rightComponent: null,
    isBackable: true,
    leftIcon: 'long-arrow-left',
    leftIconSize: 14,
    leftTextStyle: null,
    leftIconStyle: null,
    leftIconContainerStyle: null,
}

export default Header
