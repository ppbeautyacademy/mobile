import PropTypes from 'prop-types'
import React from 'react'
import {Image, View, ViewPropTypes} from 'react-native'
import FastImage from 'react-native-fast-image'
import {createImageProgress} from 'react-native-image-progress'
import ProgressIndicator from 'react-native-progress/Circle'
import Icon from 'react-native-vector-icons/MaterialIcons'

import {COLORS} from '../assets/theme/colors'
import {commonStyles} from '../assets/theme/styles'
import {getValidUrl} from '../utils/UrlUtil'

const FastImageProgress = createImageProgress(FastImage)

const imageSource = (url, defaultImage) => {
    const validUrl = getValidUrl(url)
    return validUrl ? {uri: validUrl} : defaultImage
}

const ImageProgress = ({
    url,
    defaultImage,
    defaultIcon,
    imageStyle,
    containerStyle,
    onLoadEnd,
    ...otherProps
}) => {
    const renderError = () => {
        setTimeout(() => {
            onLoadEnd()
        }, 0) // https://stackoverflow.com/a/63659469/5463717
        return (
            <View
                style={[
                    commonStyles.center,
                    containerStyle,
                    {backgroundColor: COLORS.WHITE_BACKGROUND},
                ]}>
                {defaultImage ? (
                    <Image style={imageStyle} source={defaultImage} />
                ) : (
                    <Icon size={16} name={defaultIcon} />
                )}
            </View>
        )
    }

    if (!url || !getValidUrl(url)) {
        return renderError()
    }

    return (
        <View style={[commonStyles.center, containerStyle]}>
            <FastImageProgress
                style={containerStyle}
                imageStyle={imageStyle}
                source={imageSource(url, defaultImage)}
                indicator={ProgressIndicator}
                renderError={renderError}
                onLoadEnd={onLoadEnd}
                {...otherProps}
            />
        </View>
    )
}

ImageProgress.propTypes = {
    url: PropTypes.string,
    size: PropTypes.number,
    containerStyle: ViewPropTypes.style,
    onLoadEnd: PropTypes.func,
}

ImageProgress.defaultProps = {
    url: '',
    size: 115,
    containerStyle: {},
    defaultImage: null,
    defaultIcon: 'Image',
    onLoadEnd: () => {},
}

export default ImageProgress
