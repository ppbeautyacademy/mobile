import React from 'react'
import {Dimensions, StyleSheet, View} from 'react-native'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

const gridWidth = Dimensions.get('window').width / 2 - 32

export const SKELETON_TYPE = {
    card: 'card',
    cardExpand: 'cardExpand',
}

const styles = StyleSheet.create({
    cardImage: {
        height: 120,
        width: gridWidth - 28,
        borderRadius: 10,
    },
    cardImageExpand: {
        height: 140,
        width: Dimensions.get('window').width * 0.8,
        borderRadius: 10,
    },
    infoContainer: {
        marginTop: 12,
    },
    infoLine1: {
        width: 120,
        height: 20,
        borderRadius: 4,
    },
    infoLine2: {
        marginTop: 6,
        width: 80,
        height: 20,
        borderRadius: 4,
    },
})

const SkeletonView = ({type}) => {
    if (type === SKELETON_TYPE.cardExpand) {
        return (
            <SkeletonPlaceholder>
                <View style={styles.cardImageExpand} />
                <View style={styles.infoContainer}>
                    <View style={styles.infoLine1} />
                    <View style={styles.infoLine2} />
                </View>
            </SkeletonPlaceholder>
        )
    }
    return (
        <SkeletonPlaceholder>
            <View style={styles.cardImage} />
            <View style={styles.infoContainer}>
                <View style={styles.infoLine1} />
                <View style={styles.infoLine2} />
            </View>
        </SkeletonPlaceholder>
    )
}

export default SkeletonView
