import React from 'react'
import { StyleSheet } from 'react-native'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'

const styles = StyleSheet.create({
    contentKeyboardScroll: {
        flexGrow: 1,
    },
})
// TODO must change to forward Ref
const KeyboardScrollView = ({
    contentStyle,
    children,
    extraScrollHeight,
    scrollEnabled = true,
    ref,
    pointerEvents = 'auto',
}) => {
    return (
        <KeyboardAwareScrollView
            pointerEvents={pointerEvents}
            ref={ref}
            scrollEnabled={scrollEnabled}
            keyboardShouldPersistTaps="handled"
            alwaysBounceVertical={false}
            contentContainerStyle={[styles.contentKeyboardScroll, contentStyle]}
            enableOnAndroid={true}
            extraScrollHeight={extraScrollHeight}
        >
            {children}
        </KeyboardAwareScrollView>
    )
}

export default KeyboardScrollView
