import {NormalFooter} from 'react-native-spring-scrollview/NormalFooter'
import {translate} from '../../config/i18n'

export default class ScrollViewFooter extends NormalFooter {
    constructor(props) {
        super(props)
    }

    getTitle() {
        const s = this.state.status
        if (s === 'dragging' || s === 'waiting') {
            return translate('Drag to load')
        } else if (s === 'draggingEnough') {
            return translate('Release to load')
        } else if (s === 'loading') {
            return translate('Loading')
        } else if (s === 'draggingCancel') {
            return translate('Cancelled')
        } else if (s === 'rebound') {
            return translate('Load completed')
        } else if (s === 'allLoaded') {
            return translate('Nothing new :)')
        }
    }
}
