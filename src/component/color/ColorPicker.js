import React, { useState } from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';

const ColorPicker = ({colors, selectedColor, setSelectedColor}) => {
    return(
        colors && colors.length > 0 ?
        <View style={styles.container}>
            {colors.map(({productColor}) => {
                return (
                    <View key={productColor.id} style={styles.radio}>
                        <TouchableOpacity
                            style={[styles.radioCircle, selectedColor === productColor.id ? {borderColor: productColor.color} : {backgroundColor: productColor.color, borderColor: productColor.color}]}
                            onPress={() => {
                                setSelectedColor(productColor.id)
                            }}>
                              {selectedColor === productColor.id && <View style={[styles.selectedRb, {backgroundColor: productColor.color}]} />}
                        </TouchableOpacity>
                    </View>
                );
            })}
        </View>
        :   null
    )
}

export default ColorPicker;

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    radio: {
      margin: 2,
      marginRight: 10,
      marginBottom: 10
    },
    /******** Radio Button ********/
    radioText: {
        marginRight: 35,
        fontSize: 20,
        color: '#000',
        fontWeight: '700'
    },
	radioCircle: {
		height: 25,
		width: 25,
		borderRadius: 100,
		borderWidth: 2,
		alignItems: 'center',
		justifyContent: 'center',
	},
	selectedRb: {
		width: 15,
		height: 15,
		borderRadius: 50
    }
});

