import React from 'react'
import {TouchableWithoutFeedback, View, StyleSheet, Modal} from 'react-native'
import {COLORS} from '../assets/theme/colors'
import {commonStyles} from '../assets/theme/styles'

const styles = StyleSheet.create({
    viewOverlay: {
        ...commonStyles.absoluteFill,
        opacity: 0,
    },
    modalViewOutSide: {
        justifyContent: 'center',
        ...commonStyles.resetPadding,
        flex: 1,
        padding: 16,
        backgroundColor: COLORS.BLACK_HALF_OPACITY,
    },
    viewInside: {
        alignSelf: 'center',
    },
})

const ModalPopup = ({children, visible, onPressOutSide}) => {
    return (
        <Modal
            visible={visible}
            transparent
            animationType="slide"
            presentationStyle="overFullScreen">
            <View style={styles.modalViewOutSide}>
                <TouchableWithoutFeedback onPress={onPressOutSide}>
                    <View style={styles.viewOverlay} />
                </TouchableWithoutFeedback>
                <View style={styles.viewInside}>{children}</View>
            </View>
        </Modal>
    )
}

export default ModalPopup
