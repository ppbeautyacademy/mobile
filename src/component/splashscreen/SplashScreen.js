import React, {useContext, useEffect, useState} from 'react'
import {Image, View} from 'react-native'
import SplashScreenDefault from 'react-native-splash-screen'
import {languages, setMainLocaleLanguage} from '../../../config/i18n'

import context from '../../../context/context'
import {
    APP_CURRENCY,
    getUsername,
    LOCAL_STORAGE_KEY,
    setUserInfo,
} from '../../assets/constants'
import {commonStyles} from '../../assets/theme/styles'
import {resetNavigatorToScreen, resetRootNavigatorToScreen, rootNavigationRef, setRootNavigationRef} from '../../Navigation'
import {ScreenIds} from '../../ScreenIds'
import {setUserAuth, clearAuthState} from '../../services/AuthService'
import {isSecuredBinary} from '../../services/checkDeviceSecurity'
import {getUserCurreny, getUserLanguage} from '../../services/localStorage'
import logService from '../../services/logService'
import {getUserCredentials} from '../../services/secureData'
import {getUserInfo, renewToken} from '../../services/ServiceWorker'
import LocalStorage from '../../storage/localStorage'
import {callAfterInteraction} from '../../utils/commonHooks'
import SafeAreaScreenContainer from '../SafeAreaScreenContainer'
import styles from './style'

const SplashScreen = ({navigation}) => {
    const {actions} = useContext(context)
    const [isSecurityEnv, setSecurityEnv] = useState(false)

    const checkSecurity = async () => {
        const canRunApp = await isSecuredBinary()
        if (canRunApp) {
            setSecurityEnv(true)
            callAfterInteraction(() => {
                navigation.replace(ScreenIds.MainTab)
            })
        } else {
            callAfterInteraction(() => {
                actions.showAppModal({
                    isVisible: true,
                    message: translate('errors.deviceIsJailBroke'),
                })
            })
        }
    }

    const setInitLanguage = async () => {
        const userLanguage = await getUserLanguage()
        if (userLanguage) {
            actions.setVersion(userLanguage)
            await setMainLocaleLanguage(userLanguage)
        } else {
            await LocalStorage.setItem(
                LOCAL_STORAGE_KEY.LANGUAGE,
                languages[1].code,
            )
        }
    }

    const setInitCurrency = async () => {
        const userCurrency = await getUserCurreny()
        if (userCurrency) {
            actions.setAppCurrency(userCurrency)
        } else {
            await LocalStorage.setItem(
                LOCAL_STORAGE_KEY.CURRENCY,
                APP_CURRENCY.CAD.value,
            )
        }
    }

    const checkLogin = async () => {
        let userAuthInfo
        await getUserCredentials()
            .then(authInfo => {
                userAuthInfo = authInfo
            })
            .catch(err => logService.log(err))
        if (userAuthInfo?.token && userAuthInfo?.refreshToken) {
            let userAuth
            await renewToken({
                refreshToken: userAuthInfo.refreshToken,
            })
                .then(response => {
                    userAuth = response
                })
                .catch(err => logService.log(err))

            if (userAuth?.token) {
                const {token, refreshToken, tokenType} = userAuth
                actions.setToken(token)
                actions.setRefreshToken(refreshToken)
                await setUserAuth({token, refreshToken, tokenType})

                let userName
                await getUsername()
                    .then(res => {
                        userName = res
                    })
                    .catch(err => logService.log(err))
                if (userName) {
                    const userInfo = await getUserInfo({
                        username: userName,
                    })

                    if (userInfo) {
                        actions.setUserInfo(userInfo)
                        await setUserInfo({userInfo})

                        if(!userInfo.active) {
                            resetNavigatorToScreen(navigation, ScreenIds.AuthStack, ScreenIds.Activation, {fromScreen: ScreenIds.SplashScreen})
                        } else if(!userInfo.profileSetUp) {
                            resetNavigatorToScreen(navigation, ScreenIds.AuthStack, ScreenIds.ProfileSetUp, {fromScreen: ScreenIds.SplashScreen})
                        } else {
                            actions.setIsLoggedIn(true)
                        }
                    }
                }
            } else {
                await clearAuthState()
            }
        }
    }

    const setInitialData = async () => {
        SplashScreenDefault.hide()
        await setInitLanguage()
            .then()
            .catch(err => logService.log(err))
        await setInitCurrency()
            .then()
            .catch(err => logService.log(err))
        await checkLogin()
            .then()
            .catch(err => logService.log(err))
        await checkSecurity()
            .then()
            .catch(err => logService.log(err))
    }
    useEffect(setInitialData, [])
    return (
        <SafeAreaScreenContainer style={commonStyles.fill}>
            <View style={styles.MainContainer}>
                <View style={styles.SplashScreenRootView}>
                    <View style={styles.SplashScreenChildView}>
                        <Image
                            source={require('../../assets/logo/pp-gradient-top-logo.png')}
                            style={{
                                width: '60%',
                                height: '60%',
                                resizeMode: 'contain',
                            }}
                        />
                    </View>
                </View>
            </View>
        </SafeAreaScreenContainer>
    )
}

export default SplashScreen
