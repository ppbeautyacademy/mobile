import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    MainContainer: {  
      flex: 1,  
      justifyContent: 'center',  
      alignItems: 'center',  
      paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0  
    },  
    SplashScreenRootView: {  
      justifyContent: 'center',  
      flex:1,  
      margin: 10,  
      position: 'absolute',  
      width: '100%',  
      height: '100%',  
    }, 
    SplashScreenChildView: {  
      justifyContent: 'center',  
      alignItems: 'center',  
      backgroundColor: 'white',
      flex:1,  
    }
})

export default styles;