import React from 'react'
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import {ScrollView} from 'react-native-gesture-handler'
import {Dialog} from 'react-native-simple-dialogs'

import {translate} from '../../config/i18n'
import {COLORS} from '../assets/theme/colors'

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 8,
    },
    dialogStyle: {
        borderRadius: 4,
        backgroundColor: COLORS.WHITE,
    },
    contentContainer: {
        maxHeight: 250,
    },
    scrollViewContentContainer: {
        paddingHorizontal: 16,
    },
    textMessage: {
        fontSize: 16,
        lineHeight: 24,
        color: COLORS.BLACK,
        textAlign: 'center',
    },
    hotlineNumber: {
        fontWeight: '700',
        color: COLORS.MAIN,
    },
    titleStyle: {
        fontWeight: '700',
        fontSize: 21,
        lineHeight: 31,
        color: COLORS.MAIN,
        textAlign: 'center',
    },
    buttonsStyle: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
    },
    containerButtonsStyle: {
        marginHorizontal: 16,
        marginBottom: 16,
        flexDirection: 'row',
    },
    commonButton: {
        backgroundColor: COLORS.MAIN,
        borderRadius: 5,
        paddingHorizontal: 24,
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    negativeButton: {
        backgroundColor: COLORS.WHITE,
        borderWidth: 1,
        borderColor: COLORS.MAIN,
        marginRight: 10,
    },
    negativeText: {
        fontWeight: '700',
        fontSize: 14,
        color: COLORS.SUB,
    },
    positiveButton: {
        flex: 1,
        color: COLORS.WHITE,
        backgroundColor: COLORS.SUB,
    },
    positiveText: {
        fontWeight: '700',
        fontSize: 16,
        color: COLORS.WHITE,
    },
})

export type Props = {
    isVisible: Boolean,
    title?: String,
    message: string,
    hotlineNumber: string,
    okText?: string,
    cancelText?: string,
    onOkHandler?: () => {},
    onDismiss?: () => {},
    onCancelHandler?: () => {},
}

const Popup = ({
    isVisible,
    title,
    message,
    okText,
    cancelText,
    onOkHandler = () => {},
    onDismiss = () => {},
    onCancelHandler = () => {},
}: Props) => {
    let positiveText = null
    if (cancelText) {
        positiveText = okText || translate('common.ok')
    } else {
        positiveText = okText || translate('common.close')
    }

    const titleColor = () => {
        if (title === translate('common.error')) {
            return COLORS.ERROR
        }
        if (cancelText) {
            return COLORS.BLACK
        }
        return COLORS.SUB
    }

    return (
        <Dialog
            title={title || translate('common.heyThere')}
            dialogStyle={styles.dialogStyle}
            contentStyle={styles.container}
            titleStyle={[styles.titleStyle, {color: titleColor()}]}
            visible={isVisible}
            onTouchOutside={onDismiss}
            buttonsStyle={styles.buttonsStyle}
            buttons={
                <View style={styles.containerButtonsStyle}>
                    {cancelText && (
                        <TouchableOpacity
                            style={[styles.commonButton, styles.negativeButton]}
                            onPress={() => {
                                onCancelHandler()
                                onDismiss()
                            }}>
                            <Text style={styles.negativeText}>
                                {cancelText}
                            </Text>
                        </TouchableOpacity>
                    )}
                    <TouchableOpacity
                        style={[styles.commonButton, styles.positiveButton]}
                        onPress={() => {
                            onOkHandler()
                            onDismiss()
                        }}>
                        <Text style={styles.positiveText}>{positiveText}</Text>
                    </TouchableOpacity>
                </View>
            }>
            <View style={styles.contentContainer}>
                <ScrollView
                    contentContainerStyle={styles.scrollViewContentContainer}>
                    <Text style={styles.textMessage}>{message}</Text>
                </ScrollView>
            </View>
        </Dialog>
    )
}

export default Popup
