import {Avatar, Text, Icon as KittenIcon} from '@ui-kitten/components'
import moment from 'moment'
import React, {useState} from 'react'
import {FlatList, Platform, StyleSheet, View} from 'react-native'
import {TouchableOpacity} from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { HTTP_STATUS_OK } from '../../assets/constants'
import {COLORS} from '../../assets/theme/colors'
import { saveComment } from '../../services/ServiceWorker'
import KeyboardScrollView from '../KeyboardScrollView'
import FastImage from 'react-native-fast-image'

export const handleSendComment = async(
    userId, 
    data,
    reload,
    setErrors
) => {
    let isError = false;

    if(!data || !data.data) {
        isError = true;
        if(setErrors) {
            setErrors({
                data: true
            })
        }
    }

    if(!isError) {
        const commentPost = await saveComment(userId, data?.pId, data);

        if(commentPost && commentPost.id) {
            if(reload) {
                reload();
            }

            return HTTP_STATUS_OK;
        }
    }
}

const Reviews = (props) => {
    
    const [shownParentComments, setShownParentComments] = useState([])

    const handleShowChildComments = id => {
        if (!shownParentComments.includes(id)) {
            let parentIds = [...shownParentComments]
            parentIds.push(id)

            setShownParentComments(parentIds)
        }
    }

    const hideChildComments = id => {
        if (shownParentComments.includes) {
            let parentIds = [...shownParentComments]
            let index = parentIds.indexOf(id)
            parentIds.splice(index, 1)

            setShownParentComments(parentIds)
        }
    }

    const avatarPlaceholder = () => {
        return(
            <View style={styles.avatarContainer}>
                <Icon
                    name="person"
                    size={24}
                    color={COLORS.SUB}
                />
            </View>
        )
    }

    const onReplyComment = (pId, replyTo) => {
        if(props.setReplyingTo) {
            props.setReplyingTo(pId, replyTo)
        }
    }

    const renderItem = item => {
        const itemData = item.item
        return (
            <React.Fragment>
                <View
                    style={styles.container}
                    key={itemData.id}>
                    <View
                        style={styles.reviewContainer}>
                        
                        {itemData.avatar ?
                            <Avatar source={itemData.avatar}/>
                        :
                            avatarPlaceholder()
                        }

                        {itemData.commentList &&
                            itemData.commentList.length > 0 && (
                                <View
                                    style={[
                                        styles.moreContainer
                                    ]}>
                                    {shownParentComments.includes(
                                        itemData.id,
                                    ) ? (
                                        <TouchableOpacity
                                            onPress={() =>
                                                hideChildComments(itemData.id)
                                            }>
                                            <Icon
                                                name="arrow-drop-up"
                                                size={30}
                                                color={COLORS.BLACK}
                                            />
                                        </TouchableOpacity>
                                    ) : (
                                        <TouchableOpacity
                                            onPress={() =>
                                                handleShowChildComments(
                                                    itemData.id,
                                                )
                                            }>
                                            <Icon
                                                name="arrow-drop-down"
                                                size={30}
                                                color={COLORS.BLACK}
                                            />
                                        </TouchableOpacity>
                                    )}
                                </View>
                            )}
                    </View>
                    <View>
                        <Text category="p1">
                            {itemData.fullname}
                        </Text>
                        <View style={styles.mb5}>
                            <Text
                                category="s1"
                                appearance="hint"
                                style={{fontSize: 12, paddingRight: 50}}>
                                {itemData.data}
                            </Text>
                        </View>
                        <View style={styles.actionDetails}>
                            <View style={styles.fullWidth}>
                                <Text
                                    category="s2"
                                    appearance="hint"
                                    style={styles.fs10}>
                                    {itemData.date &&
                                        moment(itemData.date).format(
                                            'MM/DD/YYYY HH:mm',
                                        )}
                                </Text>
                            </View>
                            <View style={styles.actionWrapper}>
                                <View style={styles.actionContainer}>
                                    <View style={{marginRight: 15}}>
                                        <Text
                                            category="s2"
                                            appearance="hint"
                                            style={styles.fs10}
                                        >Like</Text>
                                    </View>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => onReplyComment(itemData.id, itemData.fullname)}
                                        >
                                            <Text
                                                category="s2"
                                                appearance="hint"
                                                style={styles.fs10}
                                            >Reply</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                {itemData.commentList &&
                    itemData.commentList.length > 0 &&
                    shownParentComments.includes(itemData.id) &&
                    itemData.commentList.map((child, idx) => (
                        <View
                            style={styles.childContainer}
                            key={idx}>
                            <View
                                style={styles.reviewContainer}>

                                {child.avatar ?
                                    <Avatar source={child.avatar}/>
                                :
                                    avatarPlaceholder()
                                }
                            </View>
                            <View style={styles.mb5}>
                                <Text category="p1">
                                    {child.fullname}
                                </Text>
                                <View style={styles.mb5}>
                                    <Text
                                        category="s1"
                                        appearance="hint"
                                        style={styles.review}>
                                        {child.data}
                                    </Text>
                                </View>
                                <View style={styles.actionDetails}>
                                    <View style={styles.fullWidth}>
                                        <Text
                                            category="s2"
                                            appearance="hint"
                                            style={styles.fs10}>
                                            {child.date &&
                                                moment(child.date).format(
                                                    'MM/DD/YYYY HH:mm',
                                                )}
                                        </Text>
                                    </View>
                                    <View style={styles.actionWrapper}>
                                        <View style={styles.actionContainer}>
                                            <View style={styles.mr15}>
                                                <Text
                                                    category="s2"
                                                    appearance="hint"
                                                    style={styles.fs10}
                                                >Like</Text>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => onReplyComment(itemData.id, child.fullname)}
                                                >
                                                    <Text
                                                        category="s2"
                                                        appearance="hint"
                                                        style={styles.fs10}
                                                    >Reply</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    ))}
            </React.Fragment>
        )
    }

    return (
        props.isLoaded ?
        <View style={[styles.wrapper]}>
            {props.comments && props.comments.length > 0 ?
                <React.Fragment>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        data={props.comments}
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                    />
                </React.Fragment>
            :   
                <React.Fragment>
                    <KeyboardScrollView
                        extraScrollHeight={Platform.OS === 'ios' ? 0 : 100}
                        contentStyle={{
                            paddingTop: 20
                        }}
                    >  
                        <View>
                            <FastImage 
                                source={require('../../assets/images/comment.png')}
                                style={{
                                    height: 220
                                }}
                            />
                            <View style={styles.emptyMessageWrapper}>
                                <Text>Be the first person leaving a comment</Text>
                            </View>
                        </View>
                    </KeyboardScrollView>
                </React.Fragment>
            }
        </View>
        :   null
    )
}

export default Reviews

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    /******* favorite *********/
    moreContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    },
    favoriteContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundPink: {
        backgroundColor: 'rgba(245, 42, 42,0.2)',
    },
    backgroundGray: {
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
    /******** avatar **********/
    avatarContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: `#FDF3EB`,
        marginLeft: 5
    },
    /******** review ***********/
    container: {
        flex: 1, 
        flexDirection: 'row', 
        marginBottom: 10
    },
    childContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 50,
        marginBottom: 5,
    },
    reviewContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginRight: 15,
    },
    review: {
        fontSize: 12,
        paddingRight: 50,
    },
    actionDetails: {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    actionWrapper: {
        position: 'absolute', 
        right: 55
    },
    actionContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    commentInput: {
        width: '90%',
        marginRight: 7
    },  
    reviewBtnContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    focusInput: {
        backgroundColor: '#FDF3EB'
    },
    blurInput: {
        backgroundColor: 'rgb(247, 249, 252)'
    },
    /******* Empty review ************/
    emptyMessageWrapper: {
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    /******** generic **********/
    mb5: {
        marginBottom: 5
    },
    mr15: {
        marginRight: 15
    },
    mt25: {
        marginTop: 25
    },
    fullWidth: {
        width: '90%'
    },
    fs10: {
        fontSize: 10
    }
})

