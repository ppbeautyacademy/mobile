import {Spinner} from '@ui-kitten/components'
import React, {useRef} from 'react'
import {View} from 'react-native'
import {WebView} from 'react-native-webview'

const WebViewComp = ({uri}) => {
    const webViewRef = useRef()
    const handleWebViewNavigationStateChange = (
        newNavState = {
            url: '',
            title: '',
            loading: false,
            canGoBack: false,
            canGoForward: false,
        },
    ) => {
        const {url} = newNavState
        if (!url) return

        // handle certain doctypes
        if (url.includes('.pdf')) {
            webViewRef.current.stopLoading()
            // open a modal with the PDF viewer
        }

        // one way to handle a successful form submit is via query strings
        if (url.includes('?message=success')) {
            webViewRef.current.stopLoading()
            // maybe close this view?
        }

        // one way to handle errors is via query string
        if (url.includes('?errors=true')) {
            webViewRef.current.stopLoading()
        }

        // redirect somewhere else
        if (url.includes('google.com')) {
            const newURL = 'https://reactnative.dev/'
            const redirectTo = 'window.location = "' + newURL + '"'
            webViewRef.current.injectJavaScript(redirectTo)
        }
    }
    return (
        <WebView
            originWhitelist={['*']}
            ref={webViewRef}
            source={{uri}}
            startInLoadingState
            renderLoading={() => (
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                    }}>
                    <Spinner size="large" status="primary" />
                </View>
            )}
            onNavigationStateChange={handleWebViewNavigationStateChange}
        />
    )
}

export default WebViewComp
