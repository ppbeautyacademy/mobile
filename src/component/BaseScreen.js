import React from 'react'
import {StyleSheet, View} from 'react-native'
import {useAndroidBackHandler} from 'react-navigation-backhandler'
import {translate} from '../../config/i18n'

import {COLORS} from '../assets/theme/colors'
import Header from './Header'
import SafeAreaScreenContainer from './SafeAreaScreenContainer'

const styles = StyleSheet.create({
    safeArea: {
        backgroundColor: COLORS.WHITE,
    },
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
})

const BaseScreen = ({
    title = translate('common.back'),
    isBackable = true,
    showHeader = true,
    isFullScreen = false,
    rightComponent,
    children,
    titleProps,
    headerOptions = null,
    onBackPress,
    containerStyle = {},
    modals,
}) => {
    const exitApp = () => {
        App
    }

    useAndroidBackHandler(() => {
        if (onBackPress) {
            onBackPress()
            return true // to not auto call default back button handler
        }
        // toast.show()
        return false
    })
    return isFullScreen ? (
        <View style={styles.container}>{children}</View>
    ) : (
        <SafeAreaScreenContainer style={styles.safeArea}>
            <View style={[styles.container, containerStyle]}>
                {showHeader && (
                    <View style={styles.headerContainer}>
                        <Header
                            onBackPress={onBackPress}
                            leftTextProps={titleProps}
                            leftText={title}
                            isBackable={isBackable}
                            rightComponent={rightComponent}
                            {...headerOptions}
                        />
                    </View>
                )}
                {children}
            </View>
            {modals}
        </SafeAreaScreenContainer>
    )
}

export default BaseScreen
