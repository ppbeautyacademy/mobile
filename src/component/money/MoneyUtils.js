import React from 'react';
import { LANGUAGE_CODES, numberWithCommas } from '../../assets/constants';

export function formatMoney(money, currency) {
    if(money) {
        return currency === LANGUAGE_CODES.en.value 
                    ? `${currency} ${numberWithCommas(money)}`
                    : `${numberWithCommas(money)} ${currency}`
    }
}