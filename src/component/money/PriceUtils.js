import { Text } from '@ui-kitten/components';
import React from 'react';
import { StyleSheet } from 'react-native';
import { numberWithCommas } from '../../assets/constants';

const renderPrice = priceSetting => {  
    if (!priceSetting || priceSetting.errorMessage) {
        return 'Not set up'
    }

    if (priceSetting.price) {
        if (priceSetting.salePrice) {
            return (
                <React.Fragment>
                    <Text style={styles.salePrice}  category="p2">
                        {priceSetting.price && `${numberWithCommas(priceSetting.price)}`}
                    </Text>
                    <Text  category="p1">{priceSetting.salePrice && `${numberWithCommas(priceSetting.salePrice)}`}</Text>
                </React.Fragment>
            )
        }

        return  <Text category="p1">{numberWithCommas(priceSetting.price)}</Text>
    } else {
        if (priceSetting.min && priceSetting.max) {
            return (
                <React.Fragment>
                    <Text
                        category="p2"
                        style={[
                            styles.price,
                            styles.mr2,
                        ]}>
                        {priceSetting.min && numberWithCommas(priceSetting.min)}
                    </Text>
                    <Text category="p2" style={styles.price}>
                        {' '}
                        ~ {priceSetting.max && numberWithCommas(priceSetting.max)}
                    </Text>
                </React.Fragment>
            )
        }
    }
}

export default renderPrice;

const styles = StyleSheet.create({
    price: {
        paddingTop: 5,
    },
    salePrice: {
        textDecorationLine: 'line-through',
    },
    mr2: {
        marginRight: 2,
    },
})