import React, {useRef, useState} from 'react'
import {StyleSheet} from 'react-native'
import {LargeList} from 'react-native-largelist'
import {translate} from '../../config/i18n'
import logService from '../services/logService'
import {useMount} from '../utils/commonHooks'
import ScrollViewFooter from './ScrollViewFooter'
import ScrollViewHeader from './ScrollViewHeader'

const QUERY_STATE = {
    REFRESH: 'REFRESH',
    LOAD_MORE: 'LOAD_MORE',
}

const FRIST_PAGE = 1

const useLoadData = props => {
    const {callApi, onLoadComplete} = props
    const [state, setState] = useState({
        items: [],
        pagingInfo: {
            page: FRIST_PAGE,
        },
        loading: false,
        queryState: QUERY_STATE.REFRESH,
        error: '',
    })

    const handleOnCompleted = newItems => {
        setState({...state, loading: false})
        onLoadComplete && onLoadComplete(newItems)
    }

    const refresh = async () => {
        setState({...state, loading: true, queryState: QUERY_STATE.REFRESH})
        let response
        callApi &&
            (await callApi({pagingInfo, queryParams})
                .then(res => (response = res?.data))
                .catch(err => logService.log(err)))
        if (response) {
            setState({
                ...state,
                items: response,
            })
            handleOnCompleted(response)
        } else {
            setState({
                ...state,
                items: [],
                error: translate('errors.commonError'),
            })
        }
    }

    const loadMore = async () => {
        setState({...state, loading: true, queryState: QUERY_STATE.LOAD_MORE})
        callApi &&
            (await callApi({pagingInfo, queryParams})
                .then(res => (response = res?.data))
                .catch(err => logService.log(err)))
        if (response?.length > 0) {
            const newItems = state.items
            response?.forEach(e => {
                newItems.push(e)
            })
            setState({
                ...state,
                items: newItems,
                pagingInfo: {page: state.pagingInfo.page + 1},
            })
            handleOnCompleted(newItems)
        } else {
            setState({
                ...state,
                items: [],
                error: translate('errors.commonError'),
            })
        }
    }

    useMount(refresh)

    const actions = {
        refresh,
        loadMore,
    }
    const data = {
        data: state.items,
        error: state.error,
        loading: state.loading,
    }

    return {actions, ...data}
}

const LazyList = ({renderItems, ...props}) => {
    const listRef = useRef()
    const onLoadComplete = newItems => {
        listRef.current?.endRefresh()
        listRef.current?.endRefresh()
        props.onLoadComplete && props.onLoadComplete(newItems)
    }

    const {actions, data, error, loading} = useLoadData({
        ...props,
        onLoadComplete,
    })
    const onRefresh = () => {
        await actions.refresh()
    }

    const onLoadMore = () => {
        await actions.loadMore()
    }

    return (
        <>
            <LargeList
                ref={listRef}
                renderIndexPath={({row: index}) =>
                    renderItems({item: data[index]})
                }
                heightForIndexPath={() => props.itemHeight ?? 160}
                data={[{items: data}]}
                contentStyle={[{paddingHorizontal: 16}]}
                style={[{}]}
                onRefresh={onRefresh}
                onLoading={onLoadMore}
                refreshHeader={ScrollViewHeader}
                loadingFooter={ScrollViewFooter}
            />
        </>
    )
}

export default LazyList
