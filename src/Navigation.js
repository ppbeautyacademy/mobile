import { CommonActions } from '@react-navigation/native';
import React from 'react';

import {callAfterInteraction} from './utils/commonHooks';

const rootNavigationRef = React.createRef();
const setRootNavigationRef = navigation => {
  rootNavigationRef.current = navigation;
};

function resetRootNavigatorToScreen(name, params) {
  const newRoutes = {index: 0, routes: [{name, params}]};
  rootNavigationRef.current?.reset(newRoutes);
}

const resetNavigatorToScreen = (navigation, screenId, nestedScreen, params = {}, nestedTabScreen = null) => {
  let newRoutes = {index: 0};
  
  if(nestedTabScreen) {
    newRoutes = {
      ...newRoutes,
      routes: [
        {
          name: screenId,
          state: {
            routes: [
              {
                name:nestedScreen,
                params,
                state: {
                  routes: [
                    {
                      name: nestedTabScreen
                    }
                  ]
                }
              }
            ]
          }
        }
      ]
    }
  } else if(nestedScreen) {
    newRoutes = {
      ...newRoutes,
      routes: [
        {
          name: screenId,
          state: {
            routes: [
              {
                name:nestedScreen,
                params
              }
            ]
          }
        }
      ]
    }
  } else {
    newRoutes = {
      ...newRoutes,
      routes: [
        {name: screenId, params}
      ]
    }
  }

  navigation.dispatch({
    ...CommonActions.reset(newRoutes)
  })
};

let callbackLoginSuccess;

export const setCallbackLoginSuccess = callback => {
  callbackLoginSuccess = callback;
};

export const excuteCallbackLoginSuccess = () => {
  callAfterInteraction(() => {
    callbackLoginSuccess && callbackLoginSuccess();
    callbackLoginSuccess = null;
  });
};

export {
  resetNavigatorToScreen,
  resetRootNavigatorToScreen,
  rootNavigationRef,
  setRootNavigationRef,
};
