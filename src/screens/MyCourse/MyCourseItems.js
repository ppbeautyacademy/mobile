import { Icon } from '@ui-kitten/components'
import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import FastImage from 'react-native-fast-image'
// import ProgressCircle from 'react-native-progress-circle'

const MyCourseItems = ({navigation, courses}) => {
    return(
        <React.Fragment>
            {   courses && courses.length > 0
            ?
                courses.map((item, idx) => (
                    <View key={idx}>
                        <TouchableOpacity
                            onPress={onPress}
                            style={{
                                flexDirection:"row",
                                backgroundColor:bg,
                                padding:20,
                                marginHorizontal:20,
                                borderRadius:20,
                                alignItems:"center",
                                marginTop:10
                            }}
                        >

                        <FastImage
                            source={{uri: item.image}}
                            style={{width:40,height:40}}
                        />

                        <View>
                            <Text style={{
                                color:"#345c74",
                                fontFamily:"Bold",
                                fontSize:13,
                                paddingHorizontal:20,
                                width:170
                            }}>{title}</Text>
                            <Text style={{
                                color:"#f58084",
                                fontFamily:"Medium",
                                fontSize:12,
                                paddingHorizontal:20
                            }}>
                                10 hours, 19 lessons
                            </Text>
                        </View>
                        <Text style={{
                            color:"#345c74",
                            fontFamily:"Medium",
                            fontSize:13,
                            paddingLeft:10,
                            paddingRight:10
                        }}>
                            25%
                        </Text>
                        {/* <ProgressCircle
                            percent={30}
                            radius={17}
                            borderWidth={1.5}
                            color="f580084"
                            shadowColor="#FFF"
                            bgColor="#FFF"
                        >
                            <Icon 
                                name
                            />
                        </ProgressCircle> */}

                        </TouchableOpacity>
                    </View>
                ))
            :
                <View>

                </View>
            }
        </React.Fragment>
    )
}