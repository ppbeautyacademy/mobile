import React, {useContext, useEffect, useState} from 'react'
import {
    View,
    ImageBackground,
    TouchableOpacity,
    Image,
    StyleSheet,
} from 'react-native'
import {ScrollView} from 'react-native-gesture-handler'
import context from '../../../context/context'
import {COLORS} from '../../assets/theme/colors'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {ScreenIds} from '../../ScreenIds'
import CourseListView from './MyCourseListView'
import {
    BottomNavigation,
    BottomNavigationTab,
    Button,
    Icon as KittenIcon,
    Text,
} from '@ui-kitten/components'
import BaseScreen from '../../component/BaseScreen'
import {translate} from '../../../config/i18n'
import { getMyCourses } from '../../services/ServiceWorker'
import { useIsFocused } from '@react-navigation/core'
import FastImage from 'react-native-fast-image'
import { commonStyles } from '../../assets/theme/styles'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

// TODO Translation
const MyCourseScreen = ({navigation}) => {
    const {state} = useContext(context)

    //#region MY COURSE TABS
    const [selectedIndex, setSelectedIndex] = useState(0)
    //#endregion

    //#region MY COURSES LIST VIEW
    const [progressCourses, setProgressCourses] = useState([]);
    const [completedCourses, setCompletedCourses] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [skeletonHolders, setSkeletonHolders] = useState([
        {}, {} , {}
    ])

    const loadMyCourses = async() => {
        const myCoursesData = await getMyCourses(state.userInfo?.id, state.version);

        if(myCoursesData && Array.isArray(myCoursesData)) {
            setProgressCourses(filterCourses(myCoursesData, statusTypes.progress.value));
            setCompletedCourses(filterCourses(myCoursesData, statusTypes.completed.value));
            setIsLoaded(true)
        }
    }

    const renderEmptyCourse = () => {
        return(
            <View style={{flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
                <FastImage 
                    source={require("../../assets/images/empty.png")}
                    style={{
                        height: 160,
                        width: 240
                    }}
                />
                <Text appearance={"hint"}>Empty courses</Text>
            </View>
        )
    }

    const statusTypes = {
        progress: {label: "Progress Courses", value: "progress"},
        completed: {label: "Completed Courses", value: "completed"}
    }

    const filterCourses = (dataList, type) => {
        let filterCourses = [];
        if(!dataList || !Array.isArray(dataList) || dataList.length < 1) {
            return filterCourses;
        }

        for(let i = 0; i < dataList.length; i++) {
            if(type === statusTypes.completed.value && dataList[i].completeProgress === 100) {
                filterCourses.push(dataList[i]);
            } else if(type === statusTypes.progress.value && dataList[i].completeProgress < 100) {
                filterCourses.push(dataList[i])
            }
        }

        return filterCourses;
    }

    const isFocused = useIsFocused();

    useEffect(async() => {
        if(state.isLoggedIn && isFocused) {
            await loadMyCourses();
        }
    }, [isFocused]);
    //#endregion

    return (
        !state.isLoggedIn ?
            <React.Fragment>
                <View style={{flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center", backgroundColor: COLORS.WHITE}}>
                    <View style={{height: 250, marginTop: -40}}>
                        <View style={{padding: 20}}>
                            <FastImage 
                                source={require("../../assets/images/authenticate-required.png")}
                                style={{height: 200}}
                            />
                            <Text>You need to login to access your courses</Text>
                        </View>
                        <Button
                            onPress={() => navigation.navigate(ScreenIds.AuthStack)}
                            size={"small"}
                        >
                            {props => (
                                <Text
                                    {...props}
                                    style={[commonStyles.mainFont, commonStyles.lightColor]}
                                >
                                    LOGIN NOW
                                </Text>
                            )}
                        </Button>
                    </View>
                </View>
            </React.Fragment>
        :
            <React.Fragment>
                <BaseScreen showHeader={false} onBackPress={() => {}}>
                    <ImageBackground
                        source={require('../../assets/images/home.png')}
                        style={styles.imageBackground}>
                        <ScrollView>
                            <Text style={styles.title}>
                                {translate('myCourse.welcomeBack')} Duy
                            </Text>

                            <View style={styles.browseCoursesContainer}>
                                <View>
                                    <Text style={styles.browseCoursesTitle}>
                                        {translate('myCourse.browseForMoreCourses')}
                                    </Text>
                                    <TouchableOpacity
                                        onPress={() =>
                                            navigation.navigate(ScreenIds.Course)
                                        }
                                        style={styles.browseCoursesBtn}>
                                        <Text style={styles.browseCourseBtnLabel}>
                                            {translate('myCourse.discoverNow')}
                                        </Text>

                                        <Icon
                                            name="trending-flat"
                                            color={COLORS.WHITE}
                                            size={26}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <Image
                                    source={require('../../assets/images/undraw.png')}
                                    style={{marginLeft: -80, marginTop: 35}}
                                />
                            </View>

                            <View style={styles.myCourseContainer}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                    }}>
                                    <Text style={styles.courseProgressTitle}>
                                        {translate('myCourse.myCourses')}
                                    </Text>
                                    <View style={styles.courseProgressTitle}>
                                        <TouchableOpacity
                                            onPress={() => navigation.navigate(ScreenIds.CourseExchange)}
                                        >
                                            <View
                                                style={{
                                                    backgroundColor: COLORS.SUB,
                                                    borderRadius: 30,
                                                    paddingVertical: 5,
                                                    paddingHorizontal: 10,
                                                    width: 100,
                                                    textAlign: 'center',
                                                    justifyContent: 'center',
                                                    flexDirection: 'row',
                                                }}>
                                                <Icon
                                                    name="published-with-changes"
                                                    size={16}
                                                    color={COLORS.WHITE}
                                                    style={{
                                                        alignSelf: 'center',
                                                        marginRight: 5,
                                                    }}
                                                />
                                                <Text
                                                    style={{
                                                        color: COLORS.WHITE,
                                                        textAlign: 'center',
                                                    }}>
                                                    {translate('myCourse.claim')}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{marginBottom: 15}}>
                                    <BottomNavigation
                                        selectedIndex={selectedIndex}
                                        onSelect={index => setSelectedIndex(index)}>
                                        <BottomNavigationTab title="IN PROGRESS" />
                                        <BottomNavigationTab title="COMPLETED" />
                                    </BottomNavigation>
                                </View>

                                {!isLoaded &&
                                    <View style={styles.myCourseContainer}>
                                        {skeletonHolders.map((item, idx) => (
                                            <SkeletonPlaceholder key={idx}>
                                                <View style={styles.skeletonContainer}>
                                                    <View style={styles.skeletonWrapper}></View>
                                                </View>
                                            </SkeletonPlaceholder>
                                        ))}
                                    </View>
                                }
                                
                                {(progressCourses || completedCourses) &&
                                    (progressCourses.length < 1 && selectedIndex === 0 
                                    ?   renderEmptyCourse()
                                    :completedCourses.length < 1 && selectedIndex === 1
                                    &&  renderEmptyCourse()
                                    )
                                }

                                {/* IN PROGRESS COURSES */}
                                {isLoaded && selectedIndex === 0 &&
                                    <View style={styles.myCourseContainer}>
                                        <CourseListView dataList={progressCourses} navigation={navigation} />
                                    </View>
                                }

                                {/* COMPLETED COURSES */}
                                {isLoaded && selectedIndex === 1 &&
                                    <View style={styles.myCourseContainer}>
                                        <CourseListView dataList={completedCourses} navigation={navigation} />
                                    </View>
                                }
                                
                            </View>
                        </ScrollView>
                    </ImageBackground>
                </BaseScreen>
            </React.Fragment>
        
    )
}

export default MyCourseScreen

const styles = StyleSheet.create({
    /********* Title ***********/
    title: {
        paddingHorizontal: 20,
        fontSize: 35,
        paddingTop: 60,
        paddingBottom: 20,
        color: COLORS.WHITE,
    },
    imageBackground: {
        width: '100%',
        height: '100%',
    },
    /********* More courses ************/
    browseCoursesContainer: {
        flexDirection: 'row',
        backgroundColor: '#FFF2F2',
        marginTop: 15,
        marginHorizontal: 20,
        borderRadius: 20,
        paddingVertical: 30,
        paddingLeft: 30,
    },
    browseCoursesTitle: {
        color: '#345c74',
        fontSize: 20,
        width: 250,
        paddingRight: 100,
    },
    browseCoursesBtn: {
        flexDirection: 'row',
        backgroundColor: '#f58084',
        alignItems: 'center',
        marginTop: 20,
        width: 150,
        paddingVertical: 10,
        borderRadius: 14,
        paddingHorizontal: 10,
    },
    browseCourseBtnLabel: {
        color: COLORS.WHITE,
        fontSize: 14,
        marginRight: 3,
    },
    /********* In Progress Courses *********/
    courseProgressTitle: {
        color: COLORS.BLACK,
        fontSize: 24,
        paddingHorizontal: 20,
        marginTop: 20,
        marginBottom: 10,
    },

    myCourseContainer: {
        flex: 1,
        paddingBottom: 40,
        backgroundColor: COLORS.WHITE
    },
    /********* Search And Filter **********/
    searchContainer: {
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingBottom: 20,
    },
    searchBarInputContainer: {
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        height: 45,
    },
    searchBarContainer: {
        width: '100%',
        padding: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
    },
    searchBar: {
        height: 50,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    // Skeleton
    skeletonContainer: {
        marginHorizontal: 20,
        marginBottom: 15
    },
    skeletonWrapper: {
        height: 80,
        width: "100%",
        borderRadius: 8
    }
})
