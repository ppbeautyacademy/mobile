import React, {useContext, useEffect, useRef, useState} from 'react'
import {Keyboard, KeyboardAvoidingView, Platform, StyleSheet, View} from 'react-native'
import {ScrollView, TouchableOpacity, TouchableWithoutFeedback} from 'react-native-gesture-handler'
import BaseScreen from '../../component/BaseScreen'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {COLORS} from '../../assets/theme/colors'
import FastImage from 'react-native-fast-image'
import {
    Spinner,
    Text,
    Icon as KittenIcon,
    Button,
    Input,
} from '@ui-kitten/components'
import OtpInputs from 'react-native-otp-inputs'
import {Modalize} from 'react-native-modalize'
import KeyboardScrollView from '../../component/KeyboardScrollView'
import {translate} from '../../../config/i18n'
import context from '../../../context/context'
import { Toast } from 'popup-ui'
import { applyCourseCode, checkCourseCode, getAddressFormatted } from '../../services/ServiceWorker'
import { HTTP_STATUS_OK, LANGUAGE_CODES } from '../../assets/constants'
import { ScreenIds } from '../../ScreenIds'
import VNAddress from '../../component/address/VNAddress'
import ENAddress from '../../component/address/ENAddress'

const claimStages = {
    applyCode: {label: translate('myCourse.useCode'), value: 'applyCode'},
    inputInfo: {
        label: translate('myCourse.inputStudentInfo'),
        value: 'inputInfo',
    },
    complete: {
        label: translate('myCourse.completeClaimCode'),
        value: 'complete',
    },
}

const ClaimCourse = ({navigation}) => {
    const {state, actions} = useContext(context); 

    //#region Stages
    const [stage, setStage] = useState(claimStages.applyCode.value)
    const [isLoaded, setIsLoaded] = useState(false);
    //#endregion

    //#region Exchange Information
    const modalInfoRef = useRef()

    useEffect(() => {
        modalInfoRef.current?.close()
        setIsLoaded(true)
    }, [])
    
    //#endregion

    //#region Code Validation
    const checkValidCourseCode = async() => {
        setLoading(true)

        let isError = false;
        if(!participantCode) {
            Toast.show({

            })

            isError = true;
        }

        if(participantCode && participantCode.length < 6) {
            Toast.show({

            })

            isError = true;
        }

        if(!isError) {
            const validationData = await checkCourseCode(state.userInfo?.id, state.version, participantCode);
            if(validationData && validationData.success) {
                setStage(claimStages.inputInfo.value)
                setStudentInfo({
                    fullname: validationData.fullname,
                    email: validationData.email,
                    phone: validationData.phone,
                    address: validationData.address
                })
                setSuccess(true)
            }
        }

        setLoading(false)
    }
    //#endregion

    //#region Otp
    const [participantCode, setParticipantCode] = useState('')

    const [loading, setLoading] = useState(false)
    const [success, setSuccess] = useState(false);

    const LoadingIndicator = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" />
        </View>
    )

    const SuccessIndicator = props => (
        <KittenIcon {...props} name="checkmark-circle-outline" />
    )
    //#endregion

    //#region Student Information
    const [studentInfo, setStudentInfo] = useState({
        fullname: '',
        email: '',
        phone: '',
        address: '',
    })

    const [errors, setErrors] = useState({})

    const renderFullnameIcon = props => (
        <KittenIcon {...props} name="person-outline" />
    )

    const renderEmailIcon = props => (
        <KittenIcon {...props} name="email-outline" />
    )

    const renderPhoneIcon = props => (
        <KittenIcon {...props} name="phone-outline" />
    )

    const renderAddressIcon = props => (
        <KittenIcon  {...props} name="home-outline" />
    )

    const handleApplyCourseCode = async() => {
        let isError = false;
        
        if(!studentInfo.fullname) {
            Toast.show({

            })
            isError = true;
        }

        if(!studentInfo.email) {
            Toast.show({

            })
            isError = true;
        }

        if(!studentInfo.phone) {
            Toast.show({

            })
            isError = true;
        }

        if(!studentInfo.address) {
            Toast.show({

            })
            isError = true;
        }

        if(!isError) {
            const applyStatus = await applyCourseCode(state.userInfo?.id, state.version, participantCode, studentInfo);
            if(applyStatus === HTTP_STATUS_OK) {
                setStage(claimStages.complete.value)
                setStudentInfo({
                    fullname: '',
                    email: '',
                    phone: '',
                    addressStringBean: ''
                })
            }
        }

    } 
    //#endregion

    //#region Location
    const addressModalRef = useRef();
    const [addressBean, setAddressBean] = useState({})
    const [addressErrors, setAddressErrors] = useState({})

    const showAddressModal = () => {
        // Keyboard.dismiss();
        addressModalRef.current?.open();
    }

    const onStreetChange = street => {
        setAddressBean({
            ...addressBean,
            street: street,
        })
    }

    const onCountryChange = country => {
        setAddressBean({
            ...addressBean,
            refCountry: country,
        })
    }

    const onStateChange = state => {
        setAddressBean({
            ...addressBean,
            refState: state,
        })
    }

    const onCityChange = city => {
        setAddressBean({
            ...addressBean,
            city: city,
        })
    }

    const onProvinceChange = province => {
        setAddressBean({
            ...addressBean,
            refProvince: province,
        })
    }

    const onDistrictChange = district => {
        setAddressBean({
            ...addressBean,
            refDistrict: district,
        })
    }

    const onWardChange = ward => {
        setAddressBean({
            ...addressBean,
            refWard: ward,
        })
    }

    const onZipCodeChange = zipCode => {
        setAddressBean({
            ...addressBean,
            zipCode: zipCode,
        })
    }

    const handleUpdateAddress = async() => {
        let isError = false;
        let errors = {};

        if (state.version) {
            if (state.version === LANGUAGE_CODES.vi.value) {
                if (
                    !addressBean ||
                    !addressBean.street ||
                    !addressBean.refWard ||
                    !addressBean.refDistrict ||
                    !addressBean.refProvince
                ) {
                    errors = {
                        ...errors,
                        street: !addressBean.street,
                        refWard: !addressBean.refWard,
                        refDistrict: !addressBean.refDistrict,
                        refProvince: !addressBean.refProvince,
                    }

                    isError = true
                }
            } else {
                if (
                    !addressBean ||
                    !addressBean.street ||
                    !addressBean.city ||
                    !addressBean.refState ||
                    !addressBean.refCountry ||
                    !addressBean.zipCode
                ) {
                    errors = {
                        ...errors,
                        street: !addressBean.street,
                        city: !addressBean.city,
                        refState: !addressBean.refState,
                        refCountry: !addressBean.refCountry,
                        zipCode: !addressBean.zipCode,
                    }

                    isError = true
                }
            }
        }

        if(isError) {
            setAddressErrors(errors)
        } else {
            let data = {
                street : addressBean?.street,
                refWard : addressBean?.refWard,
                refDistrict : addressBean?.refDistrict,
                refProvince : addressBean?.refProvince,
                city : addressBean?.city,
                refState : addressBean?.refState,
                refCountry : addressBean?.refCountry,
                zipCode: addressBean?.zipCode,
            }

            const formattedAddress = await getAddressFormatted(state.version, data);
            if(formattedAddress.isError) {
                setAddressErrors(formattedAddress)
            } else {
                addressModalRef.current?.close();

                setAddressErrors({});
                setAddressBean({});
                setStudentInfo({
                    ...studentInfo,
                    addressStringBean: formattedAddress
                })
            }
        }

        
    }
    //#endregion

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate(ScreenIds.MyCourse)}>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text>{translate('myCourse.exchangeCourse')}</Text>
                    <View>
                        <TouchableOpacity
                            onPress={() => modalInfoRef.current?.open()}>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="info"
                                    size={22}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                {stage === claimStages.applyCode.value && (
                    <React.Fragment>
                        <View style={styles.imageWrapper}>
                            <FastImage
                                source={require('../../assets/images/sync.png')}
                                style={{
                                    height: 250,
                                    width: '100%',
                                }}
                            />
                        </View>
                        <KeyboardAvoidingView
                            behavior={
                                Platform.Os == 'ios' ? 'padding' : 'height'
                            }>
                            <View style={styles.exchangeSection}>
                                {isLoaded &&
                                    <OtpInputs
                                        style={styles.otpContainer}
                                        keyboardType="name-phone-pad"
                                        handleChange={code =>
                                            setParticipantCode(code.toUpperCase())
                                        }
                                        numberOfInputs={6}
                                        inputContainerStyles={styles.otpInput}
                                        inputStyles={styles.otpInputWrapper}
                                    />
                                }
                                <Button
                                    onPress={() => checkValidCourseCode()}
                                    accessoryLeft={success ? SuccessIndicator : loading && LoadingIndicator}
                                    appearance="outline"
                                    style={styles.submitButton}>
                                    {translate('myCourse.submitCap')}
                                </Button>
                            </View>
                        </KeyboardAvoidingView>
                    </React.Fragment>
                )}

                {stage === claimStages.inputInfo.value && (
                    <React.Fragment>
                        <ScrollView
                            keyboardShouldPersistTaps="handled"
                            showsVerticalScrollIndicator={false}>
                            <View style={{marginVertical: 20}}>
                                <Text category="h5">
                                    {translate('myCourse.studentInformation')}
                                </Text>
                            </View>
                            <KeyboardAvoidingView enabled>
                                <View style={styles.formContainer}>
                                    <Input
                                        style={styles.formGroup}
                                        value={studentInfo.fullname}
                                        status={errors.fullname && 'danger'}
                                        label={translate('common.fullName')}
                                        placeholder={translate(
                                            'common.fullNamePlaceholder',
                                        )}
                                        accessoryRight={renderFullnameIcon}
                                        onChangeText={username =>
                                            setStudentInfo({
                                                fullname: username,
                                            })
                                        }
                                    />
                                    <Input
                                        style={styles.formGroup}
                                        value={studentInfo.email}
                                        status={errors.email && 'danger'}
                                        label={translate('common.email')}
                                        placeholder={translate(
                                            'common.emailPlaceholder',
                                        )}
                                        accessoryRight={renderEmailIcon}
                                        onChangeText={email =>
                                            setStudentInfo({
                                                email: email,
                                            })
                                        }
                                    />
                                    <Input
                                        style={styles.formGroup}
                                        value={studentInfo.phone}
                                        status={errors.phone && 'danger'}
                                        label={translate('common.phoneNumber')}
                                        placeholder={translate(
                                            'common.phoneNumberPlaceholder',
                                        )}
                                        accessoryRight={renderPhoneIcon}
                                        onChangeText={phone =>
                                            setStudentInfo({
                                                phone: phone,
                                            })
                                        }
                                    />
                                </View>

                                <TouchableWithoutFeedback onPress={() => showAddressModal()}>
                                    <View style={{marginBottom: 30}}>
                                        <Text category="label" appearance="hint">
                                            {translate('common.address')}
                                        </Text>
                                        <View 
                                            style={styles.input}
                                        >   
                                            <View style={{width: '85%'}}>
                                                <Text>
                                                    {studentInfo.addressStringBean}
                                                </Text>
                                            </View>
                                            
                                            <View style={{alignSelf: 'center'}}>
                                                <KittenIcon style={styles.icon} name="home-outline" fill='#8F9BB3' />
                                            </View>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>

                                <Button
                                    onPress={() => handleApplyCourseCode()}
                                    appearance="outline"
                                    style={styles.submitButton}>
                                    {translate('myCourse.submitCap')}
                                </Button>
                            </KeyboardAvoidingView>
                        </ScrollView>

                        <Modalize
                            ref={addressModalRef}
                            handleStyle={styles.handleModalStyle}
                            modalStyle={{
                                borderTopLeftRadius: 30,
                                borderTopRightRadius: 30,
                                backgroundColor: COLORS.WHITE
                            }}
                            handlePosition="inside"
                            keyboardAvoidingBehavior="padding"
                            modalHeight={500}
                            disableScrollIfPossible={true}
                            keyboardAvoidingOffset={150}
                            onBackButtonPress={() => addressModalRef.current?.close()}
                            scrollViewProps={{ showsVerticalScrollIndicator:false, keyboardShouldPersistTaps: 'handled' }}
                        >   
                            <View style={{marginVertical: 30, paddingHorizontal: 20}}>
                                {state.version === LANGUAGE_CODES.vi.value ? (
                                    <VNAddress
                                        addressBean={addressBean}
                                        setStreet={onStreetChange}
                                        setRefWard={onWardChange}
                                        setRefDistrict={onDistrictChange}
                                        setRefProvince={onProvinceChange}
                                        errors={addressErrors}
                                    />
                                ) : (
                                    <ENAddress
                                        addressBean={addressBean}
                                        setStreet={onStreetChange}
                                        setCity={onCityChange}
                                        setRefState={onStateChange}
                                        setRefCountry={onCountryChange}
                                        setZipCode={onZipCodeChange}
                                        errors={addressErrors}
                                    />
                                )}

                                <View style={{marginVertical: 20}}>
                                    <Button  onPress={() => handleUpdateAddress()}>CẬP NHẬT</Button>
                                </View>
                            </View>
                        </Modalize>
                    </React.Fragment>
                )}

                {stage === claimStages.complete.value && (
                    <React.Fragment>
                        <View style={styles.imageWrapper}>
                            <FastImage
                                source={require('../../assets/images/complete-claim.png')}
                                style={{
                                    height: 250,
                                    width: '100%',
                                }}
                            />
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <Text category="h5">
                                {translate('myCourse.congratulations')}
                            </Text>
                            <Text appearance="hint">
                                {translate(
                                    'myCourse.successfullyClaimCourseQuote',
                                )}
                            </Text>
                            <Text appearance="hint" style={{marginBottom: 30}}>
                                {translate('myCourse.startLessonQuote')}
                            </Text>
                            <Button
                                appearance="outline"
                                style={styles.submitButton}>
                                {translate('myCourse.viewMyLessons')}
                            </Button>
                        </View>
                    </React.Fragment>
                )}

                <Modalize
                    ref={modalInfoRef}
                    handleStyle={styles.handleModalStyle}
                    modalStyle={{
                        borderTopLeftRadius: 30,
                        borderTopRightRadius: 30,
                    }}
                    snapPoint={350}
                    onBackButtonPress={() => modalInfoRef.current?.close()}
                    scrollViewProps={{showsVerticalScrollIndicator: false}}>
                    <View
                        style={{flex: 1, marginTop: 40, paddingHorizontal: 20}}>
                        <Text category="h6">
                            {translate('myCourse.whatIsExchange')}
                        </Text>
                        <Text
                            category="label"
                            appearance="hint"
                            style={{marginBottom: 8}}>
                            {translate('myCourse.whatIsExchangeAns')}
                        </Text>
                        <Text category="h6">
                            {translate('myCourse.whereToGetCode')}
                        </Text>
                        <Text
                            category="label"
                            appearance="hint"
                            style={{marginBottom: 8}}>
                            {translate('myCourse.whereToGetCodeAns')}
                        </Text>
                        <Text category="h6">
                            {translate('myCourse.howToUseCode')}
                        </Text>
                        <Text
                            category="label"
                            appearance="hint"
                            style={{marginBottom: 8}}>
                            {translate('myCourse.howToUseCodeAns')}
                        </Text>
                        <Text category="h6">
                            {translate('myCourse.howToTrackCodeStatus')}
                        </Text>
                        <Text
                            category="label"
                            appearance="hint"
                            style={{marginBottom: 8}}>
                            {translate('myCourse.howToTrackCodeStatusAns')}
                        </Text>
                        <Text category="h6">
                            {translate('myCourse.howToSecureCode')}
                        </Text>
                        <Text
                            category="label"
                            appearance="hint"
                            style={{marginBottom: 8}}>
                            {translate('myCourse.howToSecureCodeAns')}
                            <Text category="s2">
                                {translate('myCourse.7Days')}
                            </Text>{' '}
                            {translate('myCourse.howToSecureCodeAns1')}
                        </Text>
                    </View>
                </Modalize>
            </View>
        </BaseScreen>
    )
}

export default ClaimCourse

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor: COLORS.WHITE,
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /******* Image **********/
    imageWrapper: {
        marginVertical: 30,
    },
    /******* OTP Input ********/
    exchangeSection: {
        alignItems: 'center',
    },
    otpContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 30,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    otpInputWrapper: {
        textTransform: 'capitalize',
        textAlign: 'center',
        fontWeight: '700',
    },

    otpInput: {
        backgroundColor: COLORS.LIGHTGRAY,
        marginHorizontal: 5,
        paddingLeft: 3,
        paddingRight: 3,
        borderRadius: 3,
        fontSize: 16,
        textTransform: 'uppercase',
    },
    /******* Submit *******/
    submitButton: {
        paddingHorizontal: 40,
    },
    /********* Modal *********/
    handleModalStyle: {
        marginTop: 30,
        backgroundColor: COLORS.SUB,
        width: 80,
    },
    /********* Student Info Gathering ********/
    formContainer: {
        alignItems: 'center',
    },
    formGroup: {
        marginBottom: 16,
    },
    /******** Input *********/
    input: {
        backgroundColor: 'rgb(247, 249, 252)',
        borderColor: 'rgb(228, 233, 242)',
        borderRadius: 4,
        borderWidth: 1,
        minHeight: 40,
        paddingVertical: 7,
        paddingLeft: 20,
        paddingRight: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    /******* Icon *********/
    icon: {
        width: 24,
        height: 24,
    }, 
})
