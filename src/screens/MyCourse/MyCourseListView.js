import React, {useRef, useState} from 'react'
import {StyleSheet, View, Platform, Image} from 'react-native'
import {COLORS} from '../../assets/theme/colors'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {commonStyles} from '../../assets/theme/styles'
import { Text } from '@ui-kitten/components'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { ScreenIds } from '../../ScreenIds'
import ProgressCircle from 'react-native-progress-circle'

const CourseCardItem = ({item, navigation}, key) => {
    return (
        <View
            key={key}
            style={[
                styles.cardShadowiOS,
                Platform.OS === 'ios' ? commonStyles.shadowApp : {},
            ]}>
            <View
                style={[
                    styles.cardContainer,
                    Platform.OS === 'android' ? commonStyles.shadowApp : {},
                ]}>
                <View style={styles.cardBody}>
                    <View style={styles.thumbnailContainer}>
                        <Image 
                            source={{uri: item.imageUrl}}
                            style={styles.image}
                        />
                    </View>
                    <View  style={styles.cardHeader}>
                        <Text style={[styles.cardHeaderText]} category="s1">
                            {item?.name}
                        </Text>
                        <Text style={styles.cardMiscInfoText}>
                        {   item.totalLessons && item.totalAssignments 
                            ?  `${item.totalLessons} lessons - ${item.totalAssignments} assignments`
                            :   item.totalLessons
                            ?   `${item.totalLessons} lessons`
                            :   item.totalAssignments
                            ?   `${item.totalAssignments} assignments`
                            :   'Course not set up'
                        }
                        </Text>
                    </View>
                    <View style={styles.viewButtonWrapper}>
                        <TouchableOpacity
                            disabled={!item.ableToAccess}
                            onPress={() => navigation.navigate(ScreenIds.LessonScreen, {courseId: item.courseId, name: item.name, participantId: item.id})}
                        >
                            <ProgressCircle
                                percent={item.completeProgress}
                                radius={15}
                                borderWidth={2}
                                color="#eaa196"
                                shadowColor={COLORS.WHITE}
                            >
                                <View
                                    style={[styles.viewButtonContainer, item.ableToAccess ? styles.viewButtonBackground : styles.disableButtonBackground ]}
                                >
                                    <Icon 
                                        name="play-arrow"
                                        size={20}
                                        color={item.ableToAccess ? COLORS.SUB : COLORS.BLACK}
                                    />
                                </View>
                            </ProgressCircle>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

const MyCourseListView = ({dataList, navigation}) => {
    return(
        <View style={{flex: 1, paddingHorizontal: 20}}>
            {dataList.map((item, idx) => (
                <CourseCardItem navigation={navigation} item={item} key={idx}/>
            ))}
        </View>
        
    )
}

export default MyCourseListView;

const styles = StyleSheet.create({
    /******* Card ********/ 
    courseListContainer: {
        flexGrow: 1,
        paddingVertical: 16,
        paddingHorizontal: 20,
        backgroundColor: COLORS.WHITE_BACKGROUND,
    },
    cardContainer: {
        alignSelf: 'center',
        width: '100%',
        padding: 20,
        backgroundColor: COLORS.WHITE,
        borderRadius: 10,
        marginBottom: 10,
    },
    cardHeader: {
        marginRight: 15, 
        width: '70%'
    },
    cardHeaderText: {
        fontWeight: '700',
    },
    cardMiscInfoText: {
        fontSize: 12,
        fontWeight: '700',
        color: COLORS.SUB,
    },
    cardBody: {
        flexDirection: 'row',
    },
    cardShadowiOS: {
        flex: 1,
    },
    /******* Thumbnail ********/
    thumbnailContainer: {
        marginRight: 15, 
        justifyContent: 'center'
    },
    image: {
        height: 60, 
        width: 60, 
        borderRadius: 5
    },
    /******* Button *******/
    viewButtonWrapper: {
        position: 'absolute',
        right: 0,
        alignSelf: 'center'
    },
    viewButtonContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewButtonBackground: {
        backgroundColor: '#FDF3EB',
    },
    disableButtonBackground: {
        backgroundColor: 'rgba(0,0,0,0.2)'
    }
})
