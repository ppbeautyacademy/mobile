import React, {useContext, useEffect, useState} from 'react'
import {Keyboard, StyleSheet, View} from 'react-native'
import FastImage from 'react-native-fast-image'
import {TouchableOpacity} from 'react-native-gesture-handler'
import BaseScreen from '../../component/BaseScreen'
import {ScreenIds} from '../../ScreenIds'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {COLORS} from '../../assets/theme/colors'
import {
    Button,
    Input,
    Text,
    Icon as KittenIcon,
    Spinner,
} from '@ui-kitten/components'
import context from '../../../context/context'
import KeyboardScrollView from '../../component/KeyboardScrollView'
import {saveMessage} from '../../services/ServiceWorker'
import {Popup, Root} from 'popup-ui'
import {translate} from '../../../config/i18n'

const CourseQuestionScreen = ({navigation, route}) => {
    const {state} = useContext(context)
    const {alias} = route.params

    const isLoggedIn = state.isLoggedIn

    //#region Question
    const [errors, setErrors] = useState({})
    const [question, setQuestion] = useState({
        courseAlias: alias,
        message: '',
        type: 'question',
        email: '',
    })
    const [isSending, setIsSending] = useState(false)

    const handleSendQuestion = async () => {
        Keyboard.dismiss()
        setIsSending(true)

        let isError = false
        if (!isLoggedIn) {
            if (!question.email) {
                setErrors({
                    ...errors,
                    email: true,
                })
            }
        }

        if (!question.message) {
            setErrors({
                ...errors,
                message: true,
            })
        }

        if (!isError) {
            const postQuestion = await saveMessage(state.userInfo?.id, question)

            if (postQuestion && postQuestion.id) {
                showPopup()

                setErrors({})
                setQuestion({
                    ...question,
                    message: '',
                    email: '',
                })
            }
        }

        setIsSending(false)
    }

    const showPopup = () => {
        Popup.show({
            type: 'Success',
            title: 'COMPLETE',
            button: false,
            textBody: `We've recieved your question`,
            callback: () => {
                setTimeout(() => {
                    Popup.hide()
                }, 2000)
            },
        })
    }

    const SubmitIcon = props => (
        <KittenIcon {...props} name="paper-plane-outline" />
    )

    const LoadingIcon = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" />
        </View>
    )
    //#endregion

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.CourseDetail, {
                                    alias: alias,
                                })
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text category="h6">
                        {translate('course.courseQuestion')}
                    </Text>
                    <View></View>
                </View>

                <KeyboardScrollView
                    extraScrollHeight={Platform.OS === 'ios' ? 0 : 100}>
                    <View style={styles.contentWrapper}>
                        <FastImage
                            source={require('../../assets/images/question-course.png')}
                            style={styles.image}
                        />

                        <Text category="h6">
                            {translate('course.courseQuestionTitle1')}
                        </Text>
                        <Text appearance="hint" category="label">
                            {translate('course.courseQuestionTitle2')}
                        </Text>

                        <View style={styles.inputWrapper}>
                            {!isLoggedIn && (
                                <View style={{marginBottom: 15}}>
                                    <Input
                                        size="small"
                                        label="Email"
                                        textStyle={styles.mainFont}
                                        value={question.email}
                                        onChangeText={nextValue =>
                                            setQuestion({
                                                ...question,
                                                email: nextValue,
                                            })
                                        }
                                        style={styles.inputStyle}
                                        status={errors.email && 'danger'}
                                        placeholder="Input here..."
                                    />
                                </View>
                            )}

                            <Input
                                size="small"
                                textStyle={styles.mainFont}
                                label="Question"
                                value={question.message}
                                onChangeText={nextValue =>
                                    setQuestion({
                                        ...question,
                                        message: nextValue,
                                    })
                                }
                                style={styles.inputStyle}
                                status={errors.message && 'danger'}
                                placeholder="Input here..."
                                multiline
                                numberOfLines={4}
                            />
                        </View>

                        <Button
                            onPress={() =>
                                !isSending ? handleSendQuestion() : {}
                            }
                            style={{
                                width: '100%',
                            }}
                            appearance="outline"
                            accessoryLeft={
                                isSending ? LoadingIcon : SubmitIcon
                            }>
                            {translate('course.sendQuestionCap')}
                        </Button>
                    </View>
                </KeyboardScrollView>
            </View>
        </BaseScreen>
    )
}

export default CourseQuestionScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor: COLORS.WHITE,
    },
    contentWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /****** Image *******/
    image: {
        height: 180,
        width: '60%',
        marginTop: 40,
        marginBottom: 20,
    },
    /****** Input Wrapper *******/
    inputWrapper: {
        flex: 1,
        marginVertical: 30,
    },
    inputStyle: {
        width: '100%',
    },
    focusInput: {
        backgroundColor: '#FDF3EB',
    },
    blurInput: {
        backgroundColor: 'rgb(247, 249, 252)',
    },
    /******* generic ********/
    mainFont: {
        fontFamily: 'Prata-Regular',
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
})
