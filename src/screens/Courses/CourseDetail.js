import {
    Text,
    BottomNavigation,
    BottomNavigationTab,
    Icon as KittenIcon,
    Input,
    Spinner as SpinnerKitten,
    Radio,
} from '@ui-kitten/components'
import React, {useContext, useEffect, useRef, useState} from 'react'
import {Dimensions, Image, Keyboard, StyleSheet, View} from 'react-native'
import Carousel, {Pagination} from 'react-native-snap-carousel'
import {COLORS} from '../../assets/theme/colors'
import BaseScreen from '../../component/BaseScreen'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler'
import Animated, {FadeIn, FadeOut} from 'react-native-reanimated'
import CartAction from '../Cart/CartAction'
import Reviews, {handleSendComment} from '../../component/review/Reviews'
import {ScreenIds} from '../../ScreenIds'
import {
    addToCart,
    getCourseDetail,
    getCoursePrice,
    removeFavorite,
    saveFavorite,
} from '../../services/ServiceWorker'
import context from '../../../context/context'
import {
    cartActionTypes,
    COMMENT_MODES,
    COMMENT_REF_TYPE,
    getCourseOrderId,
    HTTP_STATUS_OK,
    setCourseOrderId,
} from '../../assets/constants'
import {Toast} from 'popup-ui'
import {translate} from '../../../config/i18n'
import OrderContext from '../../../context/OrderContext'
import { filterMainImageToTop } from '../../utils/ImageUtil'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'
import Spinner from 'react-native-loading-spinner-overlay'

// TODO translation Object Label
export const priceTypes = {
    generic: {label: 'Classroom and online learning', value: 'generic'},
    onsite: {label: 'Classroom and online learning', value: 'onsite'},
    online: {label: 'Online learning only', value: 'online'},
}

const w = Dimensions.get("window").width;

// TODO translation screen
const CourseDetail = ({route, navigation}) => {
    const carouselRef = useRef(null)
    const {state, actions} = useContext(context)
    const {orderState, orderActions} = useContext(OrderContext)
    const {alias} = route.params;

    //#region LOAD COURSE
    const [courseDetail, setCourseDetail] = useState({
        courseImages: [],
    })

    const [isLoaded, setIsLoaded] = useState(false)

    useEffect(async () => {
        if (alias) {
            await loadCourseDetail()
        } else {
            navigation.navigate(ScreenIds.Course)
        }
    }, [])

    const loadCourseDetail = async () => {
        const detailData = await getCourseDetail(state.version, alias, null, state.userInfo?.id)

        if (detailData && detailData.id) {
            
            setCourseDetail({...detailData, courseImages: filterMainImageToTop(detailData.courseImages)})
            setIsLoaded(true)

            renderPriceOptions(detailData.priceEnable)
            setSelectedType(getInitialType(detailData.priceEnable))

            setComment({
                ...comment,
                data: '',
                refId: detailData.id,
                refObj: COMMENT_REF_TYPE.courseDetail.value,
                pId: null,
            })

            setCommentMode({
                replyingTo: '',
                mode: COMMENT_MODES.self.value,
            })

            setIsPublisingComment(false)
        }
    }
    //#endregion

    //#region COURSE OPTIONS
    const [selectedType, setSelectedType] = useState(null)

    useEffect(async () => {
        if (isLoaded) {
            await loadCoursePrice(courseDetail.id, selectedType)
        }
    }, [selectedType])

    const loadCoursePrice = async (id, type) => {
        const priceDisplay = await getCoursePrice(id, type)

        if (priceDisplay && Object.keys(priceDisplay).length > 0) {
            setCourseDetail({
                ...courseDetail,
                coursePriceDisplay: priceDisplay ? priceDisplay : {},
            })
        }
    }

    const renderPriceOptions = priceEnable => {
        if (
            priceEnable &&
            Array.isArray(priceEnable) &&
            priceEnable.length > 0
        ) {
            if (priceEnable.includes(priceTypes.generic.value)) {
                return <Text category="label">Study online and onsite</Text>
            } else {
                if (
                    priceEnable.includes(priceTypes.onsite.value) &&
                    priceEnable.includes(priceTypes.online.value)
                ) {
                    return (
                        <View
                            style={{
                                flexDirection: 'column',
                            }}>
                            <Text category="label">Choose a form of learning</Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                }}>
                                <Radio
                                    checked={
                                        selectedType === priceTypes.onsite.value
                                    }
                                    onChange={nextValue =>
                                        setSelectedType(priceTypes.onsite.value)
                                    }>
                                    Onsite
                                </Radio>

                                <Radio
                                    checked={
                                        selectedType === priceTypes.online.value
                                    }
                                    onChange={nextValue =>
                                        setSelectedType(priceTypes.online.value)
                                    }>
                                    Online
                                </Radio>
                            </View>
                        </View>
                    )
                }
            }
        }
    }

    const getInitialType = priceEnable => {
        if (!priceEnable) {
            return null
        }

        if (priceEnable.includes(priceTypes.generic.value)) {
            return priceTypes.generic.value
        } else if (priceEnable.includes(priceTypes.onsite.value)) {
            return priceTypes.onsite.value
        } else if (priceEnable.includes(priceTypes.online.value)) {
            return priceTypes.online.value
        } else {
            return null
        }
    }

    const getOptionContainerHeight = priceEnable => {
        if (
            priceEnable &&
            Array.isArray(priceEnable) &&
            priceEnable.length > 0
        ) {
            if (
                priceEnable.includes(priceTypes.onsite.value) &&
                priceEnable.includes(priceTypes.online.value)
            ) {
                return 150
            }
        }

        return 135
    }
    //#endregion

    //#region COURSE IMAGES
    const [activeCarouselItem, setActiveCarouselItem] = useState(0)

    const renderCarouselItem = ({item}) => {
        const image = item.image
        return (
            <Image
                source={{uri: image.imageUrl}}
                style={{
                    width: 370,
                    height: 280,
                    resizeMode: 'contain',
                    borderRadius: 10,
                }}
            />
        )
    }
    //#endregion

    //#region TAB INDEX
    const [selectedIndex, setSelectedIndex] = useState(0)

    const handleSetSelectedIndex = index => {
        setSelectedIndex(index)
    }
    //#endregion

    //#region QUESTION
    const QuestionIcon = () => {
        return <Icon name="help" size={18} color={COLORS.SUB} />
    }
    //#endregion

    //#region FAVORITE
    const handleAddToFavorite = async () => {
        if (courseDetail.id) {
            let data = {
                courseId: courseDetail.id,
            }

            const favoritePost = await saveFavorite(state.userInfo?.id, data)

            if (favoritePost && favoritePost.id) {
                setCourseDetail({
                    ...courseDetail,
                    liked: true,
                    favoriteId: favoritePost.id,
                })
            }
        }
    }

    const handleRemoveFavorite = async () => {
        if (courseDetail.favoriteId) {
            const favoriteDelete = await removeFavorite(courseDetail.favoriteId)

            if (favoriteDelete === HTTP_STATUS_OK) {
                setCourseDetail({
                    ...courseDetail,
                    liked: false,
                    favoriteId: null,
                })
            }
        }
    }
    //#endregion

    //#region CART
    const onAddToCart = async () => {
        if(!state.isLoggedIn) {
            actions.showWhiteAppSpinner(true);
            navigation.navigate(ScreenIds.AuthStack, {name: ScreenIds.Login})
            actions.showWhiteAppSpinner(false);
        } else {
            actions.showAppSpinner(true)

            let courseTempList = []
            let courseOrder = {
                id: courseDetail.id,
                quantity: 1,
                type: selectedType ? selectedType : 'generic',
            }

            courseTempList.push(courseOrder)

            let data = {
                courseList: courseTempList,
            }

            if (state.isLoggedIn) {
                const courseOrderId = await getCourseOrderId()
                const cartOrder = await addToCart(
                    state.userInfo.id,
                    state.version,
                    courseOrderId,
                    data,
                ).finally(() => {
                    actions.showAppSpinner(false)
                })

                if(cartOrder && cartOrder.id) {
                    Toast.show({
                        title: translate('common.successTitle'),
                        text: 'Course added successfully',
                        color: COLORS.SUCCESS_NOTIFY,
                        timing: 3000,
                    })
                    
                    if(courseOrderId !== cartOrder.id) {
                        await setCourseOrderId(cartOrder.id);
                    }

                    orderActions.reloadCourseOrder(cartOrder.id);
                }
            }

            actions.showAppSpinner(false);
        }
    }

    //#endregion

    //#region COMMENTS
    const [isFocus, setIsFocus] = useState(false)
    const [errors, setErrors] = useState({})
    const [isPublishingComment, setIsPublisingComment] = useState(false)

    const [commentMode, setCommentMode] = useState({
        mode: COMMENT_MODES.self.value,
        replyingTo: '',
    })

    const [comment, setComment] = useState({
        data: '',
        refId: null,
        refObj: COMMENT_REF_TYPE.productDetail.value,
        displayed: true,
        pId: null,
    })

    const renderCommentSection = () => {
        return (
            <View style={isFocus ? styles.isSelfFocus : styles.isSelfBlur}>
                <Input
                    size="small"
                    textStyle={styles.mainFont}
                    value={comment.data}
                    onChangeText={nextValue =>
                        setComment({...comment, data: nextValue})
                    }
                    style={styles.commentInput}
                    status={errors.data && 'danger'}
                    placeholder="Input here..."
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    multiline
                />

                <TouchableOpacity
                    onPress={() =>
                        !isPublishingComment ? onCommentSubmit() : {}
                    }>
                    <View
                        style={[
                            styles.reviewBtnContainer,
                            isFocus ? styles.focusInput : styles.blurInput,
                        ]}>
                        {isPublishingComment ? (
                            <SpinnerKitten size="small" />
                        ) : (
                            <Icon
                                name="send"
                                size={16}
                                color={
                                    isFocus
                                        ? COLORS.SUB
                                        : COLORS.BLACK_HALF_OPACITY
                                }
                                style={{
                                    paddingHorizontal: 8,
                                    paddingVertical: 3,
                                }}
                            />
                        )}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    const handleSetReplyingTo = (pId, replyTo) => {
        if (pId && replyTo) {
            setCommentMode({
                mode: COMMENT_MODES.reply.value,
                replyingTo: replyTo,
            })

            setComment({
                data: '',
                pId: pId,
                displayed: true,
            })
        }
    }

    const onCancelReply = () => {
        setCommentMode({
            mode: COMMENT_MODES.self.value,
            replyingTo: '',
        })

        setComment({
            ...comment,
            pId: null,
        })

        if (isFocus) {
            setIsFocus(true)
        } else {
            setIsFocus(false)
        }
    }

    const onCommentSubmit = async () => {
        setIsPublisingComment(true)

        const postStatus = await handleSendComment(
            state.userInfo?.id,
            comment,
            loadCourseDetail,
            setErrors,
        )

        if (postStatus === HTTP_STATUS_OK) {
            setIsFocus(false)

            Keyboard.dismiss()
        }
    }

    //#endregion

    return (
        !isLoaded ?
            <View style={{flex: 1, backgroundColor: COLORS.WHITE}}>
                <Spinner
                    visible={true}
                    color={COLORS.SUB}
                    overlayColor={COLORS.TRANSPARENT}
                    animation="fade"
                />   
            </View>
        :
            <BaseScreen showHeader={false}>
                <View
                    style={{
                        paddingHorizontal: 20,
                        flex: 1,
                        marginTop: 10,
                        backgroundColor: COLORS.WHITE,
                    }}>
                    <View style={styles.navigationWrapper}>
                        <View>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate(ScreenIds.Course)
                                }>
                                <View
                                    style={[
                                        styles.navigationBackground,
                                        styles.navigationContainer,
                                    ]}>
                                    <Icon
                                        name="chevron-left"
                                        size={30}
                                        color={COLORS.SUB}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text category="h6">
                            {translate('course.courseDetail')}
                        </Text>
                        <View>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate(ScreenIds.CourseQuestion, {
                                        alias: alias,
                                    })
                                }>
                                <View
                                    style={[
                                        styles.navigationBackground,
                                        styles.navigationContainer,
                                    ]}>
                                    <QuestionIcon />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {isLoaded ?
                        <React.Fragment>
                            {selectedIndex === 0 && (
                                <React.Fragment>
                                    <Animated.View
                                        // entering={FadeIn}
                                        // exiting={FadeOut}
                                        style={styles.carouselContainer}>
                                        <Carousel
                                            ref={carouselRef}
                                            data={courseDetail.courseImages}
                                            renderItem={renderCarouselItem}
                                            sliderWidth={500}
                                            itemWidth={380}
                                            activeSlideOffset={10}
                                            onSnapToItem={index =>
                                                setActiveCarouselItem(index)
                                            }
                                            lockScrollWhileSnapping={true}
                                            layoutCardOffset={0}
                                            activeSlideAlignment="start"
                                            firstItem={activeCarouselItem}
                                        />
                                        <View
                                            style={{
                                                position: 'absolute',
                                                bottom: -35,
                                                alignSelf: 'center',
                                            }}>
                                            <Pagination
                                                dotsLength={
                                                    courseDetail.courseImages.length
                                                }
                                                activeDotIndex={activeCarouselItem}
                                                dotStyle={{
                                                    width: 10,
                                                    height: 10,
                                                    borderRadius: 5,
                                                    marginHorizontal: 8,
                                                    backgroundColor: 'rgba(0, 0, 0, 0.75)',
                                                }}
                                                inactiveDotOpacity={0.4}
                                                inactiveDotScale={0.6}
                                            />
                                        </View>
                                    </Animated.View>
                                </React.Fragment>
                            )}

                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    // paddingHorizontal: 10,
                                    marginBottom: 15,
                                    marginTop: selectedIndex !== 0 ? 20 : 0,
                                }}>
                                <View>
                                    <Text category="h5" multiline>{courseDetail.name}</Text>
                                    {(courseDetail.promoted ||
                                        courseDetail.new ||
                                        courseDetail.top) && (
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                            }}>
                                            {courseDetail.promoted && (
                                                <Text style={styles.badge}>
                                                    {translate('course.promoted')}
                                                </Text>
                                            )}
                                            {courseDetail.new && (
                                                <Text style={styles.badge}>
                                                    {translate('course.new')}
                                                </Text>
                                            )}
                                            {courseDetail.top && (
                                                <Text style={styles.badge}>
                                                    {translate('course.bestSeller')}
                                                </Text>
                                            )}
                                        </View>
                                    )}
                                </View>
                                <View style={{alignItems: 'flex-end', paddingTop: 5, marginLeft: -10}}>
                                    <TouchableOpacity
                                        onPress={() =>
                                            !courseDetail.liked
                                                ? handleAddToFavorite()
                                                : handleRemoveFavorite()
                                        }>
                                        <View
                                            style={[
                                                styles.favoriteContainer,
                                                courseDetail.liked
                                                    ? styles.backgroundPink
                                                    : styles.backgroundGray,
                                            ]}>
                                            <Icon
                                                name="favorite"
                                                size={18}
                                                color={
                                                    courseDetail.liked
                                                ?   COLORS.ERROR
                                                :   COLORS.BLACK
                                                }
                                            />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </React.Fragment>
                    :
                        <React.Fragment>
                            <SkeletonPlaceholder>
                                <View style={{ flexDirection: "row"}}>
                                    <View style={{ width: w - 40, height: 200, marginVertical: 10, borderRadius: 8, marginBottom: 10}} />
                                </View>
                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        marginBottom: 15,
                                        marginTop: 20,
                                    }}>
                                        <View style={{height: 40, width: w - 40, marginBottom: 5}}></View>
                                        <View style={{height: 25, width: w - 40}}></View>
                                </View>
                            </SkeletonPlaceholder>
                        </React.Fragment>
                    }

                    <View>
                        <BottomNavigation
                            selectedIndex={selectedIndex}
                            onSelect={index => handleSetSelectedIndex(index)}>
                            <BottomNavigationTab
                                title={evaProps => (
                                    <Text
                                        {...evaProps}
                                        category="label"
                                        style={
                                            selectedIndex === 0
                                                ? styles.selectedTab
                                                : styles.idleTab
                                        }>
                                        {translate('course.overviewCap')}
                                    </Text>
                                )}
                            />
                            <BottomNavigationTab
                                title={evaProps => (
                                    <Text
                                        {...evaProps}
                                        category="label"
                                        style={
                                            selectedIndex === 1
                                                ? styles.selectedTab
                                                : styles.idleTab
                                        }>
                                        {translate('course.detailCap')}
                                    </Text>
                                )}
                            />
                            <BottomNavigationTab
                                title={evaProps => (
                                    <Text
                                        {...evaProps}
                                        category="label"
                                        style={
                                            selectedIndex === 2
                                                ? styles.selectedTab
                                                : styles.idleTab
                                        }>
                                        LOCATION
                                    </Text>
                                )}
                            />
                            <BottomNavigationTab
                                title={evaProps => (
                                    <Text
                                        {...evaProps}
                                        category="label"
                                        style={
                                            selectedIndex === 3
                                                ? styles.selectedTab
                                                : styles.idleTab
                                        }>
                                        {translate('course.reviewsCap')}
                                    </Text>
                                )}
                            />
                        </BottomNavigation>
                    </View>

                    {/* OVERVIEW */}
                    {selectedIndex === 0 && isLoaded && (
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{paddingBottom: 200}}>
                            <View style={{flexGrow: 1}}>
                                <Text category="s2">
                                    {courseDetail.description}
                                </Text>
                            </View>
                        </ScrollView>
                    )}

                    {!isLoaded &&
                        <SkeletonPlaceholder>
                            <View style={{width: w-40, height: 100, marginBottom: 8}}></View>
                            <View style={{width: w-40, height: 100}}></View>
                        </SkeletonPlaceholder>
                    }

                    {/* DETAILS */}
                    {selectedIndex === 1 && (
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{paddingBottom: 200}}>
                            <View style={{flexGrow: 1}}>
                                {courseDetail.courseDescriptionContent && (
                                    <View style={styles.mb5}>
                                        <Text category="s2">
                                            {courseDetail.courseDescriptionContent}
                                        </Text>
                                    </View>
                                )}

                                {courseDetail.courseKnowledgeContent && (
                                    <View style={styles.mb5}>
                                        <Text category="s2">
                                            {courseDetail.courseKnowledgeContent}
                                        </Text>
                                    </View>
                                )}

                                {courseDetail.courseQualityContent && (
                                    <View style={styles.mb5}>
                                        <Text category="s2">
                                            {courseDetail.courseQualityContent}
                                        </Text>
                                    </View>
                                )}

                                {courseDetail.courseEquipContent && (
                                    <View style={styles.mb5}>
                                        <Text category="s2">
                                            {courseDetail.courseEquipContent}
                                        </Text>
                                    </View>
                                )}
                            </View>
                        </ScrollView>
                    )}

                    {/* LOCATION */}
                    {selectedIndex === 2 && (
                            <ScrollView
                                showsHorizontalScrollIndicator={false}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{paddingBottom: 200}}>
                                <View style={{flexGrow: 1}}>
                                    <View style={[styles.mb15, styles.locationItem]}>
                                        <Icon 
                                            name='place'
                                            color={COLORS.SUB}
                                            size={24}
                                            style={{marginRight: 10}}
                                        />
                                        <Text>{courseDetail.location ? `${courseDetail.location}` : ""}</Text>
                                    </View>
                                    <View style={[styles.mb15, styles.locationItem]}>
                                        <Icon 
                                            name='event'
                                            color={COLORS.SUB}
                                            size={24}
                                            style={{marginRight: 10}}
                                        />
                                        <Text>{courseDetail.startDate && courseDetail.endDate ? `${courseDetail.startDate} - ${courseDetail.endDate}` : ""}</Text>
                                    </View>
                                    <View style={[styles.mb15, styles.locationItem]}>
                                        <Icon 
                                            name='schedule'
                                            color={COLORS.SUB}
                                            size={24}
                                            style={{marginRight: 10}}
                                        />
                                        <Text>{courseDetail.startTime && courseDetail.endTime ? `${courseDetail.startTime} - ${courseDetail.endTime}` : ""}</Text>
                                    </View>
                                </View>
                            </ScrollView>
                        )
                    }

                    {/* REVIEWS */}
                    {selectedIndex === 3 && (
                        <View
                            style={{
                                flex: 1,
                                marginTop: 15,
                                marginBottom: 10,
                                justifyContent: 'space-between',
                            }}>
                            <Reviews
                                comments={courseDetail.courseComments}
                                setReplyingTo={handleSetReplyingTo}
                                isLoaded={isLoaded}
                                reload={loadCourseDetail}
                                refId={courseDetail?.id}
                                refObj={COMMENT_REF_TYPE.courseDetail.value}
                            />

                            {commentMode.mode === COMMENT_MODES.reply.value &&
                                commentMode.replyingTo && (
                                    <View
                                        style={[
                                            isFocus
                                                ? styles.isReplyFocus
                                                : styles.isReplyBlur,
                                        ]}>
                                        <View style={{flexDirection: 'row'}}>
                                            <TouchableOpacity
                                                onPress={() => onCancelReply()}
                                                style={{
                                                    paddingTop: 3,
                                                    marginRight: 5,
                                                    borderRadius: 20,
                                                }}>
                                                <Icon
                                                    name="highlight-off"
                                                    size={14}
                                                />
                                            </TouchableOpacity>
                                            <Text category="label">
                                                {translate('course.replyTo')}{' '}
                                                {commentMode.replyingTo}
                                            </Text>
                                        </View>

                                        {renderCommentSection()}
                                    </View>
                                )}

                            {commentMode.mode === COMMENT_MODES.self.value &&
                                renderCommentSection()}
                        </View>
                    )}

                    {/* CART */}
                    {selectedIndex !== 3 && (
                        <CartAction
                            type={cartActionTypes.courseDetail.value}
                            priceOptionContainerHeight={getOptionContainerHeight(
                                courseDetail.priceEnable,
                            )}
                            priceOptions={renderPriceOptions(
                                courseDetail.priceEnable,
                            )}
                            priceSetting={courseDetail.coursePriceDisplay}
                            currency={state.appCurrency}
                            label={'Price'}
                            actionLabel={'ADD TO BAG'}
                            onActionPress={onAddToCart}
                            actionEnabled
                            isLoaded={isLoaded}
                        />
                    )}
                </View>
            </BaseScreen>
    )
}

export default CourseDetail

const styles = StyleSheet.create({
    layoutContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    carouselContainer: {
        marginTop: 5,
        marginBottom: 5,
    },
    /******* favorite *********/
    favoriteContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundPink: {
        backgroundColor: 'rgba(245, 42, 42,0.2)',
    },
    backgroundGray: {
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
    button: {
        marginRight: 2,
    },
    /********* tab ***********/
    selectedTab: {
        fontWeight: '700',
        color: '#eaa196',
    },
    idleTab: {
        fontWeight: '700',
        color: COLORS.GRAY_BD,
    },
    /********* generic *********/
    mb5: {
        marginBottom: 5,
    },
    mb15: {
        marginBottom: 15
    },
    badge: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#FDF3EB',
        color: '#eaa196',
        marginRight: 2,
        borderRadius: 20,
        fontSize: 11,
    },
    mainFont: {
        fontFamily: 'Prata-Regular',
    },
    /******* Comment *********/
    isSelfFocus: {
        flex: 1,
        flexDirection: 'row',
    },
    isSelfBlur: {
        flexDirection: 'row',
        paddingVertical: 0,
        marginBottom: 10,
    },
    isReplyFocus: {
        flex: 1,
        flexDirection: 'column',
    },
    isReplyBlur: {
        flexDirection: 'column',
    },
    commentInput: {
        width: '90%',
        marginRight: 7,
    },
    reviewBtnContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    focusInput: {
        backgroundColor: '#FDF3EB',
    },
    blurInput: {
        backgroundColor: 'rgb(247, 249, 252)',
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    // Location
    locationItem: {
        flexDirection: "row"
    }
})
