import React, {useContext, useEffect, useRef, useState} from 'react'
import {View, StyleSheet, TouchableOpacity, ImageBackground, Dimensions, FlatList} from 'react-native'
import BaseScreen from '../../component/BaseScreen'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {
    Text,
    Menu,
    MenuItem,
    Icon as KittenIcon,
    IndexPath,
    BottomNavigation,
    BottomNavigationTab,
    Button,
    List,
} from '@ui-kitten/components'
import {COLORS} from '../../assets/theme/colors'
import {SearchBar} from 'react-native-elements'
import context from '../../../context/context'
import {getCourseList, removeFavorite, removeItem, saveFavorite, searchCourses, updateItemQuantity} from '../../services/ServiceWorker'
import {translate} from '../../../config/i18n'
import {ScreenWidth} from 'react-native-elements/dist/helpers'
import {LogBox} from 'react-native'
import CartIcon from '../Cart/CartIcon'
import FastImage from 'react-native-fast-image'
import { ScreenIds } from '../../ScreenIds'
import { cartActionTypes, LANGUAGE_CODES, numberWithCommas, orderTypes } from '../../assets/constants'
import { Modalize } from 'react-native-modalize'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'
import { getCoursePriceTypeName } from './CourseUtils'
import OrderContext from '../../../context/OrderContext'
import CartAction from '../Cart/CartAction';
import {SKELETON_TYPE} from '../../component/SkeletonView'
import LoadingSkeleton from '../../component/skeleton/LoadingSkeleton'
import renderPrice from '../../component/money/PriceUtils'
import { useIsFocused } from '@react-navigation/native'
import { commonStyles } from '../../assets/theme/styles'

LogBox.ignoreLogs(['VirtualizedLists should never be nested inside plain ScrollViews with the same orientation because it can break windowing and other functionality - use another VirtualizedList-backed container instead.']);

const tabBarWidth = ScreenWidth / 2

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const gridWidth = Dimensions.get('window').width / 2 - 32

const TABS = [
    {
        key: 'all',
        title: translate('common.all'),
    },
    {
        key: 'selected',
        title: translate('common.selected'),
    },
    {
        key: 'own',
        title: translate('common.own'),
    },
]

const viewModes = {
    grid: {label: 'Grid View', value: 0},
    list: {label: 'List View', value: 1},
}

const courseSelectionTypes = {
    all: {label: 'Xem tất cả', value: 'all'},
    selected: {label: 'Xem đã chọn', value: 'selected'},
}

const initialPageInfo = {
    pageNumber: 0,
    totalRecords: 0,
    rows: 6,
    totalPages: 0,
}

const courseModes = {
    viewAll: {label: 'View All Courses', value: 'viewAll'},
    search: {label: 'Search Courses', value: 'search'},
}

// TODO translation
const CourseScreen = ({navigation}) => {
    const {state, actions} = useContext(context)
    const {orderState, orderActions} = useContext(OrderContext)

    const isFocused = useIsFocused();

    useEffect(async() => {
        if(!isCourseLoaded && isFocused) {
            await loadCourseList(initialPageInfo)
        }
    }, [isFocused]);

    //#region TAB BAR
    const [selectedIndex, setSelectedIndex] = useState(0)
    //#endregion

    //#region VIEW MODE
    const GridIcon = props => <KittenIcon {...props} name="grid-outline" />

    const ListIcon = props => <KittenIcon {...props} name="list-outline" />

    const [selectedViewMode, setSelectedViewMode] = useState(
        new IndexPath(viewModes.list.value),
    )

    const modalRef = useRef(null)
    //#endregion

    //#region SEARCH
    const [searchValue, setSearchValue] = useState('')
    const [isSearching, setIsSearching] = useState(false)
    const [searchIndex, setSearchIndex] = useState(0)
    const [currentCourseMode, setCurrentCourseMode] = useState(
        courseModes.viewAll.value,
    )

    const handleSearchCourses = async searchValue => {
        if (!isSearching) {
            const filter = {
                keywords: searchValue,
            }

            const searchedCourses = await searchCourses(
                state.version,
                filter,
                state.userInfo?.id,
            )
            if (searchedCourses && Array.isArray(searchedCourses)) {
                setCourseList({
                    data: searchedCourses,
                    ...initialPageInfo,
                })
                setIsSearching(false)
                setSearchIndex(searchIndex + 1)
            }
        }
    }

    const handleSearch = async value => {
        setSearchValue(value)
        setIsSearching(true)
        setCurrentCourseMode(courseModes.search.value)

        if (value) {
            setTimeout(async () => {
                await handleSearchCourses(value)
            }, 500)
        } else {
            setCurrentCourseMode(courseModes.viewAll.value)
            setIsSearching(false)

            await loadCourseList(initialPageInfo)
        }
    }

    const handleClearSearch = async () => {
        setSearchValue('')
        setIsSearching(false)
        setSearchIndex(0)
        setCurrentCourseMode(courseModes.viewAll.value)

        await loadCourseList(initialPageInfo)
    }
    //#endregion

    //#region ALL COURSES
    const [courseList, setCourseList] = useState({
        data: [],
        ...initialPageInfo,
    })

    const [isCourseLoaded, setIsCourseLoaded] = useState(false)

    const loadCourseList = async pageInfo => {
        const {pageNumber, rows} = pageInfo ?? {}
        const courseData = await getCourseList(state.version, pageNumber, rows, state.userInfo?.id)

        if (courseData && courseData.content) {
            setCourseList({
                data: courseData.content,
                pageNumber:
                    courseData.pageable && courseData.pageable.pageNumber,
                rows: courseData.pageable && courseData.pageable.pageSize,
                totalRecords: courseData.totalElements,
                totalPages: courseData.totalPages,
            })            
        } else {
            setCourseList({
                ...courseList,
                data: [],
            })
        }
        setIsCourseLoaded(true)
    }

    const fetchMoreCourses = async () => {
        if (courseList.pageNumber + 1 >= courseList.totalPages) {
            return
        }

        const courseData = await getCourseList(
            state.version,
            courseList.pageNumber + 1,
            courseList.rows,
            state.userInfo?.id
        )
        if (courseData && courseData.content) {
            setCourseList({
                data: courseList.data.concat(courseData.content),
                pageNumber:
                    courseData.pageable && courseData.pageable.pageNumber,
                rows: courseData.pageable && courseData.pageable.pageSize,
                totalRecords: courseData.totalElements,
                totalPages: courseData.totalPages,
            })
        }
    }
    //#endregion

    //#region SELECTED COURSES
    const handleRemoveItem = async (orderId, orderDetailId) => {
        let data = {
            id: orderDetailId,
        }
        const order = await removeItem(state.userInfo?.id, orderId, data)

        if(order && order.id) {
            await orderActions.reloadCourseOrder(order.id)
        }
    }

    const handleSubtractQuantity = async(orderId, orderDetailId, quantity) => {
        let updateQuantity = quantity - 1;
        let data = {
            id: orderDetailId,
            quantity: updateQuantity
        }

        const orderUpdate = await updateItemQuantity(state.userInfo?.id, orderId, data);
        if(orderUpdate && orderUpdate.id) {
            await orderActions.reloadCourseOrder(orderUpdate.id)
        }
    }

    const handlePlusQuantity = async(orderId, orderDetailId, quantity) => {
        let updateQuantity = quantity + 1;
        let data = {
            id: orderDetailId,
            quantity: updateQuantity
        }

        const orderUpdate = await updateItemQuantity(state.userInfo?.id, orderId, data);
        if(orderUpdate && orderUpdate.id) {
            await orderActions.reloadCourseOrder(orderUpdate.id)
        }
    }
    //#endregion

    //#region FAVORITE
    const handleAddToFavorite = async (courseId) => {
        if (courseId) {
            let data = {
                courseId: courseId,
            }

            const favoritePost = await saveFavorite(state.userInfo?.id, data)

            if (favoritePost && favoritePost.id) {
                let favCourses = [];

                for(let i = 0; i < courseList.data.length; i++) {
                    if(courseList.data[i].id === courseId) {
                        courseList.data[i].liked = true;
                        courseList.data[i].favoriteId = favoritePost.id
                    }

                    favCourses.push(courseList.data[i])
                }

                setCourseList({
                    ...courseList,
                    data: favCourses
                })
            }
        }
    }

    const handleRemoveFavorite = async (favoriteId, courseId) => {
        if (favoriteId) {
            const favoriteDelete = await removeFavorite(favoriteId)

            if (favoriteDelete === HTTP_STATUS_OK) {
                let favCourses = [];
                let currentCourses = courseList.data;

                for(let i = 0; i < currentCourses.length; i++) {
                    if(currentCourses[i].id === courseId) {
                        currentCourses[i].liked = false;
                        currentCourses[i].favoriteId = null
                    }

                    favCourses.push(currentCourses[i])
                }

                setCourseList({
                    ...courseList,
                    data: favCourses
                })
            }
        }
    }
    //#endregion

    const renderCourseListHeader = () => {
        return (
            <React.Fragment>
                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate(ScreenIds.Home)}>
                        <FastImage
                            source={require('../../assets/images/pp-logo.png')}
                            style={{
                                width: 35,
                                height: 35,
                                alignSelf: 'center',
                                backgroundColor: COLORS.WHITE,
                                borderRadius: 50,
                                marginLeft: 15,
                                marginTop: 5,
                            }}
                        />
                    </TouchableOpacity>
                    <View style={styles.logoContainer}>
                        <Text
                            category="h4"
                            style={{
                                color: COLORS.WHITE,
                            }}>
                            {translate('course.COURSES')}
                        </Text>
                    </View>
                    <CartIcon
                        style={{marginRight: 15, marginTop: 5}}
                        navigation={navigation}
                    />
                </View>

                <View style={styles.searchContainer}>
                    <SearchBar
                        placeholder="Type here..."
                        onChangeText={handleSearch}
                        value={searchValue}
                        disabled={selectedIndex === 1}
                        round
                        lightTheme
                        showLoading={isSearching}
                        onClear={handleClearSearch}
                        inputStyle={styles.searchBar}
                        containerStyle={styles.searchBarContainer}
                        inputContainerStyle={styles.searchBarInputContainer}
                    />
                    <View style={styles.viewMode}>
                        <TouchableOpacity
                            onPress={() => modalRef.current?.open()}>
                            <Icon
                                name="visibility"
                                size={20}
                                color={COLORS.BLACK}
                            />
                        </TouchableOpacity>
                    </View>
                </View>

                <View>
                    <BottomNavigation
                        selectedIndex={selectedIndex}
                        onSelect={index => setSelectedIndex(index)}>
                        <BottomNavigationTab
                            title={renderTabLabel(
                                courseSelectionTypes.all.value,
                            )}
                        />
                        <BottomNavigationTab
                            title={renderTabLabel(
                                courseSelectionTypes.selected.value,
                            )}
                        />
                    </BottomNavigation>
                </View>
            </React.Fragment>
        )
    }

    const renderTabLabel = type => {
        if (type === courseSelectionTypes.all.value) {
            return (
                <View>
                    <Text category="label" style={{color: COLORS.SUB}}>
                        {translate('course.allCap')}
                    </Text>
                </View>
            )
        } else {
            return (
                <View style={{flexDirection: 'row'}}>
                    <View>
                        <Text
                            category="label"
                            style={{marginRight: 20, color: COLORS.SUB}}>
                            {translate('course.selectedCap')}
                        </Text>
                    </View>
                    <View style={styles.badgeWrapper}>
                        <View style={[styles.badge]}>
                            <Text style={styles.numOfItems}>{orderState.selectedCourses.length > 0 ? orderState.selectedCourses.length : 0}</Text>
                        </View>
                    </View>
                </View>
            )
        }
    }

    const renderCourseList = allCourses => {
        const colNumbers =
            selectedViewMode.row === viewModes.grid.value
                ? viewModes.grid.value + 2
                : viewModes.list.value

        const renderKey =
            currentCourseMode === courseModes.viewAll.value
                ? colNumbers > 1
                    ? '#'
                    : '-'
                : colNumbers > 1
                ? `${searchValue}#${searchIndex}`
                : `${searchValue}-${searchIndex}`

        return (
            <FlatList
                key={renderKey}
                showsVerticalScrollIndicator={false}
                horizontal={false}
                contentContainerStyle={[
                    {
                        flexGrow: 1,
                        backgroundColor: COLORS.WHITE,
                        paddingBottom: 100,
                    },
                ]}
                numColumns={colNumbers}
                data={allCourses}
                extraData={searchIndex}
                renderItem={item =>
                    renderCourseItem(item, colNumbers, navigation)
                }
                keyExtractor={item => {
                    return `${
                        colNumbers > 1
                            ? `grid-${item.id}`
                            : colNumbers === 1
                            ? `list-${item.id}`
                            : `search/${searchIndex}`
                    }`
                }}
                onEndReached={
                    currentCourseMode === courseModes.viewAll.value &&
                    fetchMoreCourses
                }
                onEndReachedThreshold={0.7}
            />
        )
    }

    const renderSelectedCourseList = (selectedCourses) => {
        return(
            <List
                style={{flex: 1, backgroundColor: COLORS.WHITE}}
                contentContainerStyle={{
                    paddingBottom: 250
                }}
                data={selectedCourses}
                renderItem={(item) => {
                    return renderSelectedCourseItem(item, navigation)
                }}
                keyExtractor={(item) => item.orderDetailId}
            />
        )
    }

    const renderCourseItem = ({item}, colNumbers, navigation) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() =>
                    navigation.navigate(ScreenIds.CourseDetail, {
                        alias: item.alias,
                    })
                }>
                <View
                    style={
                        colNumbers && colNumbers > 1
                            ? styles.card
                            : styles.cardExpand
                    }>
                    <FastImage
                        source={{uri: item.mainImage}}
                        style={styles.cardImage}
                    />
                    <Text style={styles.title} numberOfLines={2}>
                        {item.name}
                    </Text>
                    <View style={styles.priceContainer}>
                        <Text appearance="hint" style={styles.price} category="label">
                        {state.version === LANGUAGE_CODES.en.value && `${state.appCurrency} `}{renderPrice(item.coursePriceDisplay)}{state.version === LANGUAGE_CODES.vi.value && ` ${state.appCurrency}`}
                        </Text>
                        <TouchableOpacity onPress={() => item.liked ? handleRemoveFavorite(item.favoriteId, item.id) : handleAddToFavorite(item.id)}>
                            <View style={{alignItems: 'flex-end'}}>
                                <View
                                    style={[
                                        styles.favoriteContainer,
                                        item.liked
                                            ? styles.backgroundPink
                                            : styles.backgroundGray,
                                    ]}>
                                    <Icon
                                        name="favorite"
                                        size={16}
                                        color={
                                            item.liked
                                                ? COLORS.ERROR
                                                : COLORS.BLACK
                                        }
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    const renderSelectedCourseItem = ({item, key}, navigation) => {
        return (
            <View
                style={{
                    paddingVertical: 5,
                    marginBottom: 10
                }}
            >
            <View style={styles.orderItemWrapper} key={key}>
                <FastImage
                    source={{uri: item.mainImage}}
                    style={styles.orderItemImage}
                />

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginVertical: 10,
                        width: '70%',
                    }}>
                    <View
                        style={{
                            flexDirection: 'column',
                            justifyContent: 'space-between'
                        }}
                    >
                        <Text category="p1" numberOfLines={1}>
                            {item.name}
                        </Text>
                        <Text category="p2" appearance="hint">
                            {numberWithCommas(item.price)} {state.appCurrency}
                        </Text>
                        <Text category="label">
                            {item.type && getCoursePriceTypeName(item.type)}
                        </Text>
                    </View>
                </View>

                <View
                    style={{
                        position: 'absolute',
                        bottom: 10,
                        right: 10,
                        zIndex: 10
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}
                    >
                        <TouchableWithoutFeedback
                            disabled={item.quantity === 1}
                            onPress={() => handleSubtractQuantity(item.orderId, item.orderDetailId, item.quantity)}
                        >
                            <Icon name="remove" color={item.quantity === 1 ? COLORS.GRAY_BD : COLORS.SUB}/>
                        </TouchableWithoutFeedback>

                        <Text style={{marginHorizontal: 8}}>{item.quantity}</Text>

                        <TouchableWithoutFeedback
                            onPress={() => handlePlusQuantity(item.orderId, item.orderDetailId, item.quantity)}
                        >
                            <Icon name="add" size={18} color={COLORS.SUB}/>
                        </TouchableWithoutFeedback>
                    </View>
                </View>

                <View
                    style={{
                        position: 'absolute',
                        top: 10,
                        right: 10,
                        zIndex: 10
                    }}
                >
                    <TouchableOpacity
                        style={[
                            styles.favoriteContainer,
                            styles.backgroundGray,
                        ]}
                        onPress={() =>
                            handleRemoveItem(item.orderId, item.orderDetailId)
                        }>
                        <Icon
                            name="delete-forever"
                            size={18}
                            color={COLORS.BLACK}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            </View>
        )
    }

    const renderEmptySelectedCourses = () => {
        return (
            <View
                style={{
                    marginTop: 15,
                    alignItems: 'center',
                    backgroundColor: COLORS.WHITE,
                }}>
                <FastImage
                    source={require('../../assets/images/empty.png')}
                    style={{
                        height: 170,
                        width: 170,
                    }}
                />
                <Text appearance="hint">
                    Look like you have picked any courses yet
                </Text>

                <Button
                    style={{
                        marginVertical: 20,
                    }}
                    status={'danger'}
                    onPress={() => setSelectedIndex(0)}
                >
                    BROWSE COURSES
                </Button>
            </View>
        )
    }

    const renderEmptyCourseList = () => {
        return (
            <View
                style={{
                    marginTop: 15,
                    alignItems: 'center',
                    backgroundColor: COLORS.WHITE,
                }}>
                <FastImage
                    source={require('../../assets/images/no-courses.png')}
                    style={{
                        height: 170,
                        width: 170,
                    }}
                />
                <Text
                    appearance="hint"
                    style={{
                        textAlign: 'center',
                    }}>
                    {translate('course.searchEmptyMsg')}
                </Text>
            </View>
        )
    }

    return (
        <BaseScreen
            showHeader={false}
            onBackPress={() => {}}
        >
            <View
                style={{flex: 1, backgroundColor: COLORS.WHITE}}
            >
                <View
                    style={{
                        flex: 1,
                    }}>
                    <ImageBackground
                        source={require('../../assets/images/lesson-bg-copy.png')}
                        style={{
                            height: height,
                            backgroundColor: COLORS.WHITE,
                        }}>
                        {renderCourseListHeader()}
                        
                        {selectedIndex === 0 &&
                            <React.Fragment>
                                {!isCourseLoaded &&
                                    <LoadingSkeleton 
                                        listType={
                                            selectedViewMode.row === viewModes.grid.value
                                                ? SKELETON_TYPE.card
                                                : SKELETON_TYPE.cardExpand
                                            }
                                        length={4}    
                                    />}

                                {isCourseLoaded && courseList.data &&
                                    courseList.data.length > 0 &&
                                    renderCourseList(courseList.data)}

                                {isCourseLoaded &&
                                    courseList.data &&
                                    courseList.data.length < 1 &&
                                    currentCourseMode ===
                                        courseModes.search.value &&
                                    renderEmptyCourseList()}
                            </React.Fragment>
                        }

                        {selectedIndex === 1 && (
                            <React.Fragment>
                                {!state.isLoggedIn &&
                                    <React.Fragment>
                                        <View style={{flexDirection: "row", justifyContent: "center", alignItems: "center", backgroundColor: COLORS.WHITE}}>
                                            <View style={{height: 240}}>
                                                <View style={{padding: 20}}>
                                                    <FastImage 
                                                        source={require("../../assets/images/authenticate-required.png")}
                                                        style={{height: 160}}
                                                    />
                                                </View>
                                                <Text style={{marginBottom: 20}}>You need to login to see your registration</Text>
                                            </View>
                                        </View>
                                        <Button
                                            onPress={() => navigation.navigate(ScreenIds.AuthStack, {screen: ScreenIds.Login})}
                                            size={"small"}
                                            style={{marginHorizontal: "20%"}}
                                        >
                                            {props => (
                                                <Text
                                                    {...props}
                                                    style={[commonStyles.mainFont, commonStyles.lightColor]}
                                                >
                                                    LOGIN NOW
                                                </Text>
                                            )}
                                        </Button>
                                    </React.Fragment>
                                }

                                {orderState.isSelectedLoaded &&
                                    orderState.selectedCourses.length < 1 &&
                                    renderEmptySelectedCourses()}

                                {orderState.isSelectedLoaded && orderState.selectedCourses.length > 0 &&
                                    renderSelectedCourseList(orderState.selectedCourses)
                                }
                            </React.Fragment>
                        )}          
                    </ImageBackground>
                </View>

                
            </View>

            {orderState.isSelectedLoaded && orderState.selectedCourses.length > 0 && selectedIndex === 1 &&
                <CartAction
                    isPadding
                    type={cartActionTypes.course.value}
                    disabled={orderState.selectedCourses.length < 1}
                    currency={state.appCurrency}
                    actionLabel={'PROCEED TO CHECKOUT'}
                    onActionPress={() => navigation.navigate(ScreenIds.OrderBillingScreen, {orderType: orderTypes.course.value})}
                />
            }

            <Modalize
                ref={modalRef}
                handleStyle={styles.handleModalStyle}
                modalStyle={{
                    borderTopLeftRadius: 30,
                    borderTopRightRadius: 30,
                }}
                snapPoint={270}
                onBackButtonPress={() => modalInfoRef.current?.close()}
                scrollViewProps={{showsVerticalScrollIndicator: false}}>
                <View style={{paddingBottom: 100}}>
                    <View
                        style={{
                            paddingHorizontal: 20,
                            paddingBottom: 10,
                            paddingTop: 30,
                        }}>
                        <Text category="h5">
                            {translate('common.selectViewMode')}
                        </Text>
                    </View>
                    <Menu
                        style={styles.menuContainer}
                        selectedIndex={selectedViewMode}
                        onSelect={index => setSelectedViewMode(index)}>
                        <MenuItem title="Grid View" accessoryLeft={GridIcon} />
                        <MenuItem title="List View" accessoryLeft={ListIcon} />
                    </Menu>
                </View>
            </Modalize>
        </BaseScreen>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
    container: {
        flex: 1,
        marginBottom: 20,
        backgroundColor: COLORS.WHITE,
    },
    /********* Logo **********/
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 20,
    },
    logoContainer: {
        height: 40,
        paddingHorizontal: 20,
    },
    logo: {
        width: '100%',
        height: 50,
        maxHeight: 50,
    },
    /******** Heading *********/
    headingContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
        paddingHorizontal: 20,
    },
    heading: {
        textAlign: 'center',
    },
    /******** Tab Bar ********/
    tabBarContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    tabItemEnabled: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 8,
        width: tabBarWidth,
        borderRadius: 5,
        backgroundColor: COLORS.MAIN,
    },
    tabTitleEnabled: {
        fontSize: 13,
        color: COLORS.SUB,
        fontWeight: '700',
    },
    tabTitleDisabled: {
        fontSize: 13,
        color: COLORS.GRAY_9A,
    },
    tabItemButton: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 8,
        borderRadius: 5,
        width: tabBarWidth,
    },
    /******** Shopping Bag **********/
    shoppingBag: {
        marginRight: 15,
        marginTop: 5,
    },
    /********* Search And Filter **********/
    searchContainer: {
        flexDirection: 'row',
        paddingHorizontal: 16,
        marginBottom: 20,
    },
    searchBarInputContainer: {
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        height: 45,
    },
    searchBarContainer: {
        width: '85%',
        padding: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
    },
    searchBar: {
        height: 50,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    /********* View Mode **********/
    menuContainer: {
        marginTop: 5,
        paddingHorizontal: 20,
        backgroundColor: COLORS.TRANSPARENT,
    },
    viewMode: {
        marginLeft: 10,
        height: 45,
        width: 50,
        borderRadius: 10,
        backgroundColor: COLORS.LIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },
    /******** generic *********/
    pH20: {
        paddingHorizontal: 20,
    },
    textBold: {
        fontWeight: '700',
    },
    icon: {
        width: 25,
        height: 25,
    },
    mr2: {
        marginRight: 2,
    },
    /******** List Course ********/
    listContainer: {
        flex: 1,
        paddingBottom: 80,
        paddingHorizontal: 16,
    },
    /******** card **************/
    card: {
        height: 225,
        backgroundColor: COLORS.LIGHT,
        width: gridWidth + 5,
        marginLeft: 14,
        marginRight: 10,
        borderRadius: 10,
        marginBottom: 20,
        padding: 15,
    },
    cardExpand: {
        width: '90%',
        marginBottom: 20,
        marginHorizontal: 20,
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        padding: 15,
    },
    cardImage: {
        flex: 1,
        height: 180,
        resizeMode: 'contain',
        paddingVertical: 5,
        marginTop: 3,
        borderRadius: 10,
    },
    /******** card components **************/
    title: {
        fontSize: 14,
        marginTop: 10,
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 1,
    },
    price: {
        paddingTop: 5,
    },
    salePrice: {
        textDecorationLine: 'line-through',
    },
    /******* favorite *********/
    favoriteContainer: {
        width: 26,
        height: 26,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundPink: {
        backgroundColor: 'rgba(245, 42, 42,0.2)',
    },
    backgroundGray: {
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
    /********* Modal *********/
    handleModalStyle: {
        marginTop: 30,
        backgroundColor: COLORS.SUB,
        width: 80,
    },
    /******* Items Badge ********/
    badgeWrapper: {
        position: 'absolute',
        right: -5,
        bottom: 2,
    },
    badge: {
        backgroundColor: COLORS.ERROR,
        height: 16,
        width: 16,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    numOfItems: {
        color: 'white',
        fontSize: 10,
    },
    /****** Order Item *********/
    orderItemWrapper: {
        flexDirection: 'row',
        height: 100,
        backgroundColor: COLORS.WHITE,
        borderRadius: 10,
        marginHorizontal: 15,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    orderItemImage: {
        width: 80,
        height: 80,
        borderRadius: 10,
        margin: 10,
    },
})

export default CourseScreen
