import React from 'react';
import { translate } from "../../../config/i18n";
import { priceTypes } from "./CourseDetail";


export const getCoursePriceTypeName = (type) => {
    if(!type) {
        return null;
    }

    let priceTypeName = "";

    switch(type) {
        case priceTypes.generic.value:
            priceTypeName = translate('course.priceType.generic')
            break;
        case priceTypes.online.value:
            priceTypeName = translate('course.priceType.online')
            break;
        case priceTypes.onsite.value:
            priceTypeName = translate('course.priceType.onsite')
            break;
    }

    return priceTypeName;
}