import { Text } from '@ui-kitten/components'
import React, { useContext } from 'react'
import {View} from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import BaseScreen from '../../../component/BaseScreen'
import { ScreenIds } from '../../../ScreenIds'
import {styles} from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../../assets/theme/colors'
import { translate } from '../../../../config/i18n'
import context from '../../../../context/context'
import { LANGUAGE_CODES } from '../../../assets/constants'

const Disclaimer = ({navigation}) => {
    const {state} = useContext(context);

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View style={{marginRight: 20}}>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.Account)
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text category="h6">
                        {state.version === LANGUAGE_CODES.vi.value ? 'Tuyên bố từ chối trách nhiệm' : 'Disclaimer'}
                    </Text>
                </View>
                
                {state.version === LANGUAGE_CODES.vi.value ?
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 25
                        }}
                    >
                        <Text category={"p2"} style={styles.mb5}>Cập nhật lúc 2022-01-01</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy bằng cách này cấp cho bạn quyền truy cập vào https://ppbeautyacademy.com (“Trang web”) và mời bạn mua các dịch vụ được cung cấp tại đây.</Text>

                        <Text category={"h5"} style={styles.mb5}>Định nghĩa và thuật ngữ chính</Text>
                        <Text category={"p2"} style={styles.mb5}>Để giúp giải thích mọi thứ rõ ràng nhất có thể trong Tuyên bố từ chối trách nhiệm này, mỗi khi bất kỳ điều khoản nào trong số này được tham chiếu, đều được xác định rõ ràng là:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Cookie: một lượng nhỏ dữ liệu được tạo bởi một trang web và được lưu bởi trình duyệt web của bạn. Nó được sử dụng để xác định trình duyệt của bạn, cung cấp phân tích, ghi nhớ thông tin về bạn như tùy chọn ngôn ngữ hoặc thông tin đăng nhập của bạn.</Text>
                            <Text category={"p2"} style={styles.mb5}>Công ty: khi chính sách này đề cập đến “Công ty”, “chúng tôi”, “chúng tôi” hoặc “của chúng tôi”, nghĩa là PP Beauty &amp; Academy, (306/25 Nguyễn Thị Minh Khai, P.5, Q.3, TPHCM), chịu trách nhiệm về thông tin của bạn theo Tuyên bố từ chối trách nhiệm này.</Text>
                            <Text category={"p2"} style={styles.mb5}>Dịch vụ: đề cập đến dịch vụ do PP Beauty &amp; Academy cung cấp như được mô tả trong các điều khoản tương đối (nếu có) và trên nền tảng này.</Text>
                            <Text category={"p2"} style={styles.mb5}>Trang web: Trang web của PP Beauty &amp; Academy, có thể được truy cập qua URL này: https://ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Bạn: cá nhân hoặc tổ chức đã đăng ký với PP Beauty &amp; Academy để sử dụng Dịch vụ.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Trách nhiệm hữu hạn</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy cố gắng cập nhật và / hoặc bổ sung nội dung của trang web / ứng dụng một cách thường xuyên. Bất chấp sự quan tâm và chú ý của chúng tôi, nội dung có thể không đầy đủ và / hoặc không chính xác.</Text>
                        <Text category={"p2"} style={styles.mb5}>Các tài liệu được cung cấp trên trang web / ứng dụng được cung cấp mà không có bất kỳ hình thức đảm bảo hoặc yêu cầu nào về tính đúng đắn của chúng. Những chất liệu này có thể được thay đổi bất cứ lúc nào mà không cần thông báo trước từ PP Beauty &amp; Academy.</Text>
                        <Text category={"p2"} style={styles.mb5}>Đặc biệt, tất cả các mức giá trên trang web / ứng dụng được nêu có thể bị lỗi về đánh máy và lập trình. Không có trách nhiệm pháp lý nào được xác nhận đối với hậu quả của những sai sót như vậy. Không có thỏa thuận nào được ký kết trên cơ sở những sai sót như vậy.</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy sẽ không chịu bất kỳ trách nhiệm pháp lý nào đối với các siêu liên kết đến các trang web hoặc dịch vụ của các bên thứ ba có trên trang web / ứng dụng. Từ trang web / ứng dụng của chúng tôi, bạn có thể truy cập các trang web khác bằng cách theo các siêu liên kết đến các trang web bên ngoài đó. Mặc dù chúng tôi cố gắng chỉ cung cấp các liên kết chất lượng đến các trang web hữu ích và có đạo đức, nhưng chúng tôi không kiểm soát được nội dung và bản chất của các trang này. Những liên kết đến các trang web khác không ngụ ý đề xuất cho tất cả nội dung được tìm thấy trên các trang web này. Chủ sở hữu trang web và nội dung có thể thay đổi mà không cần thông báo và có thể xảy ra trước khi chúng tôi có cơ hội xóa một liên kết có thể đã trở thành 'xấu'.</Text>
                        <Text category={"p2"} style={styles.mb5}>Cũng xin lưu ý rằng khi bạn rời khỏi trang web / ứng dụng của chúng tôi, các trang web khác có thể có các điều khoản và chính sách bảo mật khác ngoài tầm kiểm soát của chúng tôi. Hãy đảm bảo kiểm tra Chính sách Bảo mật của các trang web này cũng như "Điều khoản Dịch vụ" của chúng trước khi tham gia vào bất kỳ hoạt động kinh doanh nào hoặc tải lên bất kỳ thông tin nào.</Text>

                        <Text category={"h5"} style={styles.mb5}>Liên kết đến các trang web khác </Text>
                        <Text category={"p2"} style={styles.mb5}>Tuyên bố từ chối trách nhiệm này chỉ áp dụng cho các Dịch vụ. Các Dịch vụ có thể chứa các liên kết đến các trang web khác không do PP Beauty &amp; Academy điều hành hoặc kiểm soát. Chúng tôi không chịu trách nhiệm về nội dung, tính chính xác hoặc ý kiến ​​được thể hiện trong các trang web đó, và các trang web đó không được chúng tôi điều tra, giám sát hoặc kiểm tra tính chính xác hoặc hoàn chỉnh. Hãy nhớ rằng khi bạn sử dụng một liên kết để đi từ Dịch vụ đến một trang web khác, Chính sách Bảo mật của chúng tôi sẽ không còn hiệu lực. Việc duyệt và tương tác của bạn trên bất kỳ trang web nào khác, bao gồm cả những trang web có liên kết trên nền tảng của chúng tôi, phải tuân theo các quy tắc và chính sách riêng của trang web đó. Các bên thứ ba như vậy có thể sử dụng cookie của riêng họ hoặc các phương pháp khác để thu thập thông tin về bạn. Nếu Bạn nhấp vào liên kết của bên thứ ba, Bạn sẽ được dẫn đến trang web của bên thứ ba đó. Chúng tôi đặc biệt khuyên Bạn nên xem lại Chính sách Bảo mật và Điều khoản của mọi trang web Bạn truy cập.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi không kiểm soát và không chịu trách nhiệm về nội dung, chính sách bảo mật hoặc thông lệ của bất kỳ trang web hoặc dịch vụ nào của bên thứ ba.</Text>

                        <Text category={"h5"} style={styles.mb5}>Tuyên bố từ chối trách nhiệm về lỗi và thiếu sót</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy không chịu trách nhiệm về bất kỳ nội dung, mã hoặc bất kỳ sự thiếu chính xác nào khác.</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy không cung cấp bảo hành hoặc bảo đảm.</Text>
                        <Text category={"p2"} style={styles.mb5}>Trong mọi trường hợp, PP Beauty &amp; Academy sẽ không chịu trách nhiệm pháp lý đối với bất kỳ thiệt hại đặc biệt, trực tiếp, gián tiếp, do hậu quả, hoặc ngẫu nhiên hoặc bất kỳ thiệt hại nào, cho dù là trong một hành động hợp đồng, sơ suất hoặc sai phạm khác, phát sinh từ hoặc liên quan đến việc sử dụng Dịch vụ hoặc nội dung của Dịch vụ. PP Beauty &amp; Academy có quyền bổ sung, xóa hoặc sửa đổi nội dung trên Dịch vụ bất kỳ lúc nào mà không cần thông báo trước.</Text>

                        <Text category={"h5"} style={styles.mb5}>Tuyên bố từ chối trách nhiệm chung</Text>
                        <Text category={"p2"} style={styles.mb5}>Dịch vụ PP Beauty &amp; Academy và các nội dung của nó được cung cấp "nguyên trạng" và "sẵn có" mà không có bất kỳ bảo hành hoặc tuyên bố nào dưới bất kỳ hình thức nào, dù rõ ràng hay ngụ ý. PP Beauty &amp; Academy là nhà phân phối và không phải là nhà xuất bản nội dung do bên thứ ba cung cấp; như vậy, PP Beauty &amp; Academy không thực hiện quyền kiểm soát biên tập đối với nội dung đó và không bảo đảm hoặc tuyên bố về tính chính xác, độ tin cậy hoặc đơn vị tiền tệ của bất kỳ thông tin, nội dung, dịch vụ hoặc hàng hóa nào được cung cấp hoặc truy cập thông qua Dịch vụ PP Beauty &amp; Academy. Không giới hạn những điều đã nói ở trên, PP Beauty &amp; Academy đặc biệt từ chối mọi bảo đảm và tuyên bố đối với bất kỳ nội dung nào được truyền tải trên hoặc liên quan đến Dịch vụ PP Beauty &amp; Academy hoặc trên các trang web có thể xuất hiện dưới dạng liên kết trên Dịch vụ PP Beauty &amp; Academy, hoặc trong các sản phẩm được cung cấp như một phần của hoặc liên quan đến Dịch vụ PP Beauty &amp; Academy, bao gồm nhưng không giới hạn bất kỳ bảo đảm nào về khả năng bán được, tính phù hợp cho một mục đích cụ thể hoặc không vi phạm quyền của bên thứ ba. PP Beauty &amp; Academy hoặc bất kỳ chi nhánh, nhân viên, cán bộ, giám đốc, đại lý nào hoặc những người tương tự sẽ không đưa ra lời khuyên bằng miệng hoặc thông tin bằng văn bản sẽ tạo ra một bảo hành. Thông tin về giá cả và tình trạng còn hàng có thể thay đổi mà không cần thông báo trước. Không giới hạn những điều đã nêu ở trên, PP Beauty &amp; Academy không đảm bảo rằng Dịch vụ của PP Beauty &amp; Academy sẽ không bị gián đoạn, không bị gián đoạn, kịp thời hoặc không có lỗi.</Text>

                        <Text category={"h5"} style={styles.mb5}>Tuyên bố từ chối trách nhiệm về bản quyền</Text>
                        <Text category={"p2"} style={styles.mb5}>Tất cả các quyền sở hữu trí tuệ liên quan đến các tài liệu này được trao cho PP Beauty &amp; Academy. Không được phép sao chép, phân phối và bất kỳ việc sử dụng nào khác các tài liệu này nếu không có sự cho phép bằng văn bản của PP Beauty &amp; Academy, ngoại trừ và chỉ trong phạm vi được quy định khác trong các quy định của luật bắt buộc (chẳng hạn như quyền được trích dẫn), trừ khi có quy định khác cho một số vật liệu.</Text>

                        <Text category={"h5"} style={styles.mb5}>Tiết lộ Liên kết Liên kết</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy có các liên kết liên kết và trong phần Tuyên bố từ chối trách nhiệm này, chúng tôi sẽ đề cập đến cách chúng tôi sử dụng các liên kết liên kết đó từ các trang web / công ty và sản phẩm khác. Các “liên kết đơn vị” này là các URL cụ thể chứa ID hoặc tên người dùng của đơn vị liên kết.</Text>
                        <Text category={"p2"} style={styles.mb5}>Để tuân thủ các nguyên tắc của FTC, vui lòng giả định những điều sau đây về các liên kết và bài đăng trên trang web này:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Bất kỳ / tất cả các liên kết trên PP Beauty &amp; Academy đều là liên kết liên kết mà chúng tôi nhận được một khoản hoa hồng nhỏ từ việc bán một số mặt hàng nhất định, nhưng mức giá là như nhau cho bạn. Khi PP Beauty &amp; Academy đã phát triển, các chi phí liên quan đến việc vận hành và duy trì nó cũng vậy, và các liên kết liên kết là một cách chúng tôi giúp bù đắp những chi phí này.</Text>
                            <Text category={"p2"} style={styles.mb5}>Nếu chúng tôi đăng một liên kết liên kết đến một sản phẩm, đó là thứ mà cá nhân chúng tôi sử dụng, hỗ trợ và muốn giới thiệu mà không có liên kết liên kết.</Text>
                            <Text category={"p2"} style={styles.mb5}>Trừ khi có ghi chú khác, tất cả các đánh giá là về các mặt hàng chúng tôi đã mua và chúng tôi không được thanh toán hoặc bồi thường dưới bất kỳ hình thức nào.</Text>
                        </View>
                        <Text category={"h5"} style={styles.mb5}>Tiết lộ giáo dục</Text>
                        <Text category={"p2"} style={styles.mb5}>Bất kỳ Thông tin nào do PP Beauty &amp; Academy cung cấp chỉ dành cho mục đích giáo dục và không được hiểu là một khuyến nghị cho một kế hoạch điều trị, sản phẩm hoặc quá trình hành động cụ thể. PP Beauty &amp; Academy là nhà phân phối và không phải là nhà xuất bản nội dung do bên thứ ba cung cấp; như vậy, PP Beauty &amp; Academy không thực hiện quyền kiểm soát biên tập đối với nội dung đó và không bảo đảm hoặc tuyên bố về tính chính xác, độ tin cậy hoặc đơn vị tiền tệ của bất kỳ thông tin hoặc nội dung giáo dục nào được cung cấp hoặc truy cập thông qua PP Beauty &amp; Academy. Không giới hạn những điều đã nói ở trên, PP Beauty &amp; Academy đặc biệt từ chối mọi bảo đảm và tuyên bố đối với bất kỳ nội dung nào được truyền tải trên hoặc liên quan đến PP Beauty &amp; Academy hoặc trên các trang web có thể xuất hiện dưới dạng liên kết trên PP Beauty &amp; Academy, hoặc trong các sản phẩm được cung cấp như một phần của, hoặc liên quan đến PP Beauty &amp; Academy. PP Beauty &amp; Academy hoặc bất kỳ chi nhánh, nhân viên, cán bộ, giám đốc, đại lý nào hoặc những người tương tự sẽ không đưa ra lời khuyên bằng miệng hoặc thông tin bằng văn bản sẽ tạo ra một bảo hành.</Text>

                        <Text category={"h5"} style={styles.mb5}>Tiết lộ lời chứng thực</Text>
                        <Text category={"p2"} style={styles.mb5}>Bất kỳ lời chứng thực nào được cung cấp trên nền tảng này đều là ý kiến ​​của những người cung cấp chúng. Thông tin được cung cấp trong lời chứng thực không được dựa vào để dự đoán kết quả trong tình huống cụ thể của bạn. Kết quả bạn trải nghiệm sẽ phụ thuộc vào nhiều yếu tố, bao gồm nhưng không giới hạn ở mức độ trách nhiệm cá nhân, cam kết và khả năng của bạn, ngoài những yếu tố mà bạn và / hoặc PP Beauty &amp; Academy có thể không lường trước được.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi sẽ cung cấp lời chứng thực trung thực cho khách truy cập của chúng tôi bất kể giảm giá nào. Bất kỳ sản phẩm hoặc dịch vụ nào mà chúng tôi thử nghiệm đều là những trải nghiệm cá nhân, phản ánh những trải nghiệm thực tế trong cuộc sống. Lời chứng thực có thể được hiển thị trên âm thanh, văn bản hoặc video và không nhất thiết phải đại diện cho tất cả những người sẽ sử dụng sản phẩm và / hoặc dịch vụ của chúng tôi</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy không đảm bảo kết quả giống như những lời chứng thực được đưa ra trên nền tảng của chúng tôi. Các ý kiến ​​đánh giá được trình bày trên PP Beauty &amp; Academy có thể áp dụng cho các cá nhân viết chúng và có thể không phải là dấu hiệu cho thấy sự thành công trong tương lai của bất kỳ cá nhân nào khác.</Text>
                        <Text category={"p2"} style={styles.mb5}>Vui lòng liên hệ với chúng tôi nếu bạn muốn biết thêm về lời chứng thực, chiết khấu hoặc bất kỳ sản phẩm / dịch vụ nào mà chúng tôi đánh giá.</Text>
                        <Text category={"h5"} style={styles.mb5}>Sự đồng ý của bạn</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi đã cập nhật Tuyên bố từ chối trách nhiệm của mình để cung cấp cho bạn sự minh bạch hoàn toàn về những gì đang được thiết lập khi bạn truy cập trang web của chúng tôi và cách nó được sử dụng. Bằng cách sử dụng trang web / ứng dụng của chúng tôi, đăng ký tài khoản hoặc mua hàng, bạn đồng ý với Tuyên bố từ chối trách nhiệm của chúng tôi và đồng ý với các điều khoản của nó.</Text>

                        <Text category={"h5"} style={styles.mb5}>Thay đổi đối với tuyên bố từ chối trách nhiệm của chúng tôi</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có nên cập nhật, sửa đổi hoặc thực hiện bất kỳ thay đổi nào đối với tài liệu này để chúng phản ánh chính xác Dịch vụ và chính sách của chúng tôi hay không. Trừ khi luật pháp yêu cầu khác, những thay đổi đó sẽ được đăng nổi bật tại đây. Sau đó, nếu bạn tiếp tục sử dụng Dịch vụ, bạn sẽ bị ràng buộc bởi Tuyên bố từ chối trách nhiệm được cập nhật. Nếu bạn không muốn đồng ý với điều này hoặc bất kỳ Tuyên bố từ chối trách nhiệm cập nhật nào, bạn có thể xóa tài khoản của mình.</Text>
                        <Text category={"h5"} style={styles.mb5}>Liên hệ chúng tôi</Text>
                        <Text category={"p2"} style={styles.mb5}>Đừng ngần ngại liên hệ với chúng tôi nếu bạn có bất kỳ câu hỏi nào liên quan đến Tuyên bố từ chối trách nhiệm này.</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Qua Email: contact@ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua số điện thoại: 099 688 228</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua liên kết này: https://ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua địa chỉ: 306/25 Nguyễn Thị Minh Khai P.5, Q.3, TPHCM</Text>
                        </View>
                    </ScrollView>
                :
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 25
                        }}
                    >
                        <Text category={"p1"}>Updated at 2022-01-01</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy hereby grants you access to https://ppbeautyacademy.com (“the Website”) and invites you to purchase the services offered here.</Text>

                        <Text category={"h5"} style={styles.mb5}>Definitions and key terms</Text>
                        <Text category={"p2"} style={styles.mb5}>To help explain things as clearly as possible in this Disclaimer, every time any of these terms are referenced, are strictly defined as:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Cookie: small amount of data generated by a website and saved by your web browser. It is used to identify your browser, provide analytics, remember information about you such as your language preference or login information.</Text>
                            <Text category={"p2"} style={styles.mb5}>Company: when this policy mentions “Company,” “we,” “us,” or “our,” it refers to PP Beauty &amp; Academy, 80 Bloor St W, Suite 401, Toronto , ON, Canada. M5S2V1 that is responsible for your information under this Disclaimer.</Text>
                            <Text category={"p2"} style={styles.mb5}>Service: refers to the service provided by PP Beauty &amp; Academy as described in the relative terms (if available) and on this platform.</Text>
                            <Text category={"p2"} style={styles.mb5}>Website:  site, which can be accessed via this URL: https://ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>You: a person or entity that is registered with PP Beauty &amp; Academy to use the Services.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Limited liability</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy endeavours to update and/or supplement the content of the website/app on a regular basis. Despite our care and attention, content may be incomplete and/or incorrect.</Text>
                        <Text category={"p2"} style={styles.mb5}>The materials offered on the website/app are offered without any form of guarantee or claim to their correctness. These materials can be changed at any time without prior notice from PP Beauty &amp; Academy.</Text>
                        <Text category={"p2"} style={styles.mb5}>Particularly, all prices on the website/app are stated subject to typing and programming errors. No liability is assumed for the implications of such errors. No agreement is concluded on the basis of such errors.</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy shall not bear any liability for hyperlinks to websites or services of third parties included on the website/app. From our website/app, you can visit other websites by following hyperlinks to such external sites. While we strive to provide only quality links to useful and ethical websites, we have no control over the content and nature of these sites. These links to other websites do not imply a recommendation for all the content found on these sites. Site owners and content may change without notice and may occur before we have the opportunity to remove a link which may have gone ‘bad’.</Text>
                        <Text category={"p2"} style={styles.mb5}>Please be also aware that when you leave our website/app, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their "Terms of Service" before engaging in any business or uploading any information.</Text>
                        <Text category={"h5"} style={styles.mb5}>Links to Other Websites Disclaimer</Text>
                        <Text category={"p2"} style={styles.mb5}>This Disclaimer applies only to the Services. The Services may contain links to other websites not operated or controlled by PP Beauty &amp; Academy. We are not responsible for the content, accuracy or opinions expressed in such websites, and such websites are not investigated, monitored or checked for accuracy or completeness by us. Please remember that when you use a link to go from the Services to another website, our Privacy Policy is no longer in effect. Your browsing and interaction on any other website, including those that have a link on our platform, is subject to that website’s own rules and policies. Such third parties may use their own cookies or other methods to collect information about you. If You click on a third party link, You will be directed to that third party's site. We strongly advise You to review the Privacy Policy and Terms of every site You visit.</Text>
                        <Text category={"p2"} style={styles.mb5}>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</Text>

                        <Text category={"h5"} style={styles.mb5}>Errors and Omissions Disclaimer</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy is not responsible for any content, code or any other imprecision.</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy does not provide warranties or guarantees.</Text>
                        <Text category={"p2"} style={styles.mb5}>In no event shall PP Beauty &amp; Academy be liable for any special, direct, indirect, consequential, or incidental damages or any damages whatsoever, whether in an action of contract, negligence or other tort, arising out of or in connection with the use of the Service or the contents of the Service. PP Beauty &amp; Academy reserves the right to make additions, deletions, or modifications to the contents on the Service at any time without prior notice.</Text>

                        <Text category={"h5"} style={styles.mb5}>General Disclaimer</Text>
                        <Text category={"p2"} style={styles.mb5}>The PP Beauty &amp; Academy Service and its contents are provided "as is" and "as available" without any warranty or representations of any kind, whether express or implied. PP Beauty &amp; Academy is a distributor and not a publisher of the content supplied by third parties; as such, PP Beauty &amp; Academy exercises no editorial control over such content and makes no warranty or representation as to the accuracy, reliability or currency of any information, content, service or merchandise provided through or accessible via the PP Beauty &amp; Academy Service.<br></br>
                        Without limiting the foregoing, PP Beauty &amp; Academy specifically disclaims all warranties and representations in any content transmitted on or in connection with the PP Beauty &amp; Academy Service or on sites that may appear as links on the PP Beauty &amp; Academy Service, or in the products provided as a part of, or otherwise in connection with, the PP Beauty &amp; Academy Service, including without limitation any warranties of merchantability, fitness for a particular purpose or non-infringement of third party rights. No oral advice or written information given by PP Beauty &amp; Academy or any of its affiliates, employees, officers, directors, agents, or the like will create a warranty. Price and availability information is subject to change without notice. Without limiting the foregoing, PP Beauty &amp; Academy does not warrant that the PP Beauty &amp; Academy Service will be uninterrupted, uncorrupted, timely, or error-free.</Text>
                        <Text category={"h5"} style={styles.mb5}>Copyright Disclaimer</Text>
                        <Text category={"p2"} style={styles.mb5}>All intellectual property rights concerning these materials are vested in PP Beauty &amp; Academy. Copying, distribution and any other use of these materials is not permitted without the written permission of PP Beauty &amp; Academy, except and only to the extent otherwise provided in regulations of mandatory law (such as the right to quote), unless otherwise stated for certain materials.</Text>
                        <Text category={"h5"} style={styles.mb5}>Affiliate Links Disclosure</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy has affiliate links and in this section of the Disclaimer we will address how we use those affiliate links from other websites/companies and products. These “affiliate links” are specific URLs that contain the affiliate's ID or username.</Text>
                        <Text category={"p2"} style={styles.mb5}>In compliance with the FTC guidelines, please assume the following about links and posts on this site:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Any/all of the links on PP Beauty &amp; Academy are affiliate links of which we receive a small commission from sales of certain items, but the price is the same for you. As PP Beauty &amp; Academy has grown, so have costs associated with running and maintaining it, and affiliate links are a way we help offset these costs.</Text>
                            <Text category={"p2"} style={styles.mb5}>If we post an affiliate link to a product, it is something that we personally use, support and would recommend without an affiliate link.</Text>
                            <Text category={"p2"} style={styles.mb5}>Unless otherwise noted, all reviews are of items we have purchased and we are not paid or compensated in any way.</Text>
                        </View>
                        <Text category={"h5"} style={styles.mb5}>Educational Disclosure</Text>
                        <Text category={"p2"} style={styles.mb5}>Any Information provided by PP Beauty &amp; Academy is for educational purposes only, and is not to be interpreted as a recommendation for a specific treatment plan, product, or course of action. PP Beauty &amp; Academy is a distributor and not a publisher of the content supplied by third parties; as such, PP Beauty &amp; Academy exercises no editorial control over such content and makes no warranty or representation as to the accuracy, reliability or currency of any information or educational content provided through or accessible via PP Beauty &amp; Academy.<br></br>
                        Without limiting the foregoing, PP Beauty &amp; Academy specifically disclaims all warranties and representations in any content transmitted on or in connection with PP Beauty &amp; Academy or on sites that may appear as links on PP Beauty &amp; Academy, or in the products provided as a part of, or otherwise in connection with, the PP Beauty &amp; Academy. No oral advice or written information given by PP Beauty &amp; Academy or any of its affiliates, employees, officers, directors, agents, or the like will create a warranty.</Text>
                        <Text category={"h5"} style={styles.mb5}>Testimonials Disclosure</Text>
                        <Text category={"p2"} style={styles.mb5}>Any testimonials provided on this platform are opinions of those providing them. The information provided in the testimonials is not to be relied upon to predict results in your specific situation. The results you experience will be dependent on many factors including but not limited to your level of personal responsibility, commitment, and abilities, in addition to those factors that you and/or PP Beauty &amp; Academy may not be able to anticipate.</Text>
                        <Text category={"p2"} style={styles.mb5}>We will give honest testimonials to our visitors regardless of any discount. Any product or service that we test are individual experiences, reflecting real life experiences. The testimonials could be displayed on audio, text or video and are not necessarily representative of all of those who will use our products and/or services.</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy does not guarantee the same results as the testimonials given on our platform. Testimonials presented on PP Beauty &amp; Academy are applicable to the individuals writing them, and may not be indicative of future success of any other individuals.</Text>
                        <Text category={"p2"} style={styles.mb5}>Please don’t hesitate to contact us if you would like to know more about testimonials, discounts, or any of the products/services that we review.</Text>
                        <Text category={"h5"} style={styles.mb5}>Your Consent</Text>
                        <Text category={"p2"} style={styles.mb5}>We've updated our Disclaimer to provide you with complete transparency into what is being set when you visit our site and how it's being used. By using our website/app, registering an account, or making a purchase, you hereby consent to our Disclaimer and agree to its terms.</Text>

                        <Text category={"h5"} style={styles.mb5}>Changes To Our Disclaimer</Text>
                        <Text category={"p2"} style={styles.mb5}>Should we update, amend or make any changes to this document so that they accurately reflect our Service and policies. Unless otherwise required by law, those changes will be prominently posted here. Then, if you continue to use the Service, you will be bound by the updated Disclaimer. If you do not want to agree to this or any updated Disclaimer, you can delete your account.</Text>
                        <Text category={"h5"} style={styles.mb5}>Contact Us</Text>
                        <Text category={"p2"} style={styles.mb5}>Don't hesitate to contact us if you have any questions regarding this Disclaimer.</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Via Email: contact@ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Via Phone Number:  +1 416 792 6268</Text>
                            <Text category={"p2"} style={styles.mb5}>Via this Link:  https://www.ppbeautyacademy.com/en/contact</Text>
                            <Text category={"p2"} style={styles.mb5}>Via this Address: 80 Bloor St W, Suite 401, Toronto, ON Canada. M5S2V1</Text>
                        </View>      
                    </ScrollView>
                }
            </View>
        </BaseScreen>
    )
}

export default Disclaimer
