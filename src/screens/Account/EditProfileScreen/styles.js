import {StyleSheet} from 'react-native'
import {COLORS} from '../../../assets/theme/colors'
import {commonStyles} from '../../../assets/theme/styles'

const styles = StyleSheet.create({
    formContainer: {
        flexGrow: 1,
        paddingHorizontal: 16,
        backgroundColor: COLORS.WHITE
    },
    button: {
        paddingRight: '20%',
        paddingLeft: '20%',
    },
    logo: {
        width: 110,
        height: 110,
    },
    profileWrapper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    secureMessage: {
        textAlign: 'center',
        fontSize: 14,
        color: COLORS.GRAY_BD,
    },
    selectWrapper: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    bottomButton: {
        ...commonStyles.center
    },
    separatorRow32: {
        height: 32,
    },
    addressTitleContainer: {
        width: '100%',
        alignItems: 'center',
    },
    addressTitle: {
        fontSize: 22,
        color: COLORS.SUB,
    },
    addressBarContainer: {
        width: '75%',
        marginBottom: 16,
        alignSelf: 'center',
    },
    addressButton: {
        ...commonStyles.rowCenter,
        paddingVertical: 8,
        paddingHorizontal: 16,
        backgroundColor: COLORS.MAIN,
        borderRadius: 20,
    },
    addressText: {
        fontSize: 14,
        textAlign: 'center',
    },
    confirmUpdateAddressButton: {
        width: '70%',
        alignSelf: 'center',
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    deleteButtonContainer: {
        position: 'absolute',
        right: -28,
        bottom: 0,
        top: 0,
    },
    deleteButton: {
        ...commonStyles.fill,
        ...commonStyles.center,
    },
    phoneTextContainer: {
        color: "#222B45"
    },
    phoneText: {
        backgroundColor: "rgb(247, 249, 252)"
    },
    phoneCodeText: {
        fontSize: 16,
        paddingBottom: 1,
        fontFamily: 'Prata-Regular'
    }
})

export default styles
