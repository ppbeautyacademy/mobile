import {Button, Icon, Input, Spinner, Text} from '@ui-kitten/components'
import React, {useContext, useRef, useState} from 'react'
import {View} from 'react-native'
import context from '../../../../context/context'
import {
    capitalize,
    dateStringFormat,
    DATE_PATTERNS,
    getUsername,
    setUserUpdateInfo,
} from '../../../assets/constants'

import {translate} from '../../../../config/i18n'
import {commonStyles} from '../../../assets/theme/styles'
import BaseScreen from '../../../component/BaseScreen'
import KeyboardScrollView from '../../../component/KeyboardScrollView'
import DatePicker from '../../../component/date/DatePicker'
import {updateUserProfile} from '../../../services/ServiceWorker'
import styles from './styles'
import PhoneInput from 'react-native-phone-number-input'
import FastImage from 'react-native-fast-image'
import { Toast } from 'popup-ui'

const LoadingIndicator = props => (
    <View style={[props.style, styles.indicator]}>
        <Spinner size="small" />
    </View>
)

const renderFullNameIcon = props => <Icon {...props} name="person-outline" />

const renderEmailIcon = props => <Icon {...props} name="email-outline" />

const EditProfileScreen = ({navigation}) => {
    const {state, actions} = useContext(context)
    const parsedUserDOB = state.userInfo?.birthdate?.split('.')[0]
    const userBirthDate = new Date(parsedUserDOB).getDate() ?? null
    const userBirthMonth = new Date(parsedUserDOB).getMonth() + 1 ?? null
    const userBirthYear = new Date(parsedUserDOB).getFullYear() ?? null
    const userFullName = state.userInfo?.fullname ?? ''
    const userEmail = state.userInfo?.email ?? ''
    const userPhoneNumber = state?.userInfo?.phone ?? ''
    const userPhoneExtension = state?.userInfo?.phoneExtension ?? {}

    const [account, setAccount] = useState({
        fullname: userFullName,
        email: userEmail,
        phone: userPhoneNumber
            ? formatPhone(userPhoneNumber, userPhoneExtension)
            : '',
        phoneExtensionBean: userPhoneExtension,
    })

    const [dateBean, setDateBean] = useState({
        day: userBirthDate,
        month: userBirthMonth,
        year: userBirthYear,
    })

    //#region Phone Input
    const [formattedValue, setFormattedValue] = useState(`+${userPhoneNumber}`)
    const phoneInput = useRef(null)

    const formatPhone = (phone, phoneExtension) => {
        if (phone && phoneExtension && phoneExtension.dialCode) {
            if(phoneExtension.dialCode.length === 1) {
                if (
                    phoneExtension.dialCode ===
                    phone[phoneExtension.dialCode.length - 1]
                ) {
                    let phoneSpliced = phone.replace(
                        phone[phoneExtension.dialCode.length - 1],
                        '',
                    )
        
                    return phoneSpliced
                }
            } else if(phoneExtension.dialCode.length > 1 && phone.includes(phoneExtension.dialCode)) {
                let matchAll = true;
                for(let i = 0; i < phoneExtension.dialCode.length; i++) {
                    if(phone[i] !== phoneExtension.dialCode[i]) {
                        matchAll = false;
                    }
                }
    
                if(matchAll) {
                    let phoneSpliced = phone.slice(phoneExtension.dialCode.length);
    
                    return phoneSpliced;
                }
            }        
        }
    
        return ''
    }
    //#endregion

    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState({
        fullname: false,
        email: false,
        invalidEmail: false,
        phone: false,
        invalidPhone: false,
        day: false,
        month: false,
        year: false,
    })

    //#region On Change Account Info
    const onFullNameChange = fullname => {
        setAccount({
            ...account,
            fullname: fullname,
        })
    }

    const onEmailChange = email => {
        setAccount({
            ...account,
            email: email,
        })
    }

    const onPhoneChange = phone => {
        setAccount({
            ...account,
            phone: phone,
            phoneExtensionBean: {
                countryCode: phoneInput.current?.getCountryCode(),
                dialCode: phoneInput.current?.getCallingCode(),
            },
        })
    }

    const onDayChange = day => {
        setDateBean({
            ...dateBean,
            day: day,
        })
    }

    const onMonthChange = month => {
        setDateBean({
            ...dateBean,
            month: month,
        })
    }

    const onYearChange = year => {
        setDateBean({
            ...dateBean,
            year: year,
        })
    }
    //#endregion

    const handleUpdateProfile = async () => {
        setLoading(true)

        let isError = false
        let errors = {}

        if (
            !account ||
            !account.fullname ||
            !account.phone ||
            !account.email ||
            !dateBean.day ||
            !dateBean.month ||
            !dateBean.year
        ) {
            errors = {
                ...errors,
                fullname: !account.fullname,
                email: !account.email,
                phone: !account.phone,
                day: !dateBean.day,
                month: !dateBean.month,
                year: !dateBean.year,
            }

            isError = true
        }

        if (account.phone) {
            if (!phoneInput.current?.isValidNumber(account.phone)) {
                errors = {
                    ...errors,
                    invalidPhone: true,
                }

                isError = true
            }
        }

        if (!isError) {
            const username = await getUsername()
            let birthDateBean = dateStringFormat(
                dateBean.day,
                dateBean.month,
                dateBean.year,
                DATE_PATTERNS.MDY.value,
            )

            let userBasicInfo = {
                ...account,
                username: username ? username.replace(/ /g, '') : '',
                fullname: account.fullname
                    ? capitalize(account.fullname)
                    : account.fullname,
                email: account.email ? account.email.replace(/ /g, '') : '',
                phone: formattedValue?.replace(/'+'/g, ''),
                phoneExtensionBean: account.phoneExtensionBean
                    ? account.phoneExtensionBean
                    : {},
                birthDateBean: birthDateBean,
            }

            const updateUserProfileStatus = await updateUserProfile(
                state.version,
                userBasicInfo,
                true,
                true,
            ).finally(() => setLoading(false))
            if (updateUserProfileStatus && updateUserProfileStatus.id) {
                await setUserUpdateInfo(updateUserProfileStatus)

                setErrors({})

                Toast.show({
                    title: translate('common.successTitle'),
                    text: translate('common.updateSuccessfully'),
                    color: '#5BAA22',
                    timing: 3500,
                })
            }
        } else {
            setErrors(errors)
        }
        setLoading(false)
    }

    return (
        <BaseScreen title={translate('account.editYourProfile')}>
            <KeyboardScrollView
                extraScrollHeight={Platform.OS === 'ios' ? 0 : 100}>
                <View style={styles.formContainer}>
                    <View
                        style={{
                            height: 130,
                            padding: 10,
                            flexDirection: 'row',
                            justifyContent: 'center',
                        }}>
                        <FastImage
                            source={require('../../../assets/images/edit-profile.png')}
                            style={{
                                width: '70%',
                                height: 120,
                            }}
                        />
                    </View>
                    <View style={commonStyles.separator.row16} />
                    <Text category={'label'} appearance={'hint'}>
                        {translate('common.fullName')}
                    </Text>
                    <Input
                        value={account.fullname}
                        placeholder={translate('common.fullNamePlaceholder')}
                        accessoryRight={renderFullNameIcon}
                        onChangeText={fullname => onFullNameChange(fullname)}
                        status={errors.fullname && 'danger'}
                        textStyle={commonStyles.mainFont}
                    />
                    {errors.fullname && (
                        <Text category={'label'} status={'danger'}>
                            {translate('errors.emptyFullName')}
                        </Text>
                    )}
                    <View style={commonStyles.separator.row16} />
                    <View style={styles.selectWrapper}>
                        <DatePicker
                            defaultDate={dateBean}
                            setDay={onDayChange}
                            setMonth={onMonthChange}
                            setYear={onYearChange}
                            errors={errors}
                        />
                    </View>
                    <Text category={'label'} appearance={'hint'}>
                        {translate('common.email')}
                    </Text>
                    <Input
                        value={account.email}
                        placeholder={'Enter your email'}
                        accessoryRight={renderEmailIcon}
                        onChangeText={email => onEmailChange(email)}
                        status={errors.email && 'danger'}
                        textStyle={commonStyles.mainFont}
                    />
                    {errors.email && (
                        <Text category={'label'} status={'danger'}>
                            {translate('error.emptyEmail')}
                        </Text>
                    )}
                    {errors.invalidEmail && (
                        <Text category={'label'} status={'danger'}>
                            {translate('error.invalidEmail')}
                        </Text>
                    )}
                    <View style={commonStyles.separator.row16} />
                    <Text category={'label'} appearance={'hint'}>
                        {translate('common.phoneNumber')}
                    </Text>
                    <PhoneInput
                        ref={phoneInput}
                        defaultValue={account.phone}
                        defaultCode={
                            account.phoneExtensionBean?.countryCode
                                ? account.phoneExtensionBean?.countryCode.toUpperCase()
                                : 'CA'
                        }
                        layout="first"
                        placeholder="Enter phone number"
                        onChangeText={text => {
                            onPhoneChange(text)
                        }}
                        onChangeFormattedText={text => {
                            setFormattedValue(text)
                        }}
                        containerStyle={commonStyles.customPhoneInputContainer}
                        textContainerStyle={[styles.phoneText, commonStyles.p0]}
                        textInputStyle={[
                            commonStyles.p0,
                            styles.phoneTextContainer,
                        ]}
                        flagButtonStyle={commonStyles.p0}
                        codeTextStyle={[
                            commonStyles.p0,
                            commonStyles.m0,
                            styles.phoneCodeText,
                        ]}
                    />
                    {errors.phone && (
                        <Text category={'label'} status={'danger'}>
                            {translate('errors.emptyPhoneNumber')}
                        </Text>
                    )}
                    {errors.invalidPhone && (
                        <Text category={'label'} status={'danger'}>
                            {translate('errors.invalidPhoneNumber')}
                        </Text>
                    )}
                    <View style={commonStyles.separator.row16} />
                    <View style={commonStyles.separator.row16} />
                    <View style={commonStyles.separator.row16} />
                    <View style={styles.bottomButton}>
                        <Button
                            status="primary"
                            appearance={'outline'}
                            onPress={() => handleUpdateProfile()}
                            style={styles.button}
                            disabled={loading}
                            accessoryLeft={loading && LoadingIndicator}>
                            {props => (
                                <Text
                                    {...props}
                                    category="p1"
                                    style={[
                                        commonStyles.mainFont,
                                        commonStyles.mainColor,
                                    ]}>
                                    {translate('common.submit')}
                                </Text>
                            )}
                        </Button>
                    </View>
                    <View style={styles.separatorRow32} />
                </View>
            </KeyboardScrollView>
        </BaseScreen>
    )
}

export default EditProfileScreen
