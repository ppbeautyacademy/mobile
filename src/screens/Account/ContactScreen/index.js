import React, { useContext, useState } from 'react'
import { KeyboardAvoidingView, View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import context from '../../../../context/context';
import { COLORS } from '../../../assets/theme/colors';
import BaseScreen from '../../../component/BaseScreen';
import { ScreenIds } from '../../../ScreenIds';
import {styles} from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Button, Divider, Text, Icon as KittenIcon, Input } from '@ui-kitten/components';
import { LANGUAGE_CODES, MAX_INPUT_LENGTH, messageSubjectTypes, messageTypes } from '../../../assets/constants';
import FastImage from 'react-native-fast-image';
import ModalPopup from '../../../component/ModalPopup';
import { commonStyles } from '../../../assets/theme/styles';
import { translate } from '../../../../config/i18n';
import { saveMessage } from '../../../services/ServiceWorker';
import {validEmail} from '../../../utils/Validation'

const ContactScreen = ({navigation}) => {
    const {state} = useContext(context);

    //#region Support Form
    const [showEnterNameModal, setShowEnterNameModal] = useState(false);

    const showEnterNamePopup = () => {
        setShowEnterNameModal(true)
    }
    const hideEnterNamePopup = () => {
        setShowEnterNameModal(false)
    }
    //#endregion

    return(
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <EnterUsernameModal
                visible={showEnterNameModal}
                onPressOutside={hideEnterNamePopup}
                subject={messageSubjectTypes.guestSupport}
                type={messageTypes.question}
            />
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View style={{marginRight: 20}}>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.Account)
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text category="h6">
                        {state.version === LANGUAGE_CODES.vi.value ? 'Hỗ trợ' : 'Support'}
                    </Text>
                </View>

                <View style={styles.bodyContainer}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 30
                        }}
                    >   
                        <View style={styles.imageContainer}>
                            <FastImage 
                                source={require('../../../assets/images/contact-us.png')}
                                style={styles.image}
                            />
                        </View>
                        
                        <Text category={"h6"} style={styles.mb5, styles.header}>PP Beauty &amp; Academy</Text>
                        <Text><Text appearance={"hint"}>{state.version === LANGUAGE_CODES.vi.value ? 'Email hỗ trợ' : 'Customer Support'}</Text> : contact@ppbeautyacademy.com</Text> 

                        <Divider  style={{marginVertical: 15, backgroundColor: COLORS.SUB}}/>   
                        <Text style={styles.header}>CANADA</Text>
                        <Text>
                            <Text appearance={"hint"}>{state.version === LANGUAGE_CODES.vi.value ? 'Điện thoại' : 'Head Office'}</Text> :
                            +1 416 792 6268
                        </Text>
                        <Text style={styles.mb5}>
                            <Text appearance={"hint"}>{state.version === LANGUAGE_CODES.vi.value ? 'Văn phòng chính' : 'Head Office'}</Text> : 
                            80 Bloor St W, Suite 401, Toronto, ON, Canada. M5S2V1
                        </Text>
                        
                        <Divider  style={{marginVertical: 15, backgroundColor: COLORS.SUB}}/>   
                        <Text style={styles.header}>VIETNAM</Text>
                        <Text>
                            <Text appearance={"hint"}>{state.version === LANGUAGE_CODES.vi.value ? 'Điện thoại' : 'Head Office'}</Text> :
                            099 688 228
                        </Text>
                        <Text style={styles.mb5}>
                            <Text appearance={"hint"}>{state.version === LANGUAGE_CODES.vi.value ? 'Văn phòng chính' : 'Head Office'}</Text> : 
                            306/25 Nguyễn Thị Minh Khai, P.5, Q.3, TPHCM
                        </Text>

                        <Divider  style={{marginVertical: 15, backgroundColor: COLORS.SUB}}/>

                        <Button style={{marginVertical: 25}}
                            accessoryLeft={(props) => <KittenIcon {...props} name='paper-plane-outline'/>}
                            onPress={() => showEnterNamePopup()}
                        >
                            {props => (
                                <Text
                                    {...props}
                                    category="p1"
                                    style={[
                                        styles.mainFont,
                                        styles.lightColor,
                                    ]}>
                                    HỖ TRỢ
                                </Text>
                            )}
                        </Button>
                    </ScrollView>                    
                </View>
            </View>
        </BaseScreen>
    )
}

export const EnterUsernameModal = ({visible, onPressOutside, subject, type}) => {
    const [email, setEmail] = useState('')
    const [message, setMessage] = useState('')
    const [showError, setShowError] = useState({})

    const {state, actions} = useContext(context);

    const handleOnPressOutSide = () => {
        setEmail('');
        setMessage('');
        setShowError({});
        onPressOutside();
    }

    const onPressSend = async() => {
        let isError = false;
        if(!state.isLoggedIn) {
            if(!email || !message) {
                setShowError({
                    email: !email,
                    message: !message
                })
    
                isError = true
            }
    
            if(email && !validEmail(email)) {
                setShowError({
                    ...showError,
                    email: true
                })
            }
        } else {
            if(!message) {
                setShowError({
                    message: !message
                })
            }
        }
        

        if(!isError) {
            let toPostData = {
                email: state.isLoggedIn ? state.userInfo?.email : email,
                message: message,
                subject: subject,
                type: type,
                initMessage: true
            }

            actions.showAppSpinner(true)
            const sendStatus = await saveMessage(state.userInfo?.id, toPostData).finally(() => actions.showAppSpinner(false));
            if(sendStatus && sendStatus.id) {
                handleOnPressOutSide();
                // TODO show success notification
                console.log("Success")
            } 
        }
    }

    return (
        <ModalPopup visible={visible} onPressOutSide={onPressOutside}>
            <KeyboardAvoidingView style={styles.enterUsernameContainer}>
                {!state.isLoggedIn &&
                    <React.Fragment>
                        <Input
                            status={!showError.email && 'danger'}
                            placeholder={"Please enter your email"}
                            textStyle={styles.mainFont}
                            value={email}
                            label={"Email"}
                            onChangeText={setEmail}
                        />
                        <View style={commonStyles.separator.row32} />
                    </React.Fragment>
                }
                <Input
                    status={showError.message && 'danger'}
                    placeholder={translate('support.pleaseEnterYourIssue')}
                    textStyle={styles.mainFont}
                    value={message}
                    label={translate('support.howMayWeHelp')}
                    multiline
                    numberOfLines={8}
                    onChangeText={(newValue) => setMessage(newValue)}
                />
                <View style={commonStyles.separator.row32} />
                <Button 
                    size={'tiny'}
                    style={{paddingHorizontal: 30}}
                    onPress={() => onPressSend()}
                >
                    
                    {props => (
                        <Text
                            {...props}
                            category="p1"
                            style={[
                                styles.mainFont,
                                styles.lightColor,
                            ]}>
                            SUBMIT
                        </Text>
                    )}
                </Button>
            </KeyboardAvoidingView>
        </ModalPopup>
    )
}

export default ContactScreen;