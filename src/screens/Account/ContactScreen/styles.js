import {StyleSheet} from 'react-native'
import { COLORS } from '../../../assets/theme/colors'
import { commonStyles } from '../../../assets/theme/styles'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingHorizontal: 16
    },
    bodyContainer: {
        paddingBottom: 30,
    },
    header: {
        color: '#ff5623',
        marginBottom: 5
    },
    imageContainer: {
        height: 180,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    image: {
        width: '50%',
        height: '100%'
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        paddingTop: 10,
        marginBottom: 15
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    mb5: {
        marginBottom: 5
    },
    mainFont: {
        fontFamily: 'Prata-Regular'
    },
    lightColor: {
        color: COLORS.WHITE
    },
    /******** Popup Modal **********/
    enterUsernameContainer: {
        width: '80%',
        padding: 16,
        backgroundColor: COLORS.WHITE_BACKGROUND,
        borderRadius: 10,
        ...commonStyles.center,
    }, 
})

export {styles}
