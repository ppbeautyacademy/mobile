import React, {useContext, useRef, useState} from 'react'
import {useFocusEffect} from '@react-navigation/native'
import {
    View,
    TouchableOpacity,
    SectionList,
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {
    Divider,
    Text,
    Button as KittenButton,
    Icon as KittenIcon,
} from '@ui-kitten/components'

import {commonStyles} from '../../assets/theme/styles'
import IconWithBadge from '../../component/IconWithBadge'
import BaseScreen from '../../component/BaseScreen'
import Avatar from '../../component/Avatar'
import {
    getAppLanguage,
    getLanguageInfo,
    languages,
    translate,
} from '../../../config/i18n'
import {AVATAR_SIZE, styles} from './styles'
import {COLORS} from '../../assets/theme/colors'
import UploadImage from './component/UploadImage'
import Context from '../../../context/context'
import {
    APP_CURRENCY,
    HTTP_STATUS_OK,
    LANGUAGE_CODES,
    LOCAL_STORAGE_KEY,
} from '../../assets/constants'
import {resetRootNavigatorToScreen, rootNavigationRef} from '../../Navigation'
import {ScreenIds} from '../../ScreenIds'
import LocalStorage from '../../storage/localStorage'
import {logout} from '../../services/AuthService'
import {IMAGES} from '../../assets/images'
import {getUserInfo, revokeRefreshToken} from '../../services/ServiceWorker'
import { Modalize } from 'react-native-modalize'
import OrderContext from '../../../context/OrderContext'
import { getUserCredentials } from '../../services/secureData'
import Video from 'react-native-video'

const mockData = {
    name: 'Nguyễn Văn A',
    email: 'mail@mail.com',
    isLoggedIn: true,
    profilePic: IMAGES.BACKGROUND_AVATAR,
}

const AvatarContainer = ({showImagePicker, avatar, isLoggedIn}) => {
    return (
        <View style={styles.avatarContainer}>
            <Avatar
                disableOnPress={!isLoggedIn}
                onPressImage={showImagePicker}
                defaultImage={mockData.profilePic}
                url={avatar}
                containerStyle={styles.avatar}
                size={AVATAR_SIZE}
            />
            {isLoggedIn && (
                <View style={styles.iconCamera}>
                    <Icon name="add-a-photo" size={25} color={COLORS.SUB} />
                </View>
            )}
        </View>
    )
}

const AccountInfoView = ({name, isLoggedIn}) => {
    return (
        <View style={styles.accountInfoContainer}>
            {isLoggedIn ? (
                <>
                    <Text style={styles.nameText} numberOfLines={2}>
                        {name}
                    </Text>
                </>
            ) : (
                <TouchableOpacity
                    onPress={() =>
                        rootNavigationRef.current.navigate(ScreenIds.AuthStack)
                    }>
                    <Text style={styles.loginText}>
                        {translate('account.clickHereToLogin')}
                    </Text>
                </TouchableOpacity>
            )}
        </View>
    )
}

const HeaderRightComp = ({onPressAccount}) => {
    return (
        <View style={styles.headerRightContainer}>
            <IconWithBadge
                iconName={'account-circle'}
                count={0}
                color={COLORS.SUB}
                backgroundColor={COLORS.WHITE}
                onPress={onPressAccount}
            />
        </View>
    )
}

const HeaderLeftComp = ({onPress, selectedLanguageCode}) => {
    const selectedLanguageStyle = {backgroundColor: COLORS.SUB}
    const blurStyle = {opacity: 0.5, color: COLORS.SUB}
    const getSelectedStyle = langCode => {
        if (langCode === selectedLanguageCode) {
            return selectedLanguageStyle
        }
        return blurStyle
    }
    return (
        <TouchableOpacity style={styles.headerLeftContainer} onPress={onPress}>
            <Text
                style={[
                    styles.headerLeftText,
                    getSelectedStyle(LANGUAGE_CODES.en.value),
                ]}>
                {LANGUAGE_CODES.en.value}
            </Text>
            <View style={styles.headerLeftSeparator} />
            <Text
                style={[
                    styles.headerLeftText,
                    getSelectedStyle(LANGUAGE_CODES.vi.value),
                ]}>
                {LANGUAGE_CODES.vi.value}
            </Text>
        </TouchableOpacity>
    )
}

const renderItemFeatureModal = (item, index) => {
    if (!item.hide) {
        return (
            <React.Fragment key={index}>
                <TouchableOpacity
                    style={styles.modalItemContainer}
                    onPress={item.actionOnPress}
                    disabled={!item.actionOnPress}
                    key={index}>
                    <View style={styles.featureDescriptionContainer} key={index}>
                        <View
                            style={[
                                styles.itemFeatureContainer,
                                styles.backgroundMain,
                            ]}>
                            <Icon name={item.icon} size={20} color={COLORS.SUB} />
                        </View>
                        <View style={commonStyles.separator.column16} />
                        <Text style={styles.featureTitleText} allowFontScaling>
                            {item.title}
                        </Text>
                        <View style={styles.arrowRight}>
                            {item.showValue ?
                                <Text style={{color: COLORS.SUB}}>{item.showValue}</Text>
                            :
                                <Icon
                                    name="chevron-right"
                                    size={24}
                                    color={COLORS.GRAY_82}
                                />
                            }
                        </View>
                    </View>
                </TouchableOpacity>
    
                <Divider />
            </React.Fragment>
        )
    }
}

const AccountView = ({
    showImagePicker,
    state,
    showAccount,
    onPressChangeLanguage
}) => (
    <React.Fragment>
        <Video
            source={require("../../assets/mp4/cover.mp4")}
            style={styles.backgroundVideo}
            muted={true}
            repeat={true}
            resizeMode={"cover"}
            rate={1.0}
            ignoreSilentSwitch={"obey"}
        />
        
        <View style={styles.headerContainer}>
            <HeaderLeftComp
                onPress={onPressChangeLanguage}
                selectedLanguageCode={getAppLanguage()}
            />
            {state.isLoggedIn &&
                <HeaderRightComp onPressAccount={showAccount} />
            }
        </View>
        <View style={styles.cardBodyContainer}>
            <AvatarContainer
                isLoggedIn={state.isLoggedIn}
                showImagePicker={showImagePicker}
                avatar={state.userInfo?.avatar}
            />
            <AccountInfoView
                name={state.userInfo?.fullname}
                isLoggedIn={state.isLoggedIn}
            />
        </View>
    </React.Fragment>
)

const AccountScreen = () => {
    const {state, actions} = useContext(Context)
    const {orderActions} = useContext(OrderContext)

    const setShowImagePicker = useRef()
    const accountModalRef = useRef()

    const getShowPicker = setShowPicker => {
        setShowImagePicker.current = setShowPicker
    }
    const showImagePicker = () => {
        setShowImagePicker.current(true)
    }

    const showAccount = () => {
        accountModalRef.current?.open()
    }

    // update user info whenever user navigate to account screen
    useFocusEffect(
        React.useCallback(() => {
            ;(async function () {
                if (!!state.userInfo?.username && state.isLoggedIn) {
                    const userInfo = await getUserInfo({
                        username: state.userInfo?.username,
                    })
                    if (userInfo) {
                        actions.setUserInfo(userInfo)
                    }
                }
            })()
        }, [state.userInfo?.username, state.isLoggedIn]),
    )

    //#region SECTION FEATURE LIST
    const data = [
        {
            title: "Cài đặt",
            data: [
                {
                    title: "Language",
                    icon: "language",
                    actionOnPress: null,
                    showValue: state.version === LANGUAGE_CODES.vi.value ? "Tiếng Việt" : "Tiếng Anh"
                },
                {
                    title: "Currency",
                    icon: 'monetization-on',
                    actionOnPress: null,
                    showValue: state.appCurrency === APP_CURRENCY.VND.value ? "VND" : "CAD"
                }
            ]
        },
        {
            title: "Thông tin",
            data: [
                {
                    title: "Privacy Policy",
                    icon: 'policy',
                    actionOnPress: () => {
                        rootNavigationRef.current.navigate(ScreenIds.PolicyScreen)
                    },
                },
                {
                    title: "Term of Use",
                    icon: 'description',
                    actionOnPress: () => {
                        rootNavigationRef.current.navigate(ScreenIds.TermOfUseScreen)
                    },
                },
                {
                    title: "Disclaimer",
                    icon: 'warning',
                    actionOnPress: () => {
                        rootNavigationRef.current.navigate(ScreenIds.DisclaimerScreen)
                    },
                },
                {
                    title: "Return and Refund",
                    icon: 'low-priority',
                    actionOnPress: () => {
                        rootNavigationRef.current.navigate(ScreenIds.ReturnAndRefundScreen)
                    },
                },
            ]
        },
        {
            title: "Liên hệ",
            data: [
                {
                    title: "Về chúng tôi",
                    icon: 'info',
                    actionOnPress: () => {
                        rootNavigationRef.current.navigate(ScreenIds.AboutScreen)
                    }
                },
                {
                    title: "Hỗ trợ",
                    icon: 'support-agent',
                    actionOnPress: () => {
                        rootNavigationRef.current.navigate(ScreenIds.ContactScreen)
                    }
                }
            ]
        }
    ]
    //#endregion

    //#region LANGUAGE SECTION
    const onPressChangeLanguage = async languageCode => {
        if (getAppLanguage() !== languageCode) {
            const currencyBasedOnLanguage =
                languageCode === languages[0].code
                    ? APP_CURRENCY.VND.value
                    : APP_CURRENCY.CAD.value
            await LocalStorage.setItem(
                LOCAL_STORAGE_KEY.CURRENCY,
                currencyBasedOnLanguage,
            )
            actions.setVersion(languageCode)
            orderActions.clearOrderState();
            await LocalStorage.setItem(LOCAL_STORAGE_KEY.LANGUAGE, languageCode)
            resetRootNavigatorToScreen(ScreenIds.SplashScreen)
        }
    }

    const showChangeLanguagePopup = () => {
        const nextAppLanguage =
            getAppLanguage() === LANGUAGE_CODES.en.value
                ? getLanguageInfo(LANGUAGE_CODES.vi.value)
                : getLanguageInfo(LANGUAGE_CODES.en.value)
        actions.showAppPopup({
            isVisible: true,
            message: translate('account.changeLanguageConfirmQuote', {
                language: translate('common.' + nextAppLanguage.label),
            }),
            okText: translate('common.yes'),
            cancelText: translate('common.no'),
            onOkHandler: () => onPressChangeLanguage(nextAppLanguage.code),
        })
    }
    //#endregion

    //#region Account Options List
    const accountOptions = [
        {
            title: 'Edit your profile',
            icon: 'edit',
            actionOnPress: () => {
                rootNavigationRef.current.navigate(ScreenIds.EditProfileScreen)
            }
        },
        {
            title: 'Change password',
            icon: 'lock',
            actionOnPress: () => {
                rootNavigationRef.current.navigate(
                    ScreenIds.ChangePasswordScreen,
                )
            }
        },
        {
            title: 'Address',
            icon: 'menu-book',
            actionOnPress: () => {
                rootNavigationRef.current.navigate(ScreenIds.AddressScreen)
            },
        },
        {
            title: "Social Connected",
            icon: 'link',
            actionOnPress: () => {
                rootNavigationRef.current.navigate(ScreenIds.SocialConnectedScreen)
            },
        },
        {
            title: "Log Out",
            icon: 'logout',
            actionOnPress: () => {
                onPressLogout()
            },
        }
    ];

    const historyOptions = [
        {
            title: 'Orders',
            icon: 'local-mall',
            actionOnPress: () => {
                rootNavigationRef.current.navigate(ScreenIds.OrderHistoryScreen)
            }
        },
        {
            title: 'Favorites',
            icon: 'favorite',
            actionOnPress: () => {
                rootNavigationRef.current.navigate(ScreenIds.FavoriteScreen)
            }
        },
        {
            title: "Supports",
            icon: 'contact-support',
            actionOnPress: () => {
                rootNavigationRef.current.navigate(ScreenIds.SupportScreen)
            },
        }
    ]

    const accountModal = (
        <Modalize
            ref={accountModalRef}
            handleStyle={styles.handleModalStyle}
            modalStyle={{
                borderTopLeftRadius: 30,
                borderTopRightRadius: 30,
            }}
            snapPoint={350}
            onBackButtonPress={() => accountModalRef.current?.close()}
            scrollViewProps={{showsVerticalScrollIndicator: false}}
            key={'accountModal'}
        >
            <View style={styles.settingModalContainer}>
                <View style={styles.modalFeatureContent}>
                    <View>
                        <Text category={"h6"} style={{paddingVertical: 10}}>History</Text>
                        {historyOptions.map((element, index) => {
                            return renderItemFeatureModal(element, index)
                        })}
                    </View>
                    <View>
                        <Text category={"h6"} style={{paddingVertical: 10}}>Account Settings</Text>
                        {accountOptions.map((element, index) => {
                            return renderItemFeatureModal(element, index)
                        })}
                    </View>
                </View>
            </View>
        </Modalize>
    )
    //#endregion
    
    
    //#region LOG OUT SECTION
    const onPressLogout = async() => {
        actions.showAppPopup({
            isVisible: true,
            message: translate('account.logoutConfirmQuote'),
            okText: translate('common.yes'),
            cancelText: translate('common.no'),
            onOkHandler: () =>
                logout(async() => {
                    actions.showWhiteAppSpinner(true);

                    await handleRevokeToken();
                    actions.resetUserAuth()
                    actions.resetUserInfo()
                    actions.setIsLoggedIn(false);
                    accountModalRef.current?.close();

                    actions.showWhiteAppSpinner(false)
                }),
        })
    }

    const handleRevokeToken = async() => {
        let userAuthInfo
        await getUserCredentials()
            .then(authInfo => {
                userAuthInfo = authInfo
            })
            .catch(err => logService.log(err))

        await revokeRefreshToken(userAuthInfo?.refreshToken)
        .then(data => {
            if(data === HTTP_STATUS_OK)  {
                return;
            }
        })
    }
    //#endregion
    
    return (
        <BaseScreen
            showHeader={false}
            modals={
                <React.Fragment>
                    <UploadImage key={'uploadImage'} getShowPicker={getShowPicker} />
                    {accountModal}
                </React.Fragment>
            }
            >
                <View style={styles.container}>
                    <AccountView
                        showImagePicker={showImagePicker}
                        state={state}
                        showAccount={showAccount}
                        onPressChangeLanguage={showChangeLanguagePopup}
                    />
                    <SectionList
                        contentContainerStyle={{
                            paddingHorizontal: 20,
                            paddingBottom: 130
                        }}
                        sections={data}
                        keyExtractor={(item, index) => item + index}
                        renderItem={({item} , idx) => renderItemFeatureModal(item, idx)}
                        renderSectionHeader={({ section: { title } }) => (
                            <Text style={styles.sectionHeader}>{title}</Text>
                        )}
                    />
                </View>
        </BaseScreen>
    )
}

export default AccountScreen;
