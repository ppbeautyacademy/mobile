import {StyleSheet} from 'react-native'
import { COLORS } from '../../../assets/theme/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingHorizontal: 16
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        paddingTop: 10,
        marginBottom: 15
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    mb5: {
        marginBottom: 5
    }
})

export {styles}
