import { Text } from '@ui-kitten/components'
import React, { useContext } from 'react'
import {View} from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import BaseScreen from '../../../component/BaseScreen'
import { ScreenIds } from '../../../ScreenIds'
import {styles} from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../../assets/theme/colors'
import { translate } from '../../../../config/i18n'
import context from '../../../../context/context'
import { LANGUAGE_CODES } from '../../../assets/constants'

const ReturnAndRefund = ({navigation}) => {
    const {state} = useContext(context);

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View style={{marginRight: 20}}>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.Account)
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text category="h6">
                        {state.version === LANGUAGE_CODES.vi.value ? 'Chính sách đổi trả và hoàn tiền' : 'Return and Refund Policy'}
                    </Text>
                </View>
                
                {state.version === LANGUAGE_CODES.vi.value ?
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 25
                        }}
                    >
                        <Text category={"p2"} style={styles.mb5}>Updated at 2022-01-01</Text>

                        <Text category={"h5"} style={styles.mb5}>Định nghĩa và thuật ngữ chính</Text>
                        <Text category={"p2"} style={styles.mb5}>Để giúp giải thích mọi thứ rõ ràng nhất có thể trong Chính sách Trả hàng &amp; Hoàn tiền này, mỗi khi bất kỳ điều khoản nào trong số này được tham chiếu, đều được xác định rõ ràng là:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Công ty: khi chính sách này đề cập đến “Công ty”, “chúng tôi”, “chúng tôi” hoặc “của chúng tôi”, nghĩa là PP Beauty &amp; Academy chịu trách nhiệm về thông tin của bạn theo Chính sách Trả hàng &amp; Hoàn tiền này.</Text>
                            <Text category={"p2"} style={styles.mb5}>Khách hàng: đề cập đến công ty, tổ chức hoặc cá nhân đăng ký sử dụng Dịch vụ PP Beauty &amp; Academy để quản lý các mối quan hệ với người tiêu dùng hoặc người sử dụng dịch vụ của bạn.</Text>
                            <Text category={"p2"} style={styles.mb5}>Thiết bị: mọi thiết bị có kết nối internet như điện thoại, máy tính bảng, máy tính hoặc bất kỳ thiết bị nào khác có thể được sử dụng để đến PP Beauty &amp; Academy và sử dụng các dịch vụ.</Text>
                            <Text category={"p2"} style={styles.mb5}>Dịch vụ: đề cập đến dịch vụ do PP Beauty &amp; Academy cung cấp như được mô tả trong các điều khoản tương đối (nếu có) và trên nền tảng này.</Text>
                            <Text category={"p2"} style={styles.mb5}>Trang web: Trang web của PP Beauty &amp; Academy, có thể được truy cập qua URL này: https://ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Bạn: cá nhân hoặc tổ chức đã đăng ký với PP Beauty &amp; Academy để sử dụng Dịch vụ.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Chính sách trả lại &amp; hoàn lại tiền</Text>
                        <Text category={"p2"} style={styles.mb5}>Cảm ơn bạn đã mua sắm tại PP Beauty &amp; Academy. Chúng tôi đánh giá cao việc bạn thích mua những thứ mà chúng tôi xây dựng. Chúng tôi cũng muốn đảm bảo bạn có trải nghiệm bổ ích khi khám phá, đánh giá và mua sản phẩm của chúng tôi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Như với bất kỳ trải nghiệm mua sắm nào, có các điều khoản và điều kiện áp dụng cho các giao dịch tại PP Beauty &amp; Academy. Chúng tôi sẽ ngắn gọn như luật sư của chúng tôi sẽ cho phép. Điều chính cần nhớ là bằng cách đặt hàng hoặc mua hàng tại PP Beauty &amp; Academy, bạn đồng ý với các điều khoản quy định bên dưới cùng với Chính sách quyền riêng tư của PP Beauty &amp; Academy.</Text>
                        <Text category={"p2"} style={styles.mb5}>Nếu có vấn đề gì xảy ra với sản phẩm / dịch vụ bạn đã mua hoặc nếu bạn không hài lòng với sản phẩm / dịch vụ đó, bạn sẽ không thể hoàn lại tiền cho mặt hàng của mình.</Text>

                        <Text category={"h5"} style={styles.mb5}>Tiền hoàn lại</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy chúng tôi cam kết phục vụ khách hàng những sản phẩm tốt nhất. Mỗi sản phẩm bạn chọn đều được kiểm tra kỹ lưỡng, kiểm tra lỗi và đóng gói một cách cẩn thận nhất. Chúng tôi làm điều này để đảm bảo rằng bạn yêu thích sản phẩm của chúng tôi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Đáng buồn thay, đôi khi chúng tôi có thể không có (các) sản phẩm mà bạn chọn trong kho hoặc có thể gặp một số vấn đề với kiểm tra chất lượng và hàng tồn kho của chúng tôi. Trong những trường hợp như vậy, chúng tôi có thể phải hủy đơn đặt hàng của bạn. Bạn sẽ được thông báo trước về nó để bạn không phải lo lắng không cần thiết về đơn đặt hàng của mình. Nếu bạn đã mua qua hình thức thanh toán Trực tuyến (không phải Giao hàng tận nơi), thì bạn sẽ được hoàn tiền sau khi nhóm của chúng tôi xác nhận yêu cầu của bạn.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi thực hiện kiểm tra chất lượng kỹ lưỡng trước khi xử lý các mục đã đặt hàng. Chúng tôi chăm sóc tối đa trong khi đóng gói sản phẩm. Đồng thời, chúng tôi đảm bảo rằng việc đóng gói tốt để các mặt hàng không bị hư hỏng trong quá trình vận chuyển. Xin lưu ý rằng PP Beauty &amp; Academy không chịu trách nhiệm về những thiệt hại gây ra cho các mặt hàng trong quá trình vận chuyển hoặc vận chuyển.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi tuân thủ các chính sách nhất định để đảm bảo sự minh bạch, hiệu quả và chất lượng chăm sóc khách hàng:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Chúng tôi KHÔNG cho phép trả lại các sản phẩm đã bán - trực tuyến hoặc tại các cửa hàng bán lẻ.</Text>
                            <Text category={"p2"} style={styles.mb5}>Chúng tôi KHÔNG chấp nhận hàng trả lại, vì chúng tôi tin rằng khách hàng sẽ nhận được sản phẩm chất lượng tốt nhất.</Text>
                            <Text category={"p2"} style={styles.mb5}>KKHÔNG được hoàn lại tiền cho bất kỳ giao dịch mua nào được thực hiện - dù là trực tuyến hoặc tại cửa hàng bán lẻ.</Text>
                            <Text category={"p2"} style={styles.mb5}>Chúng tôi KHÔNG khuyến khích trao đổi các sản phẩm của chúng tôi.</Text>
                            <Text category={"p2"} style={styles.mb5}>Chúng tôi KHÔNG tham gia vào việc bán lại các sản phẩm đã qua sử dụng và không khuyến khích những sản phẩm tương tự, vì chúng tôi không thể đảm bảo chất lượng sản phẩm tốt nhất cho khách hàng của mình.</Text>
                        </View>
                        <Text category={"p2"} style={styles.mb5}>Đối với đơn đặt hàng quốc tế:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi KHÔNG hỗ trợ Đổi hoặc Trả hàng.</Text>
                        <Text category={"p2"} style={styles.mb5}>Nếu bạn hủy đơn đặt hàng trước khi chúng tôi xử lý và gửi đơn hàng để vận chuyển, thì khoản tiền hoàn lại có thể được xử lý. Đơn đặt hàng thường mất 1-2 ngày để xử lý trước khi gửi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Đơn đặt hàng đã được vận chuyển không thể được trả lại, hủy bỏ hoặc hoàn lại tiền.</Text>
                        <Text category={"p2"} style={styles.mb5}>Nếu bạn gặp bất kỳ vấn đề nào, vui lòng liên hệ với Nhóm hỗ trợ của chúng tôi ngay lập tức.</Text>
                        </View>
                        <Text category={"h5"} style={styles.mb5}>Sự đồng ý của bạn</Text>
                        <Text category={"p2"} style={styles.mb5}>Bằng cách sử dụng trang web / ứng dụng của chúng tôi, đăng ký tài khoản hoặc mua hàng, theo đây, bạn đồng ý với Chính sách Trả hàng &amp; Hoàn tiền của chúng tôi và đồng ý với các điều khoản của Chính sách này.</Text>

                        <Text category={"h5"} style={styles.mb5}>Các thay đổi đối với chính sách trả lại và hoàn lại tiền của chúng tôi</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có nên cập nhật, sửa đổi hoặc thực hiện bất kỳ thay đổi nào đối với tài liệu này để chúng phản ánh chính xác Dịch vụ và chính sách của chúng tôi hay không. Trừ khi luật pháp yêu cầu khác, những thay đổi đó sẽ được đăng nổi bật tại đây. Sau đó, nếu bạn tiếp tục sử dụng Dịch vụ, bạn sẽ bị ràng buộc bởi Chính sách Trả hàng &amp; Hoàn tiền đã cập nhật. Nếu bạn không muốn đồng ý với điều này hoặc bất kỳ Chính sách Trả hàng &amp; Hoàn tiền đã cập nhật nào, bạn có thể xóa tài khoản của mình.</Text>

                        <Text category={"h5"} style={styles.mb5}>Liên hệ chúng tôi</Text>
                        <Text category={"p2"} style={styles.mb5}>Đừng ngần ngại liên hệ với chúng tôi nếu bạn có bất kỳ câu hỏi nào liên quan đến Tuyên bố từ chối trách nhiệm này.</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Qua Email: contact@ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua số điện thoại: 099 688 228</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua liên kết này: https://ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua địa chỉ: 306/25 Nguyễn Thị Minh Khai P.5, Q.3, TPHCM</Text>
                        </View>
                    </ScrollView>
                :
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 25
                        }}
                    >
                        <Text category={"p2"} style={styles.mb5}>Updated at 2022-01-01</Text>

                        <Text category={"h5"} style={styles.mb5}>Definitions and key terms</Text>
                        <Text category={"p2"} style={styles.mb5}>To help explain things as clearly as possible in this Return &amp; Refund Policy, every time any of these terms are referenced, are strictly defined as:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Cookie: small amount of data generated by a website and saved by your web browser. It is used to identify your browser, provide analytics, remember information about you such as your language preference or login information.</Text>
                        <Text category={"p2"} style={styles.mb5}>Company: when this policy mentions “Company,” “we,” “us,” or “our,” it refers to PP Beauty &amp; Academy, that is responsible for your information under this Return &amp; Refund Policy.</Text>
                        <Text category={"p2"} style={styles.mb5}>Customer: refers to the company, organization or person that signs up to use the PP Beauty &amp; Academy Service to manage the relationships with your consumers or service users.</Text>
                        <Text category={"p2"} style={styles.mb5}>Device: any internet connected device such as a phone, tablet, computer or any other device that can be used to visit PP Beauty &amp; Academy and use the services.</Text>
                        <Text category={"p2"} style={styles.mb5}>Service: refers to the service provided by PP Beauty &amp; Academy as described in the relative terms (if available) and on this platform.</Text>
                        <Text category={"p2"} style={styles.mb5}>Website:  site, which can be accessed via this URL: https://ppbeautyacademy.com</Text>
                        <Text category={"p2"} style={styles.mb5}>You: a person or entity that is registered with PP Beauty &amp; Academy to use the Services.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Return &amp; Refund Policy</Text>
                        <Text category={"p2"} style={styles.mb5}>Thanks for shopping at PP Beauty &amp; Academy. We appreciate the fact that you like to buy the stuff we build. We also want to make sure you have a rewarding experience while you’re exploring, evaluating, and purchasing our products.</Text>
                        <Text category={"p2"} style={styles.mb5}>As with any shopping experience, there are terms and conditions that apply to transactions at PP Beauty &amp; Academy. We’ll be as brief as our attorneys will allow. The main thing to remember is that by placing an order or making a purchase at PP Beauty &amp; Academy, you agree to the terms set forth below along with  Policy.</Text>
                        <Text category={"p2"} style={styles.mb5}>If there’s something wrong with the product/service you bought, or if you are not happy with it, you will not be able to issue a refund for your item.</Text>

                        <Text category={"h5"} style={styles.mb5}>Refunds</Text>
                        <Text category={"p2"} style={styles.mb5}>We at  ourselves to serving our customers with the best products. Every single product that you choose is thoroughly inspected, checked for defects and packaged with utmost care. We do this to ensure that you fall in love with our products.</Text>
                        <Text category={"p2"} style={styles.mb5}>Sadly, there are times when we may not have the product(s) that you choose in stock, or may face some issues with our inventory and quality check. In such cases, we may have to cancel your order. You will be intimated about it in advance so that you don't have to worry unnecessarily about your order. If you have purchased via Online payment (not Cash on Delivery), then you will be refunded once our team confirms your request.</Text>
                        <Text category={"p2"} style={styles.mb5}>We carry out thorough quality check before processing the ordered item. We take utmost care while packing the product. At the same time we ensure that the packing is good such that the items won’t get damaged during transit. Please note that PP Beauty &amp; Academy is not liable for damages that are caused to the items during transit or transportation.</Text>
                        <Text category={"p2"} style={styles.mb5}>We follow certain policies to ensure transparency, efficiency and quality customer care:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>We DO NOT allow returns on sold products - online or in retail outlets.</Text>
                        <Text category={"p2"} style={styles.mb5}>We DO NOT accept returned goods, as we believe that customers should get the best quality products.</Text>
                        <Text category={"p2"} style={styles.mb5}>Refunds are NOT given for any purchases made - be they online or in retail store.</Text>
                        <Text category={"p2"} style={styles.mb5}>We DO NOT encourage exchanges of our products.</Text>
                        <Text category={"p2"} style={styles.mb5}>We DO NOT engage in reselling used products and discourage the same, because we cannot ensure the best quality products for our customers.</Text>
                        </View>
                        <Text category={"p2"} style={styles.mb5}>For International Orders:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>We DO NOT support Exchanges or Returns.</Text>
                            <Text category={"p2"} style={styles.mb5}>If you cancel the order before we process it and dispatch for shipping, a refund can be processed. Orders generally take 1-2 days to process before dispatch.</Text>
                            <Text category={"p2"} style={styles.mb5}>Orders already in shipping cannot be returned, canceled or refunded.</Text>
                            <Text category={"p2"} style={styles.mb5}>If you face any issues, please contact our Support Team immediately.</Text>
                        </View>
                        <Text category={"h5"} style={styles.mb5}>Your Consent</Text>
                        <Text category={"p2"} style={styles.mb5}>By using our website/app, registering an account, or making a purchase, you hereby consent to our Return &amp; Refund Policy and agree to its terms.</Text>

                        <Text category={"h5"} style={styles.mb5}>Changes To Our Return &amp; Refund Policy</Text>
                        <Text category={"p2"} style={styles.mb5}>Should we update, amend or make any changes to this document so that they accurately reflect our Service and policies. Unless otherwise required by law, those changes will be prominently posted here. Then, if you continue to use the Service, you will be bound by the updated Return &amp; Refund Policy. If you do not want to agree to this or any updated Return &amp; Refund Policy, you can delete your account.</Text>

                        <Text category={"h5"} style={styles.mb5}>Contact Us</Text>
                        <Text category={"p2"} style={styles.mb5}>If, for any reason, You are not completely satisfied with any good or service that we provide, don't hesitate to contact us and we will discuss any of the issues you are going through with our product.</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Via Email: contact@ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Via Phone Number: +1 416 792 6268</Text>
                            <Text category={"p2"} style={styles.mb5}>Via this Link:  https://www.ppbeautyacademy.com/en/contact</Text>
                            <Text category={"p2"} style={styles.mb5}>Via this Address: 80 Bloor St W, Suite 401, Toronto, ON Canada. M5S2V1</Text>
                        </View>                       
                    </ScrollView>
                }
            </View>
        </BaseScreen>
    )
}

export default ReturnAndRefund
