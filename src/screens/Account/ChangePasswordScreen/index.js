import {Button, Icon, Input, Spinner, Text} from '@ui-kitten/components'
import {Toast} from 'popup-ui'
import React, {useContext, useState} from 'react'
import {TouchableWithoutFeedback, View} from 'react-native'
import {translate} from '../../../../config/i18n'
import context from '../../../../context/context'
import {HTTP_STATUS_OK} from '../../../assets/constants'
import {commonStyles} from '../../../assets/theme/styles'
import BaseScreen from '../../../component/BaseScreen'
import KeyboardScrollView from '../../../component/KeyboardScrollView'
import {changePassword} from '../../../services/ServiceWorker'
import {styles} from './styles'

const ChangePasswordScreen = ({navigation}) => {
    const {state, actions} = useContext(context)
    const [account, setAccount] = useState({
        oldPassword: '',
        newPassword: '',
        confirmNewPassword: '',
    })
    const [secureOldPassword, setSecureOldPassword] = useState(true)
    const [secureNewPassword, setSecureNewPassword] = useState(true)
    const [secureConfirmPassword, setSecureConfirmPassword] = useState(true)

    const [errors, setErrors] = useState({})
    const [loading, setLoading] = useState(false)

    const SendIndicator = props => (
        <Icon {...props} name="paper-plane-outline" />
    )

    const LoadingIndicator = props => (
        <View style={[props.style, commonStyles.center]}>
            <Spinner size="small" status="basic" />
        </View>
    )

    const renderOldPasswordEyeIcon = props => (
        <TouchableWithoutFeedback
            onPress={() => setSecureOldPassword(!secureOldPassword)}>
            <Icon {...props} name={secureOldPassword ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const renderNewPasswordEyeIcon = props => (
        <TouchableWithoutFeedback
            onPress={() => setSecureNewPassword(!secureNewPassword)}>
            <Icon {...props} name={secureNewPassword ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const renderConfirmPasswordEyeIcon = props => (
        <TouchableWithoutFeedback
            onPress={() => setSecureConfirmPassword(!secureConfirmPassword)}>
            <Icon {...props} name={secureConfirmPassword ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const renderCaption = () => {
        return (
            <View style={styles.captionContainer}>
                <Text style={[styles.captionText, styles.mainFont]}>
                    {translate('login.passwordValidation')}
                </Text>
            </View>
        )
    }

    const renderConfirmCaption = () => {
        return (
            <View style={styles.captionContainer}>
                <Text
                    style={[
                        commonStyles.mainFont,
                        errors.invalidConfirmPassword
                            ? styles.invalidCaptionText
                            : styles.captionText,
                    ]}>
                    {translate('login.passwordConfirmationValidation')}
                </Text>
            </View>
        )
    }

    const handleChangePassword = async () => {
        setLoading(true)
        let isError = false

        if (
            !account ||
            !account.oldPassword ||
            !account.newPassword ||
            !account.confirmNewPassword
        ) {
            setErrors({
                oldPassword: !account.oldPassword,
                newPassword: !account.newPassword,
                confirmNewPassword: !account.confirmNewPassword,
            })

            isError = true
        }

        if (
            account.newPassword &&
            account.newPassword &&
            account.newPassword !== account.confirmNewPassword
        ) {
            setErrors({
                ...errors,
                invalidPassword: true,
            })

            isError = true
        }

        if (!isError) {
            let toSendData = {
                ...account,
                username: state.userInfo?.username,
            }

            const resetPasswordStatus = await changePassword(
                state.version,
                toSendData,
            ).finally(() => setLoading(false))
            if (resetPasswordStatus === HTTP_STATUS_OK) {
                setErrors({})
                Toast.show({
                    title: translate('common.successTitle'),
                    text: translate('account.changePasswordSuccess'),
                    color: '#5BAA22',
                    timing: 3500,
                })
            }
        }

        setLoading(false)
    }

    return (
        <BaseScreen title={translate('account.changePassword')}>
            <KeyboardScrollView
                extraScrollHeight={Platform.OS === 'ios' ? 0 : 100}
                contentStyle={styles.container}>
                <Input
                    textStyle={commonStyles.mainFont}
                    value={account.oldPassword}
                    status={errors.oldPassword && 'danger'}
                    label={translate('common.oldPassword')}
                    placeholder={translate('account.oldPasswordPlaceholder')}
                    accessoryRight={renderOldPasswordEyeIcon}
                    secureTextEntry={secureOldPassword}
                    onChangeText={oldPassword =>
                        setAccount({
                            ...account,
                            oldPassword: oldPassword,
                        })
                    }
                />
                {errors.oldPassword && (
                    <Text category={'label'} status={'danger'}>
                        {translate('errors.emptyOldPassword')}
                    </Text>
                )}
                <View style={commonStyles.separator.row16} />
                <Input
                    textStyle={commonStyles.mainFont}
                    value={account.newPassword}
                    status={
                        (errors.newPassword || errors.invalidPassword) &&
                        'danger'
                    }
                    label={translate('common.newPassword')}
                    placeholder={translate('account.newPasswordPlaceholder')}
                    caption={renderCaption}
                    accessoryRight={renderNewPasswordEyeIcon}
                    secureTextEntry={secureNewPassword}
                    onChangeText={newPassword =>
                        setAccount({
                            ...account,
                            newPassword: newPassword,
                        })
                    }
                />
                {errors.newPassword && (
                    <Text category={'label'} status={'danger'}>
                        {translate('errors.emptyNewPassword')}
                    </Text>
                )}
                <View style={commonStyles.separator.row16} />
                <Input
                    textStyle={commonStyles.mainFont}
                    value={account.confirmNewPassword}
                    status={
                        (errors.confirmNewPassword || errors.invalidPassword) &&
                        'danger'
                    }
                    label={translate('login.confirmPassword')}
                    placeholder={translate(
                        'account.confirmPasswordPlaceholder',
                    )}
                    caption={renderConfirmCaption}
                    accessoryRight={renderConfirmPasswordEyeIcon}
                    secureTextEntry={secureConfirmPassword}
                    onChangeText={confirmNewPassword =>
                        setAccount({
                            ...account,
                            confirmNewPassword,
                        })
                    }
                />
                {errors.confirmNewPassword && (
                    <Text category={'label'} status={'danger'}>
                        {translate('errors.emptyConfirmPassword')}
                    </Text>
                )}
                {errors.invalidPassword && (
                    <Text category={'label'} status={'danger'}>
                        {translate('errors.mismatchConfirmPassword')}
                    </Text>
                )}
                <View style={commonStyles.separator.row16} />
                <View style={commonStyles.separator.row16} />
                <View style={commonStyles.separator.row16} />
                <Button
                    status="primary"
                    onPress={() => handleChangePassword()}
                    style={styles.button}
                    accessoryLeft={loading ? LoadingIndicator : SendIndicator}>
                    {props => (
                        <Text
                            {...props}
                            category="p1"
                            style={[
                                commonStyles.mainFont,
                                commonStyles.lightColor,
                            ]}>
                            {translate('common.submit')}
                        </Text>
                    )}
                </Button>
            </KeyboardScrollView>
        </BaseScreen>
    )
}

export default ChangePasswordScreen
