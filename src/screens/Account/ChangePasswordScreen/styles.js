import {StyleSheet} from 'react-native'
import {COLORS} from '../../../assets/theme/colors'

export const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
    },
    formGroup: {
        marginBottom: '5%',
    },
    button: {
        marginHorizontal: '20%',
    },
    captionContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    captionIcon: {
        width: 10,
        height: 10,
        marginRight: 5,
    },
    captionText: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'opensans-regular',
        color: '#8F9BB3',
    },
    invalidCaptionText: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'opensans-regular',
        color: COLORS.ERROR,
    },
})
