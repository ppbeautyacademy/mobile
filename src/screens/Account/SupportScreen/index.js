import {Input, Text} from '@ui-kitten/components'
import React, {useContext, useRef, useState} from 'react'
import {
    View,
    FlatList,
    TextInput,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    UIManager,
    LayoutAnimation,
} from 'react-native'
import Header from '../../../component/Header'
import {translate} from '../../../../config/i18n'
import context from '../../../../context/context'
import {COLORS} from '../../../assets/theme/colors'
import SafeAreaScreenContainer from '../../../component/SafeAreaScreenContainer'
import {useMount} from '../../../utils/commonHooks'
import styles from './styles'
import Animated, {
    useAnimatedStyle,
    useSharedValue,
} from 'react-native-reanimated'
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler'
import {ScreenHeight} from 'react-native-elements/dist/helpers'
import Icon from 'react-native-vector-icons/MaterialIcons'

if (
    Platform.OS === 'android' &&
    UIManager.setLayoutAnimationEnabledExperimental
) {
    UIManager.setLayoutAnimationEnabledExperimental(true)
}

const renderMessages = ({item, username}) => {
    const messagePosition =
        item?.name === username ? styles.messageSender : styles.messageReceiver
    const textColor =
        item?.name === username ? {} : {color: COLORS.WHITE_BACKGROUND}
    return (
        <View style={{width: '100%'}}>
            <View style={[styles.messageContainer, messagePosition]}>
                <Text style={textColor}>{item?.message}</Text>
            </View>
        </View>
    )
}

const SupportScreenMessages = ({navigation, route}) => {
    const {username} = route.params ?? {}
    const {state} = useContext(context)
    const [keyboardHeight, setKeyboardHeight] = useState(0)
    const [message, setMessage] = useState('')
    const chatListRef = useRef()
    const messages = [
        {
            name: 'A',
            message: 'Hello, How can we help you?',
        },
        {
            name: username ?? state?.userInfo.username,
            message: 'Hi, I have a problem with subscribing to the ABC course',
        },
        {
            name: 'A',
            message: "We've got your message, please wait a moment.",
        },
        {
            name: 'A',
            message: 'Please provide us more detail about your transaction.',
        },
        {
            name: username ?? state?.userInfo.username,
            message: 'Hi, I have a problem with subscribing to the ABC course',
        },
        {
            name: 'A',
            message: "We've got your message, please wait a moment.",
        },
        {
            name: 'A',
            message: 'Please provide us more detail about your transaction.',
        },
        {
            name: username ?? state?.userInfo.username,
            message: 'Hi, I have a problem with subscribing to the ABC course',
        },
        {
            name: 'A',
            message: "We've got your message, please wait a moment.",
        },
        {
            name: 'A',
            message: 'Please provide us more detail about your transaction.',
        },
        {
            name: username ?? state?.userInfo.username,
            message: 'Hi, I have a problem with subscribing to the ABC course',
        },
        {
            name: 'A',
            message: "We've got your message, please wait a moment.",
        },
        {
            name: 'A',
            message: 'Please provide us more detail about your transaction.',
        },
    ]

    const flatListContainerStyle = useAnimatedStyle(() => {
        return {
            flex: 1,
            marginBottom: 80,
        }
    }, [keyboardHeight])

    const handleOnKeyboardShow = keyboardEvent => {
        if (!keyboardEvent) {
            return
        }
        const keyboardHeight = keyboardEvent.endCoordinates.height
        setKeyboardHeight(keyboardHeight)
    }
    const handleOnKeyboardHide = keyboardEvent => {
        if (!keyboardEvent) {
            return
        }
        const keyboardHeight = keyboardEvent.endCoordinates.height
        setKeyboardHeight(keyboardHeight)
    }
    useMount(() => {
        setTimeout(() => chatListRef.current.scrollToEnd({animated: true}), 300)
        const keyboardOnShowListener =
            Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow'
        const keyboardOnHideListener =
            Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide'
        const keyboardShowListener = Keyboard.addListener(
            keyboardOnShowListener,
            handleOnKeyboardShow,
        )
        const keyboardHideListener = Keyboard.addListener(
            keyboardOnHideListener,
            handleOnKeyboardHide,
        )
        return () => {
            keyboardShowListener.remove()
            keyboardHideListener.remove()
        }
    })

    const sendMessage = () => {
        console.log('send message')
    }

    const onFocusInput = () => {
        setTimeout(() => chatListRef.current.scrollToEnd({animated: true}), 300)
    }

    return (
        <SafeAreaScreenContainer
            style={{backgroundColor: COLORS.WHITE_BACKGROUND}}>
            <Header isBackable leftText={translate('common.support')} />
            <KeyboardAvoidingView
                style={{
                    flex: 1,
                }}
                enabled={Platform.OS === 'ios'}
                behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                keyboardVerticalOffset={Platform.select({
                    ios: 40,
                    android: 0,
                })}>
                <Animated.View style={flatListContainerStyle}>
                    <FlatList
                        ref={chatListRef}
                        data={messages}
                        renderItem={e =>
                            renderMessages({
                                ...e,
                                username: username ?? state?.userInfo?.username,
                            })
                        }
                        contentContainerStyle={styles.chatListContainer}
                    />
                </Animated.View>

                <View style={[styles.inputContainer]}>
                    <Input
                        size="large"
                        multiline={true}
                        maxLength={500}
                        numberOfLines={2}
                        accessoryRight={
                            <TouchableOpacity onPress={sendMessage}>
                                <Icon name="send" size={28} />
                            </TouchableOpacity>
                        }
                        value={message}
                        onChangeText={setMessage}
                        placeholder={translate('common.inputPlaceholder')}
                        onFocus={onFocusInput}
                    />
                </View>
            </KeyboardAvoidingView>
        </SafeAreaScreenContainer>
    )
}

export default SupportScreenMessages
