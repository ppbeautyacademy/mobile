import { Input, Text, Spinner as SpinnerKitten } from '@ui-kitten/components';
import moment from 'moment';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Spinner from 'react-native-loading-spinner-overlay';
import context from '../../../../context/context';
import { COLORS } from '../../../assets/theme/colors';
import BaseScreen from '../../../component/BaseScreen';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import { getTicketDetail, replyMessage, saveMessage } from '../../../services/ServiceWorker';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { HTTP_STATUS_OK } from '../../../assets/constants';

// TODO translation
const SupportDetailScreen = ({route, navigation}) => {
    const {ticketId} = route.params;

    const {state, actions} = useContext(context);
    const keyboardAvoidRef = useRef();

    const [toPostMessage, setToPostMessage] = useState({
        subject: "[Response]",
        message: "",
        type: "ticket",
    })
    const [isFocus, setIsFocus] = useState(false);
    const [isSending, setIsSending] = useState(false);

    const [data, setData] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(async() => {
        await loadTicketDetail();
        keyboardAvoidRef.current?.scrollToEnd({animated: true})
    }, [])

    const loadTicketDetail = async() => {
        const ticketData = await getTicketDetail(state.userInfo?.id, ticketId);
        if(ticketData && ticketData.id) {
            setData(ticketData);
            setIsLoaded(true);
        }
    }

    const handleReply = async() => {
        if(toPostMessage.message) {
            actions.showAppSpinner(true)
            const postStatus = await replyMessage(state.userInfo?.id, data?.id, toPostMessage).finally(() => actions.showAppSpinner(false));
            if(postStatus && postStatus === HTTP_STATUS_OK) {
                setToPostMessage({
                    ...toPostMessage,
                    message: ""
                })

                await loadTicketDetail();
                keyboardAvoidRef.current?.scrollToEnd({animated: true})
            }
        }
    }

    return(
            !isLoaded 
        ?
            <Spinner
                visible={true}
                color={COLORS.SUB}
                overlayColor={COLORS.WHITE}
                animation="fade"
            />
        :
            <BaseScreen title='Ticket Detail'>
                <KeyboardAwareScrollView
                    ref={keyboardAvoidRef}
                    keyboardShouldPersistTaps="handled"
                    pointerEvents = 'auto'
                    alwaysBounceVertical={false}
                    enableOnAndroid={true}
                    contentContainerStyle={{
                        flexGrow: 1
                    }}
                    scrollEnabled
                    extraHeight={10}
                > 
                    <ScrollView                      
                        showsVerticalScrollIndicator={false}
                        style={styles.container}
                        contentContainerStyle={{
                            paddingBottom: 20,
                            backgroundColor: COLORS.WHITE
                        }}
                        horizontal={false}
                    >
                        <View style={{flexDirection: "column"}}>
                            <View style={styles.senderWrapper}>
                                <View style={styles.messageWrapper}>
                                    <Text category={"p2"}>{data.message}</Text>
                                    <Text category={"label"} appearance={"hint"}>{data.date ? moment(data.date).format("yyyy-MM-DD HH:mm") : ""}</Text>
                                </View>
                            </View>

                            {data.replies && data.replies.length > 0 &&
                                data.replies.map((item, idx) => (
                                        item.refId === data.refId 
                                    ?
                                        <View style={styles.senderWrapper} key={idx}>
                                            <View style={styles.messageWrapper}>
                                                <Text category={"p2"}>{item.message}</Text>
                                                <Text category={"label"} appearance={"hint"}>{item.date ? moment(item.date).format("yyyy-MM-DD HH:mm") : ""}</Text>
                                            </View>
                                        </View>
                                    :
                                        <View style={styles.replierWrapper} key={idx}>
                                            <View style={styles.messageWrapper}>
                                                <Text category={"p2"}>{item.message}</Text>
                                                <Text category={"label"} appearance={"hint"}>{item.date ? moment(item.date).format("yyyy-MM-DD HH:mm") : ""}</Text>
                                            </View>
                                        </View>
                                ))
                            }                            
                        </View>
                        
                        <View style={{flexDirection: "row", paddingHorizontal: 10}}>
                            <Input 
                                size={"small"}
                                multiline
                                value={toPostMessage.message}
                                onChangeText={(message) => 
                                    setToPostMessage({
                                        ...toPostMessage,
                                        message: message
                                    })
                                }
                                onFocus={() => setIsFocus(true)}
                                onBlur={() => setIsFocus(false)}
                                style={styles.commentInput}
                                placeholder='Input here'
                            />
                            <TouchableOpacity
                                onPress={() => handleReply()}
                            >
                                <View
                                    style={[
                                        styles.reviewBtnContainer,
                                        isFocus ? styles.focusInput : styles.blurInput,
                                    ]}>
                                    {isSending ? (
                                        <SpinnerKitten size="small" />
                                    ) : (
                                        <Icon
                                            name="send"
                                            size={16}
                                            color={
                                                isFocus
                                                    ? COLORS.SUB
                                                    : COLORS.BLACK_HALF_OPACITY
                                            }
                                            style={{
                                                paddingHorizontal: 8,
                                                paddingVertical: 3,
                                            }}
                                        />
                                    )}
                                </View>
                            </TouchableOpacity>
                        </View>
                        
                    </ScrollView>
                </KeyboardAwareScrollView>
            </BaseScreen>
    )
}

export default SupportDetailScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column"
    },
    senderWrapper: {
        flexDirection: 'row',
        justifyContent: "flex-end",
        marginBottom: 15,
        marginRight: 8,
        marginLeft: "20%"
    },
    replierWrapper: {
        flexDirection: 'row',
        marginBottom: 15,
        marginLeft: 8,
        maxWidth: "80%",
        minWidth: "40%"
    },
    messageWrapper: {
        borderRadius: 8,
        backgroundColor: COLORS.LIGHT,
        paddingVertical: 10,
        paddingHorizontal: 16
    },
    commentInput: {
        width: '90%',
        marginRight: 7,
    },
    reviewBtnContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    focusInput: {
        backgroundColor: '#FDF3EB',
    },
    blurInput: {
        backgroundColor: 'rgb(247, 249, 252)',
    },
})