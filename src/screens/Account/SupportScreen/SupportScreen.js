import { Text } from '@ui-kitten/components';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { Badge } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Spinner from 'react-native-loading-spinner-overlay';
import context from '../../../../context/context';
import { COLORS } from '../../../assets/theme/colors';
import BaseScreen from '../../../component/BaseScreen';
import { ScreenIds } from '../../../ScreenIds';
import { getPageMessages } from '../../../services/ServiceWorker';

// TODO translation
const SupportScreen = ({navigation}) => {
    //#region Declaration
    const {state, actions} = useContext(context)

    const [support, setSupport] = useState({
        data: [],
        pageNumber: 0,
        totalRecords: 0,
        rows: 6,
        totalPages: 0,
    });

    const [isLoaded, setIsLoaded] = useState(false);
    //#endregion

    //#region Load Data
    useEffect(async() => {
        await loadSupportTickets();
    }, [])

    const loadSupportTickets = async() => {
        const supportData = await getPageMessages(state.userInfo?.id, support.pageNumber, support.rows);
        if(supportData && supportData.content) {
            setSupport({
                data: supportData.content,
                pageNumber: supportData.pageable && supportData.pageable.pageNumber,
                rows: supportData.pageable && supportData.pageable.pageSize,
                totalRecords: supportData.totalElements,
                totalPages: supportData.totalPages,
            })
        } 
        setIsLoaded(true)
    }

    const fetchMore = async () => {
        if (support.pageNumber + 1 >= support.totalPages) {
            return
        }

        const supportData = await getPageMessages(
            state.userInfo?.id,
            support.pageNumber + 1,
            support.rows
        )
        if (supportData && supportData.content) {
            setSupport({
                data: support.data.concat(supportData.content),
                pageNumber: supportData.pageable && supportData.pageable.pageNumber,
                rows: supportData.pageable && supportData.pageable.pageSize,
                totalRecords: supportData.totalElements,
                totalPages: supportData.totalPages,
            })
        }
    }
    //#endregion

    //#region Render
    const renderItem = ({item}) => {
        return(
            <TouchableOpacity onPress={() => handleViewTicketDetail(item.id)}>
                <View style={styles.itemContainer}>
                    <View style={styles.itemWrapper}>
                        <Text category={"p1"}>{item.subject}</Text>
                        <Text>
                            <Text category={"p2"} appearance={"hint"}>Created Date : </Text>
                            <Text category={"p2"}>{item.date ? moment(item.date).format("yyyy-MM-DD HH:mm") : ""}</Text>
                        </Text>
                        <Text>
                            <Text category={"p2"} appearance={"hint"}>Status : </Text>
                            {renderTicketStatus(item.statusValue)}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    const handleViewTicketDetail = (ticketId) => {
        navigation.navigate(ScreenIds.SupportDetailScreen, {
            ticketId: ticketId
        })
    }

    const renderTicketStatus = (status) => {
        if(status) {
            if(status === 'new_msg') {
                return (
                    <Text category={"p2"}>
                        <Text>{` `}</Text>
                        <Badge
                            status={"primary"}
                        />
                        <Text>{` `}</Text>
                        <Text category={"p2"}>Chưa được trả lời</Text>
                    </Text>
                )
            } else if(status === 'replied') {
                return (
                    <Text>
                        <Text>{` `}</Text>
                        <Badge
                            status={"success"}
                        />
                        <Text>{` `}</Text>
                        <Text category={"p2"}>Đã được trả lời</Text>
                    </Text>
                )
            } else if(status === 'danger') {
                return (
                    <Text>
                        <Text>{` `}</Text>
                        <Badge
                            status={"primary"}
                        />
                        <Text>{` `}</Text>
                        <Text category={"p2"}>Đã đóng</Text>
                    </Text>
                )
            }
        }
    }

    //#endregion

    return(
            !isLoaded 
        ?
            <Spinner
                visible={true}
                color={COLORS.SUB}
                overlayColor={COLORS.WHITE}
                animation="fade"
            />
        :
            <BaseScreen title='Support Tickets'>
                <View style={styles.container}>
                    {support.data && support.data.length > 0 ?
                        <FlatList 
                            horizontal={false}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={[
                                {
                                    flexGrow: 1,
                                    backgroundColor: COLORS.WHITE,
                                    paddingBottom: 80,
                                },
                            ]}
                            numColumns={1}
                            data={support.data}
                            renderItem={item =>
                                renderItem(item)
                            }
                            keyExtractor={item => item.id}
                            onEndReached={fetchMore}
                            onEndReachedThreshold={0.7}
                        />
                    :
                        <View style={styles.emptyContainer}>
                            <FastImage 
                                source={require("../../../assets/images/empty-records.png")}
                                style={{
                                    height: 150,
                                    width: 200,
                                    marginBottom: 20
                                }}
                            />
                            <Text>Currently no support tickets</Text>
                        </View>
                    }
                </View>
            </BaseScreen>
    )
}

export default SupportScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    emptyContainer: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "20%"
    },
    itemContainer: {
        flexDirection: "row",
        padding: 10
    },
    itemWrapper: {
        padding: 16,
        width: "100%",
        borderRadius: 10,
        backgroundColor:"#FFF",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        
        elevation: 3,
    }
})