import React from 'react'
import {StyleSheet} from 'react-native'
import {COLORS} from '../../../assets/theme/colors'

const styles = StyleSheet.create({
    chatListContainer: {
        flexGrow: 1,
    },
    messageContainer: {
        padding: 12,
        borderRadius: 20,
        marginHorizontal: 16,
        marginVertical: 8,
        maxWidth: '70%',
    },
    messageSender: {
        backgroundColor: COLORS.MAIN,
        alignSelf: 'flex-end',
    },
    messageReceiver: {
        backgroundColor: COLORS.SUB,
        alignSelf: 'flex-start',
    },
    inputContainer: {
        position: 'absolute',
        bottom: 8,
        left: 16,
        right: 16,
    },
})

export default styles
