import React, { useContext } from 'react'
import { View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import context from '../../../../context/context';
import { COLORS } from '../../../assets/theme/colors';
import BaseScreen from '../../../component/BaseScreen';
import { ScreenIds } from '../../../ScreenIds';
import {styles} from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Text } from '@ui-kitten/components';
import { LANGUAGE_CODES } from '../../../assets/constants';
import WebViewComp from '../../../component/Webview';

const AboutScreen = ({navigation}) => {
    const {state} = useContext(context);

    return(
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View style={{marginRight: 20}}>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.Account)
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text category="h6">
                        {state.version === LANGUAGE_CODES.vi.value ? 'Về chúng tôi' : 'About Us'}
                    </Text>
                </View>

                <WebViewComp 
                    uri={state.version === LANGUAGE_CODES.vi.value ? 'https://www.ppbeautyacademy.com/vi/gioi-thieu' : 'https://www.ppbeautyacademy.com/en/about-us'} />
            </View>
        </BaseScreen>
    )
}

export default AboutScreen;