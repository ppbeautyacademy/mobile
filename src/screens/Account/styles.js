import {Dimensions, StyleSheet} from 'react-native'

import {COLORS} from '../../assets/theme/colors'
import {commonStyles} from '../../assets/theme/styles'

export const AVATAR_SIZE = 120

export const styles = StyleSheet.create({
    //#region Account Top Settings
    headerStyle: {
        backgroundColor: COLORS.MAIN,
    },
    headerRightContainer: {
        flexDirection: 'row',
    },
    headerLeftContainer: {
        width: 100,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        backgroundColor: COLORS.MAIN,
        borderRadius: 20,
        overflow: 'hidden',
    },
    headerLeftText: {
        color: COLORS.BLACK,
        textAlign: 'center',
        flex: 1,
    },
    headerLeftSeparator: {
        height: '100%',
        width: 1,
        backgroundColor: COLORS.GRAY_82,
    },
    headerContainer: {
        width: '100%',
        padding: 16,
        alignItems: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    //#endregion

    //#region Avatar
    avatar: {
        width: AVATAR_SIZE,
        height: AVATAR_SIZE,
        borderRadius: AVATAR_SIZE / 2,
        backgroundColor: COLORS.WHITE_BACKGROUND,
    },
    avatarContainer: {
        ...commonStyles.center
    },
    //#endregion

    //#region Body Settings
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    name: {
        fontSize: 16,
        fontWeight: '700',
    },
    featureName: {
        fontSize: 16,
    },
    accountCardContainer: {
        width: '100%',
        minHeight: '50%',
        paddingBottom: 16,
        backgroundColor:COLORS.WHITE
    },
    cardContainer: {
        position: 'absolute',
        bottom: 16,
        left: 32,
        right: 32,
        backgroundColor:COLORS.WHITE
    },
    cardBodyContainer: {
        ...commonStyles.center,
        backgroundColor: COLORS.TRANSPARENT,
        marginTop: 140
    },
    sectionHeader: {
        paddingTop: 15
    },
    separatorLine: {
        width: 16,
        alignSelf: 'center',
    },
    accountInfoContainer: {
        ...commonStyles.center,
        paddingVertical: 16
    },
    nameText: {
        fontSize: 16,
        fontWeight: '800',
        color: COLORS.BLACK,
    },
    emailText: {
        fontSize: 14,
        color: COLORS.BLACK,
    },
    itemContainer: {
        width: '50%',
        backgroundColor:"#FFF",
        borderRadius:10,
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        
        elevation: 4,
        padding: 10
    },
    modalItemContainer: {
        marginTop: 15,
        marginBottom: 15,
        justifyContent: 'center'
    },
    featureDescriptionContainer: {
        flexDirection: 'row'
    },
    featureTitleText: {
        fontSize: 16,
    },
    featureSubTitleText: {
        textAlign: 'center',
        fontSize: 14,
    },
    loginText: {
        fontSize: 15,
        color: COLORS.BLACK,
        fontWeight: '700',
    },
    languageModalContainer: {
        height: 200,
        width: '100%',
        marginBottom: 50,
        backgroundColor: COLORS.WHITE,
        borderRadius: 10,
        padding: 16,
        ...commonStyles.center,
    },
    optionText: {
        fontSize: 14,
        color: COLORS.WHITE,
        fontWeight: '700',
    },
    separatorHeight50: {
        height: 50,
    },
    modalFeatureContent: {
        marginVertical: 35,
        paddingHorizontal: 20

    },
    settingModalContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: COLORS.WHITE,
        borderRadius: 10,
        paddingBottom: 70,
    },
    logoutContainer: {
        width: '100%',
        paddingVertical: 24,
        alignItems: 'center',
        backgroundColor: COLORS.SUB,
    },
    rowFeatureContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingHorizontal: 15,
        paddingVertical: 12
    },
    iconCamera: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        height: 30,
        width: 30,
    },
    //#endregion
    /********* Modal *********/
    handleModalStyle: {
        marginTop:30,
        backgroundColor:COLORS.SUB,
        width:80
    }, 
    /******* favorite *********/
    itemFeatureContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundMain: {
        backgroundColor: 'rgba(253, 243, 235, 1)',
    },
    arrowRight: {
        position: 'absolute',
        right: 0,
        justifyContent: 'center',
        zIndex: 10

    },
    // Background Video
    backgroundVideo: {
        height: "35%",
        position: "absolute",
        top: 0,
        left: 0,
        alignItems: "stretch",
        bottom: 0,
        right: 0
    }
})
