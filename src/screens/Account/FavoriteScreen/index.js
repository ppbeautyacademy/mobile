import {Text} from '@ui-kitten/components'
import React, {useContext, useEffect, useState} from 'react'
import {FlatList, View} from 'react-native'
import {translate} from '../../../../config/i18n'
import context from '../../../../context/context'
import BaseScreen from '../../../component/BaseScreen'
import {getUserFavorites, removeFavorite} from '../../../services/ServiceWorker'
import {StyleSheet} from 'react-native'
import FastImage from 'react-native-fast-image'
import {
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {COLORS} from '../../../assets/theme/colors'
import {HTTP_STATUS_OK} from '../../../assets/constants'
import {Toast} from 'popup-ui'
import {ScreenIds} from '../../../ScreenIds'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

const FavoriteScreen = ({navigation}) => {
    const {state} = useContext(context)

    const [favorites, setFavorites] = useState([
        {}, {}, {}, {}, {}, {}
    ]);
    const [isFavLoaded, setIsFavLoaded] = useState(false);

    const loadUserFavorites = async() => {
        const favData = await getUserFavorites(state.userInfo?.id, state.version);
        if(favData && Array.isArray(favData)) {
            setFavorites(favData);

            setIsFavLoaded(true)
        }
    }

    useEffect(async () => {
        await loadUserFavorites()
    }, [])

    const renderItem = ({item, index}) => {
        return (
            <View
                style={{
                    paddingHorizontal: 10,
                    borderTopColor: 'transparent',
                }}>
                <View style={styles.itemContainer} key={index}>
                    <View style={styles.itemContentWrapper}>
                        {isFavLoaded ? (
                            <React.Fragment>
                                <FastImage
                                    source={{uri: item.mainImage}}
                                    style={{
                                        height: 60,
                                        width: 60,
                                        borderRadius: 10,
                                        marginRight: 15,
                                    }}
                                />
                                <View
                                    style={{
                                        justifyContent: 'center',
                                    }}>
                                    <TouchableWithoutFeedback
                                        onPress={() =>
                                            handleViewFavorite(
                                                item.type,
                                                item.version,
                                                item.alias,
                                            )
                                        }>
                                        <Text numberOfLines={1}>
                                            {item.name}
                                        </Text>
                                        <Text
                                            category="label"
                                            appearance="hint">
                                            {renderFavoriteType(item.type)}
                                        </Text>
                                    </TouchableWithoutFeedback>
                                </View>
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                <SkeletonPlaceholder>
                                    <View style={{flexDirection: 'row'}}>
                                        <View
                                            style={{
                                                width: 60,
                                                height: 60,
                                                borderRadius: 10,
                                            }}></View>
                                        <View
                                            style={{
                                                justifyContent: 'center',
                                                marginLeft: 10,
                                            }}>
                                            <View
                                                style={{
                                                    width: 60,
                                                    height: 30,
                                                    marginBottom: 5,
                                                    width: 260,
                                                }}></View>
                                            <View
                                                style={{
                                                    width: 60,
                                                    height: 20,
                                                    width: 260,
                                                }}></View>
                                        </View>
                                    </View>
                                </SkeletonPlaceholder>
                            </React.Fragment>
                        )}
                    </View>

                    {isFavLoaded && (
                        <View
                            style={{
                                position: 'absolute',
                                right: 10,
                                top: '45%',
                                alignItems: 'center',
                                zIndex: 10,
                            }}>
                            <TouchableOpacity
                                onPress={() => handleRemoveFavorite(item.id)}>
                                <View
                                    style={[
                                        styles.favoriteContainer,
                                        styles.backgroundRed,
                                    ]}>
                                    <Icon
                                        name="remove-circle"
                                        size={16}
                                        color={'#DB646A'}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                    )}
                </View>
            </View>
        )
    }

    const handleViewFavorite = (type, version, alias) => {
        let isError = false
        if (state.version !== version) {
            Toast.show({
                title: translate('common.errorTitle'),
                text: (
                    <Text>
                        {translate('account.itemNotAvailableInYourCountry')}
                    </Text>
                ),
                color: '#D8A62B',
                timing: 3500,
            })

            isError = true
        }

        if (!type || !alias) {
            Toast.show({
                title: translate('common.errorTitle'),
                text: translate('account.itemHasBeenRemoved'),
                color: '#DB646A',
                timing: 3500,
            })

            isError = true
        }

        if (!isError) {
            switch (type) {
                case 'product':
                    navigation.navigate(ScreenIds.ProductDetail, {alias: alias})
                    break
                case 'course':
                    navigation.navigate(ScreenIds.CourseDetail, {alias: alias})
                    break
                // case "blog":
                //     navigation.navigate(ScreenIds., {alias: alias})
                //     break;
            }
        }
    }

    const renderFavoriteType = type => {
        if (!type) {
            return null
        }

        let typeName = ''

        // TODO translation
        switch (type) {
            case 'product':
                typeName = 'Product'
                break
            case 'course':
                typeName = 'Course'
                break
            case 'blog':
                typeName = 'Blog'
                break
        }

        return typeName
    }

    const handleRemoveFavorite = async id => {
        const doDelete = await removeFavorite(id)

        if (doDelete === HTTP_STATUS_OK) {
            const filteredData = favorites.filter(item => item.id !== id)
            setFavorites(filteredData)
        }
    }

    return (
        <BaseScreen title={translate('common.favorite')}>
            <View style={styles.container}>
                {isFavLoaded && favorites.length < 1 ?
                    <View style={styles.emptyContainer}>
                        <FastImage 
                            source={require("../../../assets/images/empty-records.png")}
                            style={{
                                height: 150,
                                width: 200,
                                marginBottom: 20
                            }}
                        />
                        <Text>Currently no data found for favorites</Text>
                    </View>
                :
                    <FlatList
                        key={'#'}
                        keyExtractor={(e, i) => i}
                        data={favorites}
                        renderItem={(item, index) => {
                            return renderItem(item, index)
                        }}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 30,
                        }}
                    />     
                }
            </View>
        </BaseScreen>
    )
}

export default FavoriteScreen

const styles = StyleSheet.create({
    /********* Card *********/
    container: {
        flex: 1,
        marginHorizontal: 10,
    },
    itemContainer: {
        width: '100%',
        backgroundColor: '#FFF',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        marginVertical: 8,
        height: 80,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 10,
    },
    itemContentWrapper: {
        flexDirection: 'row',
    },
    emptyContainer: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "20%"
    },
    /******** Favorite *********/
    favoriteContainer: {
        width: 25,
        height: 25,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundRed: {
        backgroundColor: 'rgba(255, 141, 137, 0.24)',
    },
})
