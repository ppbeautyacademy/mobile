import React, {useContext, useEffect, useState} from 'react'
import OpenAppSettings from 'react-native-app-settings'

import {translate} from '../../../../config/i18n'
import ImagePicker from '../../../component/ImagePicker'
import {
    updateUserAvatar,
    uploadImageToServer,
} from '../../../services/ServiceWorker'
import ImagePickerUtils from '../../../utils/ImagePickerUtils'
import RNFS from 'react-native-fs'
import context from '../../../../context/context'
import {HTTP_STATUS_OK, uploadImageTypes} from '../../../assets/constants'

const DEFAULT_EDITTING_ID = -1
const limitNumberImages = 5
const maxFileSize = 5 * 1024 * 1024
const validateUpdateImages = (
    images,
    selectedImages,
    editingId = DEFAULT_EDITTING_ID,
) => {
    let validateNumber =
        images.length + selectedImages.length > limitNumberImages // Subtract add image
    if (editingId !== DEFAULT_EDITTING_ID) {
        validateNumber = images.length > limitNumberImages // Subtract add image
    }
    let isOverFileSize = false
    let isWrongMime = false
    selectedImages.forEach(element => {
        if (element.size && element.size > maxFileSize) {
            isOverFileSize = true
        }
        if (element.mime && !ImagePickerUtils.checkMime(element)) {
            isWrongMime = true
        }
    })
    return validateNumber || isOverFileSize || isWrongMime
}

const mapDefaultImages = defaultImages => {
    const imagesStringURLArray = [...defaultImages]?.map(e => e.url || e.uri)
    const formattedImagesArray = ImagePickerUtils.mapImagesUrisToUI(
        imagesStringURLArray,
        0,
    )
    return formattedImagesArray
}

const UploadImage = ({
    buttonTitle = translate('account.clickToUpload'),
    defaultImages,
    onImageChange = () => {},
    errorCode,
    isDisabled = false,
    isShowOnly = false,
    onWrongMime = () => {},
    getShowPicker,
    uploadType,
    displayImage
}) => {
    const {state, actions} = useContext(context)
    const [images, setImages] = useState(mapDefaultImages([]))
    const [editingId, setEditingId] = useState(DEFAULT_EDITTING_ID)
    const [isMaxImageUpload, setIsMaxImageUpload] = useState(false)
    const setDefaultData = () => {
        if (defaultImages) {
            const imagesStringURLArray = [...defaultImages]?.map(e => e.url)
            const formattedImagesArray = ImagePickerUtils.mapImagesUrisToUI(
                imagesStringURLArray,
                0,
            )
            setImages(formattedImagesArray)
            setIsMaxImageUpload(
                formattedImagesArray.length >= limitNumberImages,
            )
        }
    }
    useEffect(setDefaultData, [defaultImages])

    const linkToSetting = () => {
        OpenAppSettings.open()
    }
    const handleOnNoPermission = () => {
        actions.showAppPopup({
            isVisible: true,
            message: translate('errors.cameraForbidden'),
            cancelText: translate('common.close'),
            onOkHandler: linkToSetting,
        })
    }

    const StartUploadImage = async imagePicked => {
        let base64ImageContent
        await RNFS.readFile(imagePicked[0].uri, 'base64').then(data => {
            // binary data
            base64ImageContent = data
        })
        if (base64ImageContent) {
            var formData = new FormData()
            formData.append('image', base64ImageContent)

            const response = await uploadImageToServer(formData)
            if (
                response &&
                response.status === 200 &&
                response.data &&
                response.data.display_url
            ) {
                const avatarUrl = response.data.display_url

                if(uploadType && uploadType === uploadImageTypes.submission.value) {
                    displayImage(avatarUrl)
                } else {
                    const resUploadPhoto = await updateUserAvatar(
                        state?.userInfo?.id,
                        avatarUrl,
                    )
                    if (resUploadPhoto === HTTP_STATUS_OK) {
                        actions.setUserInfo({...state?.userInfo, avatar: avatarUrl})
                    }
                }
            }
        } else {
            actions.showAppPopup({
                isVisible: true,
                message: translate('errors.commonError'),
            })
        }
    }

    const onSelectedImages = async (imageSources, _editingId) => {
        if (validateUpdateImages(images, imageSources, _editingId)) {
            onErrorUploadImages({
                message: translate('errors.errorPickPhotoLargeSize'),
            })
            return
        }
        const mappedImageUris = imageSources.map(e =>
            ImagePickerUtils.getImageSource(e.path),
        )
        setEditingId(_editingId)
        actions.showAppSpinner(true)
        await StartUploadImage(mappedImageUris)
        actions.showAppSpinner(false)
    }

    const onDeleteImage = sortedImages => {
        setImages(sortedImages)
        onImageChange(sortedImages)
        setIsMaxImageUpload(sortedImages.length >= limitNumberImages)
    }

    return (
        <ImagePicker
            getShowPicker={getShowPicker}
            defaultImages={images}
            onWrongMime={onWrongMime}
            errorCode={errorCode}
            buttonTitle={buttonTitle}
            isDisabled={isDisabled}
            isMaxImageUpload={isMaxImageUpload}
            isShowOnly={isShowOnly}
            onNoPermission={handleOnNoPermission}
            onSelectedImages={onSelectedImages}
            onDeleteImage={onDeleteImage}
        />
    )
}

export default UploadImage
