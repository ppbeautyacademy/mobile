import { Toggle, Divider, Text } from '@ui-kitten/components';
import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import context from '../../../../context/context';
import { FACEBOOK_SIGN_IN_APP_ID, GOOGLE_SIGN_IN_CLIENT_ID, HTTP_STATUS_OK } from '../../../assets/constants';
import BaseScreen from '../../../component/BaseScreen';
import { getLinkedSocialAccounts, linkWithFacebook, linkWithGoogle, unlinkSocialAccount } from '../../../services/ServiceWorker';
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-google-signin/google-signin';
import { Settings } from 'react-native-fbsdk-next';
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk-next';
import auth from '@react-native-firebase/auth';

const socialTypes = {
    google: "google",
    facebook: "facebook"
}

const SocialConnectedScreen = () => {
    //#region Declaration
    const {state, actions} = useContext(context);

    const [facebookSocial, setFacebookSocial] = useState({});
    const [googleSocial, setGoogleSocial] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);
    //#endregion

    //#region Social Accounts
    useEffect(async() => {
        await loadSocialAccountsLinked()
    }, [])

    const loadSocialAccountsLinked = async() => {
        const socialLinkedAccounts = await getLinkedSocialAccounts(state.userInfo?.id);
        if(socialLinkedAccounts && Array.isArray(socialLinkedAccounts)) {
            for(let i = 0; i < socialLinkedAccounts.length; i++) {
                if(socialLinkedAccounts[i].provider === socialTypes.facebook) {
                    setFacebookSocial(socialLinkedAccounts[i]);
                } else if(socialLinkedAccounts[i].provider === socialTypes.google) {
                    setGoogleSocial(socialLinkedAccounts[i]);
                }
            }
        }
        setIsLoaded(true)
    }   

    const handleUnlinkSocialAccount = async(type) => {
        if(type === socialTypes.facebook) {
            const unlinkStatus = await unlinkSocialAccount(state.userInfo?.id, facebookSocial.id, facebookSocial.provider);
            if(unlinkStatus === HTTP_STATUS_OK) {
                setFacebookSocial({});
                await auth().signOut();
            }
        } else if(type === socialTypes.google) {
            const unlinkStatus = await unlinkSocialAccount(state.userInfo?.id, googleSocial.id, googleSocial.provider);
            if(unlinkStatus === HTTP_STATUS_OK) {
                setGoogleSocial({});
                await auth().signOut();
            }
        }
    }
    //#endregion

    //#region GOOGLE
    useEffect(() => {
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly', 'email'],
            webClientId: GOOGLE_SIGN_IN_CLIENT_ID,
            offlineAccess: false,
            forceCodeForRefreshToken: true,
        })
    },[])

    const handleLinkWithGoogle = async() => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            const {idToken} = userInfo;

            // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);

            // Sign-in the user with the credential
            const userData = await auth().signInWithCredential(googleCredential);

            if(userData && userData.additionalUserInfo) {
                actions.showAppSpinner(true)
                if(userData.additionalUserInfo?.profile) {
                    let toPostData = {
                        email: userData.additionalUserInfo?.profile.email,
                        name: userData.additionalUserInfo?.profile.name,
                        googleId: userData.additionalUserInfo?.profile.sub,
                        imageUrl: userData.additionalUserInfo?.profile.picture
                    }

                    const linkStatus = await linkWithGoogle(state.userInfo?.id, state.version, toPostData);
                    if(linkStatus === HTTP_STATUS_OK) {
                        await loadSocialAccountsLinked();
                    }
                }
                actions.showAppSpinner(false)
            }
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // when user cancels sign in process,
                Alert.alert('Process Cancelled');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // when in progress already
                Alert.alert('Process in progress');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // when play services not available
                Alert.alert('Play services are not available');
            } else {
                // some other error
                Alert.alert('Something else went wrong... ', error.toString());
                setError(error);
            }
        }
    }
    //#endregion

    //#region FACEBOOK
    useEffect(async() => {
        await Settings.setAppID(FACEBOOK_SIGN_IN_APP_ID);
        await Settings.initializeSDK();
    }, [])

    const handleLinkWithFacebook = async() => {
        try {
            // Attempt login with permissions
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

            if (result.isCancelled) {
              throw 'User cancelled the login process';
            }

            // Once signed in, get the users AccesToken
            const data = await AccessToken.getCurrentAccessToken();
            
            if (!data) {
                throw 'Something went wrong obtaining access token';
            }

            // Create a Firebase credential with the AccessToken
            const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

            // Sign-in the user with the credential
            const userData = await auth().signInWithCredential(facebookCredential);

            if(userData && userData.additionalUserInfo) {
                actions.showAppSpinner(true)
                if(userData.additionalUserInfo?.profile) {
                    let toPostData = {
                        email: userData.additionalUserInfo?.profile.email,
                        name: userData.additionalUserInfo?.profile.name,
                        googleId: userData.additionalUserInfo?.profile.id,
                        imageUrl: userData.additionalUserInfo?.profile.picture.imageUrl
                    }

                    const linkStatus = await linkWithFacebook(state.userInfo?.id, state.version, toPostData);
                    if(linkStatus === HTTP_STATUS_OK) {
                        await loadSocialAccountsLinked();
                    }
                }
                actions.showAppSpinner(false)
            }
        } catch(error) {
            console.log({error});
        }
    }
    //#endregion

    return(
        <BaseScreen title='Social Connected'>
            <View style={styles.container}>
                <Divider style={styles.sectionSpacing}/>
                <View style={styles.itemContainer}>
                    <View style={{flexDirection: "row"}}>
                        <FastImage 
                            source={require("../../../assets/images/google-logo-sm.png")}
                            style={styles.image}
                        />
                        <Text style={styles.text}>
                            {googleSocial.id ? "Linked with Google" : "Connect to Google"}
                        </Text>
                    </View>
                    <View>
                        <Toggle
                            disabled={!isLoaded}
                            checked={googleSocial.id ? true : false}
                            onChange={() => {
                                googleSocial.id ? handleUnlinkSocialAccount(socialTypes.google) : handleLinkWithGoogle()
                            }}>
                        </Toggle>
                    </View>
                </View>
                <Divider style={styles.sectionSpacing}/>
                <View style={styles.itemContainer}>
                    <View style={{flexDirection: "row"}}>
                        <FastImage 
                            source={require("../../../assets/images/facebook-logo-sm.png")}
                            style={styles.image}
                        />
                        <Text  style={styles.text}>
                            {facebookSocial.id ? "Linked with Facebook" : "Connect to Facebook"}
                        </Text>
                    </View>
                    <View>
                        <Toggle
                            disabled={!isLoaded}
                            checked={facebookSocial.id ? true : false}
                            onChange={() => {
                                facebookSocial.id ? handleUnlinkSocialAccount(socialTypes.facebook) : handleLinkWithFacebook()
                            }}>
                        </Toggle>
                    </View>
                </View>
                <Divider style={styles.sectionSpacing}/>
            </View>
        </BaseScreen>
    )
}

export default SocialConnectedScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 16
    },
    itemContainer: {
        margin: 10,
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    text: {
        alignSelf: "center"
    },
    sectionSpacing: {
        marginVertical: 10
    },
    image: {
        height: 30, 
        width: 30,
        marginRight: 10
    }
})