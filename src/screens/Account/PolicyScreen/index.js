import { Text } from '@ui-kitten/components'
import React, { useContext } from 'react'
import {View} from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import BaseScreen from '../../../component/BaseScreen'
import { ScreenIds } from '../../../ScreenIds'
import {styles} from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../../assets/theme/colors'
import { translate } from '../../../../config/i18n'
import context from '../../../../context/context'
import { LANGUAGE_CODES } from '../../../assets/constants'

const PolicyScreen = ({navigation}) => {
    const {state} = useContext(context);

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View style={{marginRight: 20}}>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.Account)
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text category="h6">
                        {state.version === LANGUAGE_CODES.vi.value ? 'Chính sách bảo mật' : 'Privacy Policy'}
                    </Text>
                </View>
                
                {state.version === LANGUAGE_CODES.vi.value ?
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 25
                        }}
                    >
                        <Text category={"p2"} style={styles.mb5}>Cập nhật ngày 2022-01-01</Text>

                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy (“chúng tôi,” “của chúng tôi” hoặc “chúng tôi”) cam kết bảo vệ sự riêng tư của bạn. Chính sách Bảo mật này giải thích cách PP Beauty &amp; Academy thu thập, sử dụng và tiết lộ thông tin cá nhân của bạn.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chính sách Bảo mật này áp dụng cho trang web của chúng tôi và các miền phụ liên quan của nó (gọi chung là “Dịch vụ” của chúng tôi) cùng với ứng dụng của chúng tôi, PP Beauty &amp; Academy. Bằng cách truy cập hoặc sử dụng Dịch vụ của chúng tôi, bạn có nghĩa là bạn đã đọc, hiểu và đồng ý với việc chúng tôi thu thập, lưu trữ, sử dụng và tiết lộ thông tin cá nhân của bạn như được mô tả trong Chính sách Bảo mật này và Điều khoản Dịch vụ của chúng tôi.</Text>

                        <Text category={"h5"} style={styles.mb5}>Định nghĩa và thuật ngữ chính</Text>
                        <Text category={"p2"} style={styles.mb5}>Để giúp giải thích mọi thứ rõ ràng nhất có thể trong Chính sách quyền riêng tư này, mỗi khi bất kỳ điều khoản nào trong số này được tham chiếu, đều được xác định rõ ràng là:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Cookie: một lượng nhỏ dữ liệu được tạo bởi một trang web và được lưu bởi trình duyệt web của bạn. Nó được sử dụng để xác định trình duyệt của bạn, cung cấp phân tích, ghi nhớ thông tin về bạn như tùy chọn ngôn ngữ hoặc thông tin đăng nhập của bạn.</Text>
                        <Text category={"p2"} style={styles.mb5}>Công ty: khi chính sách này đề cập đến “Công ty”, “chúng tôi”, “chúng tôi” hoặc “của chúng tôi”, nghĩa là PP Beauty &amp; Academy, (306/25 Nguyễn Thị Minh Khai P.5, Q.3, TPHCM), chịu trách nhiệm về thông tin của bạn theo Chính sách bảo mật này.</Text>
                        <Text category={"p2"} style={styles.mb5}>Quốc gia: nơi PP Beauty &amp; Academy hoặc chủ sở hữu / người sáng lập PP Beauty &amp; Academy có trụ sở, trong trường hợp này là Việt Nam</Text>
                        <Text category={"p2"} style={styles.mb5}>Khách hàng: đề cập đến công ty, tổ chức hoặc cá nhân đăng ký sử dụng Dịch vụ PP Beauty &amp; Academy để quản lý các mối quan hệ với người tiêu dùng hoặc người sử dụng dịch vụ của bạn.</Text>
                        <Text category={"p2"} style={styles.mb5}>Thiết bị: mọi thiết bị có kết nối internet như điện thoại, máy tính bảng, máy tính hoặc bất kỳ thiết bị nào khác có thể được sử dụng để đến PP Beauty &amp; Academy và sử dụng các dịch vụ.</Text>
                        <Text category={"p2"} style={styles.mb5}>Địa chỉ IP: Mọi thiết bị được kết nối với Internet đều được gán một số được gọi là địa chỉ giao thức Internet (IP). Những con số này thường được ấn định trong các khối địa lý. Địa chỉ IP thường có thể được sử dụng để xác định vị trí mà thiết bị đang kết nối với Internet.</Text>
                        <Text category={"p2"} style={styles.mb5}>Nhân sự: là những cá nhân được PP Beauty &amp; Academy tuyển dụng hoặc theo hợp đồng thực hiện dịch vụ thay mặt cho một trong các bên.</Text>
                        <Text category={"p2"} style={styles.mb5}>Dữ liệu Cá nhân: bất kỳ thông tin nào trực tiếp, gián tiếp hoặc liên quan đến thông tin khác - bao gồm cả số nhận dạng cá nhân - cho phép nhận dạng hoặc nhận dạng một thể nhân.</Text>
                        <Text category={"p2"} style={styles.mb5}>Dịch vụ: đề cập đến dịch vụ do PP Beauty &amp; Academy cung cấp như được mô tả trong các điều khoản tương đối (nếu có) và trên nền tảng này.</Text>
                        <Text category={"p2"} style={styles.mb5}>Dịch vụ của bên thứ ba: đề cập đến các nhà quảng cáo, nhà tài trợ cuộc thi, các đối tác quảng cáo và tiếp thị và những người khác cung cấp nội dung của chúng tôi hoặc có sản phẩm hoặc dịch vụ mà chúng tôi nghĩ có thể khiến bạn quan tâm.</Text>
                        <Text category={"p2"} style={styles.mb5}>Trang web: Trang web của PP Beauty &amp; Academy, có thể được truy cập qua URL này: https://ppbeautyacademy.com</Text>
                        <Text category={"p2"} style={styles.mb5}>Bạn: cá nhân hoặc tổ chức đã đăng ký với PP Beauty &amp; Academy để sử dụng Dịch vụ.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Các thông tin chúng tôi thu thập ?</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi thu thập thông tin từ bạn khi bạn truy cập trang web / ứng dụng của chúng tôi, đăng ký trên trang web của chúng tôi, đặt hàng, đăng ký nhận bản tin của chúng tôi, trả lời khảo sát hoặc điền vào biểu mẫu.</Text>

                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Tên / Tên người dùng</Text>
                        <Text category={"p2"} style={styles.mb5}>Số điện thoại</Text>
                        <Text category={"p2"} style={styles.mb5}>Địa chỉ Email</Text>
                        <Text category={"p2"} style={styles.mb5}>Địa chỉ gửi thư</Text>
                        <Text category={"p2"} style={styles.mb5}>Địa chỉ thanh toán</Text>
                        <Text category={"p2"} style={styles.mb5}>Số thẻ ghi nợ / tín dụng</Text>
                        <Text category={"p2"} style={styles.mb5}>Mật khẩu</Text>
                        </View>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi cũng thu thập thông tin từ các thiết bị di động để có trải nghiệm người dùng tốt hơn, mặc dù các tính năng này là hoàn toàn tùy chọn:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Máy ảnh (Hình ảnh): Việc cấp quyền máy ảnh cho phép người dùng tải lên bất kỳ hình ảnh nào trực tiếp từ máy ảnh, bạn có thể từ chối quyền máy ảnh một cách an toàn cho việc này.</Text>
                        <Text category={"p2"} style={styles.mb5}>Thư viện ảnh (Pictures): Cấp quyền truy cập thư viện ảnh cho phép người dùng tải lên bất kỳ hình ảnh nào từ thư viện ảnh của họ, bạn có thể từ chối quyền truy cập thư viện ảnh một cách an toàn cho việc này..</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}> Chúng tôi sử dụng thông tin mà chúng tôi thu thập được như thế nào? </Text>
                        <Text category={"p2"} style={styles.mb5}> Bất kỳ thông tin nào chúng tôi thu thập từ bạn đều có thể được sử dụng theo một trong những cách sau: </Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}> Để cá nhân hóa trải nghiệm của bạn (thông tin của bạn giúp chúng tôi đáp ứng tốt hơn nhu cầu cá nhân của bạn) </Text>
                        <Text category={"p2"} style={styles.mb5}> Để cải thiện trang web / ứng dụng của chúng tôi (chúng tôi liên tục cố gắng cải thiện các dịch vụ trang web / ứng dụng dựa trên thông tin và phản hồi mà chúng tôi nhận được từ bạn) </Text>
                        <Text category={"p2"} style={styles.mb5}> Để cải thiện dịch vụ khách hàng (thông tin của bạn giúp chúng tôi đáp ứng hiệu quả hơn các yêu cầu dịch vụ khách hàng và nhu cầu hỗ trợ của bạn) </Text>
                        <Text category={"p2"} style={styles.mb5}> Để xử lý các giao dịch </Text>
                        <Text category={"p2"} style={styles.mb5}> Để quản lý một cuộc thi, quảng cáo, khảo sát hoặc các tính năng khác của trang web </Text>
                        <Text category={"p2"} style={styles.mb5}> Để gửi email định kỳ </Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Khi nào PP Beauty &amp; Academy sử dụng thông tin người dùng cuối từ bên thứ ba?</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy sẽ thu thập Dữ liệu người dùng cuối cần thiết để cung cấp dịch vụ PP Beauty &amp; Academy cho khách hàng của chúng tôi. Người dùng cuối có thể tự nguyện cung cấp cho chúng tôi thông tin mà họ đã cung cấp trên các trang web truyền thông xã hội. </Text>
                        <Text category={"p2"} style={styles.mb5}>Nếu bạn cung cấp cho chúng tôi bất kỳ thông tin nào như vậy, chúng tôi có thể thu thập thông tin công khai từ các trang web truyền thông xã hội mà bạn đã chỉ định. Bạn có thể kiểm soát lượng thông tin mà các trang web mạng xã hội công khai bằng cách truy cập các trang web này và thay đổi cài đặt quyền riêng tư của bạn.</Text>

                        <Text category={"h5"} style={styles.mb5}>PP Beauty &amp; Academy sử dụng thông tin khách hàng từ bên thứ ba khi nào?</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi nhận được một số thông tin từ các bên thứ ba khi bạn liên hệ với chúng tôi. Ví dụ: khi bạn gửi địa chỉ email của mình cho chúng tôi để thể hiện mong muốn trở thành khách hàng của PP Beauty &amp; Academy, chúng tôi nhận được thông tin từ một bên thứ ba cung cấp dịch vụ phát hiện gian lận tự động cho PP Beauty &amp; Academy. Đôi khi chúng tôi cũng thu thập thông tin được công bố rộng rãi trên các trang web truyền thông xã hội. Bạn có thể kiểm soát lượng thông tin mà các trang web mạng xã hội công khai bằng cách truy cập các trang web này và thay đổi cài đặt quyền riêng tư của bạn.</Text>

                        <Text category={"h5"} style={styles.mb5}>Chúng tôi có chia sẻ thông tin chúng tôi thu thập được với bên thứ ba không?</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có thể chia sẻ thông tin mà chúng tôi thu thập, cả cá nhân và phi cá nhân, với các bên thứ ba, chẳng hạn như nhà quảng cáo, nhà tài trợ cuộc thi, đối tác quảng cáo và tiếp thị và những người khác cung cấp nội dung của chúng tôi hoặc có sản phẩm hoặc dịch vụ mà chúng tôi nghĩ rằng bạn có thể quan tâm. Chúng tôi cũng có thể chia sẻ thông tin này với các công ty liên kết và đối tác kinh doanh hiện tại và trong tương lai của chúng tôi và nếu chúng tôi tham gia vào việc sáp nhập, bán tài sản hoặc tổ chức lại doanh nghiệp khác, chúng tôi cũng có thể chia sẻ hoặc chuyển thông tin cá nhân và phi cá nhân của bạn cho người kế nhiệm của chúng tôi - quan tâm.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có thể mời các nhà cung cấp dịch vụ bên thứ ba đáng tin cậy thực hiện các chức năng và cung cấp dịch vụ cho chúng tôi, chẳng hạn như lưu trữ và bảo trì máy chủ của chúng tôi và trang web / ứng dụng, lưu trữ và quản lý cơ sở dữ liệu, quản lý e-mail, tiếp thị lưu trữ, xử lý thẻ tín dụng, dịch vụ khách hàng và hoàn thành đơn đặt hàng cho các sản phẩm và dịch vụ mà bạn có thể mua thông qua trang web / ứng dụng. Chúng tôi có thể sẽ chia sẻ thông tin cá nhân của bạn và có thể cả một số thông tin phi cá nhân với các bên thứ ba này để cho phép họ thực hiện các dịch vụ này cho chúng tôi và cho bạn.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có thể chia sẻ các phần dữ liệu tệp nhật ký của mình, bao gồm cả địa chỉ IP, cho mục đích phân tích với các bên thứ ba, chẳng hạn như đối tác phân tích trang web, nhà phát triển ứng dụng và mạng quảng cáo. Nếu địa chỉ IP của bạn được chia sẻ, nó có thể được sử dụng để ước tính vị trí chung và các thông số kỹ thuật khác như tốc độ kết nối, cho dù bạn đã truy cập trang web / ứng dụng ở vị trí được chia sẻ hay không và loại thiết bị được sử dụng để truy cập trang web / ứng dụng. Họ có thể tổng hợp thông tin về quảng cáo của chúng tôi và những gì bạn thấy trên trang web / ứng dụng và sau đó cung cấp kiểm tra, nghiên cứu và báo cáo cho chúng tôi và các nhà quảng cáo của chúng tôi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi cũng có thể tiết lộ thông tin cá nhân và phi cá nhân về bạn cho các quan chức chính phủ hoặc cơ quan thực thi pháp luật hoặc các bên tư nhân vì chúng tôi, theo quyết định riêng của chúng tôi, tin rằng cần thiết hoặc thích hợp để phản hồi các khiếu nại, quy trình pháp lý (bao gồm trát đòi hầu tòa), để bảo vệ quyền và lợi ích của bên thứ ba, sự an toàn của công chúng hoặc bất kỳ cá nhân nào, để ngăn chặn hoặc dừng bất kỳ hoạt động bất hợp pháp, phi đạo đức hoặc có thể hành động hợp pháp nào hoặc để tuân thủ các lệnh tòa, luật, quy tắc và quy định hiện hành.</Text>

                        <Text category={"h5"} style={styles.mb5}>Thông tin được thu thập từ khách hàng và người dùng cuối ở đâu và khi nào?</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy sẽ thu thập thông tin cá nhân mà bạn gửi cho chúng tôi. Chúng tôi cũng có thể nhận thông tin cá nhân về bạn từ các bên thứ ba như được mô tả ở trên.</Text>

                        <Text category={"h5"} style={styles.mb5}>Chúng tôi sử dụng địa chỉ email của bạn như thế nào?</Text>
                        <Text category={"p2"} style={styles.mb5}>Bằng cách gửi địa chỉ email của bạn trên trang web / ứng dụng này, bạn đồng ý nhận email từ chúng tôi. Bạn có thể hủy tham gia vào bất kỳ danh sách email nào trong số này bất kỳ lúc nào bằng cách nhấp vào liên kết chọn không tham gia hoặc tùy chọn hủy đăng ký khác có trong email tương ứng. Chúng tôi chỉ gửi email cho những người đã cho phép chúng tôi liên hệ với họ, trực tiếp hoặc thông qua một bên thứ ba. Chúng tôi không gửi các email thương mại không mong muốn, bởi vì chúng tôi cũng ghét spam như bạn. Bằng cách gửi địa chỉ email của bạn, bạn cũng đồng ý cho phép chúng tôi sử dụng địa chỉ email của bạn để nhắm mục tiêu đối tượng khách hàng trên các trang web như Facebook, nơi chúng tôi hiển thị quảng cáo tùy chỉnh cho những người cụ thể đã chọn nhận thông tin liên lạc từ chúng tôi. Địa chỉ email chỉ được gửi qua trang xử lý đơn đặt hàng sẽ được sử dụng cho mục đích duy nhất là gửi cho bạn thông tin và cập nhật liên quan đến đơn đặt hàng của bạn. Tuy nhiên, nếu bạn đã cung cấp cùng một email cho chúng tôi thông qua một phương thức khác, chúng tôi có thể sử dụng nó cho bất kỳ mục đích nào được nêu trong Chính sách này. Lưu ý: Nếu bất kỳ lúc nào bạn muốn hủy đăng ký nhận các email trong tương lai, chúng tôi bao gồm hướng dẫn hủy đăng ký chi tiết ở cuối mỗi email.</Text>

                        <Text category={"h5"} style={styles.mb5}>Chúng tôi lưu giữ thông tin của bạn trong bao lâu?</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi chỉ lưu giữ thông tin của bạn chừng nào chúng tôi cần để cung cấp PP Beauty &amp; Academy cho bạn và thực hiện các mục đích được mô tả trong chính sách này. Đây cũng là trường hợp của bất kỳ ai mà chúng tôi chia sẻ thông tin của bạn và những người thực hiện các dịch vụ thay mặt chúng tôi. Khi chúng tôi không còn cần sử dụng thông tin của bạn nữa và chúng tôi không cần phải lưu giữ thông tin đó để tuân thủ các nghĩa vụ pháp lý hoặc quy định của chúng tôi, chúng tôi sẽ xóa thông tin đó khỏi hệ thống của mình hoặc phi cá nhân hóa thông tin để chúng tôi không thể nhận dạng bạn.</Text>

                        <Text category={"h5"} style={styles.mb5}>Làm thế nào để chúng tôi bảo vệ thông tin của bạn?</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi thực hiện nhiều biện pháp bảo mật khác nhau để duy trì sự an toàn cho thông tin cá nhân của bạn khi bạn đặt hàng hoặc nhập, gửi hoặc truy cập thông tin cá nhân của bạn. Chúng tôi cung cấp việc sử dụng một máy chủ an toàn. Tất cả thông tin nhạy cảm / tín dụng được cung cấp đều được truyền qua công nghệ Lớp cổng bảo mật (SSL) và sau đó được mã hóa vào cơ sở dữ liệu của nhà cung cấp Cổng thanh toán của chúng tôi để chỉ những người được ủy quyền có quyền truy cập đặc biệt vào các hệ thống đó mới có thể truy cập được và được yêu cầu giữ bí mật thông tin. Sau một giao dịch, thông tin cá nhân của bạn (thẻ tín dụng, số an sinh xã hội, tài chính, v.v.) sẽ không bao giờ được lưu trong hồ sơ. Tuy nhiên, chúng tôi không thể đảm bảo hoặc bảo đảm sự an toàn tuyệt đối của bất kỳ thông tin nào bạn truyền đến PP Beauty &amp; Academy hoặc đảm bảo rằng thông tin của bạn trên Dịch vụ có thể không bị truy cập, tiết lộ, thay đổi hoặc phá hủy do vi phạm bất kỳ cơ sở vật chất, kỹ thuật nào của chúng tôi , hoặc các biện pháp bảo vệ quản lý.</Text>

                        <Text category={"h5"} style={styles.mb5}>Thông tin của tôi có thể được chuyển sang các quốc gia khác không?</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy được thành lập tại Việt Nam. Thông tin được thu thập qua trang web của chúng tôi, thông qua tương tác trực tiếp với bạn hoặc từ việc sử dụng các dịch vụ trợ giúp của chúng tôi có thể được chuyển đôi khi đến các văn phòng hoặc nhân viên của chúng tôi, hoặc cho các bên thứ ba, ở khắp nơi trên thế giới và có thể được xem và lưu trữ ở bất kỳ đâu trong thế giới, bao gồm cả các quốc gia có thể không có luật áp dụng chung quy định việc sử dụng và chuyển giao dữ liệu đó. Trong phạm vi tối đa được pháp luật hiện hành cho phép, bằng cách sử dụng bất kỳ điều nào ở trên, bạn tự nguyện đồng ý với việc chuyển giao xuyên biên giới và lưu trữ thông tin đó.</Text>

                        <Text category={"h5"} style={styles.mb5}>Thông tin được thu thập thông qua PP Beauty &amp; Academy Service có bảo mật không?</Text>
                        <Text category={"p2"} style={styles.mb5}>We take precautions to protect the security of your information. We have physical, electronic, and managerial procedures to help safeguard, prevent unauthorized access, maintain data security, and correctly use your information. However, neither people nor security systems are foolproof, including encryption systems. In addition, people can commit intentional crimes, make mistakes or fail to follow policies. Therefore, while we use reasonable efforts to protect your personal information, we cannot guarantee its absolute security. If applicable law imposes any non-disclaimable duty to protect your personal information, you agree that intentional misconduct will be the standards used to measure our compliance with that duty.</Text>

                        <Text category={"h5"} style={styles.mb5}>Tôi có thể cập nhật hoặc sửa thông tin của mình không?</Text>
                        <Text category={"p2"} style={styles.mb5}>Các quyền bạn có để yêu cầu cập nhật hoặc chỉnh sửa thông tin mà PP Beauty &amp; Academy thu thập tùy thuộc vào mối quan hệ của bạn với PP Beauty &amp; Academy. Nhân sự có thể cập nhật hoặc chỉnh sửa thông tin của họ như được nêu chi tiết trong chính sách tuyển dụng nội bộ của công ty chúng tôi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Khách hàng có quyền yêu cầu hạn chế sử dụng và tiết lộ những thông tin cá nhân như sau. Bạn có thể liên hệ với chúng tôi để (1) cập nhật hoặc chỉnh sửa thông tin nhận dạng cá nhân của bạn, (2) thay đổi tùy chọn của bạn đối với thông tin liên lạc và thông tin khác mà bạn nhận được từ chúng tôi, hoặc (3) xóa thông tin nhận dạng cá nhân được duy trì về bạn trên hệ thống (tùy thuộc vào đoạn sau), bằng cách hủy tài khoản của bạn. Các cập nhật, chỉnh sửa, thay đổi và xóa như vậy sẽ không ảnh hưởng đến thông tin khác mà chúng tôi duy trì hoặc thông tin mà chúng tôi đã cung cấp cho bên thứ ba theo Chính sách quyền riêng tư này trước khi cập nhật, chỉnh sửa, thay đổi hoặc xóa. Để bảo vệ quyền riêng tư và bảo mật của bạn, chúng tôi có thể thực hiện các bước hợp lý (chẳng hạn như yêu cầu một mật khẩu duy nhất) để xác minh danh tính của bạn trước khi cấp cho bạn quyền truy cập hồ sơ hoặc thực hiện các chỉnh sửa. Bạn có trách nhiệm duy trì bí mật của mật khẩu duy nhất và thông tin tài khoản của bạn mọi lúc.</Text>
                        <Text category={"p2"} style={styles.mb5}>Bạn nên biết rằng không thể xóa từng bản ghi thông tin mà bạn đã cung cấp cho chúng tôi khỏi hệ thống của chúng tôi về mặt công nghệ. Sự cần thiết phải sao lưu hệ thống của chúng tôi để bảo vệ thông tin khỏi sự mất mát vô tình có nghĩa là một bản sao thông tin của bạn có thể tồn tại ở dạng không thể xóa được mà chúng tôi sẽ khó hoặc không thể xác định được. Ngay sau khi nhận được yêu cầu của bạn, tất cả thông tin cá nhân được lưu trữ trong cơ sở dữ liệu mà chúng tôi chủ động sử dụng và các phương tiện có thể tìm kiếm dễ dàng khác sẽ được cập nhật, sửa chữa, thay đổi hoặc xóa, nếu thích hợp, ngay khi và trong phạm vi hợp lý và có thể thực hiện được về mặt kỹ thuật.</Text>
                        <Text category={"p2"} style={styles.mb5}>Nếu bạn là người dùng cuối và muốn cập nhật, xóa hoặc nhận bất kỳ thông tin nào chúng tôi có về bạn, bạn có thể làm như vậy bằng cách liên hệ với tổ chức mà bạn là khách hàng.</Text>
                        <Text category={"h5"} style={styles.mb5}>Nhân viên</Text>
                        <Text category={"p2"} style={styles.mb5}>Nếu bạn là nhân viên hoặc ứng viên của PP Beauty &amp; Academy, chúng tôi sẽ thu thập thông tin mà bạn tự nguyện cung cấp cho chúng tôi. Chúng tôi sử dụng thông tin thu thập được cho mục đích Nhân sự để quản lý các quyền lợi cho người lao động và sàng lọc các ứng viên.</Text>
                        <Text category={"p2"} style={styles.mb5}>Bạn có thể liên hệ với chúng tôi để (1) cập nhật hoặc chỉnh sửa thông tin của bạn, (2) thay đổi tùy chọn của bạn đối với thông tin liên lạc và thông tin khác mà bạn nhận được từ chúng tôi, hoặc (3) nhận hồ sơ về thông tin mà chúng tôi có liên quan đến bạn. Các cập nhật, chỉnh sửa, thay đổi và xóa như vậy sẽ không ảnh hưởng đến thông tin khác mà chúng tôi duy trì hoặc thông tin mà chúng tôi đã cung cấp cho bên thứ ba theo Chính sách quyền riêng tư này trước khi cập nhật, chỉnh sửa, thay đổi hoặc xóa.</Text>
                        <Text category={"h5"} style={styles.mb5}>Doanh nghiệp</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có quyền chuyển thông tin cho bên thứ ba trong trường hợp bán, sáp nhập hoặc chuyển nhượng khác tất cả hoặc về cơ bản tất cả tài sản của PP Beauty &amp; Academy hoặc bất kỳ Chi nhánh Công ty nào của nó (như được định nghĩa ở đây), hoặc phần đó của PP Beauty &amp; Academy hoặc bất kỳ Chi nhánh nào của Công ty mà Dịch vụ có liên quan, hoặc trong trường hợp chúng tôi ngừng kinh doanh hoặc nộp đơn khởi kiện hoặc đã nộp đơn kiện chúng tôi phá sản, tổ chức lại hoặc thủ tục tương tự, với điều kiện là bên thứ ba đồng ý tuân thủ các điều khoản của Chính sách Bảo mật này.</Text>

                        <Text category={"h5"} style={styles.mb5}>Chi nhánh</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có thể tiết lộ thông tin (bao gồm thông tin cá nhân) về bạn cho các Chi nhánh Công ty của chúng tôi. Đối với các mục đích của Chính sách Bảo mật này, "Đơn vị liên kết của Công ty" có nghĩa là bất kỳ cá nhân hoặc tổ chức nào kiểm soát trực tiếp hoặc gián tiếp, được kiểm soát bởi hoặc dưới sự kiểm soát chung của PP Beauty &amp; Academy, cho dù theo quyền sở hữu hay cách khác. Mọi thông tin liên quan đến bạn mà chúng tôi cung cấp cho các Chi nhánh Công ty của chúng tôi sẽ được các Chi nhánh Công ty đó xử lý theo các điều khoản của Chính sách Bảo mật này.</Text>

                        <Text category={"h5"} style={styles.mb5}>Luật chi phối</Text>
                        <Text category={"p2"} style={styles.mb5}>Chính sách Bảo mật này được điều chỉnh bởi luật pháp Việt Nam mà không liên quan đến điều khoản xung đột pháp luật. Bạn đồng ý với quyền tài phán duy nhất của tòa án liên quan đến bất kỳ hành động hoặc tranh chấp nào phát sinh giữa các bên theo hoặc liên quan đến Chính sách quyền riêng tư này, ngoại trừ những cá nhân có quyền đưa ra yêu cầu theo Privacy Shield hoặc khuôn khổ Thụy Sĩ-Hoa Kỳ.</Text>
                        <Text category={"p2"} style={styles.mb5}>Luật pháp Việt Nam, không bao gồm các quy tắc xung đột luật, sẽ điều chỉnh Thỏa thuận này và việc bạn sử dụng trang web / ứng dụng. Việc bạn sử dụng trang web / ứng dụng cũng có thể tuân theo các luật khác của địa phương, tiểu bang, quốc gia hoặc quốc tế.</Text>
                        <Text category={"p2"} style={styles.mb5}>Bằng cách sử dụng PP Beauty &amp; Academy hoặc liên hệ trực tiếp với chúng tôi, bạn đồng nghĩa với việc bạn chấp nhận Chính sách Bảo mật này. Nếu bạn không đồng ý với Chính sách Bảo mật này, bạn không nên tham gia vào trang web của chúng tôi hoặc sử dụng các dịch vụ của chúng tôi. Việc tiếp tục sử dụng trang web, tham gia trực tiếp với chúng tôi hoặc sau khi đăng các thay đổi đối với Chính sách Bảo mật này mà không ảnh hưởng đáng kể đến việc sử dụng hoặc tiết lộ thông tin cá nhân của bạn sẽ có nghĩa là bạn chấp nhận những thay đổi đó.</Text>

                        <Text category={"h5"} style={styles.mb5}>Sự đồng ý của bạn</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi đã cập nhật Chính sách quyền riêng tư của mình để cung cấp cho bạn sự minh bạch hoàn toàn về những gì đang được thiết lập khi bạn truy cập trang web của chúng tôi và cách nó được sử dụng. Bằng cách sử dụng PP Beauty &amp; Academy của chúng tôi, đăng ký tài khoản hoặc mua hàng, bạn đồng ý với Chính sách quyền riêng tư của chúng tôi và đồng ý với các điều khoản của chính sách này.
                        </Text>

                        <Text category={"h5"} style={styles.mb5}>Liên kết đến các trang web khác</Text>
                        <Text category={"p2"} style={styles.mb5}>Chính sách Bảo mật này chỉ áp dụng cho các Dịch vụ. Các Dịch vụ có thể chứa các liên kết đến các trang web khác không do PP Beauty &amp; Academy điều hành hoặc kiểm soát. Chúng tôi không chịu trách nhiệm về nội dung, tính chính xác hoặc ý kiến ​​được thể hiện trong các trang web đó, và các trang web đó không được chúng tôi điều tra, giám sát hoặc kiểm tra tính chính xác hoặc hoàn chỉnh. Hãy nhớ rằng khi bạn sử dụng một liên kết để đi từ Dịch vụ đến một trang web khác, Chính sách Bảo mật của chúng tôi sẽ không còn hiệu lực. Việc duyệt và tương tác của bạn trên bất kỳ trang web nào khác, bao gồm cả những trang web có liên kết trên nền tảng của chúng tôi, phải tuân theo các quy tắc và chính sách riêng của trang web đó. Các bên thứ ba như vậy có thể sử dụng cookie của riêng họ hoặc các phương pháp khác để thu thập thông tin về bạn.</Text>
                        <Text category={"h5"} style={styles.mb5}>Cookies</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy sử dụng "Cookie" để xác định các khu vực trên trang web của chúng tôi mà bạn đã truy cập. Cookie là một phần dữ liệu nhỏ được trình duyệt web của bạn lưu trữ trên máy tính hoặc thiết bị di động. Chúng tôi sử dụng Cookie để nâng cao hiệu suất và chức năng của trang web / ứng dụng của chúng tôi nhưng không cần thiết cho việc sử dụng chúng. Tuy nhiên, nếu không có những cookie này, một số chức năng nhất định như video có thể không khả dụng hoặc bạn sẽ được yêu cầu nhập chi tiết đăng nhập của mình mỗi khi truy cập trang web / ứng dụng vì chúng tôi sẽ không thể nhớ rằng bạn đã đăng nhập trước đó. Hầu hết các trình duyệt web có thể được thiết lập để vô hiệu hóa việc sử dụng Cookie. Tuy nhiên, nếu bạn tắt Cookie, bạn có thể không truy cập được chức năng trên trang web của chúng tôi một cách chính xác hoặc hoàn toàn. Chúng tôi không bao giờ đặt Thông tin Nhận dạng Cá nhân trong Cookie.</Text>

                        <Text category={"h5"} style={styles.mb5}>Chặn và vô hiệu hóa cookie và các công nghệ tương tự</Text>
                        <Text category={"p2"} style={styles.mb5}>Dù bạn đang ở đâu, bạn cũng có thể đặt trình duyệt của mình để chặn cookie và các công nghệ tương tự, nhưng hành động này có thể chặn các cookie cần thiết của chúng tôi và ngăn trang web của chúng tôi hoạt động bình thường và bạn có thể không sử dụng được đầy đủ tất cả các tính năng và dịch vụ của nó. Bạn cũng nên biết rằng bạn cũng có thể mất một số thông tin đã lưu (ví dụ: chi tiết đăng nhập đã lưu, tùy chọn trang web) nếu bạn chặn cookie trên trình duyệt của mình. Các trình duyệt khác nhau cung cấp các điều khiển khác nhau cho bạn. Việc tắt cookie hoặc danh mục cookie không xóa cookie khỏi trình duyệt của bạn, bạn sẽ cần tự thực hiện việc này từ bên trong trình duyệt của mình, bạn nên truy cập menu trợ giúp của trình duyệt để biết thêm thông tin.</Text>
                        <Text category={"h5"} style={styles.mb5}>Dịch vụ tiếp thị lại</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi sử dụng dịch vụ tiếp thị lại. Tiếp thị lại là gì? Trong tiếp thị kỹ thuật số, tiếp thị lại (hoặc nhắm mục tiêu lại) là hoạt động phân phát quảng cáo trên internet cho những người đã truy cập trang web của bạn. Nó cho phép công ty của bạn có vẻ như họ đang “theo dõi” mọi người trên internet bằng cách phân phát quảng cáo trên các trang web và nền tảng mà họ sử dụng nhiều nhất.</Text>
                        <Text category={"h5"} style={styles.mb5}>Chi tiết thanh toán</Text>
                        <Text category={"p2"} style={styles.mb5}>Đối với bất kỳ thẻ tín dụng hoặc các chi tiết xử lý thanh toán khác mà bạn đã cung cấp cho chúng tôi, chúng tôi cam kết rằng thông tin bí mật này sẽ được lưu trữ theo cách an toàn nhất có thể.</Text>
                        <Text category={"h5"} style={styles.mb5}>Quyền riêng tư của Trẻ em</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi thu thập thông tin từ trẻ em dưới 13 tuổi chỉ để cải thiện dịch vụ của chúng tôi. Nếu Bạn là cha mẹ hoặc người giám hộ và Bạn biết rằng con Bạn đã cung cấp cho Chúng tôi Dữ liệu Cá nhân mà không có sự cho phép của Bạn, vui lòng liên hệ với Chúng tôi. Nếu Chúng tôi biết rằng Chúng tôi đã thu thập Dữ liệu Cá nhân từ bất kỳ ai dưới 13 tuổi mà không có sự xác minh của sự đồng ý của cha mẹ, Chúng tôi sẽ thực hiện các bước để xóa thông tin đó khỏi máy chủ của Chúng tôi.</Text>
                        <Text category={"h5"} style={styles.mb5}>Các thay đổi đối với chính sách bảo mật của chúng tôi</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có thể thay đổi Dịch vụ và chính sách của mình và chúng tôi có thể cần thực hiện các thay đổi đối với Chính sách Bảo mật này để chúng phản ánh chính xác Dịch vụ và chính sách của chúng tôi. Trừ khi luật pháp yêu cầu khác, chúng tôi sẽ thông báo cho bạn (ví dụ: thông qua Dịch vụ của chúng tôi) trước khi chúng tôi thực hiện các thay đổi đối với Chính sách quyền riêng tư này và cho bạn cơ hội xem xét chúng trước khi chúng có hiệu lực. Sau đó, nếu bạn tiếp tục sử dụng Dịch vụ, bạn sẽ bị ràng buộc bởi Chính sách bảo mật được cập nhật. Nếu bạn không muốn đồng ý với điều này hoặc bất kỳ Chính sách Bảo mật được cập nhật nào, bạn có thể xóa tài khoản của mình.</Text>

                        <Text category={"h5"} style={styles.mb5}>Dịch vụ của bên thứ ba</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có thể hiển thị, bao gồm hoặc cung cấp nội dung của bên thứ ba (bao gồm dữ liệu, thông tin, ứng dụng và các dịch vụ sản phẩm khác) hoặc cung cấp liên kết đến các trang web hoặc dịch vụ của bên thứ ba ("Dịch vụ của bên thứ ba").</Text>
                        <Text category={"p2"} style={styles.mb5}>Bạn thừa nhận và đồng ý rằng PP Beauty &amp; Academy sẽ không chịu trách nhiệm đối với bất kỳ Dịch vụ của bên thứ ba nào, bao gồm cả tính chính xác, đầy đủ, kịp thời, hợp lệ, tuân thủ bản quyền, tính hợp pháp, tính trung thực, chất lượng hoặc bất kỳ khía cạnh nào khác của chúng. PP Beauty &amp; Academy không chịu và sẽ không có bất kỳ nghĩa vụ hoặc trách nhiệm nào đối với bạn hoặc bất kỳ cá nhân hoặc tổ chức nào khác đối với bất kỳ Dịch vụ của Bên thứ ba nào.</Text>
                        <Text category={"p2"} style={styles.mb5}>Dịch vụ của bên thứ ba và các liên kết tới đó chỉ được cung cấp như một sự thuận tiện cho bạn và bạn truy cập và sử dụng chúng hoàn toàn tự chịu rủi ro và tuân theo các điều khoản và điều kiện của bên thứ ba đó.</Text>
                        <Text category={"h5"} style={styles.mb5}>Facebook Pixel</Text>
                        <Text category={"p2"} style={styles.mb5}>Facebook pixel là một công cụ phân tích cho phép bạn đo lường hiệu quả của quảng cáo bằng cách hiểu các hành động mà mọi người thực hiện trên trang web của bạn. Bạn có thể sử dụng pixel để: Đảm bảo rằng quảng cáo của bạn được hiển thị cho đúng người. Facebook pixel có thể thu thập thông tin từ thiết bị của bạn khi bạn sử dụng dịch vụ. Facebook pixel thu thập thông tin được lưu giữ theo Chính sách quyền riêng tư của nó
                        </Text>
                        <Text category={"h5"} style={styles.mb5}>Công nghệ theo dõi</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Google Maps API</Text>
                            <Text category={"p2"} style={styles.mb5}>API Google Maps là một công cụ mạnh mẽ có thể được sử dụng để tạo bản đồ tùy chỉnh, bản đồ có thể tìm kiếm, chức năng đăng ký, hiển thị đồng bộ hóa dữ liệu trực tiếp với vị trí, lập kế hoạch tuyến đường hoặc tạo một bản kết hợp chỉ để đặt tên cho một số.</Text>
                            <Text category={"p2"} style={styles.mb5}>API Google Maps có thể thu thập thông tin từ Bạn và từ Thiết bị của Bạn cho các mục đích bảo mật.</Text>
                            <Text category={"p2"} style={styles.mb5}>API Google Maps thu thập thông tin được lưu giữ theo Chính sách quyền riêng tư của nó</Text>
                            <Text category={"p2"} style={styles.mb5}>Lưu trữ cục bộ</Text>
                            <Text category={"p2"} style={styles.mb5}>Bộ nhớ cục bộ đôi khi được gọi là bộ nhớ DOM, cung cấp các ứng dụng web với các phương pháp và giao thức để lưu trữ dữ liệu phía máy khách. Lưu trữ web hỗ trợ lưu trữ dữ liệu liên tục, tương tự như cookie nhưng với dung lượng được nâng cao hơn rất nhiều và không có thông tin nào được lưu trữ trong tiêu đề yêu cầu HTTP.</Text>
                        </View>
                        <Text category={"h5"} style={styles.mb5}>Thông tin về Quy định chung về bảo vệ dữ liệu (GDPR)</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi có thể đang thu thập và sử dụng thông tin từ bạn nếu bạn đến từ Khu vực kinh tế Châu Âu (EEA) và trong phần này của Chính sách quyền riêng tư của chúng tôi, chúng tôi sẽ giải thích chính xác cách thức và lý do tại sao dữ liệu này được thu thập cũng như cách chúng tôi duy trì dữ liệu này theo bảo vệ khỏi bị sao chép hoặc sử dụng sai cách.</Text>

                        <Text category={"h5"} style={styles.mb5}>GDPR là gì?</Text>
                        <Text category={"p2"} style={styles.mb5}>GDPR là luật bảo vệ dữ liệu và quyền riêng tư trên toàn EU quy định cách dữ liệu của cư dân EU được các công ty bảo vệ và tăng cường quyền kiểm soát của cư dân EU đối với dữ liệu cá nhân của họ.</Text>
                        <Text category={"p2"} style={styles.mb5}>GDPR có liên quan đến bất kỳ công ty hoạt động trên toàn cầu nào chứ không chỉ các doanh nghiệp có trụ sở tại Liên minh Châu Âu và cư dân Liên minh Châu Âu. Dữ liệu khách hàng của chúng tôi rất quan trọng bất kể họ ở đâu, đó là lý do tại sao chúng tôi đã triển khai các biện pháp kiểm soát GDPR làm tiêu chuẩn cơ bản cho tất cả các hoạt động của chúng tôi trên toàn thế giới.</Text>

                        <Text category={"h5"} style={styles.mb5}>Dữ liệu cá nhân là gì?</Text>
                        <Text category={"p2"} style={styles.mb5}>Bất kỳ dữ liệu nào liên quan đến một cá nhân có thể nhận dạng hoặc được xác định. GDPR bao gồm nhiều loại thông tin có thể được sử dụng riêng hoặc kết hợp với các phần thông tin khác, để xác định một người. Dữ liệu cá nhân vượt ra ngoài tên hoặc địa chỉ email của một người. Một số ví dụ bao gồm thông tin tài chính, ý kiến ​​chính trị, dữ liệu di truyền, dữ liệu sinh trắc học, địa chỉ IP, địa chỉ thực, khuynh hướng tình dục và dân tộc.</Text>
                        <Text category={"p2"} style={styles.mb5}>Các Nguyên tắc Bảo vệ Dữ liệu bao gồm các yêu cầu như:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Dữ liệu cá nhân được thu thập phải được xử lý theo cách công bằng, hợp pháp và minh bạch và chỉ được sử dụng theo cách mà một người mong đợi một cách hợp lý.</Text>
                        <Text category={"p2"} style={styles.mb5}>Dữ liệu cá nhân chỉ nên được thu thập để thực hiện một mục đích cụ thể và nó chỉ được sử dụng cho mục đích đó. Các tổ chức phải nêu rõ lý do tại sao họ cần dữ liệu cá nhân khi họ thu thập dữ liệu đó..</Text>
                        <Text category={"p2"} style={styles.mb5}>Dữ liệu cá nhân không được lưu giữ lâu hơn mức cần thiết để thực hiện mục đích của nó.</Text>
                        <Text category={"p2"} style={styles.mb5}>Những người thuộc phạm vi điều chỉnh của GDPR có quyền truy cập vào dữ liệu cá nhân của họ. Họ cũng có thể yêu cầu một bản sao dữ liệu của họ và dữ liệu của họ được cập nhật, xóa, hạn chế hoặc chuyển sang một tổ chức khác.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Tại sao GDPR lại quan trọng?</Text>
                        <Text category={"p2"} style={styles.mb5}>GDPR bổ sung một số yêu cầu mới về cách các công ty nên bảo vệ dữ liệu cá nhân của các cá nhân mà họ thu thập và xử lý. Nó cũng tăng tiền đặt cọc cho việc tuân thủ bằng cách tăng cường thực thi và áp dụng các khoản tiền phạt cao hơn nếu vi phạm. Ngoài những sự thật này, đó đơn giản là điều đúng đắn cần làm. Tại PP Beauty &amp; Academy, chúng tôi tin tưởng mạnh mẽ rằng quyền riêng tư về dữ liệu của bạn là rất quan trọng và chúng tôi đã áp dụng các biện pháp bảo mật và quyền riêng tư vững chắc vượt ra ngoài các yêu cầu của quy định mới này.</Text>

                        <Text category={"h5"} style={styles.mb5}>Quyền của Chủ thể Dữ liệu Cá nhân - Truy cập Dữ liệu, Khả năng Di chuyển và Xóa</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi cam kết giúp khách hàng của mình đáp ứng các yêu cầu về quyền đối tượng dữ liệu của GDPR. PP Beauty &amp; Academy xử lý hoặc lưu trữ tất cả dữ liệu cá nhân trong các nhà cung cấp tuân thủ DPA, đã được kiểm duyệt đầy đủ. Chúng tôi lưu trữ tất cả các cuộc trò chuyện và dữ liệu cá nhân trong tối đa 6 năm trừ khi tài khoản của bạn bị xóa. Trong trường hợp đó, chúng tôi xử lý tất cả dữ liệu theo Điều khoản Dịch vụ và Chính sách Bảo mật của chúng tôi, nhưng chúng tôi sẽ không giữ dữ liệu đó lâu hơn 60 ngày.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi biết rằng nếu bạn đang làm việc với khách hàng ở Liên minh Châu Âu, bạn cần cung cấp cho họ khả năng truy cập, cập nhật, truy xuất và xóa dữ liệu cá nhân. Chúng tôi có bạn! Chúng tôi đã được thiết lập như một dịch vụ tự phục vụ ngay từ đầu và luôn cấp cho bạn quyền truy cập vào dữ liệu của bạn và dữ liệu khách hàng của bạn. Nhóm hỗ trợ khách hàng của chúng tôi ở đây để bạn trả lời bất kỳ câu hỏi nào bạn có thể có về cách làm việc với API.</Text>
                        <Text category={"h5"} style={styles.mb5}>Cư dân California</Text>
                        <Text category={"p2"} style={styles.mb5}>Đạo luật về quyền riêng tư của người tiêu dùng California (CCPA) yêu cầu chúng tôi tiết lộ các danh mục Thông tin cá nhân mà chúng tôi thu thập và cách chúng tôi sử dụng thông tin đó, danh mục nguồn mà chúng tôi thu thập Thông tin cá nhân và các bên thứ ba mà chúng tôi chia sẻ thông tin đó, điều mà chúng tôi đã giải thích ở trên .</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi cũng được yêu cầu cung cấp thông tin về các quyền mà cư dân California có theo luật California. Bạn có thể thực hiện các quyền sau:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Quyền được biết và tiếp cận. Bạn có thể gửi một yêu cầu có thể xác minh về thông tin liên quan đến: (1) các loại Thông tin Cá nhân mà chúng tôi thu thập, sử dụng hoặc chia sẻ; (2) các mục đích mà các loại Thông tin Cá nhân được chúng tôi thu thập hoặc sử dụng; (3) danh mục nguồn mà chúng tôi thu thập Thông tin Cá nhân; và (4) các mẩu Thông tin Cá nhân cụ thể mà chúng tôi đã thu thập về bạn.</Text>
                        <Text category={"p2"} style={styles.mb5}>Quyền được Phục vụ Bình đẳng. Chúng tôi sẽ không phân biệt đối xử chống lại bạn nếu bạn thực hiện các quyền riêng tư của mình.</Text>
                        <Text category={"p2"} style={styles.mb5}>Quyền Xóa. Bạn có thể gửi một yêu cầu có thể xác minh để đóng tài khoản của mình và chúng tôi sẽ xóa Thông tin Cá nhân về bạn mà chúng tôi đã thu thập.</Text>
                        <Text category={"p2"} style={styles.mb5}>Yêu cầu doanh nghiệp bán dữ liệu cá nhân của người tiêu dùng, không bán dữ liệu cá nhân của người tiêu dùng.</Text>
                        </View>
                        <Text category={"p2"} style={styles.mb5}>Nếu bạn đưa ra yêu cầu, chúng tôi có một tháng để trả lời bạn. Nếu bạn muốn thực hiện bất kỳ quyền nào trong số này, vui lòng liên hệ với chúng tôi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi không bán Thông tin cá nhân của người dùng của chúng tôi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Để biết thêm thông tin về các quyền này, vui lòng liên hệ với chúng tôi.</Text>
                        <Text category={"h5"} style={styles.mb5}>Đạo luật Bảo vệ Quyền riêng tư Trực tuyến của California (CalOPPA)</Text>
                        <Text category={"p2"} style={styles.mb5}>CalOPPA yêu cầu chúng tôi tiết lộ các loại Thông tin Cá nhân mà chúng tôi thu thập và cách chúng tôi sử dụng nó, các loại nguồn mà chúng tôi thu thập Thông tin Cá nhân từ đó và các bên thứ ba mà chúng tôi chia sẻ thông tin mà chúng tôi đã giải thích ở trên.</Text>
                        <Text category={"p2"} style={styles.mb5}>Người dùng CalOPPA có các quyền sau:</Text>
                        <View style={{marginLeft: 3}}>
                        <Text category={"p2"} style={styles.mb5}>Quyền được biết và tiếp cận. Bạn có thể gửi một yêu cầu có thể xác minh về thông tin liên quan đến: (1) các loại Thông tin Cá nhân mà chúng tôi thu thập, sử dụng hoặc chia sẻ; (2) các mục đích mà các loại Thông tin Cá nhân được chúng tôi thu thập hoặc sử dụng; (3) danh mục nguồn mà chúng tôi thu thập Thông tin Cá nhân; và (4) các mẩu Thông tin Cá nhân cụ thể mà chúng tôi đã thu thập về bạn.</Text>
                        <Text category={"p2"} style={styles.mb5}>Quyền được Phục vụ Bình đẳng. Chúng tôi sẽ không phân biệt đối xử chống lại bạn nếu bạn thực hiện các quyền riêng tư của mình.</Text>
                        <Text category={"p2"} style={styles.mb5}>Quyền Xóa. Bạn có thể gửi một yêu cầu có thể xác minh để đóng tài khoản của mình và chúng tôi sẽ xóa Thông tin Cá nhân về bạn mà chúng tôi đã thu thập.</Text>
                        <Text category={"p2"} style={styles.mb5}>Quyền yêu cầu doanh nghiệp bán dữ liệu cá nhân của người tiêu dùng, không bán dữ liệu cá nhân của người tiêu dùng.</Text>
                        </View>
                        <Text category={"p2"} style={styles.mb5}>Nếu bạn đưa ra yêu cầu, chúng tôi có một tháng để trả lời bạn. Nếu bạn muốn thực hiện bất kỳ quyền nào trong số này, vui lòng liên hệ với chúng tôi..</Text>
                        <Text category={"p2"} style={styles.mb5}>Chúng tôi không bán Thông tin cá nhân của người dùng của chúng tôi.</Text>
                        <Text category={"p2"} style={styles.mb5}>Để biết thêm thông tin về các quyền này, vui lòng liên hệ với chúng tôi.</Text>
                        <Text category={"h5"} style={styles.mb5}>Liên hệ chúng tôi</Text>
                        <Text category={"p2"} style={styles.mb5}>Đừng ngần ngại liên hệ với chúng tôi nếu bạn có bất kỳ câu hỏi nào liên quan đến Tuyên bố từ chối trách nhiệm này.</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Qua Email: contact@ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua số điện thoại: 099 688 228</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua liên kết này:  https://ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Qua địa chỉ: 306/25 Nguyễn Thị Minh Khai P.5, Q.3, TPHCM</Text>
                        </View>
                    </ScrollView>
                :
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{
                            paddingBottom: 25
                        }}
                    >
                        <Text category={"p1"}>Updated at 2022-01-01</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy (“we,” “our,” or “us”) is committed to protecting your privacy. This Privacy Policy explains how your personal information is collected, used, and disclosed by PP Beauty &amp; Academy.</Text>
                        <Text category={"p2"} style={styles.mb5}>This Privacy Policy applies to our website, and its associated subdomains (collectively, our “Service”) alongside our application, PP Beauty &amp; Academy. By accessing or using our Service, you signify that you have read, understood, and agree to our collection, storage, use, and disclosure of your personal information as described in this Privacy Policy and our Terms of Service.</Text>
                        <Text category={"h5"} style={styles.mb5}>Definitions and key terms</Text>
                        <Text category={"p2"} style={styles.mb5}>To help explain things as clearly as possible in this Privacy Policy, every time any of these terms are referenced, are strictly defined as:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Cookie: small amount of data generated by a website and saved by your web browser. It is used to identify your browser, provide analytics, remember information about you such as your language preference or login information.</Text>
                            <Text category={"p2"} style={styles.mb5}>Company: when this policy mentions “Company,” “we,” “us,” or “our,” it refers to PP Beauty &amp; Academy, 80 Bloor St W, Suite 401, Toronto, ON Canada. M5S2V1 that is responsible for your information under this Privacy Policy.</Text>
                            <Text category={"p2"} style={styles.mb5}>Country: where PP Beauty &amp; Academy or the owners/founders of PP Beauty &amp; Academy are based, in this case is Canada</Text>
                            <Text category={"p2"} style={styles.mb5}>Customer: refers to the company, organization or person that signs up to use the PP Beauty &amp; Academy Service to manage the relationships with your consumers or service users.</Text>
                            <Text category={"p2"} style={styles.mb5}>Device: any internet connected device such as a phone, tablet, computer or any other device that can be used to visit PP Beauty &amp; Academy and use the services.</Text>
                            <Text category={"p2"} style={styles.mb5}>IP address: Every device connected to the Internet is assigned a number known as an Internet protocol (IP) address. These numbers are usually assigned in geographic blocks. An IP address can often be used to identify the location from which a device is connecting to the Internet.</Text>
                            <Text category={"p2"} style={styles.mb5}>Personnel: refers to those individuals who are employed by PP Beauty &amp; Academy or are under contract to perform a service on behalf of one of the parties.</Text>
                            <Text category={"p2"} style={styles.mb5}>Personal Data: any information that directly, indirectly, or in connection with other information — including a personal identification number — allows for the identification or identifiability of a natural person.</Text>
                            <Text category={"p2"} style={styles.mb5}>Service: refers to the service provided by PP Beauty &amp; Academy as described in the relative terms (if available) and on this platform.</Text>
                            <Text category={"p2"} style={styles.mb5}>Third-party service: refers to advertisers, contest sponsors, promotional and marketing partners, and others who provide our content or whose products or services we think may interest you.</Text>
                            <Text category={"p2"} style={styles.mb5}>Website: PP Beauty &amp; Academy's site, which can be accessed via this URL: https://ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>You: a person or entity that is registered with PP Beauty &amp; Academy to use the Services.</Text>
                        </View>
                        <Text category={"h5"} style={styles.mb5}>What Information Do We Collect?</Text>
                        <Text category={"p2"} style={styles.mb5}>We collect information from you when you visit our website/app, register on our site, place an order, subscribe to our newsletter, respond to a survey or fill out a form.</Text>

                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Name / Username</Text>
                            <Text category={"p2"} style={styles.mb5}>Phone Numbers</Text>
                            <Text category={"p2"} style={styles.mb5}>Email Addresses</Text>
                            <Text category={"p2"} style={styles.mb5}>Mailing Addresses</Text>
                            <Text category={"p2"} style={styles.mb5}>Billing Addresses</Text>
                            <Text category={"p2"} style={styles.mb5}>Debit/credit card numbers</Text>
                            <Text category={"p2"} style={styles.mb5}>Password</Text>
                        </View>

                        <Text category={"p2"} style={styles.mb5}>We also collect information from mobile devices for a better user experience, although these features are completely optional:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Camera (Pictures): Granting camera permission allows the user to upload any picture straight from the website/app, you can safely deny camera permissions for this website/app.</Text>
                            <Text category={"p2"} style={styles.mb5}>Photo Gallery (Pictures): Granting photo gallery access allows the user to upload any picture from their photo gallery, you can safely deny photo gallery access for this website/app.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>How Do We Use The Information We Collect?</Text>
                        <Text category={"p2"} style={styles.mb5}>Any of the information we collect from you may be used in one of the following ways:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>To personalize your experience (your information helps us to better respond to your individual needs)</Text>
                            <Text category={"p2"} style={styles.mb5}>To improve our website/app (we continually strive to improve our website/app offerings based on the information and feedback we receive from you)</Text>
                            <Text category={"p2"} style={styles.mb5}>To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</Text>
                            <Text category={"p2"} style={styles.mb5}>To process transactions</Text>
                            <Text category={"p2"} style={styles.mb5}>To administer a contest, promotion, survey or other site feature</Text>
                            <Text category={"p2"} style={styles.mb5}>To send periodic emails</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>When does PP Beauty &amp; Academy use end user information from third parties?</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy will collect End User Data necessary to provide the PP Beauty &amp; Academy services to our customers.</Text>
                        <Text category={"p2"} style={styles.mb5}>End users may voluntarily provide us with information they have made available on social media websites. If you provide us with any such information, we may collect publicly available information from the social media websites you have indicated. You can control how much of your information social media websites make public by visiting these websites and changing your privacy settings.</Text>

                        <Text category={"h5"} style={styles.mb5}>When does PP Beauty &amp; Academy use customer information from third parties?</Text>
                        <Text category={"p2"} style={styles.mb5}>We receive some information from the third parties when you contact us. For example, when you submit your email address to us to show interest in becoming a PP Beauty &amp; Academy customer, we receive information from a third party that provides automated fraud detection services to PP Beauty &amp; Academy. We also occasionally collect information that is made publicly available on social media websites. You can control how much of your information social media websites make public by visiting these websites and changing your privacy settings.</Text>

                        <Text category={"h5"} style={styles.mb5}>Do we share the information we collect with third parties?</Text>
                        <Text category={"p2"} style={styles.mb5}>We may share the information that we collect, both personal and non-personal, with third parties such as advertisers, contest sponsors, promotional and marketing partners, and others who provide our content or whose products or services we think may interest you. We may also share it with our current and future affiliated companies and business partners, and if we are involved in a merger, asset sale or other business reorganization, we may also share or transfer your personal and non-personal information to our successors-in-interest.</Text>
                        <Text category={"p2"} style={styles.mb5}>We may engage trusted third party service providers to perform functions and provide services to us, such as hosting and maintaining our servers and the website/app, database storage and management, e-mail management, storage marketing, credit card processing, customer service and fulfilling orders for products and services you may purchase through the website/app. We will likely share your personal information, and possibly some non-personal information, with these third parties to enable them to perform these services for us and for you.</Text>
                        <Text category={"p2"} style={styles.mb5}>We may share portions of our log file data, including IP addresses, for analytics purposes with third parties such as web analytics partners, application developers, and ad networks. If your IP address is shared, it may be used to estimate general location and other technographics such as connection speed, whether you have visited the website/app in a shared location, and type of the device used to visit the website/app. They may aggregate information about our advertising and what you see on the website/app and then provide auditing, research and reporting for us and our advertisers. We may also disclose personal and non-personal information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate in order to respond to claims, legal process (including subpoenas), to protect our rights and interests or those of a third party, the safety of the public or any person, to prevent or stop any illegal, unethical, or legally actionable activity, or to otherwise comply with applicable court orders, laws, rules and regulations.</Text>

                        <Text category={"h5"} style={styles.mb5}>Where and when is information collected from customers and end users?</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy will collect personal information that you submit to us. We may also receive personal information about you from third parties as described above.</Text>

                        <Text category={"h5"} style={styles.mb5}>How Do We Use Your Email Address?</Text>
                        <Text category={"p2"} style={styles.mb5}>By submitting your email address on this website/app, you agree to receive emails from us. You can cancel your participation in any of these email lists at any time by clicking on the opt-out link or other unsubscribe option that is included in the respective email. We only send emails to people who have authorized us to contact them, either directly, or through a third party. We do not send unsolicited commercial emails, because we hate spam as much as you do. By submitting your email address, you also agree to allow us to use your email address for customer audience targeting on sites like Facebook, where we display custom advertising to specific people who have opted-in to receive communications from us. Email addresses submitted only through the order processing page will be used for the sole purpose of sending you information and updates pertaining to your order. If, however, you have provided the same email to us through another method, we may use it for any of the purposes stated in this Policy. Note: If at any time you would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</Text>

                        <Text category={"h5"} style={styles.mb5}>How Long Do We Keep Your Information?</Text>
                        <Text category={"p2"} style={styles.mb5}>We keep your information only so long as we need it to provide PP Beauty &amp; Academy to you and fulfill the purposes described in this policy. This is also the case for anyone that we share your information with and who carries out services on our behalf. When we no longer need to use your information and there is no need for us to keep it to comply with our legal or regulatory obligations, we’ll either remove it from our systems or depersonalize it so that we can't identify you.</Text>

                        <Text category={"h5"} style={styles.mb5}>How Do We Protect Your Information?</Text>
                        <Text category={"p2"} style={styles.mb5}>We implement a variety of security measures to maintain the safety of your personal information when you place an order or enter, submit, or access your personal information. We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Payment gateway providers database only to be accessible by those authorized with special access rights to such systems, and are required to keep the information confidential. After a transaction, your private information (credit cards, social security numbers, financials, etc.) is never kept on file. We cannot, however, ensure or warrant the absolute security of any information you transmit to PP Beauty &amp; Academy or guarantee that your information on the Service may not be accessed, disclosed, altered, or destroyed by a breach of any of our physical, technical, or managerial safeguards.</Text>

                        <Text category={"h5"} style={styles.mb5}>Could my information be transferred to other countries?</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy is incorporated in Canada. Information collected via our website, through direct interactions with you, or from use of our help services may be transferred from time to time to our offices or personnel, or to third parties, located throughout the world, and may be viewed and hosted anywhere in the world, including countries that may not have laws of general applicability regulating the use and transfer of such data. To the fullest extent allowed by applicable law, by using any of the above, you voluntarily consent to the trans-border transfer and hosting of such information.</Text>

                        <Text category={"h5"} style={styles.mb5}>Is the information collected through the PP Beauty &amp; Academy Service secure?</Text>
                        <Text category={"p2"} style={styles.mb5}>We take precautions to protect the security of your information. We have physical, electronic, and managerial procedures to help safeguard, prevent unauthorized access, maintain data security, and correctly use your information. However, neither people nor security systems are foolproof, including encryption systems. In addition, people can commit intentional crimes, make mistakes or fail to follow policies. Therefore, while we use reasonable efforts to protect your personal information, we cannot guarantee its absolute security. If applicable law imposes any non-disclaimable duty to protect your personal information, you agree that intentional misconduct will be the standards used to measure our compliance with that duty.</Text>

                        <Text category={"h5"} style={styles.mb5}>Can I update or correct my information?</Text>
                        <Text category={"p2"} style={styles.mb5}>The rights you have to request updates or corrections to the information PP Beauty &amp; Academy collects depend on your relationship with PP Beauty &amp; Academy. Personnel may update or correct their information as detailed in our internal company employment policies.</Text>
                        <Text category={"p2"} style={styles.mb5}>Customers have the right to request the restriction of certain uses and disclosures of personally identifiable information as follows. You can contact us in order to (1) update or correct your personally identifiable information, (2) change your preferences with respect to communications and other information you receive from us, or (3) delete the personally identifiable information maintained about you on our systems (subject to the following paragraph), by cancelling your account. Such updates, corrections, changes and deletions will have no effect on other information that we maintain, or information that we have provided to third parties in accordance with this Privacy Policy prior to such update, correction, change or deletion. To protect your privacy and security, we may take reasonable steps (such as requesting a unique password) to verify your identity before granting you profile access or making corrections. You are responsible for maintaining the secrecy of your unique password and account information at all times.</Text>
                        <Text category={"p2"} style={styles.mb5}>You should be aware that it is not technologically possible to remove each and every record of the information you have provided to us from our system. The need to back up our systems to protect information from inadvertent loss means that a copy of your information may exist in a non-erasable form that will be difficult or impossible for us to locate. Promptly after receiving your request, all personal information stored in databases we actively use, and other readily searchable media will be updated, corrected, changed or deleted, as appropriate, as soon as and to the extent reasonably and technically practicable.</Text>
                        <Text category={"p2"} style={styles.mb5}>If you are an end user and wish to update, delete, or receive any information we have about you, you may do so by contacting the organization of which you are a customer.</Text>
                        <Text category={"h5"} style={styles.mb5}>Personnel</Text>
                        <Text category={"p2"} style={styles.mb5}>If you are a PP Beauty &amp; Academy worker or applicant, we collect information you voluntarily provide to us. We use the information collected for Human Resources purposes in order to administer benefits to workers and screen applicants.</Text>
                        <Text category={"p2"} style={styles.mb5}>You may contact us in order to (1) update or correct your information, (2) change your preferences with respect to communications and other information you receive from us, or (3) receive a record of the information we have relating to you. Such updates, corrections, changes and deletions will have no effect on other information that we maintain, or information that we have provided to third parties in accordance with this Privacy Policy prior to such update, correction, change or deletion.</Text>
                        <Text category={"h5"} style={styles.mb5}>Sale of Business</Text>
                        <Text category={"p2"} style={styles.mb5}>We reserve the right to transfer information to a third party in the event of a sale, merger or other transfer of all or substantially all of the assets of PP Beauty &amp; Academy or any of its Corporate Affiliates (as defined herein), or that portion of PP Beauty &amp; Academy or any of its Corporate Affiliates to which the Service relates, or in the event that we discontinue our business or file a petition or have filed against us a petition in bankruptcy, reorganization or similar proceeding, provided that the third party agrees to adhere to the terms of this Privacy Policy.</Text>

                        <Text category={"h5"} style={styles.mb5}>Affiliates</Text>
                        <Text category={"p2"} style={styles.mb5}>We may disclose information (including personal information) about you to our Corporate Affiliates. For purposes of this Privacy Policy, "Corporate Affiliate" means any person or entity which directly or indirectly controls, is controlled by or is under common control with PP Beauty &amp; Academy, whether by ownership or otherwise. Any information relating to you that we provide to our Corporate Affiliates will be treated by those Corporate Affiliates in accordance with the terms of this Privacy Policy.</Text>

                        <Text category={"h5"} style={styles.mb5}>Governing Law</Text>
                        <Text category={"p2"} style={styles.mb5}>This Privacy Policy is governed by the laws of Canada without regard to its conflict of laws provision. You consent to the exclusive jurisdiction of the courts in connection with any action or dispute arising between the parties under or in connection with this Privacy Policy except for those individuals who may have rights to make claims under Privacy Shield, or the Swiss-US framework.</Text>
                        <Text category={"p2"} style={styles.mb5}>The laws of Canada, excluding its conflicts of law rules, shall govern this Agreement and your use of the website/app. Your use of the website/app may also be subject to other local, state, national, or international laws.</Text>
                        <Text category={"p2"} style={styles.mb5}>By using PP Beauty &amp; Academy or contacting us directly, you signify your acceptance of this Privacy Policy. If you do not agree to this Privacy Policy, you should not engage with our website, or use our services. Continued use of the website, direct engagement with us, or following the posting of changes to this Privacy Policy that do not significantly affect the use or disclosure of your personal information will mean that you accept those changes.</Text>

                        <Text category={"h5"} style={styles.mb5}>Your Consent</Text>
                        <Text category={"p2"} style={styles.mb5}>We've updated our Privacy Policy to provide you with complete transparency into what is being set when you visit our site and how it's being used. By using our website/app, registering an account, or making a purchase, you hereby consent to our Privacy Policy and agree to its terms.</Text>

                        <Text category={"h5"} style={styles.mb5}>Links to Other Websites</Text>
                        <Text category={"p2"} style={styles.mb5}>This Privacy Policy applies only to the Services. The Services may contain links to other websites not operated or controlled by PP Beauty &amp; Academy. We are not responsible for the content, accuracy or opinions expressed in such websites, and such websites are not investigated, monitored or checked for accuracy or completeness by us. Please remember that when you use a link to go from the Services to another website, our Privacy Policy is no longer in effect. Your browsing and interaction on any other website, including those that have a link on our platform, is subject to that website’s own rules and policies. Such third parties may use their own cookies or other methods to collect information about you.</Text>
                        <Text category={"h5"} style={styles.mb5}>Cookies</Text>
                        <Text category={"p2"} style={styles.mb5}>PP Beauty &amp; Academy uses "Cookies" to identify the areas of our website that you have visited. A Cookie is a small piece of data stored on your computer or mobile device by your web browser. We use Cookies to enhance the performance and functionality of our website/app but are non-essential to their use. However, without these cookies, certain functionality like videos may become unavailable or you would be required to enter your login details every time you visit the website/app as we would not be able to remember that you had logged in previously. Most web browsers can be set to disable the use of Cookies. However, if you disable Cookies, you may not be able to access functionality on our website correctly or at all. We never place Personally Identifiable Information in Cookies.</Text>

                        <Text category={"h5"} style={styles.mb5}>Blocking and disabling cookies and similar technologies</Text>
                        <Text category={"p2"} style={styles.mb5}>Wherever you're located you may also set your browser to block cookies and similar technologies, but this action may block our essential cookies and prevent our website from functioning properly, and you may not be able to fully utilize all of its features and services. You should also be aware that you may also lose some saved information (e.g. saved login details, site preferences) if you block cookies on your browser. Different browsers make different controls available to you. Disabling a cookie or category of cookie does not delete the cookie from your browser, you will need to do this yourself from within your browser, you should visit your browser's help menu for more information.</Text>
                        <Text category={"h5"} style={styles.mb5}>Remarketing Services</Text>
                        <Text category={"p2"} style={styles.mb5}>We use remarketing services. What Is Remarketing? In digital marketing, remarketing (or retargeting) is the practice of serving ads across the internet to people who have already visited your website. It allows your company to seem like they're “following” people around the internet by serving ads on the websites and platforms they use most.</Text>
                        <Text category={"h5"} style={styles.mb5}>Payment Details</Text>
                        <Text category={"p2"} style={styles.mb5}>In respect to any credit card or other payment processing details you have provided us, we commit that this confidential information will be stored in the most secure manner possible.</Text>
                        <Text category={"h5"} style={styles.mb5}>Kids' Privacy</Text>
                        <Text category={"p2"} style={styles.mb5}>We collect information from kids under the age of 13 just to better our services. If You are a parent or guardian and You are aware that Your child has provided Us with Personal Data without your permission, please contact Us. If We become aware that We have collected Personal Data from anyone under the age of 13 without verification of parental consent, We take steps to remove that information from Our servers.</Text>
                        <Text category={"h5"} style={styles.mb5}>Changes To Our Privacy Policy</Text>
                        <Text category={"p2"} style={styles.mb5}>We may change our Service and policies, and we may need to make changes to this Privacy Policy so that they accurately reflect our Service and policies. Unless otherwise required by law, we will notify you (for example, through our Service) before we make changes to this Privacy Policy and give you an opportunity to review them before they go into effect. Then, if you continue to use the Service, you will be bound by the updated Privacy Policy. If you do not want to agree to this or any updated Privacy Policy, you can delete your account.</Text>

                        <Text category={"h5"} style={styles.mb5}>Third-Party Services</Text>
                        <Text category={"p2"} style={styles.mb5}>We may display, include or make available third-party content (including data, information, applications and other products services) or provide links to third-party websites or services ("Third- Party Services").</Text>
                        <Text category={"p2"} style={styles.mb5}>You acknowledge and agree that PP Beauty &amp; Academy shall not be responsible for any Third-Party Services, including their accuracy, completeness, timeliness, validity, copyright compliance, legality, decency, quality or any other aspect thereof. PP Beauty &amp; Academy does not assume and shall not have any liability or responsibility to you or any other person or entity for any Third-Party Services.</Text>
                        <Text category={"p2"} style={styles.mb5}>Third-Party Services and links thereto are provided solely as a convenience to you and you access and use them entirely at your own risk and subject to such third parties' terms and conditions.</Text>
                        <Text category={"h5"} style={styles.mb5}>Facebook Pixel</Text>
                        <Text category={"p2"} style={styles.mb5}>Facebook pixel is an analytics tool that allows you to measure the effectiveness of your advertising by understanding the actions people take on your website. You can use the pixel to: Make sure your ads are shown to the right people. Facebook pixel may collect information from your device when you use the service. Facebook pixel collects information that is held in accordance with its Privacy Policy</Text>
                        <Text category={"h5"} style={styles.mb5}>Tracking Technologies</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p1"} style={styles.mb5}>Google Maps API</Text>
                            <Text category={"p2"} style={styles.mb5}>Google Maps API is a robust tool that can be used to create a custom map, a searchable map, check-in functions, display live data synching with location, plan routes, or create a mashup just to name a few.</Text>
                            <Text category={"p2"} style={styles.mb5}>Google Maps API may collect information from You and from Your Device for security purposes.</Text>
                            <Text category={"p2"} style={styles.mb5}>Google Maps API collects information that is held in accordance with its Privacy Policy</Text>
                            <Text category={"p2"} style={styles.mb5}>Local Storage</Text>
                           
                            <Text category={"p2"} style={styles.mb5}>Local Storage sometimes known as DOM storage, provides web apps with methods and protocols for storing client-side data. Web storage supports persistent data storage, similar to cookies but with a greatly enhanced capacity and no information stored in the HTTP request header.</Text>
                        </View>
                        <Text category={"h5"} style={styles.mb5}>Information about General Data Protection Regulation (GDPR)</Text>
                        <Text category={"p2"} style={styles.mb5}>We may be collecting and using information from you if you are from the European Economic Area (EEA), and in this section of our Privacy Policy we are going to explain exactly how and why is this data collected, and how we maintain this data under protection from being replicated or used in the wrong way.</Text>

                        <Text category={"h5"} style={styles.mb5}>What is GDPR?</Text>
                        <Text category={"p2"} style={styles.mb5}>GDPR is an EU-wide privacy and data protection law that regulates how EU residents' data is protected by companies and enhances the control the EU residents have, over their personal data.</Text>
                        <Text category={"p2"} style={styles.mb5}>The GDPR is relevant to any globally operating company and not just the EU-based businesses and EU residents. Our customers’ data is important irrespective of where they are located, which is why we have implemented GDPR controls as our baseline standard for all our operations worldwide.</Text>

                        <Text category={"h5"} style={styles.mb5}>What is personal data?</Text>
                        <Text category={"p2"} style={styles.mb5}>Any data that relates to an identifiable or identified individual. GDPR covers a broad spectrum of information that could be used on its own, or in combination with other pieces of information, to identify a person. Personal data extends beyond a person’s name or email address. Some examples include financial information, political opinions, genetic data, biometric data, IP addresses, physical address, sexual orientation, and ethnicity.</Text>
                        <Text category={"p2"} style={styles.mb5}>The Data Protection Principles include requirements such as:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Personal data collected must be processed in a fair, legal, and transparent way and should only be used in a way that a person would reasonably expect.</Text>
                            <Text category={"p2"} style={styles.mb5}>Personal data should only be collected to fulfil a specific purpose and it should only be used for that purpose. Organizations must specify why they need the personal data when they collect it.</Text>
                            <Text category={"p2"} style={styles.mb5}>Personal data should be held no longer than necessary to fulfil its purpose.</Text>
                            <Text category={"p2"} style={styles.mb5}>People covered by the GDPR have the right to access their own personal data. They can also request a copy of their data, and that their data be updated, deleted, restricted, or moved to another organization.</Text>
                        </View>

                        <Text category={"h5"} style={styles.mb5}>Why is GDPR important?</Text>
                        <Text category={"p2"} style={styles.mb5}>GDPR adds some new requirements regarding how companies should protect individuals' personal data that they collect and process. It also raises the stakes for compliance by increasing enforcement and imposing greater fines for breach. Beyond these facts it's simply the right thing to do. At PP Beauty &amp; Academy we strongly believe that your data privacy is very important and we already have solid security and privacy practices in place that go beyond the requirements of this new regulation.</Text>

                        <Text category={"h5"} style={styles.mb5}>Individual Data Subject's Rights - Data Access, Portability and Deletion</Text>
                        <Text category={"p2"} style={styles.mb5}>We are committed to helping our customers meet the data subject rights requirements of GDPR. PP Beauty &amp; Academy processes or stores all personal data in fully vetted, DPA compliant vendors. We do store all conversation and personal data for up to 6 years unless your account is deleted. In which case, we dispose of all data in accordance with our Terms of Service and Privacy Policy, but we will not hold it longer than 60 days.</Text>
                        <Text category={"p2"} style={styles.mb5}>We are aware that if you are working with EU customers, you need to be able to provide them with the ability to access, update, retrieve and remove personal data. We got you! We've been set up as self service from the start and have always given you access to your data and your customers data. Our customer support team is here for you to answer any questions you might have about working with the API.</Text>
                        <Text category={"h5"} style={styles.mb5}>California Residents</Text>
                        <Text category={"p2"} style={styles.mb5}>The California Consumer Privacy Act (CCPA) requires us to disclose categories of Personal Information we collect and how we use it, the categories of sources from whom we collect Personal Information, and the third parties with whom we share it, which we have explained above.</Text>
                        <Text category={"p2"} style={styles.mb5}>We are also required to communicate information about rights California residents have under California law. You may exercise the following rights:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Right to Know and Access. You may submit a verifiable request for information regarding the: (1) categories of Personal Information we collect, use, or share; (2) purposes for which categories of Personal Information are collected or used by us; (3) categories of sources from which we collect Personal Information; and (4) specific pieces of Personal Information we have collected about you.</Text>
                            <Text category={"p2"} style={styles.mb5}>Right to Equal Service. We will not discriminate against you if you exercise your privacy rights.</Text>
                            <Text category={"p2"} style={styles.mb5}>Right to Delete. You may submit a verifiable request to close your account and we will delete Personal Information about you that we have collected.</Text>
                            <Text category={"p2"} style={styles.mb5}>Request that a business that sells a consumer's personal data, not sell the consumer's personal data.</Text>
                        </View>
                        <Text category={"p2"} style={styles.mb5}>If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.</Text>
                        <Text category={"p2"} style={styles.mb5}>We do not sell the Personal Information of our users.</Text>
                        <Text category={"p2"} style={styles.mb5}>For more information about these rights, please contact us.</Text>
                        <Text category={"h5"} style={styles.mb5}>California Online Privacy Protection Act (CalOPPA)</Text>
                        <Text category={"p2"} style={styles.mb5}>CalOPPA requires us to disclose categories of Personal Information we collect and how we use it, the categories of sources from whom we collect Personal Information, and the third parties with whom we share it, which we have explained above.</Text>
                        <Text category={"p2"} style={styles.mb5}>CalOPPA users have the following rights:</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Right to Know and Access. You may submit a verifiable request for information regarding the: (1) categories of Personal Information we collect, use, or share; (2) purposes for which categories of Personal Information are collected or used by us; (3) categories of sources from which we collect Personal Information; and (4) specific pieces of Personal Information we have collected about you.</Text>
                            <Text category={"p2"} style={styles.mb5}>Right to Equal Service. We will not discriminate against you if you exercise your privacy rights.</Text>
                            <Text category={"p2"} style={styles.mb5}>Right to Delete. You may submit a verifiable request to close your account and we will delete Personal Information about you that we have collected.</Text>
                            <Text category={"p2"} style={styles.mb5}>Right to request that a business that sells a consumer's personal data, not sell the consumer's personal data.</Text>
                        </View>
                            <Text category={"p2"} style={styles.mb5}>If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.</Text>
                            <Text category={"p2"} style={styles.mb5}>We do not sell the Personal Information of our users.</Text>
                            <Text category={"p2"} style={styles.mb5}>For more information about these rights, please contact us.</Text>
                            <Text category={"h5"} style={styles.mb5}>Contact Us</Text>
                            <Text category={"p2"} style={styles.mb5}>Don't hesitate to contact us if you have any questions.</Text>
                        <View style={{marginLeft: 3}}>
                            <Text category={"p2"} style={styles.mb5}>Via Email: contact@ppbeautyacademy.com</Text>
                            <Text category={"p2"} style={styles.mb5}>Via Phone Number:  +1 416 792 6268</Text>
                            <Text category={"p2"} style={styles.mb5}>Via this Link:  https://www.ppbeautyacademy.com/en/contact</Text>
                            <Text category={"p2"} style={styles.mb5}>Via this Address: 80 Bloor St W, Suite 401, Toronto, ON Canada. M5S2V1</Text>
                        </View>
                    </ScrollView>
                }
            </View>
        </BaseScreen>
    )
}

export default PolicyScreen
