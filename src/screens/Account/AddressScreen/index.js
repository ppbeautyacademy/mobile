import {Button, Card, CheckBox, Spinner, Text} from '@ui-kitten/components'
import React, {useContext, useEffect, useRef, useState} from 'react'
import {Platform, StyleSheet, View} from 'react-native'
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler'
import {translate} from '../../../../config/i18n'
import context from '../../../../context/context'
import {HTTP_STATUS_OK, LANGUAGE_CODES} from '../../../assets/constants'
import {COLORS} from '../../../assets/theme/colors'
import {commonStyles} from '../../../assets/theme/styles'
import ENAddress from '../../../component/address/ENAddress'
import VNAddress from '../../../component/address/VNAddress'
import BaseScreen from '../../../component/BaseScreen'
import ModalWithModalize from '../../../component/ModalWithModalize'
import {useDeleteUserAddress, useGetUserInfo} from '../../../services/Hooks'
import Icon from 'react-native-vector-icons/MaterialIcons'
import KeyboardScrollView from '../../../component/KeyboardScrollView'
import {
    deleteAddress,
    updateAddress,
    updateMainAddress,
} from '../../../services/ServiceWorker'
import {Toast} from 'popup-ui'

const EditAddressModal = ({
    isShow,
    onHide,
    editAddress,
    onUpdateAddress = () => {},
    onDeleteAddress = () => {},
}) => {
    const {state, actions} = useContext(context)
    const modalRef = useRef()
    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState({})
    const [address, setAddress] = useState({
        id: editAddress?.id,
        refDistrict: editAddress?.refDistrict,
        refCountry: editAddress?.refCountry,
        refProvince: editAddress?.refProvince,
        refState: editAddress?.refState,
        refWard: editAddress?.refWard,
        city: editAddress?.city,
        country: editAddress?.country,
        district: editAddress?.district,
        province: editAddress?.province,
        state: editAddress?.state,
        ward: editAddress?.ward,
        street: editAddress?.street,
        zipCode: editAddress?.zipCode,
        address: editAddress?.address,
        main: editAddress?.main,
    })
    const [isMain, setIsMain] = useState(editAddress?.main)

    useEffect(() => {
        if (isShow) {
            handleOnShowAddressModal()
            setAddress(editAddress)
        } else {
            handleOnHideAddressModal()
            setAddress({})
            setIsMain(false)
        }
    }, [isShow])

    const onStreetChange = street => {
        setAddress({
            ...address,
            street: street,
        })
    }

    const onCountryChange = country => {
        setAddress({
            ...address,
            refCountry: country,
        })
    }

    const onStateChange = state => {
        setAddress({
            ...address,
            refState: state,
        })
    }

    const onCityChange = city => {
        setAddress({
            ...address,
            city: city,
        })
    }

    const onProvinceChange = province => {
        setAddress({
            ...address,
            refProvince: province,
        })
    }

    const onDistrictChange = district => {
        setAddress({
            ...address,
            refDistrict: district,
        })
    }

    const onWardChange = ward => {
        setAddress({
            ...address,
            refWard: ward,
        })
    }

    const onZipCodeChange = zipCode => {
        setAddress({
            ...address,
            zipCode: zipCode,
        })
    }
    const handleOnShowAddressModal = () => {
        modalRef.current?.open()
    }
    const handleOnHideAddressModal = () => {
        onHide()
        modalRef.current?.close()
    }
    const handleOnUpdateAddress = async () => {
        setLoading(true)
        let isError = false
        let errors = {}
        if (state.version) {
            if (state.version === LANGUAGE_CODES.vi.value) {
                if (
                    !address ||
                    !address.street ||
                    !address.refWard ||
                    !address.refDistrict ||
                    !address.refProvince
                ) {
                    errors = {
                        ...errors,
                        street: !address.street,
                        refWard: !address.refWard,
                        refDistrict: !address.refDistrict,
                        refProvince: !address.refProvince,
                    }

                    isError = true
                }
            } else {
                if (
                    !address ||
                    !address.street ||
                    !address.city ||
                    !address.refState ||
                    !address.refCountry ||
                    !address.zipCode
                ) {
                    errors = {
                        ...errors,
                        street: !address.street,
                        city: !address.city,
                        refState: !address.refState,
                        refCountry: !address.refCountry,
                        zipCode: !address.zipCode,
                    }

                    isError = true
                }
            }
        }
        if (!isError) {
            actions.showAppSpinner(true)
            const updateAddressResponse = await updateAddress(
                state.userInfo?.id,
                state.version,
                address,
            ).finally(() => {
                actions.showAppSpinner(false)
            })

            if (updateAddressResponse && Array.isArray(updateAddressResponse)) {
                onUpdateAddress(updateAddressResponse)
                handleOnHideAddressModal()
            } else {
                Toast.show({
                    title: translate('common.errorTitle'),
                    text: 'Fail to update main address',
                    color: COLORS.ERROR_NOTIFY,
                    timing: 3500,
                })
            }
        } else {
            setErrors(errors)
        }
        setLoading(false)
    }

    return (
        <ModalWithModalize getModalRef={modalRef} onClosed={() => onHide()}>
            <KeyboardScrollView
                extraScrollHeight={Platform.OS === 'ios' ? 0 : 100}>
                <View
                    style={{
                        padding: 32,
                        borderRadius: 15,
                        backgroundColor: COLORS.WHITE_BACKGROUND,
                        zIndex: 5,
                    }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                        <CheckBox
                            disabled={address.main && isMain}
                            style={{width: 200}}
                            checked={address.main}
                            onChange={() =>
                                setAddress({...address, main: !address.main})
                            }>
                            {translate('account.isMainAddress')}
                        </CheckBox>

                        {address && address.id && (
                            <View>
                                <TouchableOpacity
                                    onPress={() => onDeleteAddress(address)}>
                                    <Text
                                        category={'label'}
                                        appearance={'hint'}
                                        status={'danger'}>
                                        {translate('account.deleteAddress')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    </View>
                    <View style={commonStyles.separator.row16} />
                    {state.version === LANGUAGE_CODES.vi.value ? (
                        <VNAddress
                            addressBean={address}
                            setStreet={onStreetChange}
                            setRefWard={onWardChange}
                            setRefDistrict={onDistrictChange}
                            setRefProvince={onProvinceChange}
                            errors={errors}
                        />
                    ) : (
                        <ENAddress
                            addressBean={address}
                            setStreet={onStreetChange}
                            setCity={onCityChange}
                            setRefState={onStateChange}
                            setRefCountry={onCountryChange}
                            setZipCode={onZipCodeChange}
                            errors={errors}
                        />
                    )}
                    <View style={commonStyles.separator.row32} />
                    <View style={commonStyles.separator.row32} />
                    <Button
                        status="primary"
                        onPress={handleOnUpdateAddress}
                        accessoryLeft={loading && LoadingIndicator}>
                        {translate('common.update')}
                    </Button>
                </View>
            </KeyboardScrollView>
        </ModalWithModalize>
    )
}

const LoadingIndicator = props => (
    <View style={[props.style, styles.indicator]}>
        <Spinner size="small" />
    </View>
)

const AddressScreen = () => {
    //#region Declaration
    const {state, actions} = useContext(context)
    const userAddresses = state.userInfo?.addresses ?? ''

    const [isShowAddressForm, setIsShowAddressForm] = useState(false)
    const [addressEditting, setAddressEditting] = useState({})
    //#endregion

    const onUpdateAddressInfo = address => {
        setIsShowAddressForm(true)
        setAddressEditting(address ? {...address} : {})
    }

    const onUpdateAddress = async addresses => {
        actions.showAppSpinner(true)
        actions.setUserInfo({
            ...state.userInfo,
            addresses: addresses ? addresses : [],
        })
        actions.showAppSpinner(false)
    }

    const handleDeleteAddress = async addressId => {
        const deleteAddressResponse = await deleteAddress(
            state.userInfo?.id,
            addressId,
        )
        if (deleteAddressResponse && Array.isArray(deleteAddressResponse)) {
            setIsShowAddressForm(false)
            setAddressEditting({})
            actions.setUserInfo({
                ...state.userInfo,
                addresses: deleteAddressResponse ? deleteAddressResponse : [],
            })
        }
    }

    const onDeleteAddress = address => {
        actions.showAppPopup({
            isVisible: true,
            okText: translate('common.yes'),
            cancelText: translate('common.no'),
            message: translate('account.areYouSureDeleteAddress'),
            onOkHandler: async () => {
                actions.showAppSpinner(true)
                await handleDeleteAddress(address?.id)
                actions.showAppSpinner(false)
            },
        })
    }

    return (
        <BaseScreen
            title="Address"
            modals={
                <EditAddressModal
                    isShow={isShowAddressForm}
                    onHide={() => setIsShowAddressForm(false)}
                    editAddress={addressEditting}
                    onUpdateAddress={onUpdateAddress}
                    onDeleteAddress={onDeleteAddress}
                />
            }>
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    paddingBottom: 70,
                }}>
                <View style={styles.itemWrapper}>
                    <Card status={'basic'}>
                        <View style={styles.cardWrapper}>
                            <Text style={styles.addressWidth}>
                                {translate('account.addNewAddress')}
                            </Text>
                            <View style={styles.navigationWrapper}>
                                <TouchableOpacity
                                    onPress={() => onUpdateAddressInfo({})}>
                                    <View
                                        style={[
                                            styles.navigationContainer,
                                            styles.navigationBackground,
                                        ]}>
                                        <Icon
                                            name="add"
                                            size={20}
                                            color={COLORS.SUB}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Card>
                </View>

                {userAddresses.map(
                    (address, idx) =>
                        address.version === state.version && (
                            <View key={idx} style={styles.itemWrapper}>
                                <Card
                                    status={
                                        address.main ? 'success' : 'primary'
                                    }>
                                    <View style={styles.cardWrapper}>
                                        <Text style={styles.addressWidth}>
                                            {address.address}
                                        </Text>
                                        <View style={styles.navigationWrapper}>
                                            <TouchableOpacity
                                                onPress={() =>
                                                    onUpdateAddressInfo(address)
                                                }>
                                                <View
                                                    style={[
                                                        styles.navigationContainer,
                                                        styles.navigationBackground,
                                                    ]}>
                                                    <Icon
                                                        name="settings"
                                                        size={16}
                                                        color={COLORS.SUB}
                                                    />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Card>
                            </View>
                        ),
                )}
            </ScrollView>
        </BaseScreen>
    )
}

export default AddressScreen

const styles = StyleSheet.create({
    itemWrapper: {
        paddingHorizontal: 16,
        marginBottom: 10,
    },
    cardWrapper: {
        flexDirection: 'row',
    },
    addressWidth: {
        width: '90%',
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
})
