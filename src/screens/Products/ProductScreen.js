import React, {useContext, useEffect, useState, useRef} from 'react'
import {
    View,
    TouchableOpacity,
    FlatList,
    StyleSheet,
    Dimensions,
    ImageBackground,
} from 'react-native'
import {COLORS} from '../../assets/theme/colors'
import BaseScreen from '../../component/BaseScreen'
import {
    Text,
    Icon as KittenIcon,
    IndexPath,
    Menu,
    MenuItem,
    BottomNavigation,
    BottomNavigationTab,
    Radio,
} from '@ui-kitten/components'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {SearchBar} from 'react-native-elements'
import {ScreenIds} from '../../ScreenIds'
import context from '../../../context/context'
import {
    getProductBundleList,
    getProductCategoryOptions,
    getProductList,
    removeFavorite,
    saveFavorite,
} from '../../services/ServiceWorker'
import FastImage from 'react-native-fast-image'
import {LANGUAGE_CODES} from '../../assets/constants'
import CartIcon from '../Cart/CartIcon'
import { Modalize } from 'react-native-modalize'
import renderPrice from '../../component/money/PriceUtils'
import {translate} from '../../../config/i18n'
import LoadingSkeleton from '../../component/skeleton/LoadingSkeleton'
import { SKELETON_TYPE } from '../../component/SkeletonView'

const gridWidth = Dimensions.get('window').width / 2 - 32
const height = Dimensions.get('window').height

const viewModes = {
    grid: {label: 'Grid View', value: 0},
    list: {label: 'List View', value: 1},
}

const popularityTypes = {
    new: {label: 'New', value: 0},
    top: {label: 'Top', value: 1},
    promoted: {label: 'promoted', value: 2},
    sale: {label: 'sale', value: 3},
}

const productModes = {
    viewAll: {label: 'View All', value: 'viewAll'},
    search: {label: 'Search Products', value: 'search'},
}

const initialPageInfo = {
    pageNumber: 0,
    totalRecords: 0,
    rows: 6,
    totalPages: 0,
}

export const renderProductItem = ({item, key}, numColumns, navigation, isPadding = false, version, currency, addFav, removeFav, showFav = true, useHomeCard = false) => {
    return (
        <TouchableOpacity
            key={key}
            activeOpacity={0.8}
            onPress={() =>
                navigation.navigate(ScreenIds.ProductDetail, {
                    alias: item.alias,
                })
            }>
            <View
                style={
                    useHomeCard ? styles.homeCard :
                    numColumns && numColumns > 1
                        ? [
                              styles.card,
                              isPadding
                                  ? styles.smallMargin
                                  : styles.mediumMargin,
                          ]
                        : styles.cardExpand
                }>
                <FastImage
                    source={{uri: item.mainImage}}
                    style={styles.cardImage}
                />

                <Text style={styles.title} numberOfLines={2}>
                    {item.name}
                </Text>
                <View style={styles.priceContainer}>
                    <Text appearance="hint" style={styles.price} category="label">
                        {version === LANGUAGE_CODES.en.value && `${currency} `}{renderPrice(item.productPriceDisplay)}{version === LANGUAGE_CODES.vi.value && ` ${currency}`}
                    </Text>
                    {showFav &&
                        <TouchableOpacity onPress={() => item.liked ? removeFav(item.favoriteId, item.id) : addFav(item.id)}>
                            <View style={{alignItems: 'flex-end'}}>
                                <View
                                    style={[
                                        styles.favoriteContainer,
                                        item.liked
                                            ? styles.backgroundPink
                                            : styles.backgroundGray,
                                    ]}>
                                    <Icon
                                        name="favorite"
                                        size={18}
                                        color={
                                            item.liked ? COLORS.ERROR : COLORS.BLACK
                                        }
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        </TouchableOpacity>
    )
}

const ProductScreen = ({navigation}) => {
    const {state} = useContext(context)

    const currentProductMode = useState(productModes.viewAll.value)

    //#region BUNDLES
    const [bundle, setBundle] = useState({
        bundleProducts: [],
        ...initialPageInfo,
    })

    const [isBundleProductLoaded, setIsBundleProductLoaded] = useState(false)

    const loadBundleProduct = async pageInfo => {
        const {pageNumber, rows} = pageInfo ?? {}
        const bundleData = await getProductBundleList(
            state.version,
            pageNumber ?? bundle.pageNumber,
            rows ?? bundle.rows,
            state.userInfo?.id
        )
        if (bundleData && bundleData.content) {
            setBundle({
                bundleProducts: bundleData.content,
                pageNumber:
                    bundleData.pageable && bundleData.pageable.pageNumber,
                rows: bundleData.pageable && bundleData.pageable.pageSize,
                totalRecords: bundleData.totalElements,
                totalPages: bundleData.totalPages,
            })

            setIsBundleProductLoaded(true)
        }
    }

    const fetchMoreProductBundles = async () => {
        if (bundle.pageNumber + 1 >= bundle.totalPages) {
            return
        }
        const bundleData = await getProductBundleList(
            state.version,
            bundle.pageNumber + 1,
            bundle.rows,
            state.userInfo?.id
        )
        if (bundleData && bundleData.content) {
            setBundle({
                bundleProducts: bundle.bundleProducts.concat(
                    bundleData.content,
                ),
                pageNumber:
                    bundleData.pageable && bundleData.pageable.pageNumber,
                rows: bundleData.pageable && bundleData.pageable.pageSize,
                totalRecords: bundleData.totalElements,
                totalPages: bundleData.totalPages,
            })
        }
    }

    useEffect(async () => {
        if (!isBundleProductLoaded) {
            await loadBundleProduct(initialPageInfo)
        }
    }, [])

    const renderBundleHeader = () => {
        return(
            <View style={{marginTop: 5, marginBottom: 15}}>
                <View style={{display: "flex", justifyContent: "center", alignItems: "center", marginBottom: 10}}>
                    <FastImage source={require("../../assets/images/FullSet.png")}
                        style={{width: 360, height: 180, resizeMode: "contain"}}
                    />
                </View>
                <View style={{marginHorizontal: 20}}>
                    <Text style={styles.textJustify, styles.mb5} category={"label"}>{`With PP Beauty & Academy’s color collection, we intend to start your own beauty narrative. These color sets are the result of research by the founder Jennipher Pham and collaboration with color experts by synthesizing color trends from around the world to create perfect products.`}</Text>
                    <Text style={styles.textJustify, styles.mb5} category={"label"}>{`The innovative formula brings two superior effects to customers. There are a bold with long-lasting effect andinstant coloring with pure and standard appearance.`}</Text>
                    <Text style={styles.textJustify, styles.mb5} category={"label"}>Seven color sets of modern colors, each with its own colorant. Each set contains three major colors with distinctshades to provide you creative inspiration. You can entirely blend colors to make the bold, stunning ones that to bring out these gorgeous beauty. On the other hand, the color sets also handle error strokes or color shading to help you create a perfect work.</Text>
                </View>
            </View>
        )
    }

    const renderBundleList = bundleList => {
        const colNumbers =
            selectedViewMode.row === viewModes.grid.value
                ? viewModes.grid.value + 2
                : viewModes.list.value

        const renderKey =
            colNumbers > 1
                ? '#bundle'
                : '-bundle'

        return (
            <FlatList
                key={renderKey}
                showsVerticalScrollIndicator={false}
                horizontal={false}
                contentContainerStyle={[
                    {
                        flexGrow: 1,
                        backgroundColor: COLORS.WHITE,
                        paddingBottom: 100,
                    },
                ]}
                numColumns={colNumbers}
                data={bundleList}
                renderItem={item =>
                    renderProductItem(item, colNumbers, navigation, true, state.version, state.appCurrency, addFav, removeFav)
                }
                keyExtractor={item => {
                    return `${
                        colNumbers > 1
                            ? `bundle-grid-${item.id}`
                            : `bundle-list-${item.id}`
                    }`
                }}
                ListHeaderComponent={renderBundleHeader}
                onEndReached={fetchMoreProductBundles}
                onEndReachedThreshold={0.7}
            />
        )
    }
    //#endregion

    //#region INDIVIDUAL
    const [product, setProduct] = useState({
        products: [],
        ...initialPageInfo,
    })
    const [isProductLoaded, setIsProductLoaded] = useState(false)

    const [categoryOptions, setCategoryOptions] = useState([])
    const [selectedCategory, setSelectedCategory] = useState(null)

    const loadProducts = async pageInfo => {
        const {pageNumber, rows} = pageInfo ?? {}
        const productData = await getProductList(
            state.version,
            pageNumber ?? product.pageNumber,
            rows ?? product.rows,
            state.userInfo?.id
        )

        if (productData && productData.content) {
            setProduct({
                products: productData.content,
                pageNumber:
                    productData.pageable && productData.pageable.pageNumber,
                rows: productData.pageable && productData.pageable.pageSize,
                totalRecords: productData.totalElements,
                totalPages: productData.totalPages,
            })

            setIsProductLoaded(true)
        }
    }

    const fetchMoreProducts = async () => {
        if (product.pageNumber + 1 >= product.totalPages) {
            return
        }

        const productData = await getProductList(
            state.version,
            product.pageNumber + 1,
            product.rows,
            state.userInfo?.id
        )

        if (productData && productData.content) {
            setProduct({
                products: product.products.concat(
                    filterBundleProducts(productData.content),
                ),
                pageNumber:
                    productData.pageable && productData.pageable.pageNumber,
                rows: productData.pageable && productData.pageable.pageSize,
                totalRecords: productData.totalElements,
                totalPages: productData.totalPages,
            })
        }
    }

    const filterBundleProducts = productList => {
        if (productList && Array.isArray(productList)) {
            let returnedList = []

            for (let i = 0; i < productList.length; i++) {
                if (
                    !productList[i].bundle ||
                    (productList[i].bundle && productList[i].displayBundle)
                ) {
                    returnedList.push(productList[i])
                }
            }

            return returnedList
        }
    }

    const loadProductCategories = async () => {
        const categoryOptions = await getProductCategoryOptions()
        if (categoryOptions && Array.isArray(categoryOptions)) {
            setCategoryOptions(categoryOptions)
        }
    }

    useEffect(async () => {
        if (!isProductLoaded) {
            await loadProducts(initialPageInfo)
        }
    }, [])

    const renderProductList = productList => {
        const colNumbers =
            selectedViewMode.row === viewModes.grid.value
                ? viewModes.grid.value + 2
                : viewModes.list.value

        const renderKey =
            colNumbers > 1
                ? '#product'
                : '-product'

        return (
            <FlatList
                key={renderKey}
                showsVerticalScrollIndicator={false}
                horizontal={false}
                contentContainerStyle={[
                    {
                        flexGrow: 1,
                        backgroundColor: COLORS.WHITE,
                        paddingBottom: 100,
                    },
                ]}
                numColumns={colNumbers}
                data={productList}
                renderItem={item =>
                    renderProductItem(item, colNumbers, navigation, true, state.version, state.appCurrency, addFav, removeFav)
                }
                keyExtractor={item => {
                    return `${
                        colNumbers > 1
                            ? `product-grid-${item.id}`
                            : `product-list-${item.id}`
                    }`
                }}
                onEndReached={fetchMoreProducts}
                onEndReachedThreshold={0.7}
            />
        )
    }
    //#endregion

    //#region Top Products Fav
    const addFav = async(productId) => {
        if (productId) {
            let data = {
                productId: productId,
            }

            const favoritePost = await saveFavorite(state.userInfo?.id, data)

            if (favoritePost && favoritePost.id) {
                const {productsFavorite} = favoritePost;

                if(productsFavorite && productsFavorite.id) {
                    let favProducts = [];
                    let currentProducts = selectedIndex === 0 ? bundle.bundleProducts : product.products;

                    for(let i = 0; i < currentProducts.length; i++) {
                        if(currentProducts[i].id === productsFavorite.id) {
                            currentProducts[i].liked = true;
                            currentProducts[i].favoriteId = favoritePost.id;
                        }

                        favProducts.push(currentProducts[i])
                    }
                    
                    if(selectedIndex === 0) {
                        setBundle({
                            ...bundle,
                            bundleProducts: favProducts
                        })
                    } else {
                        setProduct({
                            ...product,
                            products: favProducts
                        })
                    }
                }
            }
        }
    }

    const removeFav = async(favoriteId, productId) => {
        if(favoriteId) {
            const favoriteDelete = await removeFavorite(favoriteId)

            if(favoriteDelete === HTTP_STATUS_OK) {
                if(productId) {
                    let favProducts = [];
                    let currentProducts = selectedIndex === 0 ? bundle.bundleProducts : product.products;

                    for(let i = 0; i < currentProducts.length; i++) {
                        if(currentProducts[i].id === productId) {
                            currentProducts[i].liked = false;
                            currentProducts[i].favoriteId = null;
                        }

                        favProducts.push(currentProducts[i])
                    }

                    if(selectedIndex === 0) {
                        setBundle({
                            ...bundle,
                            bundleProducts: favProducts
                        })
                    } else {
                        setProduct({
                            ...product,
                            products: favProducts
                        })
                    }
                }
            }
        }
    }
    //#endregion

    //#region VIEW MODE AND FILTER
    const modalRef = useRef()

    const [selectedIndex, setSelectedIndex] = useState(0)
    const [selectedViewMode, setSelectedViewMode] = useState(
        new IndexPath(viewModes.grid.value),
    )

    const GridIcon = props => <KittenIcon {...props} name="grid-outline" />

    const ListIcon = props => <KittenIcon {...props} name="list-outline" />

    const showViewOption = () => {
        modalRef.current.open()
    }

    const [selectedPopularity, setSelectedPopularity] = useState(null)

    //#endregion

    const onSelectedIndexChange = index => {
        setSelectedIndex(index)
    }

    const renderProductListHeader = () => {
        return (
            <React.Fragment>
                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate(ScreenIds.Home)}>
                        <FastImage
                            source={require('../../assets/images/pp-logo.png')}
                            style={{
                                width: 35,
                                height: 35,
                                alignSelf: 'center',
                                backgroundColor: COLORS.WHITE,
                                borderRadius: 50,
                                marginLeft: 15,
                                marginTop: 5,
                            }}
                        />
                    </TouchableOpacity>
                    <View style={styles.logoContainer}>
                        <Text
                            category="h4"
                            style={{
                                color: COLORS.WHITE,
                            }}>
                            {translate('product.productsCap')}
                        </Text>
                    </View>
                    <CartIcon
                        style={{marginRight: 15, marginTop: 5}}
                        navigation={navigation}
                    />
                </View>

                <View style={styles.searchContainer}>
                    <SearchBar
                        placeholder={translate('common.inputSearchPlaceholder')}
                        round
                        lightTheme
                        onFocus={() =>
                            navigation.navigate(ScreenIds.ProductSearch)
                        }
                        inputStyle={styles.searchBar}
                        containerStyle={styles.searchBarContainer}
                        inputContainerStyle={styles.searchBarInputContainer}
                    />
                    <View style={styles.viewMode}>
                        <TouchableOpacity
                            onPress={() => modalRef.current?.open()}>
                            <Icon name="tune" size={20} color={COLORS.BLACK} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View>
                    <BottomNavigation
                        selectedIndex={selectedIndex}
                        onSelect={index => onSelectedIndexChange(index)}>
                        <BottomNavigationTab
                            title={translate('product.bundlesCap')}
                        />
                        <BottomNavigationTab
                            title={translate('product.productsCap')}
                        />
                    </BottomNavigation>
                </View>
            </React.Fragment>
        )
    }

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <ImageBackground
                        source={require('../../assets/images/lesson-bg-copy.png')}
                        style={{
                            height: height,
                            backgroundColor: COLORS.WHITE,
                        }}>
                        {renderProductListHeader()}

                        {selectedIndex === 0 && isBundleProductLoaded &&
                            renderBundleList(bundle.bundleProducts)}

                        {selectedIndex === 0 && !isBundleProductLoaded &&
                            <LoadingSkeleton 
                                listType={
                                    selectedViewMode.row === viewModes.grid.value
                                        ? SKELETON_TYPE.card
                                        : SKELETON_TYPE.cardExpand
                                    }
                                length={4}    
                            />
                        }

                        {selectedIndex === 1 && isProductLoaded &&
                            renderProductList(product.products)}

                        {selectedIndex === 1 && !isProductLoaded &&
                            <LoadingSkeleton 
                                listType={
                                    selectedViewMode.row === viewModes.grid.value
                                        ? SKELETON_TYPE.card
                                        : SKELETON_TYPE.cardExpand
                                    }
                                length={4}    
                            />
                        }
                    </ImageBackground>
                </View>
            </View>

            <Modalize
                ref={modalRef}
                handleStyle={styles.handleModalStyle}
                modalStyle={{
                    borderTopLeftRadius: 30,
                    borderTopRightRadius: 30,
                }}
                snapPoint={270}
                onBackButtonPress={() => modalInfoRef.current?.close()}
                scrollViewProps={{showsVerticalScrollIndicator: false}}>
                <View style={{paddingHorizontal: 20}}>
                    <View
                        style={{
                            paddingTop: 30,
                        }}>
                        <Text category="h5">
                            {translate('common.selectViewMode')}
                        </Text>
                    </View>
                    <Menu
                        style={styles.menuContainer}
                        selectedIndex={selectedViewMode}
                        onSelect={index => setSelectedViewMode(index)}>
                        <MenuItem title="Grid View" accessoryLeft={GridIcon} />
                        <MenuItem title="List View" accessoryLeft={ListIcon} />
                    </Menu>

                    <View>
                        <Text category="h5">
                            {translate('common.filtering')}
                        </Text>
                    </View>
                    <View
                        style={{
                            backgroundColor: COLORS.LIGHT,
                            marginBottom: 5,
                            paddingVertical: 3,
                        }}>
                        <Text category="label" style={{marginHorizontal: 20}}>
                            {translate('common.byOthers')}
                        </Text>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                        <Radio
                            style={styles.radio}
                            checked={
                                selectedPopularity === popularityTypes.new.value
                            }
                            onChange={nextChecked =>
                                setSelectedPopularity(nextChecked)
                            }>
                            {translate('common.new')}
                        </Radio>

                        <Radio
                            style={styles.radio}
                            checked={
                                selectedPopularity === popularityTypes.top.value
                            }
                            onChange={nextChecked =>
                                setSelectedPopularity(nextChecked)
                            }>
                            {translate('common.top')}
                        </Radio>
                        <Radio
                            style={styles.radio}
                            checked={
                                selectedPopularity === popularityTypes.new.value
                            }
                            onChange={nextChecked =>
                                setSelectedPopularity(nextChecked)
                            }>
                            {translate('common.new')}
                        </Radio>

                        <Radio
                            style={styles.radio}
                            checked={
                                selectedPopularity === popularityTypes.top.value
                            }
                            onChange={nextChecked =>
                                setSelectedPopularity(nextChecked)
                            }>
                            {translate('common.top')}
                        </Radio>
                    </View>
                </View>
            </Modalize>
        </BaseScreen>
    )
}

export default ProductScreen

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        marginBottom: 20,
    },
    container: {
        flex: 1,
    },
    headerContainer: {
        marginVertical: 20,
    },
    tabViewContainer: {
        flex: 1,
        paddingHorizontal: 16,
    },
    /********* Logo **********/
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 20,
    },
    logoContainer: {
        height: 40,
        paddingHorizontal: 20,
    },
    logo: {
        width: '100%',
        height: 50,
        maxHeight: 50,
    },
    /******** Heading *********/
    headingContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
        paddingHorizontal: 20,
    },
    heading: {
        textAlign: 'center',
    },
    /******** Shopping Bag **********/
    shoppingBag: {
        marginRight: 15,
        marginTop: 5,
    },
    /********* Search And Filter **********/
    searchContainer: {
        flexDirection: 'row',
        marginHorizontal: 16,
        marginBottom: 20,
    },
    searchBarInputContainer: {
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        height: 45,
    },
    searchBarContainer: {
        width: '85%',
        padding: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
    },
    searchBar: {
        height: 50,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    /********* View Mode **********/
    gridContainer: {
        paddingLeft: 19,
    },
    listContainer: {
        paddingHorizontal: 20,
    },
    menuContainer: {
        marginTop: 5,
        backgroundColor: COLORS.TRANSPARENT,
    },
    viewMode: {
        marginLeft: 10,
        height: 45,
        width: 50,
        borderRadius: 10,
        backgroundColor: COLORS.LIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },
    /******** card **************/
    card: {
        height: 225,
        backgroundColor: COLORS.LIGHT,
        width: gridWidth + 20,
        borderRadius: 10,
        marginBottom: 20,
        padding: 15
    },
    homeCard: {
        height: 225,
        backgroundColor: COLORS.WHITE,
        width: gridWidth + 15,
        borderRadius: 10,
        marginBottom: 20,
        padding: 10
    },
    cardExpand: {
        width: '90%',
        marginBottom: 20,
        marginHorizontal: 20,
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        padding: 15
    },
    cardImage: {
        flex: 1,
        height: 150,
        width: null,
        resizeMode: 'contain',
        paddingVertical: 5,
        marginTop: 3,
        borderRadius: 10,
    },
    mediumMargin: {
        marginLeft: 14,
        marginRight: 10,
    },
    smallMargin: {
        marginLeft: 6,
        marginRight: 6,
    },
    /******** card components **************/
    title: {
        fontSize: 13,
        marginTop: 10,
        minHeight: 30
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 1,
    },
    price: {
        paddingTop: 5,
    },
    salePrice: {
        textDecorationLine: 'line-through',
    },
    /******* favorite *********/
    favoriteContainer: {
        width: 26,
        height: 26,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundPink: {
        backgroundColor: 'rgba(245, 42, 42,0.2)',
    },
    backgroundGray: {
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
    /********* Modal *********/
    handleModalStyle: {
        marginTop: 30,
        backgroundColor: COLORS.SUB,
        width: 80,
    },
    /******** generic *********/
    pH20: {
        paddingHorizontal: 20,
    },
    textBold: {
        fontWeight: 'bold',
    },
    mr2: {
        marginRight: 2,
    },
    textJustify: {
        textAlign: "justify"
    },
    mb5: {
        marginBottom: 5
    }
})
