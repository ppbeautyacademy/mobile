import {Icon as KittenIcon, Input, Text} from '@ui-kitten/components'
import React, {useContext, useEffect, useRef, useState} from 'react'
import {
    StyleSheet,
    View,
    Image,
    ScrollView,
    useWindowDimensions,
    Dimensions,
} from 'react-native'
import {
    PanGestureHandler,
    State,
    TouchableOpacity,
} from 'react-native-gesture-handler'
import Animated, {
    useAnimatedGestureHandler,
    useAnimatedStyle,
    useSharedValue,
    withSpring,
} from 'react-native-reanimated'
import BaseScreen from '../../component/BaseScreen'
import CartAction from '../Cart/CartAction'
import {COLORS} from '../../assets/theme/colors'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Carousel, {Pagination} from 'react-native-snap-carousel'
import ColorPicker from '../../component/color/ColorPicker'
import {ScreenIds} from '../../ScreenIds'
import {addToCart, getProductDetail, getProductPrice, removeFavorite, saveFavorite} from '../../services/ServiceWorker'
import context from '../../../context/context'
import {cartActionTypes, getProductOrderId, HTTP_STATUS_OK, setProductOrderId} from '../../assets/constants'
import SizePicker from '../../component/size/SizePicker'
import OrderContext from '../../../context/OrderContext'
import { Toast } from 'popup-ui'
import { translate } from '../../../config/i18n'
import { filterMainImageToTop } from '../../utils/ImageUtil'
import Spinner from 'react-native-loading-spinner-overlay'

const snapValue = 24;

const w = Dimensions.get("window").width;

const ProductDetailScreen = ({route, navigation}) => {
    const {alias} = route.params
    const {state, actions} = useContext(context)
    const {orderState, orderActions} = useContext(OrderContext)

    //#region Product
    const [productDetail, setProductDetail] = useState({
        productImages: [],
        productColors: [],
        productSizes: [],
        productComments: [],
    })
    const [isLoaded, setIsLoaded] = useState(false);
    
    useEffect(async() => {
        if (alias) {
            await loadProductDetail()
        } else {
            navigation.navigate(ScreenIds.Product)
        }
    }, [])

    const loadProductDetail = async () => {
        const productDetailData = await getProductDetail(state.version, alias, null, null, state.userInfo?.id)

        if (productDetailData && productDetailData.id) {
            setProductDetail({
                ...productDetailData,
                productImages: filterMainImageToTop(productDetailData.productImages)
            })
            setIsLoaded(true)
        }
    }
    //#endregion
    const loadProductPrice = async(id, selectedColor, selectedSize) => {
        const priceDisplay = await getProductPrice(id, selectedColor, selectedSize);

        if(priceDisplay && Object.keys(priceDisplay).length > 0) {
            setProductDetail({
                ...productDetail,
                productPriceDisplay: priceDisplay ? priceDisplay : {}
            })
        }
    }
    //#region Price

    //#endregion

    //#region Taxonomy
    const [selectedColor, setSelectedColor] = useState(null)
    const [selectedSize, setSelectedSize] = useState(null)

    const handleSetSelectedColor = async colorId => {
        setSelectedColor(colorId);

        await loadProductPrice(productDetail.id, colorId, selectedSize);
    }

    const handleSetSelectedSize = async sizeId => {
        setSelectedSize(sizeId)

        await loadProductPrice(productDetail.id, selectedColor, sizeId);
    }
    //#endregion

    //#region Animation
    const carouselRef = useRef(null)

    const [activeCarouselItem, setActiveCarouselItem] = useState(0)

    const dimentions = useWindowDimensions()
    const sheetHeight = useSharedValue(0)
    const scale = useSharedValue(1)

    const sheetAnimtedStyle = useAnimatedStyle(() => ({
        height: sheetHeight.value,
    }))

    const handleAnimatedStyle = useAnimatedStyle(() => ({
        transform: [{scale: withSpring(scale.value, {stiffness: 300})}],
        backgroundColor:
            scale.value > 1 ? `rgba(0, 0, 41, 0.5)` : `rgba(0, 0, 41, 0.3)`,
    }))

    const resizeEventHandler = useAnimatedGestureHandler({
        onStart: (event, ctx) => {
            ctx.startY = event.absoluteY
            ctx.startHeight = sheetHeight.value
        },
        onActive: (event, ctx) => {
            sheetHeight.value = withSpring(
                ctx.startHeight + (ctx.startY - event.absoluteY),
            )
        },
        onEnd: (_, ctx) => {
            if (
                sheetHeight.value - ctx.startHeight > snapValue &&
                ctx.startHeight < dimentions.height / 1.5
            ) {
                sheetHeight.value = withSpring(dimentions.height / 1.5)
            } else if (
                sheetHeight.value - ctx.startHeight < snapValue &&
                ctx.startHeight < dimentions.height / 1.5
            ) {
                sheetHeight.value = withSpring(dimentions.height / 3)
            }
            if (
                sheetHeight.value - ctx.startHeight > snapValue &&
                ctx.startHeight > dimentions.height / 3
            ) {
                sheetHeight.value = withSpring(dimentions.height / 1.5)
            } else if (
                sheetHeight.value - ctx.startHeight < snapValue &&
                ctx.startHeight > dimentions.height / 3
            ) {
                sheetHeight.value = withSpring(dimentions.height / 3)
            }
        },
    })

    const touchEventHandler = ({nativeEvent}) => {
        if (
            nativeEvent.state === State.BEGAN ||
            nativeEvent.state === State.ACTIVE
        ) {
            scale.value = 1.3
            return
        }
        scale.value = 1
    }

    useEffect(() => {
        sheetHeight.value = withSpring(dimentions.height / 3, {
            damping: 9,
            stiffness: 50,
        })
    }, [])

    const renderCarouselItem = ({item}) => {
        const image = item?.image
        return (
            <Image
                source={{uri: image.imageUrl}}
                style={[
                    styles.image,
                    {
                        resizeMode: 'contain',
                        marginLeft: 10,
                        width: '80%',
                        borderRadius: 10,
                        alignSelf: 'center',
                    },
                ]}
            />
        )
    }
    //#endregion

     //#region FAVORITE
    const handleAddToFavorite = async () => {
        if (productDetail.id) {
            let data = {
                productId: productDetail.id,
            }

            const favoritePost = await saveFavorite(state.userInfo?.id, data)

            if (favoritePost && favoritePost.id) {
                setProductDetail({
                    ...productDetail,
                    liked: true,
                    favoriteId: favoritePost.id,
                })
            }
        }
    }

    const handleRemoveFavorite = async () => {
        if (productDetail.favoriteId) {
            const favoriteDelete = await removeFavorite(productDetail.favoriteId)

            if (favoriteDelete === HTTP_STATUS_OK) {
                setProductDetail({
                    ...productDetail,
                    liked: false,
                    favoriteId: null,
                })
            }
        }
    }
    //#endregion

    //#region Cart
    const [requireTaxon, setRequireTaxon] = useState(true);

    const onAddToCart = async() => {
        if(!state.isLoggedIn) {
            actions.showWhiteAppSpinner(true);
            navigation.navigate(ScreenIds.AuthStack, {screen: ScreenIds.Login});
            actions.showWhiteAppSpinner(false);
        } else {
            actions.showAppSpinner(true);

            let isError = false;
            if(!productDetail.bundle && ((productDetail.productColors && productDetail.productColors.length > 0 && !selectedColor) || (productDetail.productSizes && productDetail.productSizes.length > 0 && !selectedSize))) {
                isError = true;
                setRequireTaxon(true);
            }

            if(!isError) {
                setRequireTaxon(false)

                let productTempList = [];
                let productOrder = {
                    id: productDetail.id,
                    quantity: 1,
                    colorId: productDetail.bundle ? null : selectedColor,
                    sizeId: productDetail.bundle ? null : selectedSize
                }
        
                productTempList.push(productOrder);
        
                let data = {
                    productList: productTempList
                }
        
                const productOrderId = await getProductOrderId();
                const cartOrder = await addToCart(state.userInfo.id, state.version, productOrderId, data)
                                        .finally(() => {actions.showAppSpinner(false)});
    
                if(cartOrder && cartOrder.id) {
                    if(productOrderId !== cartOrder.id) {
                        await setProductOrderId(cartOrder.id);
                    }
    
                    orderActions.reloadProductOrder(cartOrder.id);
    
                    Toast.show({
                        title: translate('common.successTitle'),
                        text: 'Course added successfully',
                        color: '#5BAA22',
                        timing: 3500,
                    })
                }
            }
            
            actions.showAppSpinner(false);
        }
    }
    //#region 

    return (
        <BaseScreen showHeader={false}>
            {!isLoaded ?
                <Spinner
                    visible={true}
                    color={COLORS.SUB}
                    overlayColor={COLORS.WHITE}
                    animation="fade"
                />    
            :
                <View style={styles.container}>
                    <View style={styles.navigationWrapper}>
                        <View>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate(ScreenIds.Product)
                                }>
                                <View
                                    style={[
                                        styles.navigationBackground,
                                        styles.navigationContainer,
                                    ]}>
                                    <Icon
                                        name="chevron-left"
                                        size={30}
                                        color={COLORS.SUB}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text category="h6">
                            {translate('product.productDetail')}
                        </Text>
                        <View>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate(ScreenIds.ProductComment, {
                                        alias: productDetail.alias,
                                    })
                                }>
                                <View
                                    style={[
                                        styles.navigationBackground,
                                        styles.navigationContainer,
                                    ]}>
                                    <Icon
                                        color={`#eaa196`}
                                        name="rate-review"
                                        size={16}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    
                    <Animated.View style={styles.image}>
                        <Carousel
                            ref={carouselRef}
                            data={productDetail.productImages}
                            renderItem={renderCarouselItem}
                            sliderWidth={500}
                            itemWidth={400}
                            activeSlideOffset={10}
                            onSnapToItem={index => setActiveCarouselItem(index)}
                            lockScrollWhileSnapping={true}
                            layoutCardOffset={0}
                            activeSlideAlignment="start"
                            firstItem={activeCarouselItem}
                        />
                        <View
                            style={{
                                position: 'absolute',
                                bottom: 40,
                                alignSelf: 'center',
                            }}>
                            <Pagination
                                dotsLength={productDetail.productImages.length}
                                activeDotIndex={activeCarouselItem}
                                dotStyle={{
                                    width: 10,
                                    height: 10,
                                    borderRadius: 5,
                                    marginHorizontal: 8,
                                    backgroundColor: 'rgba(0, 0, 0, 0.75)',
                                }}
                                inactiveDotOpacity={0.4}
                                inactiveDotScale={0.6}
                            />
                        </View>
                    </Animated.View>

                    <Animated.View style={sheetAnimtedStyle}>
                        <View style={styles.prodInfo}>
                            <PanGestureHandler
                                onHandlerStateChange={touchEventHandler}
                                onGestureEvent={resizeEventHandler}
                            >
                                <Animated.View style={styles.handleContainer}>
                                    <Animated.View
                                        style={[styles.handle, handleAnimatedStyle]}
                                    />
                                </Animated.View>
                            </PanGestureHandler>

                            <View style={styles.prodHeader}>
                                <Text category="h5">{productDetail.name}</Text>
                                <View style={{alignItems: 'flex-end'}}>
                                    <TouchableOpacity
                                        onPress={() =>
                                            !productDetail.liked
                                                ? handleAddToFavorite()
                                                : handleRemoveFavorite()
                                        }>
                                        <View
                                            style={[
                                                styles.favoriteContainer,
                                                productDetail.liked
                                                    ? styles.backgroundPink
                                                    : styles.backgroundGray,
                                            ]}>
                                            <Icon
                                                name="favorite"
                                                size={18}
                                                color={
                                                    productDetail.liked
                                                ?   COLORS.ERROR
                                                :   COLORS.BLACK
                                                }
                                            />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {productDetail.productColors &&
                                productDetail.productColors.length > 0 && (
                                    <View style={styles.colorWrapper}>
                                        <View style={styles.colorLabel}>
                                            <Text category="label">
                                                {translate('product.colorsCap')}
                                            </Text>
                                        </View>
                                        <ColorPicker
                                            colors={productDetail.productColors}
                                            selectedColor={selectedColor}
                                            setSelectedColor={
                                                handleSetSelectedColor
                                            }
                                        />
                                    </View>
                                )}

                            {productDetail.productSizes &&
                                productDetail.productSizes.length > 0 && (
                                    <View style={styles.sizeWrapper}>
                                        <View style={styles.sizeLabel}>
                                            <Text category="label">
                                                {translate('product.sizesCap')}
                                            </Text>
                                        </View>
                                        <SizePicker
                                            sizes={productDetail.productSizes}
                                            selectedSize={selectedSize}
                                            setSelectedSize={handleSetSelectedSize}
                                        />
                                    </View>
                                )}

                            <ScrollView style={styles.contentWrapper}
                                showsVerticalScrollIndicator={false}
                            >
                                <Text category="p1" style={styles.description}>
                                    {productDetail.description}
                                </Text>
                            </ScrollView>
                        </View>
                    </Animated.View>

                    <CartAction
                        type={cartActionTypes.productDetail.value}
                        priceSetting={productDetail.productPriceDisplay}
                        currency={state.appCurrency}
                        label={translate('common.price')}
                        actionLabel={'ADD TO BAG'}
                        onActionPress={onAddToCart}
                        actionEnabled
                        isLoaded={isLoaded}
                    />
                </View>
            }
        </BaseScreen>
    )
}

export default ProductDetailScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
    image: {
        position: 'relative',
        flex: 1,
        width: '100%',
    },
    /********* Contant Wrapper *********/
    prodInfo: {
        paddingHorizontal: 40,
        backgroundColor: COLORS.MAIN_RGB_05,
        borderRadius: 30,
        flex: 1,
        top: -50,
    },
    prodHeader: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        width: '100%',
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingHorizontal: 20,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /******** Pangesture **********/
    handleContainer: {
        height: 50,
        width: '100%',
        justifyContent: 'center',
    },
    handle: {
        height: 5,
        width: 30,
        alignSelf: 'center',
        borderRadius: 50,
    },
    /********* Name **********/
    title: {
        fontSize: 32,
        color: `rgb(15, 5, 33)`,
        marginBottom: 30,
    },
    /******** Description **********/
    contentWrapper: {
        paddingVertical: 5,
    },
    description: {
        textAlign: 'justify',
    },
    /******** Colors ************/
    colorWrapper: {
        marginBottom: 5,
    },
    colorLabel: {
        paddingLeft: 3,
        marginBottom: 5,
    },
    /******** Sizes ************/
    sizeWrapper: {
        marginBottom: 5,
    },
    sizeLabel: {
        paddingLeft: 3,
        marginBottom: 5,
    },
    /********* Cart Button **********/
    cart: {
        height: 40,
        width: 40,
        marginRight: 30,
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    /******* favorite *********/
    favoriteContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundPink: {
        backgroundColor: 'rgba(245, 42, 42,0.2)',
    },
    backgroundGray: {
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
    /********* Forward/Backward Button **********/
    forwardBtn: {
        position: 'absolute',
        right: 10,
        top: '40%',
        zIndex: 5,
    },
    backwardBtn: {
        position: 'absolute',
        left: 10,
        top: '40%',
        zIndex: 5,
    },
    /********** Comment ***************/
    emptyCommentWrapper: {
        minHeight: 310,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    commentWrapper: {
        minHeight: 700,
        flexDirection: 'column',
        flex: 1,
    },
    commentButton: {
        position: 'absolute',
        right: 15,
        top: 20,
    },
    /********* Back ***************/
    backButton: {
        position: 'absolute',
        left: 15,
        top: 20,
    },
    /********* Modal *********/
    handleModalStyle: {
        marginTop: 30,
        backgroundColor: COLORS.SUB,
        width: 80,
    },
})
