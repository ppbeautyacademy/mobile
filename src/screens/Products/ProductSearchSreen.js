import React, {useContext, useState} from 'react'
import {Dimensions, StyleSheet, View} from 'react-native'
import {COLORS} from '../../assets/theme/colors'
import {ScreenIds} from '../../ScreenIds'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {TouchableOpacity} from 'react-native-gesture-handler'
import BaseScreen from '../../component/BaseScreen'
import {SearchBar} from 'react-native-elements'
import FastImage from 'react-native-fast-image'
import {Divider, List, ListItem, Text} from '@ui-kitten/components'
import {filterProductByKeywords} from '../../services/ServiceWorker'
import context from '../../../context/context'
import {translate} from '../../../config/i18n'

const height = Dimensions.get('window').height

const ProductSearchScreen = ({navigation}) => {
    const {state} = useContext(context)

    //#region SEARCH
    const [results, setResults] = useState([])
    const [hasSearched, setHasSearched] = useState(false)
    const [searchValue, setSearchValue] = useState('')
    const [isSearching, setIsSearching] = useState(false)
    const [isEmpty, setIsEmpty] = useState(false)

    const handleSearch = async value => {
        setSearchValue(value)
        setHasSearched(true)
        setIsSearching(true)

        if (value) {
            const productList = await filterProductByKeywords(
                state.version,
                state.userInfo?.id,
                value,
            )

            if (productList && Array.isArray(productList)) {
                if (productList.length > 0) {
                    setResults(productList)
                    setIsSearching(false)
                    setIsEmpty(false)
                } else {
                    setIsEmpty(true)
                    setResults([])
                    setIsSearching(false)
                }                
            }
        } else {
            setIsSearching(false)
            setHasSearched(false)
            setIsEmpty(false)
            setResults([])
        }
    }

    const handleClearSearch = () => {
        setSearchValue('')
        setIsSearching(false)
        setResults([])
    }

    const renderItem = ({item, index}) => (
        <TouchableOpacity
            key={index}
            style={{
                paddingVertical: 10,
                paddingHorizontal: 20,
            }}
            onPress={() =>
                navigation.navigate(ScreenIds.ProductDetail, {
                    alias: item.alias,
                })
            }>
            <Text category="label">{item.name}</Text>
        </TouchableOpacity>
    )

    //#endregion

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <View>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.Product)
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.searchContainer}>
                        <SearchBar
                            placeholder={translate(
                                'common.inputSearchPlaceholder',
                            )}
                            onChangeText={handleSearch}
                            value={searchValue}
                            round
                            lightTheme
                            showLoading={isSearching}
                            onClear={handleClearSearch}
                            inputStyle={styles.searchBar}
                            containerStyle={styles.searchBarContainer}
                            inputContainerStyle={styles.searchBarInputContainer}
                        />
                    </View>
                </View>

                {results && results.length > 0 && (
                    <List
                        style={styles.wrapper}
                        data={results}
                        ItemSeparatorComponent={Divider}
                        renderItem={renderItem}
                    />
                )}

                {isEmpty && (
                    <View
                        style={{
                            margin: 20,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <FastImage
                            source={require('../../assets/images/no-courses.png')}
                            style={{
                                width: '60%',
                                height: 200,
                            }}
                        />

                        <View>
                            <Text>{translate('product.noProductFound')}</Text>
                        </View>
                    </View>
                )}

                {!hasSearched && (
                    <View
                        style={{
                            margin: 20,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <FastImage
                            source={require('../../assets/images/product-search.png')}
                            style={{
                                width: '60%',
                                height: 200,
                            }}
                        />

                        <View>
                            <Text>
                                {translate('product.searchProductQuote')}
                            </Text>
                        </View>
                    </View>
                )}
            </View>
        </BaseScreen>
    )
}

export default ProductSearchScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
    /******* Header ********/
    headerContainer: {
        marginTop: 10,
        paddingHorizontal: 20,
        flexDirection: 'row',
    },
    /********* Back ***************/
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /********* Search Bar **********/
    searchContainer: {
        flexDirection: 'row',
        marginHorizontal: 10,
        marginBottom: 20,
    },
    searchBarInputContainer: {
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        height: 30,
    },
    searchBarContainer: {
        width: '97%',
        padding: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
    },
    searchBar: {
        height: 50,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        fontSize: 14,
    },
    /******** Result *******/
    wrapper: {
        height,
        backgroundColor: COLORS.WHITE,
    },
})
