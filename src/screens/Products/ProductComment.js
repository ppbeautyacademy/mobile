import React, {useContext, useEffect, useState} from 'react'
import {Keyboard, StyleSheet, View} from 'react-native'
import {TouchableOpacity} from 'react-native-gesture-handler'
import context from '../../../context/context'
import BaseScreen from '../../component/BaseScreen'
import {ScreenIds} from '../../ScreenIds'
import {getProductDetail} from '../../services/ServiceWorker'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {COLORS} from '../../assets/theme/colors'
import {Input, Spinner, Text} from '@ui-kitten/components'
import Reviews, {handleSendComment} from '../../component/review/Reviews'
import {
    COMMENT_MODES,
    COMMENT_REF_TYPE,
    HTTP_STATUS_OK,
} from '../../assets/constants'
import {translate} from '../../../config/i18n'

const ProductCommentScreen = ({navigation, route}) => {
    const {alias} = route.params
    const {state} = useContext(context)

    //#region LOAD COMMENTS
    const [productDetail, setProductDetail] = useState({
        productComments: [],
    })
    const [isLoaded, setIsLoaded] = useState(false)

    useEffect(async () => {
        if (!alias) {
            navigation.navigate(ScreenIds.Product)
        } else {
            await loadProductDetail()
        }
    }, [])

    const loadProductDetail = async () => {
        const productData = await getProductDetail(
            state.version,
            alias,
            null,
            null,
            state.userInfo?.id,
        )

        if (productData && productData.id) {
            setProductDetail(productData)
            setIsLoaded(true)

            setComment({
                ...comment,
                data: '',
                refId: productData.id,
                pId: null,
            })

            setCommentMode({
                replyingTo: '',
                mode: COMMENT_MODES.self.value,
            })

            setIsPublisingComment(false)
        }
    }
    //#endregion

    //#region COMMENTS
    const [isFocus, setIsFocus] = useState(false)
    const [errors, setErrors] = useState({})
    const [isPublishingComment, setIsPublisingComment] = useState(false)

    const [commentMode, setCommentMode] = useState({
        mode: COMMENT_MODES.self.value,
        replyingTo: '',
    })

    const [comment, setComment] = useState({
        data: '',
        refId: null,
        refObj: COMMENT_REF_TYPE.productDetail.value,
        displayed: true,
        pId: null,
    })

    const renderCommentSection = () => {
        return (
            <View style={isFocus ? styles.isSelfFocus : styles.isSelfBlur}>
                <Input
                    size="small"
                    textStyle={styles.mainFont}
                    value={comment.data}
                    onChangeText={nextValue =>
                        setComment({...comment, data: nextValue})
                    }
                    style={styles.commentInput}
                    status={errors.data && 'danger'}
                    placeholder={translate('common.inputMultilinePlaceholder')}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    multiline
                />

                <TouchableOpacity
                    onPress={() =>
                        !isPublishingComment ? onCommentSubmit() : {}
                    }>
                    <View
                        style={[
                            styles.reviewBtnContainer,
                            isFocus ? styles.focusInput : styles.blurInput,
                        ]}>
                        {isPublishingComment ? (
                            <Spinner size="small" />
                        ) : (
                            <Icon
                                name="send"
                                size={16}
                                color={
                                    isFocus
                                        ? COLORS.SUB
                                        : COLORS.BLACK_HALF_OPACITY
                                }
                                style={{
                                    paddingHorizontal: 8,
                                    paddingVertical: 3,
                                }}
                            />
                        )}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    const handleSetReplyingTo = (pId, replyTo) => {
        if (pId && replyTo) {
            setCommentMode({
                mode: COMMENT_MODES.reply.value,
                replyingTo: replyTo,
            })

            setComment({
                data: '',
                pId: pId,
                displayed: true,
            })
        }
    }

    const onCancelReply = () => {
        setCommentMode({
            mode: COMMENT_MODES.self.value,
            replyingTo: '',
        })

        setComment({
            ...comment,
            pId: null,
        })

        if (isFocus) {
            setIsFocus(true)
        } else {
            setIsFocus(false)
        }
    }

    const onCommentSubmit = async () => {
        setIsPublisingComment(true)

        const postStatus = await handleSendComment(
            state.userInfo?.id,
            comment,
            loadProductDetail,
            setErrors,
        )

        if (postStatus === HTTP_STATUS_OK) {
            setIsFocus(false)

            Keyboard.dismiss()
        }
    }

    //#endregion

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.ProductDetail, {
                                    alias: alias,
                                })
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text category="h6">{translate('common.comments')}</Text>
                    <View></View>
                </View>

                <View
                    style={{
                        flex: 1,
                        marginTop: 15,
                        marginBottom: 10,
                        justifyContent: 'space-between',
                        paddingHorizontal: 15,
                    }}>
                    <Reviews
                        comments={productDetail.productComments}
                        setReplyingTo={handleSetReplyingTo}
                        isLoaded={isLoaded}
                        reload={loadProductDetail}
                        refId={productDetail?.id}
                        refObj={COMMENT_REF_TYPE.productDetail.value}
                    />

                    {commentMode.mode === COMMENT_MODES.reply.value &&
                        commentMode.replyingTo && (
                            <View
                                style={[
                                    isFocus
                                        ? styles.isReplyFocus
                                        : styles.isReplyBlur,
                                ]}>
                                <View style={{flexDirection: 'row'}}>
                                    <TouchableOpacity
                                        onPress={() => onCancelReply()}
                                        style={{
                                            paddingTop: 3,
                                            marginRight: 5,
                                            borderRadius: 20,
                                        }}>
                                        <Icon name="highlight-off" size={14} />
                                    </TouchableOpacity>
                                    <Text category="label">
                                        {translate('product.replyTo')}{' '}
                                        {commentMode.replyingTo}
                                    </Text>
                                </View>

                                {renderCommentSection()}
                            </View>
                        )}

                    {commentMode.mode === COMMENT_MODES.self.value &&
                        renderCommentSection()}
                </View>
            </View>
        </BaseScreen>
    )
}

export default ProductCommentScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
    /******* Container Styles *******/
    isSelfFocus: {
        flex: 1,
        flexDirection: 'row',
    },
    isSelfBlur: {
        flexDirection: 'row',
        paddingVertical: 0,
        marginBottom: 10,
    },
    isReplyFocus: {
        flex: 1,
        flexDirection: 'column',
    },
    isReplyBlur: {
        flexDirection: 'column',
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingHorizontal: 20,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /******** Action **********/
    commentInput: {
        width: '90%',
        marginRight: 7,
    },
    reviewBtnContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    focusInput: {
        backgroundColor: '#FDF3EB',
    },
    blurInput: {
        backgroundColor: 'rgb(247, 249, 252)',
    },
    /******** generic *********/
    mainFont: {
        fontFamily: 'Prata-Regular',
    },
})
