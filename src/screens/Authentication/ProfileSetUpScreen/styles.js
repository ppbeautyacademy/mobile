import {StyleSheet} from 'react-native'
import {COLORS} from '../../../assets/theme/colors'
import {commonStyles} from '../../../assets/theme/styles'

const styles = StyleSheet.create({
    logoContainer: {
        width: '100%',
        paddingTop: 16,
        alignItems: 'center',
    },
    container: {
        paddingHorizontal: 16,
    },  
    formContainer: {
        flex: 1,
        // alignItems: 'center',
    },
    button: {
        paddingRight: '20%',
        paddingLeft: '20%',
        marginBottom: 50
    },
    logo: {
        width: 90,
        height: 80,
    },
    profileWrapper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    secureMessage: {
        textAlign: 'center',
        fontSize: 14,
        color: COLORS.GRAY_BD,
    },
    selectWrapper: {
        width: '100%',
    },
    bottomButton: {
        ...commonStyles.center,
    },
    separatorRow32: {
        height: 32,
    },
    selectWrapper: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    phoneTextContainer: {
        color: "#222B45"
    },
    phoneText: {
        backgroundColor: "rgb(247, 249, 252)"
    },
    phoneCodeText: {
        fontSize: 16,
        paddingBottom: 1,
        fontFamily: 'Prata-Regular'
    }
})

export default styles
