import {Button, Icon, Input, Spinner, Text} from '@ui-kitten/components'
import React, {useContext, useRef, useState} from 'react'
import {Image, View} from 'react-native'
import context from '../../../../context/context'
import {
    capitalize,
    dateStringFormat,
    DATE_PATTERNS,
    getUsername,
    HTTP_STATUS_OK,
    LANGUAGE_CODES,
    setUserUpdateInfo,
} from '../../../assets/constants'

import {translate} from '../../../../config/i18n'
import {IMAGES} from '../../../assets/images'
import {commonStyles} from '../../../assets/theme/styles'
import ENAddress from '../../../component/address/ENAddress'
import VNAddress from '../../../component/address/VNAddress'
import BaseScreen from '../../../component/BaseScreen'
import KeyboardScrollView from '../../../component/KeyboardScrollView'
import DatePicker from '../../../component/date/DatePicker'
import {ScreenIds} from '../../../ScreenIds'
import {revokeRefreshToken, updateUserProfile} from '../../../services/ServiceWorker'
import styles from './styles'
import { getUserCredentials } from '../../../services/secureData'
import { resetNavigatorToScreen, resetRootNavigatorToScreen, rootNavigationRef} from '../../../Navigation'
import PhoneInput from 'react-native-phone-number-input'
import { validEmail } from '../../../utils/Validation'

// TODO translation
const ProfileSetUpScreen = ({route, navigation}) => {
    const fromScreen = route && route.params && route.params.fromScreen ? route.params.fromScreen : null;

    //#region Phone Input
    const [formattedValue, setFormattedValue] = useState("");
    const phoneInput = useRef(null);
    //#endregion

    const [account, setAccount] = useState({})
    const [addressBean, setAddressBean] = useState({})
    const [dateBean, setDateBean] = useState({})
    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState({})

    const {state, actions} = useContext(context)

    const renderFullNameIcon = props => (
        <Icon {...props} name="person-outline" />
    )

    const renderPhoneIcon = props => <Icon {...props} name="phone-outline" />

    const LoadingIndicator = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" status={'basic'} />
        </View>
    )

    const onStreetChange = street => {
        setAddressBean({
            ...addressBean,
            street: street,
        })
    }

    const onCountryChange = country => {
        setAddressBean({
            ...addressBean,
            refCountry: country,
        })
    }

    const onStateChange = state => {
        setAddressBean({
            ...addressBean,
            refState: state,
        })
    }

    const onCityChange = city => {
        setAddressBean({
            ...addressBean,
            city: city,
        })
    }

    const onProvinceChange = province => {
        setAddressBean({
            ...addressBean,
            refProvince: province,
        })
    }

    const onDistrictChange = district => {
        setAddressBean({
            ...addressBean,
            refDistrict: district,
        })
    }

    const onWardChange = ward => {
        setAddressBean({
            ...addressBean,
            refWard: ward,
        })
    }

    const onZipCodeChange = zipCode => {
        setAddressBean({
            ...addressBean,
            zipCode: zipCode,
        })
    }

    const onFullNameChange = fullname => {
        setAccount({
            ...account,
            fullname: fullname,
        })
    }

    const onPhoneChange = phone => {
        setAccount({
            ...account,
            phone: phone,
            phoneExtensionBean: {
                countryCode: phoneInput.current?.getCountryCode(),
                dialCode: phoneInput.current?.getCallingCode()
            }
        })
    }

    const onDayChange = day => {
        setDateBean({
            ...dateBean,
            day: day,
        })
    }

    const onMonthChange = month => {
        setDateBean({
            ...dateBean,
            month: month,
        })
    }

    const onYearChange = year => {
        setDateBean({
            ...dateBean,
            year: year,
        })
    }

    const handleUpdateProfile = async () => {
        let isError = false
        let errors = {}

        if (
            !account ||
            !account.fullname ||
            !account.phone ||
            !dateBean.day ||
            !dateBean.month ||
            !dateBean.year
        ) {
            errors = {
                ...errors,
                fullname: !account.fullname,
                phone: !account.phone,
                day: !dateBean.day,
                month: !dateBean.month,
                year: !dateBean.year,
            }

            isError = true
        }

        if(account.email && !validEmail(account.email)) {
            errors = {
                ...errors,
                invalidEmail: true
            }

            isError = true
        } 

        if(account.phone) {
            if(!phoneInput.current?.isValidNumber(account.phone)) {
                errors = {
                    ...errors,
                    invalidPhone: true
                }

                isError = true
            }
        }

        if (state.version) {
            if (state.version === LANGUAGE_CODES.vi.value) {
                if (
                    !addressBean ||
                    !addressBean.street ||
                    !addressBean.refWard ||
                    !addressBean.refDistrict ||
                    !addressBean.refProvince
                ) {
                    errors = {
                        ...errors,
                        street: !addressBean.street,
                        refWard: !addressBean.refWard,
                        refDistrict: !addressBean.refDistrict,
                        refProvince: !addressBean.refProvince,
                    }

                    isError = true
                }
            } else {
                if (
                    !addressBean ||
                    !addressBean.street ||
                    !addressBean.city ||
                    !addressBean.refState ||
                    !addressBean.refCountry ||
                    !addressBean.zipCode
                ) {
                    errors = {
                        ...errors,
                        street: !addressBean.street,
                        city: !addressBean.city,
                        refState: !addressBean.refState,
                        refCountry: !addressBean.refCountry,
                        zipCode: !addressBean.zipCode,
                    }

                    isError = true
                }
            }
        }

        if (!isError) {
            setLoading(true)

            const username = await getUsername()

            let birthDateBean = dateStringFormat(dateBean.day, dateBean.month, dateBean.year, DATE_PATTERNS.MDY.value);
            let toSendData = {
                ...account,
                username: username ? username.replace(/ /g, '') : '',
                fullname: account.fullname ? capitalize(account.fullname) : account.fullname,
                email: account.email ? account.email.replace(/ /g, '') : '',
                phone: formattedValue?.replace(/'+'/g, ''),
                phoneExtensionBean: account.phoneExtensionBean ? account.phoneExtensionBean : {},
                birthDateBean: birthDateBean,
                addressBean,
            }

            const updateUserProfileStatus = await updateUserProfile(
                state.version,
                toSendData,
            )
            if (updateUserProfileStatus && updateUserProfileStatus.id) {
                await setUserUpdateInfo(updateUserProfileStatus)

                setLoading(false)
                setErrors({})
                setAccount({})
                setAddressBean({})
                setDateBean({})
                actions.setUserInfo(updateUserProfileStatus)
                actions.setIsLoggedIn(true)

                resetNavigatorToScreen(navigation, ScreenIds.MainStack, ScreenIds.MainTab, {}, ScreenIds.Account);
            }
            setLoading(false)
        } else {
            setErrors(errors)
        }
    }

    //#region Cancel set up process
    const handleOnBackPress = async() => {
        actions.showWhiteAppSpinner(true);

        await handleRevokeToken();

        actions.resetUserAuth()
        actions.resetUserInfo()
        actions.setIsLoggedIn(false);

        if(fromScreen && fromScreen === ScreenIds.SplashScreen) {
            resetNavigatorToScreen(navigation, ScreenIds.MainStack, ScreenIds.Home)
        } else {
            resetRootNavigatorToScreen(ScreenIds.Login);
        }
        
        actions.showWhiteAppSpinner(false);
    }

    const handleRevokeToken = async() => {
        let userAuthInfo
        await getUserCredentials()
            .then(authInfo => {
                userAuthInfo = authInfo
            })
            .catch(err => logService.log(err))

        await revokeRefreshToken(userAuthInfo?.refreshToken)
        .then(data => {
            if(data === HTTP_STATUS_OK)  {
                return;
            }
        })
    }
    //#endregion

    return (
        <BaseScreen title="" onBackPress={() => handleOnBackPress()}>
            <KeyboardScrollView
                extraScrollHeight={Platform.OS === 'ios' ? 0 : 100}
            >
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={IMAGES.LOGO}
                        resizeMode="contain"
                    />
                </View>
                <View style={styles.container}>
                    <View style={styles.formContainer}>
                        <Text category={"label"} appearance={"hint"}>
                            {translate('login.privacyPolicyDescription')}
                        </Text>
                        <View style={commonStyles.separator.row16} />
                        <Input
                            value={account.fullname}
                            label={translate('common.fullName')}
                            placeholder={translate('common.fullNamePlaceholder')}
                            accessoryRight={renderFullNameIcon}
                            onChangeText={fullname => onFullNameChange(fullname)}
                            status={errors.fullname && 'danger'}
                        />
                        {errors.fullname && <Text category={"label"} status={"danger"}>Empty full name</Text>}
                        <View style={commonStyles.separator.row16} />
                        <Text category={"label"} appearance={"hint"}>Phone</Text>
                        <PhoneInput
                            ref={phoneInput}
                            defaultValue={account.phone}
                            defaultCode={account.phoneExtensionBean?.countryCode ? account.phoneExtensionBean?.countryCode.toUpperCase() : "CA"}
                            layout="first"
                            placeholder={"Enter phone number"}
                            onChangeText={(text) => {
                                onPhoneChange(text);
                            }}
                            onChangeFormattedText={(text) => {
                                setFormattedValue(text);
                            }}
                            containerStyle={commonStyles.customPhoneInputContainer}
                            textContainerStyle={[commonStyles.phoneText, commonStyles.p0]}
                            textInputStyle={[commonStyles.p0, styles.phoneTextContainer]}
                            flagButtonStyle={commonStyles.p0}
                            codeTextStyle={[commonStyles.p0, commonStyles.m0, styles.phoneCodeText]}
                            
                        />
                        {errors.phone && <Text category={"label"} status={"danger"}>Empty phone number</Text>}
                        {errors.invalidPhone && <Text category={"label"} status={"danger"}>Invalid phone number</Text>}
                    </View>
                    <View style={commonStyles.separator.row16} />
                    <View style={styles.selectWrapper}>
                        <DatePicker
                            setDay={onDayChange}
                            setMonth={onMonthChange}
                            setYear={onYearChange}
                            errors={errors}
                        />
                    </View>
                    <View style={commonStyles.separator.separatorRow32} />
                        {state.version === LANGUAGE_CODES.vi.value ? (
                            <VNAddress
                                addressBean={addressBean}
                                setStreet={onStreetChange}
                                setRefWard={onWardChange}
                                setRefDistrict={onDistrictChange}
                                setRefProvince={onProvinceChange}
                                errors={errors}
                            />
                        ) : (
                            <ENAddress
                                addressBean={addressBean}
                                setStreet={onStreetChange}
                                setCity={onCityChange}
                                setRefState={onStateChange}
                                setRefCountry={onCountryChange}
                                setZipCode={onZipCodeChange}
                                errors={errors}
                            />
                        )}
                    <View style={styles.separatorRow32} />
                        <View style={styles.bottomButton}>
                            <Button
                                status="primary"
                                onPress={handleUpdateProfile}
                                style={styles.button}
                                accessoryLeft={loading && LoadingIndicator}>
                                {translate('common.update')}
                            </Button>
                        </View>
                    <View style={styles.separatorRow32} />
                </View>
            </KeyboardScrollView>
        </BaseScreen>
    )
}

export default ProfileSetUpScreen
