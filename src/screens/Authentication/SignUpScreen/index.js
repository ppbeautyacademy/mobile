import {Icon, Input, Text, Button, Spinner, Radio, CheckBox} from '@ui-kitten/components'
import React, {useState} from 'react'
import {
    Image,
    KeyboardAvoidingView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { translate } from '../../../../config/i18n'
import {
    getVersion,
} from '../../../assets/constants'
import {IMAGES} from '../../../assets/images'
import BaseScreen from '../../../component/BaseScreen'
import {ScreenIds} from '../../../ScreenIds'
import {registerUser} from '../../../services/ServiceWorker'
import {validEmail} from '../../../utils/Validation'
import styles from './styles'

const SignUpScreen = ({navigation}) => {
    const [account, setAccount] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
    })
    const [isAgree, setIsAgree] = useState(false);
    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState({
        username: false,
        email: false,
        password: false,
        confirmPassword: false,
    })
    const [secureTextEntry, setSecureTextEntry] = useState(true)

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry)
    }

    const LoadingIndicator = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" status="basic" />
        </View>
    )

    const renderUserIcon = props => <Icon {...props} name="person-outline" />

    const renderEmailIcon = props => <Icon {...props} name="email-outline" />

    const renderEyeIcon = props => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const renderCaption = () => {
        return (
            <View style={styles.captionContainer}>
                <Text style={styles.captionText}>
                    {translate('signUp.passwordValidation')}
                </Text>
            </View>
        )
    }

    const renderConfirmCaption = () => {
        return (
            <View style={styles.captionContainer}>
                <Text
                    style={
                        errors.invalidConfirmPassword
                            ? styles.invalidCaptionText
                            : styles.captionText
                    }>
                    {translate('signUp.passwordConfirmationValidation')}
                </Text>
            </View>
        )
    }

    const renderUsernameCaption = () => {
        return (
            <View style={styles.captionContainer}>
                {AlertIcon(styles.captionIcon)}
                <Text style={styles.invalidCaptionText}>{translate('signUp.invalidUsername')}</Text>
            </View>
        )
    }

    const renderEmailCaption = () => {
        return (
            <View style={styles.captionContainer}>
                {AlertIcon(styles.captionIcon)}
                <Text style={styles.invalidCaptionText}>{translate('signUp.invalidEmail')}</Text>
            </View>
        )
    }

    const onUsernameChange = username => {
        let whiteSpaceRemoved = username.replace(/ /g, '')
        let toSetUsername = whiteSpaceRemoved.toLowerCase()
        setAccount({...account, username: toSetUsername})
        setErrors({...errors, username: false, invalidUsername: false})
    }

    const onEmailChange = email => {
        let whiteSpaceRemoved = email.replace(/ /g, '')
        let toSetEmail = whiteSpaceRemoved.toLowerCase()
        setAccount({...account, email: toSetEmail})
        setErrors({...errors, email: false, invalidEmail: false})
    }

    const onPasswordChange = password => {
        let toSetPassword = password.replace(/ /g, '')
        setAccount({...account, password: toSetPassword})
        setErrors({...errors, password: false, invalidPassword: false})
    }

    const onConfirmPasswordChange = confirmPassword => {
        let toSetConfirmPassword = confirmPassword.replace(/ /g, '')
        setAccount({...account, confirmPassword: toSetConfirmPassword})
        setErrors({...errors, confirmPassword: false, invalidPassword: false})
    }

    const handleSignUp = async () => {
        let isError = false
        if (
            !account ||
            !account.username ||
            !account.email ||
            !account.password ||
            !account.confirmPassword
        ) {
            setErrors({
                ...errors,
                username: !account.username,
                email: !account.email,
                password: !account.password,
                confirmPassword: !account.confirmPassword,
            })

            isError = true
        } else {
            if (
                account.username.length < 6 ||
                !validEmail(account.email) ||
                account.password !== account.confirmPassword
            ) {
                setErrors({
                    ...errors,
                    username: !account.username,
                    email: !account.email,
                    password: !account.password,
                    confirmPassword: !account.confirmPassword,
                    invalidUsername:
                        account.username && account.username.length < 6,
                    invalidEmail: account.email && !validEmail(account.email),
                    invalidConfirmPassword:
                        account.password &&
                        account.confirmPassword &&
                        account.password !== account.confirmPassword,
                })

                isError = true
            }
        }

        if (!isError) {
            setLoading(true)
            const version = await getVersion()
            const accountInfo = await registerUser(version, account).finally(() => {setLoading(false)})
            if (accountInfo && accountInfo.id) {
                navigation.navigate(ScreenIds.Activation, {accountInfo})
            }
            setLoading(false)
        }
    }

    return (
        <BaseScreen title=''>
            <ScrollView
                keyboardShouldPersistTaps="handled" 
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    paddingBottom: 50,
                }}
            >   
                <View style={styles.logoContainer}>
                    <Image 
                        style={styles.logo} 
                        source={IMAGES.LOGO_GRADIENT_TOP} 
                        resizeMode="contain"
                    />
                </View>
                <KeyboardAvoidingView enabled>
                    <View style={styles.container}>
                        <View style={styles.formContainer}>
                            <Input
                                style={styles.formGroup}
                                textStyle={styles.mainFont}
                                value={account.username}
                                status={errors.username && 'danger'}
                                label={translate('signUp.username')}
                                placeholder={translate('signUp.usernamePlaceholder')}
                                caption={ errors.invalidUsername && renderUsernameCaption }
                                accessoryRight={renderUserIcon}
                                onChangeText={username => onUsernameChange(username)}
                            />
                            <Input
                                style={styles.formGroup}
                                textStyle={styles.mainFont}
                                value={account.email}
                                status={errors.email && 'danger'}
                                label={translate('signUp.email')}
                                placeholder={translate('signUp.emailPlaceholder')}
                                caption={errors.invalidEmail && renderEmailCaption}
                                accessoryRight={renderEmailIcon}
                                onChangeText={email => onEmailChange(email)}
                            />
                            <Input
                                style={styles.formGroup}
                                textStyle={styles.mainFont}
                                value={account.password}
                                status={errors.password && 'danger'}
                                label={translate('signUp.password')}
                                placeholder={translate('signUp.passwordPlaceholder')}
                                caption={renderCaption}
                                accessoryRight={renderEyeIcon}
                                secureTextEntry={secureTextEntry}
                                onChangeText={password => onPasswordChange(password)}
                            />
                            <Input
                                style={styles.formGroup}
                                textStyle={styles.mainFont}
                                value={account.confirmPassword}
                                status={errors.confirmPassword && 'danger'}
                                label={translate('signUp.confirmPassword')}
                                placeholder={translate('signUp.confirmPasswordPlaceholder')}
                                caption={renderConfirmCaption}
                                accessoryRight={renderEyeIcon}
                                secureTextEntry={secureTextEntry}
                                onChangeText={confirmPassword =>
                                    onConfirmPasswordChange(confirmPassword)
                                }
                            />
                        </View>
                    </View>
                    <View style={{paddingHorizontal: 30, flexDirection: "row"}}>
                        <CheckBox
                            textStyle={styles.mainFont}
                            style={styles.checkbox}
                            checked={isAgree}
                            onChange={nextChecked => setIsAgree(nextChecked)}></CheckBox>
                        <TouchableOpacity
                            onPress={() => navigation.navigate(ScreenIds.TermOfUseScreen)}
                        >
                            <Text category="p2">
                                I agree with the Term and Conditions
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.bottomButton}>
                        <Button
                            style={styles.button}
                            status="primary"
                            size={"small"}
                            disabled={!isAgree}
                            accessoryLeft={loading && LoadingIndicator}
                            onPress={() => (!loading ? handleSignUp(): {})}
                        >
                            {props => (
                                <Text
                                    {...props}
                                    category="p1"
                                    style={[styles.mainFont, styles.lightColor]}>
                                    {translate('signUp.signUpCap')}
                                </Text>
                            )}
                        </Button>

                        <View style={styles.signUp}>
                            <TouchableOpacity
                                onPress={() => navigation.navigate(ScreenIds.Login)}>
                                <Text category="s2">{translate('signUp.signInQuote')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </BaseScreen>
    )
}

export default SignUpScreen
