import {StyleSheet} from 'react-native'
import {COLORS} from '../../../assets/theme/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    logoContainer: {
        marginTop: -20,
        width: '100%',
        alignItems: 'center',
    },
    formContainer: {
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    formGroup: {
        marginBottom: 16,
    },
    logo: {
        width: 220,
        height: 160,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkbox: {
        marginRight: 10
    },  
    captionContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    captionIcon: {
        width: 10,
        height: 10,
        marginRight: 5,
    },
    captionText: {
        fontSize: 12,
        fontWeight: '400',
        color: '#8F9BB3',
    },
    invalidCaptionText: {
        fontSize: 12,
        fontWeight: '400',
        color: COLORS.ERROR,
    },
    bottomButton: {
        width: '100%',
        paddingHorizontal: 30,
        paddingVertical: 20
    },
    signUp: {
        marginTop: 16,
        alignSelf: 'center',
    },
    mainFont: {
        fontFamily: 'Prata-Regular'
    },
    lightColor: {
        color: COLORS.WHITE
    },
    itemPicker: {
        width: '30%'
    }
})

export default styles
