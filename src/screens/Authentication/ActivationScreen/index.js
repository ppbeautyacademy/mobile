import {Text, Button, Spinner, Icon} from '@ui-kitten/components'
import React, {useContext, useEffect, useState} from 'react'
import {Image, Platform, TouchableOpacity, View} from 'react-native'
import styles from './styles'
import OtpInputs from 'react-native-otp-inputs'
import {
    getUserId,
    getVersion,
    HTTP_STATUS_OK,
    setUserInfo,
} from '../../../assets/constants'
import {
    activateUser,
    resendActivationCode,
    revokeRefreshToken,
} from '../../../services/ServiceWorker'
import {ScreenIds} from '../../../ScreenIds'
import {IMAGES} from '../../../assets/images'
import {translate} from '../../../../config/i18n'
import BaseScreen from '../../../component/BaseScreen'
import {setUserAuth} from '../../../services/AuthService'
import KeyboardScrollView from '../../../component/KeyboardScrollView'
import context from '../../../../context/context'
import { resetNavigatorToScreen, resetRootNavigatorToScreen } from '../../../Navigation'
import { getUserCredentials } from '../../../services/secureData'

const ActivationScreen = ({route, navigation}) => {
    const {accountInfo} = route?.params ?? {}
    const fromScreen = route && route.params && route.params.fromScreen ? route.params.fromScreen : null

    const {actions} = useContext(context)
    const [activationCode, setActivationCode] = useState('')
    const [loading, setLoading] = useState(false)
    const [timer, setTimer] = useState(60)
    const [success, setSuccess] = useState(false)

    useEffect(() => {
        handleResendActivationCode(false)
    }, [])

    useEffect(() => {
        let isMounted = true; 
        if(isMounted) {
            const timerId = setInterval(() => {
                if (timer <= 0) {
                    setTimer(0)
                } else setTimer(s => s - 1)
            }, 1000)
            return () => clearInterval(timerId)
        }

        return () => { isMounted = false };
    }, [timer])

    const LoadingIndicator = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" />
        </View>
    )

    const SuccessIndicator = props => (
        <Icon {...props} name="checkmark-circle-outline" />
    )

    const handleResendActivationCode = async (showNotification = true) => {
        const version = await getVersion()
        if (accountInfo?.id && version) {
            resendActivationCode(accountInfo.id, version).then(data => {
                if (data === HTTP_STATUS_OK) {
                    if (showNotification) {
                        toast.show(translate('activation.activationCodeResent'))
                    }

                    setTimer(60)
                }
            })
        }
    }

    const handleActivateAccount = async () => {
        let isError = false
        if (!activationCode || activationCode.length < 6) {
            toast.show(translate('activation.invalidActivationCode'))
            isError = true
        }

        if (!isError) {
            if (accountInfo?.id) {
                const userData = await activateUser(
                    accountInfo.id,
                    activationCode,
                )

                if (userData && userData.userToken && userData.userInfo) {
                    const {refreshToken, tokenType, token} =
                        userData.userToken ?? {}
                    await setUserInfo(userData)
                    await setUserAuth({refreshToken, tokenType, token})

                    const {profileSetUp} = userData.userInfo
                    setLoading(false)
                    setSuccess(true)
                    setActivationCode('')

                    if (!profileSetUp) {
                        navigation.navigate(ScreenIds.ProfileSetUp)
                    } else {
                        actions.setUserInfo(userData.userInfo)
                        actions.setIsLoggedIn(true);
                        resetNavigatorToScreen(navigation, ScreenIds.MainStack, ScreenIds.MainTab, {}, ScreenIds.Account);
                    }
                } else {
                    setActivationCode('')
                    setLoading(false)
                }
            }
        }
    }

    //#region Cancel activation process
    const handleOnBackPress = async() => {
        actions.showWhiteAppSpinner(true);

        await handleRevokeToken();

        actions.resetUserAuth()
        actions.resetUserInfo()
        actions.setIsLoggedIn(false);

        if(fromScreen && fromScreen === ScreenIds.SplashScreen) {
            resetNavigatorToScreen(navigation, ScreenIds.MainStack, ScreenIds.MainTab, {}, ScreenIds.Account)
        } else {
            resetRootNavigatorToScreen(ScreenIds.Login);
        }
        
        actions.showWhiteAppSpinner(false);
    }

    const handleRevokeToken = async() => {
        let userAuthInfo
        await getUserCredentials()
            .then(authInfo => {
                userAuthInfo = authInfo
            })
            .catch(err => logService.log(err))

        await revokeRefreshToken(userAuthInfo?.refreshToken)
        .then(data => {
            if(data === HTTP_STATUS_OK)  {
                return;
            }
        })
    }
    //#e~ndregion  

    return (
        <BaseScreen title='' onBackPress={() => handleOnBackPress()}>
            <KeyboardScrollView
                extraScrollHeight={Platform.OS === 'ios' ? 0 : 100}
            >
                <View style={styles.logoTopWrapper}>
                    <Image
                        style={styles.logoTop}
                        source={IMAGES.LOGO_GRADIENT_TOP}
                    />
                    <Text category="s2">
                        {translate('activation.mainTitle')}
                    </Text>
                </View>
                <View style={styles.bodyContainer}>
                    <OtpInputs
                        style={styles.otpContainer}
                        keyboardType="name-phone-pad"
                        handleChange={code =>
                            setActivationCode(code.toUpperCase())
                        }
                        numberOfInputs={6}
                        inputContainerStyles={styles.otpInput}
                        inputStyles={styles.otpInputWrapper}
                    />

                    {timer > 0 ? (
                        <TouchableOpacity disabled={true}>
                            <Text style={styles.mb5} category="p2">
                                {translate('activation.resendActivationTimer', {
                                    time: timer,
                                })}
                            </Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity onPress={handleResendActivationCode}>
                            <Text style={styles.mb5} category="p2">
                                {translate('activation.resendActivationCode')}
                            </Text>
                        </TouchableOpacity>
                    )}

                    <Button
                        style={styles.button}
                        appearance="outline"
                        onPress={handleActivateAccount}
                        accessoryLeft={
                            loading
                                ? LoadingIndicator
                                : success && SuccessIndicator
                        }>
                        {translate('activation.activateBtn')}
                    </Button>
                </View>
            </KeyboardScrollView>
        </BaseScreen>
    )
}

export default ActivationScreen
