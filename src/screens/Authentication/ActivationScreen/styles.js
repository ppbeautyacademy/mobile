import {StyleSheet} from 'react-native'
import {COLORS} from '../../../assets/theme/colors'
import {commonStyles} from '../../../assets/theme/styles'

const styles = StyleSheet.create({
    container: {},
    otpContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '5%',
        marginBottom: '5%',
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    otpInputWrapper: {
        textTransform: 'capitalize',
        textAlign: 'center',
        fontWeight: '700',
    },

    otpInput: {
        backgroundColor: COLORS.LIGHTGRAY,
        marginRight: 5,
        paddingLeft: 3,
        paddingRight: 3,
        borderRadius: 3,
        fontSize: 16,
        textTransform: 'uppercase',
    },

    mb5: {
        marginBottom: '5%',
    },
    button: {
        paddingRight: '20%',
        paddingLeft: '20%',
    },
    logoTopWrapper: {
        width: '100%',
        alignItems: 'center',
    },
    logoTop: {
        width: 230,
        height: 150,
        marginBottom: 8,
    },
    logoContainer: {
        width: '100%',
        paddingTop: '30%',
        alignItems: 'center',
    },
    bottomButton: {
        ...commonStyles.center,
        marginBottom: 40,
    },
    bodyContainer: {
        alignItems: 'center',
    },
})

export default styles
