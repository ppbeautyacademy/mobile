import {Button, Icon, Input, Spinner, Text} from '@ui-kitten/components'
import {TouchableWithoutFeedback} from '@ui-kitten/components/devsupport'
import React, {useContext, useEffect, useState} from 'react'
import {Image, View} from 'react-native'
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler'
import OtpInputs from 'react-native-otp-inputs'
import {translate} from '../../../../config/i18n'
import context from '../../../../context/context'
import {HTTP_STATUS_OK} from '../../../assets/constants'
import {IMAGES} from '../../../assets/images'
import BaseScreen from '../../../component/BaseScreen'
import KeyboardScrollView from '../../../component/KeyboardScrollView'
import {ScreenIds} from '../../../ScreenIds'
import {
    checkResetPasswordCode,
    forgetPassword,
    resetPassword,
} from '../../../services/ServiceWorker'
import styles from './styles'

const stages = {
    REQUEST: 'REQUEST', // Request forget password
    CONFIRM: 'CONFIRM',
    CHANGE: 'CHANGE', // Change password progress
    SUCCESS: 'SUCCESS', // Success to change password
}

const ForgetPasswordScreen = ({navigation}) => {
    const {state, actions} = useContext(context)

    const [username, setUsername] = useState('')
    const [confirmationCode, setConfirmationCode] = useState('')
    const [account, setAccount] = useState({})
    const [timer, setTimer] = useState(5)
    const [secureTextEntry, setSecureTextEntry] = useState(true)

    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState({})
    const [stage, setStage] = useState(stages.REQUEST)

    useEffect(() => {
        let isMounted = true; 
        if(isMounted) {
            const timerId = setInterval(() => {
                if (timer <= 0) {
                    setTimer(0)
                } else setTimer(s => s - 1)
            }, 1000)
            return () => clearInterval(timerId)
        }

        return () => { isMounted = false };
    }, [timer])

    const renderFullNameIcon = props => (
        <Icon {...props} name="person-outline" />
    )

    const SendIndicator = props => (
        <Icon {...props} name="paper-plane-outline" />
    )

    const LoadingIndicator = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" />
        </View>
    )

    const renderEyeIcon = props => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry)
    }

    const renderCaption = () => {
        return (
            <View style={styles.captionContainer}>
                <Text style={styles.captionText}>
                    {translate('forgetPassword.passwordValidation')}
                </Text>
            </View>
        )
    }

    const renderConfirmCaption = () => {
        return (
            <View style={styles.captionContainer}>
                <Text
                    style={
                        errors.invalidConfirmPassword
                            ? styles.invalidCaptionText
                            : styles.captionText
                    }>
                    {translate('forgetPassword.passwordConfirmationValidation')}
                </Text>
            </View>
        )
    }

    const handleRequestForgetPassword = async () => {
        setLoading(true)

        let isError = false
        if (!username) {
            setErrors({
                ...errors,
                username: true,
            })
            isError = true
        }

        if (!isError) {
            let toSendData = {
                username: username,
            }

            const fpResponse = await forgetPassword(state.version, toSendData)
            if (fpResponse === HTTP_STATUS_OK) {
                setErrors({})
                setStage(stages.CONFIRM)
            }

            setLoading(false)
        } else {
            setLoading(false)
        }
    }

    const handleResendResetPasswordCode = async () => {
        setLoading(true)

        let isError = false
        if (!username) {
            setErrors({
                ...errors,
                username: true,
            })
            isError = true
        }

        if (!isError) {
            let toSendData = {
                username: username,
            }

            const fpResponse = await forgetPassword(state.version, toSendData)
            if (fpResponse === HTTP_STATUS_OK) {
                setErrors({})

                setTimer(60)
            }

            setLoading(false)
        } else {
            setLoading(false)
        }
    }

    const handleConfirmCode = async () => {
        setLoading(true)
        let isError = false

        if (!confirmationCode) {
            setErrors({
                ...errors,
                confirmationCode: true,
            })

            toast.show(translate('forgetPassword.emptyConfirmationCode'))

            isError = true
        }

        if (!isError) {
            const confirmationStatus = await checkResetPasswordCode(
                state.version,
                username,
                confirmationCode,
            )
            if (confirmationStatus === HTTP_STATUS_OK) {
                setErrors({})
                setStage(stages.CHANGE)
            }
            setLoading(false)
        } else {
            setLoading(false)
        }
    }

    const handleChangePassword = async () => {
        setLoading(true)
        let isError = false

        if (!account || !account.newPassword || !account.confirmPassword) {
            setErrors({
                ...account,
                newPassword: !account.newPassword,
                confirmPassword: !account.confirmPassword,
                invalidPassword:
                    account.newPassword &&
                    account.confirmPassword &&
                    account.newPassword !== account.confirmPassword,
            })

            isError = true
        }

        if (!isError) {
            let toSendData = {
                ...account,
                username: username,
            }

            const resetPasswordStatus = await resetPassword(
                state.version,
                toSendData,
            )
            if (resetPasswordStatus === HTTP_STATUS_OK) {
                setErrors({})
                setStage(stages.SUCCESS)
            }
            setLoading(false)
        } else {
            setLoading(false)
        }
    }

    const handleSetConfirmationCode = (code) => {
        setConfirmationCode(code.toUpperCase())
    }

    return (
        <BaseScreen title=''>
            <ScrollView
                keyboardShouldPersistTaps="handled" 
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    paddingTop: 50,
                    paddingBottom: 100
                }}
            >
                <View style={styles.container}>
                    <View style={styles.logoWrapper}>
                        <Image style={styles.logo} source={IMAGES.LOGO_GRADIENT_TOP} />
                    </View>

                    <Text category="h5" style={styles.mb5}>
                        {translate('forgetPassword.mainTitle')}
                    </Text>

                    {stage === stages.REQUEST ? (
                        <React.Fragment>
                            <Input
                                style={styles.formGroup}
                                textStyle={styles.mainFont}
                                value={username}
                                label={translate('forgetPassword.usernameOrEmail')}
                                placeholder={translate('forgetPassword.usernameOrEmailPlaceholder')}
                                accessoryLeft={renderFullNameIcon}
                                onChangeText={username => setUsername(username)}
                                status={errors.username && 'danger'}
                            />

                            <Button
                                status="primary"
                                size={"small"}
                                onPress={() => handleRequestForgetPassword()}
                                style={styles.button}
                                accessoryLeft={
                                    loading ? LoadingIndicator : SendIndicator
                                }>
                                {props => (
                                    <Text
                                        {...props}
                                        style={[styles.mainFont, styles.lightColor]}>
                                        {translate('forgetPassword.sendRequestBtn')}
                                    </Text>
                                )}
                            </Button>
                        </React.Fragment>
                    ) : stage === stages.CONFIRM ? (
                        <React.Fragment>
                            <Text appearance="hint" category="s2">
                                {translate('forgetPassword.confirmCodeSent')}
                            </Text>
                            <Text appearance="hint" style={styles.mb5}>
                                {translate('forgetPassword.checkCodeSentMsg')}
                            </Text>

                            <OtpInputs
                                style={styles.otpContainer}
                                keyboardType="name-phone-pad"
                                handleChange={code =>
                                    handleSetConfirmationCode(code)
                                }
                                numberOfInputs={6}
                                inputContainerStyles={styles.otpInput}
                                inputStyles={styles.otpInputWrapper}
                            />

                            {timer > 0 ? (
                                <TouchableOpacity disabled={true}>
                                    <Text style={styles.mb5} category="p2">
                                        {translate('forgetPassword.resendConfirmationTimer', {time: timer})}
                                    </Text>
                                </TouchableOpacity>
                            ) : (
                                <TouchableOpacity
                                    onPress={() =>
                                        handleResendResetPasswordCode()
                                    }>
                                    <Text style={styles.mb5} category="p2">
                                        {translate('forgetPassword.resendConfirmationCode')}
                                    </Text>
                                </TouchableOpacity>
                            )}

                            <Button
                                status="primary"
                                onPress={() => handleConfirmCode()}
                                style={styles.button}
                                accessoryLeft={
                                    loading ? LoadingIndicator : SendIndicator
                                }>
                                {props => (
                                    <Text
                                        {...props}
                                        category="label"
                                        style={[styles.mainFont, styles.lightColor]}>
                                        {translate('forgetPassword.submitBtn')}
                                    </Text>
                                )}
                            </Button>
                        </React.Fragment>
                    ) : stage === stages.CHANGE ? (
                        <React.Fragment>
                            <Input
                                style={styles.formGroup}
                                textStyle={styles.mainFont}
                                value={account.newPassword}
                                status={errors.newPassword && 'danger'}
                                label={translate('forgetPassword.password')}
                                placeholder={translate('forgetPassword.passwordPlaceholder')}
                                caption={renderCaption}
                                accessoryRight={renderEyeIcon}
                                secureTextEntry={secureTextEntry}
                                onChangeText={newPassword =>
                                    setAccount({
                                        ...account,
                                        newPassword: newPassword,
                                    })
                                }
                            />

                            <Input
                                style={styles.formGroup}
                                value={account.confirmPassword}
                                status={errors.confirmPassword && 'danger'}
                                label={translate('forgetPassword.confirmPassword')}
                                placeholder={translate('forgetPassword.confirmPasswordPlaceholder')}
                                caption={renderConfirmCaption}
                                accessoryRight={renderEyeIcon}
                                secureTextEntry={secureTextEntry}
                                onChangeText={confirmPassword =>
                                    setAccount({
                                        ...account,
                                        confirmPassword: confirmPassword,
                                    })
                                }
                            />
                            <Button
                                status="primary"
                                onPress={() => handleChangePassword()}
                                style={styles.button}
                                accessoryLeft={
                                    loading ? LoadingIndicator : SendIndicator
                                }>
                                {props => (
                                    <Text
                                        {...props}
                                        category="label"
                                        style={[styles.mainFont, styles.lightColor]}>
                                        {translate('forgetPassword.submitBtn')}
                                    </Text>
                                )}
                            </Button>
                        </React.Fragment>
                    ) : (
                        stage === stages.SUCCESS && (
                            <React.Fragment>
                                <Text style={styles.mb5} appearance="hint">
                                    {translate('forgetPassword.passwordChangedSuccess')}
                                </Text>
                                <Button
                                    status="primary"
                                    onPress={() =>
                                        navigation.navigate(ScreenIds.Login)
                                    }
                                    style={styles.button}>
                                        {props => (
                                            <Text
                                                {...props}
                                                category="label"
                                                style={[styles.mainFont, styles.lightColor]}>
                                                {translate('forgetPassword.loginNow')}
                                            </Text>
                                        )}
                                </Button>
                            </React.Fragment>
                        )
                    )}

                    {stage !== stages.SUCCESS && (
                        <React.Fragment>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate(ScreenIds.Login)
                                }>
                                    <View
                                        style={{justifyContent: 'center', alignItems: 'center'}}
                                    >
                                        <Text category="p2">{translate('forgetPassword.hadAccountMsg')}</Text>
                                    </View>
                            </TouchableOpacity>
                        </React.Fragment>
                    )}
                </View>
            </ScrollView>
        </BaseScreen>
    )
}

export default ForgetPasswordScreen
