import {StyleSheet} from 'react-native'
import {COLORS} from '../../../assets/theme/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30
    },
    formGroup: {
        marginBottom: '5%',
    },
    button: {
        width: '100%',
        marginBottom: '10%',
    },
    logo: {
        width: 200,
        height: 110,
        marginBottom: 8,
    },
    logoTopWrapper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: '5%',
    },
    logoTop: {
        width: 230,
        height: 150,
        marginBottom: 8,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    forgetPasswordWrapper: {},

    forgetPassword: {
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginBottom: '5%',
    },

    forgetPasswordFs: {
        fontSize: 13,
        fontStyle: 'italic',
    },

    otpContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '5%',
        marginBottom: '5%',
    },

    otpInputWrapper: {
        textTransform: 'capitalize',
        textAlign: 'center',
        fontWeight: '700',
    },

    otpInput: {
        backgroundColor: COLORS.LIGHTGRAY,
        marginRight: 5,
        paddingLeft: 3,
        paddingRight: 3,
        borderRadius: 3,
        fontSize: 16,
        textTransform: 'uppercase',
    },

    mb5: {
        marginBottom: '5%',
    },

    profileWrapper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },

    secureMessage: {
        textAlign: 'center',
        fontSize: 14,
    },

    selectWrapper: {
        display: 'flex',
        alignSelf: 'stretch',
        textAlign: 'center',
        width: '100%',
        marginBottom: '7%',
    },

    captionContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    captionIcon: {
        width: 10,
        height: 10,
        marginRight: 5,
    },
    captionText: {
        fontSize: 12,
        fontWeight: '400',
        color: '#8F9BB3',
    },
    invalidCaptionText: {
        fontSize: 12,
        fontWeight: '400',
        color: COLORS.ERROR,
    },
    mainFont: {
        fontFamily: 'Prata-Regular'
    },
    lightColor: {
        color: COLORS.WHITE
    }
})

export default styles
