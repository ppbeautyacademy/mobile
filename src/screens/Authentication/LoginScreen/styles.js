import {StyleSheet} from 'react-native'
import { COLORS } from '../../../assets/theme/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        paddingHorizontal: 30
    },
    formContainer: {
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    formGroup: {
        marginBottom: 16
    },
    button: {
        paddingHorizontal: 30
    },
    logo: {
        width: 220,
        height: 160,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    forgetPassword: {
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'flex-end',
    },
    forgetPasswordFs: {
        fontSize: 13,
        fontStyle: 'italic',
    },
    signUp: {
        marginTop: 16,
        alignSelf: 'center',
    },
    logoContainer: {
        width: '100%',
        alignItems: 'center',
    },
    bottomButton: {
        alignItems: 'center',
    },
    mainFont: {
        fontFamily: 'Prata-Regular'
    },
    lightColor: {
        color: COLORS.WHITE
    }
})

export default styles
