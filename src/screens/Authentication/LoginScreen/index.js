import {Icon, Input, Text, Button, Spinner} from '@ui-kitten/components'
import React, {useContext, useEffect, useState} from 'react'
import {
    Image,
    KeyboardAvoidingView,
    Platform,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    Text as RNText,
    Alert,
} from 'react-native'
import {translate} from '../../../../config/i18n'
import {FACEBOOK_SIGN_IN_APP_ID, GOOGLE_SIGN_IN_CLIENT_ID, setUserId, setUserInfo, setUsername} from '../../../assets/constants'
import {IMAGES} from '../../../assets/images'
import BaseScreen from '../../../component/BaseScreen'
import {ScreenIds} from '../../../ScreenIds'
import {appleAuthenticate, authorizeAppleCode, googleAuthenticate, signIn} from '../../../services/ServiceWorker'
import styles from './styles'
import context from '../../../../context/context'
import {setUserAuth} from '../../../services/AuthService'
import {ScrollView} from 'react-native-gesture-handler'
import {COLORS} from '../../../assets/theme/colors'
import { appleAuthAndroid, AppleButton } from '@invertase/react-native-apple-authentication';
import { v4 as uuid } from 'uuid'
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-google-signin/google-signin';
import { Settings } from 'react-native-fbsdk-next';
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk-next';
import auth from '@react-native-firebase/auth';
import FastImage from 'react-native-fast-image'

// TODO Translation
const LoginScreen = ({navigation}) => {
    const {state, actions} = useContext(context)
    const [account, setAccount] = useState({username: '', password: ''})
    const [loading, setLoading] = useState(false)
    const [errors, setErrors] = useState({username: false, password: false})
    const [secureTextEntry, setSecureTextEntry] = useState(true)

    const toggleSecureEntry = () => {
        setSecureTextEntry(!secureTextEntry)
    }

    const LoadingIndicator = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" status="basic" />
        </View>
    )

    const renderUserIcon = props => <Icon {...props} name="person-outline" />

    const renderEyeIcon = props => (
        <TouchableWithoutFeedback onPress={toggleSecureEntry}>
            <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
        </TouchableWithoutFeedback>
    )

    const onUsernameChange = username => {
        let whiteSpaceRemoved = username.replace(/ /g, '')
        let toSetUsername = whiteSpaceRemoved.toLowerCase()
        setAccount({...account, username: toSetUsername})
    }

    const onPasswordChange = password => {
        let toSetPassword = password.replace(/ /g, '')
        setAccount({...account, password: toSetPassword})
    }

    const authenticateUser = async () => {
        setLoading(true)

        let isError = false
        if (
            !account ||
            (!account.username && !account.password) ||
            !account.username ||
            !account.password
        ) {
            setErrors({
                ...errors,
                username: !account.username,
                password: !account.password,
            })

            isError = true
        }

        if (!isError) {
            const accountInfo = await signIn(account)

            if (accountInfo && accountInfo.userInfo) {
                if (accountInfo?.userToken) {
                    actions.setToken(accountInfo.userToken?.token)
                    actions.setRefreshToken(accountInfo.userToken?.refreshToken)
                    const {refreshToken, tokenType, token} =
                        accountInfo?.userToken ?? {}
                    await setUserAuth({refreshToken, tokenType, token})
                }
                if (accountInfo?.userInfo?.active) {
                    await setUserInfo(accountInfo)
                    setAccount({
                        username: '',
                        password: '',
                    })

                    actions.setUserInfo(accountInfo?.userInfo)
                    if (accountInfo?.userInfo.profileSetUp) {
                        actions.setIsLoggedIn(true)
                        navigation.goBack()
                    } else {
                        navigation.replace(ScreenIds.ProfileSetUp)
                    }
                } else {
                    setUserId(accountInfo?.userInfo?.id)
                    setUsername(accountInfo?.userInfo?.username)

                    setAccount({
                        username: '',
                    })

                    navigation.replace(ScreenIds.Activation, {
                        accountInfo: accountInfo?.userInfo,
                    })
                }
            } else {
                setAccount({
                    ...account,
                    password: '',
                })
            }
        } else {
            setAccount({
                ...account,
                password: '',
            })
        }
        setLoading(false)
    }

    // Apple Authentication
    const onAppleButtonPress = async() => {
        // Generate secure, random values for state and nonce
        const rawNonce = uuid();
        const state = uuid();

        // Configure the request
        appleAuthAndroid.configure({
            // The Service ID you registered with Apple
            clientId: 'com.ppbeautyacademy.elearning',

            // Return URL added to your Apple dev console. We intercept this redirect, but it must still match
            // the URL you provided to Apple. It can be an empty route on your backend as it's never called.
            redirectUri: 'https://ppbeautyacademy.com/en/sign-in',

            // The type of response requested - code, id_token, or both.
            responseType: appleAuthAndroid.ResponseType.ALL,

            // The amount of user information requested from Apple.
            scope: appleAuthAndroid.Scope.ALL,

            // Random nonce value that will be SHA256 hashed before sending to Apple.
            nonce: rawNonce,

            // Unique state value used to prevent CSRF attacks. A UUID will be generated if nothing is provided.
            state,
        });

        // Open the browser window for user sign in
        const response = await appleAuthAndroid.signIn();

        // Send the authorization code to your backend for verification
        if(response && response?.code) {
            actions.showWhiteAppSpinner(true)
            const params = new URLSearchParams();
            params.append('code', response?.code);
            
            const authData = await appleAuthenticate(params);
            if(authData && authData.userInfo) {
                if (authData?.userToken) {
                    actions.setToken(authData.userToken?.token)
                    actions.setRefreshToken(authData.userToken?.refreshToken)
                    const {refreshToken, tokenType, token} =
                    authData?.userToken ?? {}
                    await setUserAuth({refreshToken, tokenType, token})
                }
                if (authData?.userInfo?.active) {
                    await setUserInfo(authData)
                    setAccount({
                        username: '',
                        password: '',
                    })

                    actions.setUserInfo(authData?.userInfo)
                    if (authData?.userInfo.profileSetUp) {
                        actions.setIsLoggedIn(true)
                        navigation.goBack()
                    } else {
                        navigation.replace(ScreenIds.ProfileSetUp)
                    }
                } else {
                    setUserId(authData?.userInfo?.id)
                    setUsername(authData?.userInfo?.username)

                    setAccount({
                        username: '',
                    })

                    navigation.replace(ScreenIds.Activation, {
                        accountInfo: authData?.userInfo,
                    })
                }
            }
            actions.showWhiteAppSpinner(false)
        } else {
            // TODO
            // Invalid Credentials
            // Toast.show({
                
            // })
        }
    }

    // Google Authentication
    const [error, setError] = useState(null);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly', 'email'],
            webClientId: GOOGLE_SIGN_IN_CLIENT_ID,
            offlineAccess: false,
            forceCodeForRefreshToken: true,
        })
    },[])

    const googleSignIn = async() => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            const {idToken} = userInfo;

            // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);

            // Sign-in the user with the credential
            const userData = await auth().signInWithCredential(googleCredential);

            if(userData && userData.additionalUserInfo) {
                actions.showWhiteAppSpinner(true);
                if(userData.additionalUserInfo?.profile) {
                    let toPostData = {
                        email: userData.additionalUserInfo?.profile.email,
                        name: userData.additionalUserInfo?.profile.name,
                        googleId: userData.additionalUserInfo?.profile.sub,
                        imageUrl: userData.additionalUserInfo?.profile.picture
                    }

                    const authData = await googleAuthenticate(toPostData)
                    if(authData && authData.userInfo) {
                        if (authData?.userToken) {
                            actions.setToken(authData.userToken?.token)
                            actions.setRefreshToken(authData.userToken?.refreshToken)
                            const {refreshToken, tokenType, token} =
                            authData?.userToken ?? {}
                            await setUserAuth({refreshToken, tokenType, token})
                        }
                        if (authData?.userInfo?.active) {
                            await setUserInfo(authData)
                            setAccount({
                                username: '',
                                password: '',
                            })

                            actions.setUserInfo(authData?.userInfo)
                            if (authData?.userInfo.profileSetUp) {
                                actions.setIsLoggedIn(true)
                                navigation.goBack()
                            } else {
                                navigation.replace(ScreenIds.ProfileSetUp)
                            }
                        } else {
                            setUserId(authData?.userInfo?.id)
                            setUsername(authData?.userInfo?.username)

                            setAccount({
                                username: '',
                            })

                            navigation.replace(ScreenIds.Activation, {
                                accountInfo: authData?.userInfo,
                            })
                        }
                    }
                }
                actions.showWhiteAppSpinner(false);
            }
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // when user cancels sign in process,
                Alert.alert('Process Cancelled');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // when in progress already
                Alert.alert('Process in progress');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // when play services not available
                Alert.alert('Play services are not available');
            } else {
                // some other error
                Alert.alert('Something else went wrong... ', error.toString());
                setError(error);
            }
        }
    }

    // Facebook Authentication
    useEffect(async() => {
        await Settings.setAppID(FACEBOOK_SIGN_IN_APP_ID);
        await Settings.initializeSDK();
    }, [])

    const facebookLogin = async() => {
        try {
            // Attempt login with permissions
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

            if (result.isCancelled) {
              throw 'User cancelled the login process';
            }

            // Once signed in, get the users AccesToken
            const data = await AccessToken.getCurrentAccessToken();
            
            if (!data) {
                throw 'Something went wrong obtaining access token';
            }

            // Create a Firebase credential with the AccessToken
            const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

            // Sign-in the user with the credential
            const userData = await auth().signInWithCredential(facebookCredential);

            if(userData && userData.additionalUserInfo) {
                actions.showWhiteAppSpinner(true)
                if(userData.additionalUserInfo?.profile) {
                    let toPostData = {
                        email: userData.additionalUserInfo?.profile.email,
                        name: userData.additionalUserInfo?.profile.name,
                        googleId: userData.additionalUserInfo?.profile.id,
                        imageUrl: userData.additionalUserInfo?.profile.picture.imageUrl
                    }

                    const authData = await googleAuthenticate(toPostData)
                    if(authData && authData.userInfo) {
                        if (authData?.userToken) {
                            actions.setToken(authData.userToken?.token)
                            actions.setRefreshToken(authData.userToken?.refreshToken)
                            const {refreshToken, tokenType, token} =
                            authData?.userToken ?? {}
                            await setUserAuth({refreshToken, tokenType, token})
                        }
                        if (authData?.userInfo?.active) {
                            await setUserInfo(authData)
                            setAccount({
                                username: '',
                                password: '',
                            })

                            actions.setUserInfo(authData?.userInfo)

                            if (authData?.userInfo.profileSetUp) {
                                actions.setIsLoggedIn(true)
                                navigation.goBack()
                            } else {
                                navigation.replace(ScreenIds.ProfileSetUp)
                            }
                        } else {
                            setUserId(authData?.userInfo?.id)
                            setUsername(authData?.userInfo?.username)

                            setAccount({
                                username: '',
                            })

                            navigation.replace(ScreenIds.Activation, {
                                accountInfo: authData?.userInfo,
                            })
                        }
                    }
                }
                actions.showWhiteAppSpinner(false)
            }
        } catch(error) {
            console.log({error});
        }
    }

    const FacebookIcon = (props) => (
        <Icon {...props} name="facebook" />
    )

    const GoogleIcon = (props) => (
        <Icon {...props} name="google-outline" />
    )

    return (
        <BaseScreen title={''}>
            <ScrollView
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={IMAGES.LOGO_GRADIENT_TOP}
                        resizeMode="contain"
                    />
                </View>
                <KeyboardAvoidingView enabled>
                    <View style={[styles.formContainer, styles.container]}>
                        <Input
                            style={styles.formGroup}
                            textStyle={styles.mainFont}
                            value={account.username}
                            status={errors.username && 'danger'}
                            label={translate('login.username')}
                            placeholder={translate('login.usernamePlaceholder')}
                            accessoryRight={renderUserIcon}
                            onChangeText={username =>
                                onUsernameChange(username)
                            }
                        />
                        <Input
                            style={[styles.formGroup]}
                            textStyle={styles.mainFont}
                            value={account.password}
                            status={errors.password && 'danger'}
                            label={translate('login.password')}
                            placeholder={translate('login.passwordPlaceholder')}
                            accessoryRight={renderEyeIcon}
                            secureTextEntry={secureTextEntry}
                            onChangeText={password =>
                                onPasswordChange(password)
                            }
                        />
                        <View style={styles.forgetPassword}>
                            <TouchableOpacity
                                onPress={() => {
                                    navigation.navigate(
                                        ScreenIds.ForgetPassword,
                                    )
                                }}>
                                <Text
                                    appearance="hint"
                                    style={[
                                        styles.mainFont,
                                        styles.forgetPasswordFs,
                                    ]}>
                                    {translate('login.forgetPassword')}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{paddingHorizontal: 30, paddingVertical: 20}}>
                        <Button
                            size={'small'}
                            status="primary"
                            accessoryLeft={loading && LoadingIndicator}
                            onPress={() =>
                                !loading ? authenticateUser() : {}
                            }>
                            {props => (
                                <Text
                                    {...props}
                                    category="p1"
                                    style={[
                                        styles.mainFont,
                                        styles.lightColor,
                                    ]}>
                                    {translate('login.loginCap')}
                                </Text>
                            )}
                        </Button>
                    </View>

                    <Text category={"s2"} style={{textAlign: "center", marginBottom: 15}}>
                        Or login with 
                    </Text>

                    <View style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <View>
                            <TouchableWithoutFeedback
                                onPress={() => googleSignIn()}
                            >
                                <View 
                                    style={{
                                        width: 200,
                                        height: 40,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 1,
                                        },
                                        shadowOpacity: 0.22,
                                        shadowRadius: 2.22,
                                        
                                        elevation: 3,
                                        backgroundColor:"#FFF",
                                        paddingVertical: 0,
                                        marginBottom: 8,
                                        color: "grey",
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        alignItems: "center",
                                        borderRadius: 3
                                    }}>
                                        <Image 
                                            source={require("../../../assets/images/google-sign-btn.png")}
                                            style={{
                                                height: 40,
                                                width: 200
                                            }}
                                            resizeMode='cover'
                                        />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                        <View>
                            <Button onPress={() => facebookLogin()}
                                style={{
                                    width: 200,
                                    height: 40,
                                    backgroundColor: '#3b5998',
                                    color: 'white',
                                    borderColor: 'transparent',
                                    paddingVertical: 0,
                                    paddingHorizontal: 0,
                                    marginBottom: 8
                                }}
                                accessoryLeft={FacebookIcon}
                            >                                
                                Sign in with Facebook
                            </Button>
                        </View>
                        <View>
                            {appleAuthAndroid.isSupported && (
                                <AppleButton
                                    style={{width: 200, height: 40, paddingLeft: 8, marginBottom: 8, fontFamily: "Prata-Regular", fontWeight: "bold"}}
                                    buttonStyle={AppleButton.Style.BLACK}
                                    buttonType={AppleButton.Type.SIGN_IN}
                                    onPress={() => onAppleButtonPress()}
                                    leftView={
                                        <View style={{marginRight: 40, marginLeft: -15}}>
                                            <FastImage source={require('../../../assets/logo/apple_logo_white.png')} style={{height: 18, width: 15, paddingBottom: 0}}/>
                                        </View>
                                    }
                                />
                            )}
                        </View>
                    </View>
                   
                    <View style={[styles.signUp]}>
                        <TouchableOpacity
                            onPress={() => {
                                setErrors({})
                                navigation.navigate(ScreenIds.SignUp)
                            }}>
                            <Text category="s1" style={styles.mainFont}>
                                {translate('login.signUpQuote')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </BaseScreen>
    )
}

export default LoginScreen
