import React, { useContext, useEffect, useState } from 'react';
import { Alert, RefreshControl, StyleSheet, View } from 'react-native';
import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import BaseScreen from '../../component/BaseScreen';
import OrderItem, { displayTypes } from './OrderItem';
import { Button, Divider, Icon as KittenIcon, Input, Spinner, Text } from '@ui-kitten/components';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../assets/theme/colors';
import CartAction from '../Cart/CartAction';
import { ScreenIds } from '../../ScreenIds';
import { cartActionTypes, orderTypes } from '../../assets/constants';
import OrderContext from '../../../context/OrderContext';
import context from '../../../context/context';
import { removeItem } from '../../services/ServiceWorker';
import { translate } from '../../../config/i18n';
import { formatMoney } from '../../component/money/MoneyUtils';
import { useIsFocused } from '@react-navigation/native';

// Unused SCREEN
const OrderScreen = ({navigation, route}) => {
    const {orderType} = route.params;

    const {state, actions} = useContext(context);
    const {orderState, orderActions} = useContext(OrderContext);

    //#region Load Order
    const [orderDetail, setOrderDetail] = useState({})
    const [orders, setOrders] = useState([])
    const [isLoaded, setIsLoaded] = useState(false)

    const loadUserOrder = () => {
        if(orderType === orderTypes.product.value) {
            if(orderState.productOrder.orderDetailList && orderState.productOrder.orderDetailList.length < 1) {
                navigation.navigate(ScreenIds.Cart)
            } else {
                setOrders(orderState.productOrder ? [orderState.productOrder] : [])
                setOrderDetail(orderState.productOrder)
                setIsLoaded(true)
            }
        } else {
            if(orderState.courseOrder.orderDetailList && orderState.courseOrder.orderDetailList.length < 1) {
                navigation.navigate(ScreenIds.Course)
            } else {
                setOrders(orderState.courseOrder ? [orderState.courseOrder] : [])
                setOrderDetail(orderState.courseOrder)
                setIsLoaded(true)
            }
        }
    }

    const isFocused = useIsFocused();

    useEffect(async() => {
        if(isFocused) {
            await loadUserOrder();
        }
    }, [isFocused]);
    //#endregion

    //#region Refresh data
    const [isLoading, setIsLoading] = useState(false);
    const [isRefreshing, setIsRefreshing] = useState(false);

    const LoadingIndicator = props => (
        <View style={[props.style, styles.indicator]}>
            <Spinner size="small" />
        </View>
    );
    //#endregion
    
    //#region Action
    const handleCheckOut = () => {
        navigation.navigate(ScreenIds.OrderBillingScreen, {orderType: orderType})
    }
    //#endregion
    
    //#region Screen template
    const renderOrderHeader = () => (
        <React.Fragment>
            <View style={{marginVertical: 10}}>
                <Text appearance="hint">Before continue to proceed, please check your cart and order summary first</Text>
            </View>
        </React.Fragment>
    )
    
    const renderOrderFooter = () => {
        return (
            <React.Fragment>
                <View style={styles.cartTotalContainer}>
                    <View style={styles.cartTotalTitle}>
                        <Text category="h6">
                            {translate('order.cartSummary')}
                        </Text>
                    </View>
                    <Divider />
                    <View style={styles.summaryWrapper}>
                        <Text category="p1">
                            {translate('order.itemTotal')} :{' '}
                        </Text>
                        <Text category="p1">{orderDetail.totalPrice && formatMoney(orderDetail.totalPrice, orderDetail.currency)}</Text>
                    </View>

                    <View style={styles.summaryWrapper}>
                        <Text category="p1">
                            {translate('order.discount')} :{' '}
                        </Text>
                        <Text category="p1">{orderDetail.discount && formatMoney(orderDetail.discount, orderDetail.currency)}</Text>
                    </View>
                    <View style={styles.summaryWrapper}>
                        <Text category="p1">
                            {translate('order.shippingFee')} :{' '}
                        </Text>
                        <Text category="p1">{orderDetail.shippingFee && formatMoney(orderDetail.shippingFee, orderDetail.currency)}</Text>
                    </View>
                    <View style={styles.summaryWrapper}>
                        <Text category="p1">
                            {translate('order.extraFee')} :{' '}
                        </Text>
                        <Text category="p1">{orderDetail.extraFee && formatMoney(orderDetail.extraFee, orderDetail.currency)}</Text>
                    </View>
                    <Divider />
                    <View style={styles.totalWrapper}>
                        <Text category="h6">{translate('order.total')}</Text>
                        <Text category="h6">{orderDetail.totalCheckout && formatMoney(orderDetail.totalCheckout, orderDetail.currency)}</Text>
                    </View>
                </View>
            </React.Fragment>
        )
    }
    //#endregion

    //#region Item template
    const renderItem = (item, index) => (
        <OrderItem 
            key={index}
            orderItem={item} 
            onRemoveItem={handleRemoveItem}
            displayType={displayTypes.orderCart.value}
            orderType={orderType}
        />
    )

    const handleRemoveItem = async(orderDetailId) => {
        actions.showAppSpinner(true)

        let data = {
            id: orderDetailId
        }

        if(orderType === orderTypes.product.value) {
            const order = await removeItem(state.userInfo?.id, orderState.productOrder?.id, data);

            if(order && order.id) {
                await orderActions.reloadProductOrder(order.id);
    
                if(!order.orderDetailList || order.orderDetailList.length < 1) {
                    navigation.navigate(ScreenIds.Cart)
                }

                setOrders(order ? [order] : [])
                setOrderDetail(order)
                setIsLoaded(true)
            }
        } else {
            const order = await removeItem(state.userInfo?.id, orderState.courseOrder?.id, data);

            if(order && order.id) {
                await orderActions.reloadCourseOrder(order.id);

                if(!order.orderDetailList || order.orderDetailList.length < 1) {
                    navigation.navigate(ScreenIds.Course)
                }

                setOrders(order ? [order] : [])
                setOrderDetail(order)
                setIsLoaded(true)
            }
        }

        actions.showAppSpinner(false)
    }
    //#endregion

    return(
        isLoaded ?
        <BaseScreen title='Order Summary'>
            <View style={styles.container}>                
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{
                        paddingHorizontal: 20,
                        paddingBottom: 10,
                        marginBottom: 70
                    }}
                >   
                    {renderOrderHeader()} 
                    <View style={{marginBottom: 10}}>
                        {orders && orders.length > 0 &&
                            orders.map((item, idx) => (
                                renderItem(item, idx)
                            ))
                        }
                    </View>
                    {renderOrderFooter()}
                </ScrollView> 
            </View>
            <CartAction 
                type={cartActionTypes.order.value}
                actionTitle="To cart"
                label="Price"
                amount={5000000}
                iconName="shopping-cart-outline"
                onActionPress={handleCheckOut}
                actionEnabled
            />    
        </BaseScreen>
        :   null
    )
}

export default OrderScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 70
    },
    itemContainer: {
        flexDirection: 'row',
    },
    listItem: {
        flexDirection: 'row',
        height: 60,
        backgroundColor: 'white',
        borderRadius: 10,
        alignItems: 'center',
        marginTop: 5,
        justifyContent: 'space-between',
        paddingHorizontal: 10,
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /******** Header ******/ 
    headerContainer: {
        paddingVertical: 10,
    },
    /******** Cart Summary **********/
    cartTotalContainer: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 5,
        justifyContent: 'space-between',
        marginBottom: 50,
        width: '100%',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        padding: 10
    },
    cartTotalTitle: {
        paddingVertical: 10,
    },
    summaryWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 5,
    },
    totalWrapper: {
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row', 
        paddingTop: 10
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
})
