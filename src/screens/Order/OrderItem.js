import {Icon as KittenIcon, Text} from '@ui-kitten/components'
import React, {useCallback} from 'react'
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native'
import { Badge } from 'react-native-elements'
import FastImage from 'react-native-fast-image'
import Animated, {
    useSharedValue,
    useAnimatedStyle,
    withSpring,
    useAnimatedRef,
    runOnUI,
    measure,
    FadeInDown,
    SlideOutRight,
    FadeIn,
} from 'react-native-reanimated'
import { checkoutStates, numberWithCommas, orderStatuses, orderTypes } from '../../assets/constants'
import { COLORS } from '../../assets/theme/colors'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { TouchableWithoutFeedback } from '@ui-kitten/components/devsupport'
import moment from 'moment'
import { formatMoney } from '../../component/money/MoneyUtils'
import { getCoursePriceTypeName } from '../Courses/CourseUtils'

const LIST_ITEM_HEIGHT = 100

export const displayTypes = {
    orderCart: {label: 'Order Cart', value: 'orderCart'},
    orderHistory: {label: 'Order History', value: 'orderHistory'},
}


// TODO TRANSLATION + Badge Status
const OrderItem = ({
    orderItem: {orderNumber, dateFmt, type, status, cartCheckoutState, totalCheckout, currency, orderDetailList},
    displayType,
    onRemoveItem,
    orderType,
    onActionPress
}) => {
    //#region Animations...
    const rotation = useSharedValue(0)
    const height = useSharedValue(0)
    const aRef = useAnimatedRef()

    const iconAnimatedStyle = useAnimatedStyle(() => ({
        transform: [{rotate: `${rotation.value}deg`}],
    }))

    const listAnimatedStyle = useAnimatedStyle(() => ({
        height: 1 + height.value,
        opacity: height.value === 0 ? 0 : 1,
        marginBottom: height.value > 0 ? 5 : 0,
    }))

    const handleItemPress = useCallback(() => {
        if (height.value === 0 && rotation.value < 180) {
            runOnUI(() => {
                'worklet'
                height.value = withSpring(measure(aRef).height, {
                    damping: 15,
                    stiffness: 150,
                })
            })()
            rotation.value = withSpring(180, {
                damping: 20,
                stiffness: 250,
            })
        } else if (height.value > 0 && rotation.value > 0) {
            runOnUI(() => {
                'worklet'
                height.value = withSpring(0, {
                    damping: 13,
                    stiffness: 130,
                    overshootClamping: true,
                })
            })()
            rotation.value = withSpring(0, {
                damping: 20,
                stiffness: 250,
            })
        }
    }, [])
    //#endregion

    return (
        <View style={displayType === displayTypes.orderHistory.value && styles.orderHistoryWrapper}
        >
            <TouchableWithoutFeedback  onPress={() => displayType === displayTypes.orderHistory.value ? onActionPress() : handleItemPress()}>
                <View style={[styles.container, displayType !== displayTypes.orderHistory.value && styles.cardShadow]}>
                    {displayType === displayTypes.orderCart.value && (
                        <React.Fragment>
                            <View>
                                <View style={styles.cardHeader}>
                                    <View style={styles.cardIcon}>
                                        <Icon name="receipt-long" color={COLORS.SUB} size={24}/>
                                    </View>
                                    <View style={styles.cardTitle}>
                                        <Text category="h6">Your Cart</Text>
                                    </View>
                                </View>
                                {orderType ?
                                    orderType === orderTypes.product.value 
                                        ?   <Text category="label" appearance="hint">Product items</Text>
                                        :   orderType === orderTypes.course.value
                                        &&  <Text category="label" appearance="hint">Course items</Text>
                                    :   null
                                }
                            </View>
                            <View style={styles.detailsContainer}>
                            <TouchableOpacity onPress={handleItemPress}>
                                <Animated.View
                                    style={[iconAnimatedStyle, {marginEnd: 10}]}>
                                    <View
                                        style={{
                                            backgroundColor: `rgb(251,236,234)`,
                                            borderRadius: 50,
                                        }}>
                                        <Icon
                                            name="arrow-drop-down"
                                            color="#eaa196"
                                            size={26}
                                        />
                                    </View>
                                </Animated.View>
                            </TouchableOpacity>
                        </View>
                     </React.Fragment>
                    )}

                    {displayType === displayTypes.orderHistory.value && (
                        <View style={{
                            flexDirection: 'column', 
                            width: '100%', 
                            paddingHorizontal: 5
                        }}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <View>
                                    <Text category="p2" style={styles.bold}>
                                        {orderNumber ? `#${orderNumber}` : ""}
                                    </Text>
                                </View>
                                <View>
                                    <Text appearance="hint" category="label">{dateFmt ? moment(dateFmt).format("yyyy-MM-DD") : ""}</Text>
                                </View>
                            </View>

                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View>
                                    <Text  style={{alignSelf: 'center'}} category="label" appearance="hint">
                                        {/* TODO TRANSLATION "label" in orderTypes Object */}
                                        Type : {type ? type === orderTypes.product.value ? orderTypes.product.label : orderTypes.course.label : ""}
                                    </Text>
                                </View>
                                <View>
                                    <Text  style={{alignSelf: 'center'}} category="label" appearance="hint">
                                        Total : {totalCheckout ? formatMoney(totalCheckout, currency) : ""}
                                    </Text>
                                </View>
                            </View>
                            
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text  style={{alignSelf: 'center'}} category="label" appearance="hint">
                                            Status : 
                                        </Text>
                                        <Text style={{alignSelf: 'center'}}>
                                            <Text>{`  `}</Text>
                                            <Badge 
                                                badgeStyle={
                                                    status ?
                                                    status === orderStatuses.NEW.value ? styles.newStatusBg
                                                :   status === orderStatuses.CONFIRMED.value ? styles.confirmStatusBg
                                                :   status === orderStatuses.PREPARED.value ? styles.prepareStatusBg
                                                :   status === orderStatuses.DONE.value ? styles.doneStatusBg
                                                :   status === orderStatuses.CANCELLED.value && styles.cancelledStatusBg
                                                :   styles.newStatusBg
                                                }
                                            />
                                            <Text>{` `}</Text>
                                            <Text category="label" appearance="hint"> 
                                            {/* TODO TRANSLATION "label" in orderStatuses Object */}
                                            {   status ?
                                                    status === orderStatuses.NEW.value ? orderStatuses.NEW.label
                                                :   status === orderStatuses.CONFIRMED.value ? orderStatuses.CONFIRMED.label
                                                :   status === orderStatuses.PREPARED.value ? orderStatuses.PREPARED.label
                                                :   status === orderStatuses.DONE.value ? orderStatuses.DONE.label
                                                :   status === orderStatuses.CANCELLED.value && orderStatuses.CANCELLED.label
                                                :   ""
                                            }
                                            </Text>
                                        </Text>
                                    </View>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                    <Text  style={{alignSelf: 'center'}} category="label" appearance="hint">
                                        Payment : 
                                    </Text>
                                    <Text style={{alignSelf: 'center'}}>
                                        <Text>{`  `}</Text>
                                        <Badge 
                                            status={
                                                cartCheckoutState && cartCheckoutState === checkoutStates.completed.value 
                                            ?   "success"
                                            :   "primary"
                                            }
                                        />
                                        <Text>{` `}</Text>
                                        <Text category="label" appearance="hint"> 
                                        {/* TODO TRANSLATION "label" in checkoutState Object */}
                                        {   cartCheckoutState ?
                                                cartCheckoutState === checkoutStates.pending.value ? checkoutStates.pending.label
                                            :   cartCheckoutState === checkoutStates.completed.value && checkoutStates.completed.label
                                            :   ""
                                        }
                                        </Text>
                                    </Text>
                                </View>    
                            </View>
                            
                        </View>
                    )}
                </View>
            </TouchableWithoutFeedback>
            <Animated.View
                style={[listAnimatedStyle, styles.listAnimatedContainer]}>
                <View ref={aRef} style={styles.listContainer}>
                    {orderDetailList.map((cartItem, idx) => {
                        return (
                            <Animated.View
                                key={idx}
                                style={styles.cartItem}
                                // exiting={() => SlideOutRight()}
                                >
                                <View style={styles.imageContainer}>
                                    <FastImage
                                        style={styles.image}
                                        source={{uri: cartItem.images && cartItem.images[0] && cartItem.images[0].image ? cartItem.images[0].image.imageUrl : ""}}
                                    />
                                    <View style={[styles.titleContainer]}>
                                        <View>
                                            <Text category="p2" style={styles.title}>
                                                {cartItem.name}
                                            </Text>
                                            <Text category="label">
                                                {cartItem.unitPrice ? numberWithCommas(cartItem.unitPrice) : 0} x{' '}
                                                {cartItem.quantity}
                                            </Text>
                                            {cartItem.coursePriceType && <Text  category="label">{getCoursePriceTypeName(cartItem.coursePriceType)}</Text>}
                                            {(cartItem.colorName || cartItem.sizeName) 
                                            ?
                                                (cartItem.colorName && cartItem.sizeName) 
                                                ?
                                                    <Text category="label">{cartItem.colorName - cartItem.sizeName}</Text>
                                                :   cartItem.colorName ?
                                                    <Text category="label">{cartItem.colorName}</Text>
                                                :   <Text category="label">{cartItem.sizeName}</Text>
                                            :   null
                                            }
                                        </View>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        position: 'absolute',
                                        top: "40%",
                                        right: 20,
                                    }}>
                                    <TouchableOpacity
                                        style={styles.delete}
                                        onPress={() => onRemoveItem(cartItem.id)}>
                                        <Icon
                                            name="close"
                                            fill={COLORS.BLACK}
                                            style={{height: 14, width: 14, justifyContent: "center", alignSelf: "center", marginLeft: 2}}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </Animated.View>
                        )
                    })}
                </View>
            </Animated.View>
        </View>
    )
}

export default OrderItem

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: displayTypes.orderHistory.value ? LIST_ITEM_HEIGHT : 60,
        backgroundColor: 'white',
        borderRadius: 10,
        alignItems: 'center',
        marginTop: 5,
        justifyContent: 'space-between'
    },
    cardShadow: {
        paddingHorizontal: 10,
        width: '100%',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    detailsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    bold: {
        fontWeight: '700',
    },
    totalAmount: {
        marginEnd: 20,
        fontSize: 20,
        color: `black`,
    },
    date: {
        fontSize: 16,
        color: `black`,
    },
    listContainer: {
        backgroundColor: 'rgba(255,255,255, 0.7)',
        borderRadius: 10,
        paddingTop: 20,
    },
    listAnimatedContainer: {
        overflow: 'hidden',
        marginTop: 5,
    },
    cartItem: {
        flexDirection: 'row',
        height: 80,
        justifyContent: 'space-between',
        marginBottom: 20,
        backgroundColor: COLORS.SUB_RGB_02,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 8
    },
    imageContainer: {
        flexDirection: 'row',
    },
    image: {
        height: '100%',
        width: 70,
        borderRadius: 8,
    },
    titleContainer: {
        marginStart: 10,
        justifyContent: 'center',
        width: '100%',
    },
    title: {
        marginBottom: 5,
    },
    quantity: {
        fontSize: 16,
        color: 'black',
    },
    total: {
        fontSize: 18,
        color: 'black',
        alignSelf: 'center',
        marginEnd: 10,
    },
    delete: {
        height: 22,
        width: 22,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: `rgba(0, 0, 41, 0.15)`,
        flexDirection: "column",
        borderRadius: 50
    },
    newStatusBg: {
        backgroundColor: "rgba(51, 102, 255, 0.9)"
    },
    confirmStatusBg: {
        backgroundColor: "rgb(51, 102, 255)"
    },
    prepareStatusBg: {
        backgroundColor: COLORS.SUB
    },
    doneStatusBg: {
        backgroundColor: "#76C62F"
    },
    cancelledStatusBg: {
        backgroundColor: "#DB646A"
    },
    orderHistoryWrapper: {
        backgroundColor:"#FFF",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        
        elevation: 3,
        marginBottom: 15,
        paddingHorizontal: 10
    },
    cardHeader: {
        flexDirection: 'row'
    },
    cardIcon: {
        alignSelf: 'center', marginRight: 10
    },
})
