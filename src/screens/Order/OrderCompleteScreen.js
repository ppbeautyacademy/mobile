import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import BaseScreen from '../../component/BaseScreen';
import { ScreenIds } from '../../ScreenIds';
import { Button, Divider, Text } from '@ui-kitten/components';
import OrderContext from '../../../context/OrderContext';
import { checkoutStates, CHECKOUT_TYPE, HTTP_NO_CONTENT, orderStatuses, orderTypes } from '../../assets/constants';
import { loadUserCompleteOrder } from '../../services/ServiceWorker';
import context from '../../../context/context';
import moment from 'moment';
import { COLORS } from '../../assets/theme/colors';
import { commonStyles } from '../../assets/theme/styles';

// TODO Translation
const OrderCompleteScreen = ({navigation, route}) => {
    const {orderType} = route.params;

    const {state, actions} = useContext(context)
    const {orderState, orderActions} = useContext(OrderContext);

    //#region LOAD ORDER
    const [order, setOrder] = useState({});
    const [isLoaded, setIsLoaded] = useState(false)

    const getUserCompleteOrder = async() => {
        const orderId = orderType === orderTypes.product.value ? orderState.productOrder?.id : orderState.courseOrder?.id;

        const orderComplete = await loadUserCompleteOrder(state.userInfo?.id, state.version, orderId);
        if(orderComplete && orderComplete.id) {
            setOrder(orderComplete);
            setIsLoaded(true);
        } else {
            if(orderComplete === HTTP_NO_CONTENT) {
                navigation.navigate(ScreenIds.Home)
            }
        }
        
    }

    useEffect(async() => {
        await getUserCompleteOrder(); 
    }, [])
    //#endregion

    //#region REDIRECT
    const handleGoToHome = async() => {
        if(orderType === orderTypes.product.value) {
            await orderActions.clearProductOrder();
        } else {
            await orderActions.clearCourseOrder();
        }

        navigation.navigate(ScreenIds.Home)
    }

    const handleViewDetail = async() => {

    }
    //#endregion

    return(
        isLoaded ?
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View styles={styles.container}>
                <FastImage 
                    source={require('../../assets/images/complete-payment.png')}
                    style={styles.image}
                />

                <View style={styles.contentWrapper}>
                    <Text style={{marginVertical: 10}}>
                        Thank you for your order
                    </Text>

                    <Text category="p1" appearance="hint" style={{textAlign: 'center'}}>
                        We've recieved your order and an email will be sent to you shortly as a confirmation
                    </Text>
                </View>

                <Divider style={styles.divider}/>

                <View style={styles.summaryWrapper}>
                    <View><Text category={"p2"}>Order Number</Text></View>
                    <View><Text category={"p2"}>{order.orderNumber ? `#${order.orderNumber}` : ""}</Text></View>
                </View>

                <View style={styles.summaryWrapper}>
                    <View><Text category={"p2"}>Created Date</Text></View>
                    <View><Text category={"p2"}>{order.dateFmt ? moment(order.dateFmt).format("yyyy-MM-DD HH:mm") : ""}</Text></View>
                </View>
                
                <View style={styles.summaryWrapper}>
                    <View><Text category={"p2"}>Status</Text></View>
                    <View>
                        <Text category={"p2"}>
                            {   order ?   
                                    order.status === orderStatuses.NEW.value ? "Đơn hàng mới"
                                :   order.status === orderStatuses.CONFIRMED.value  ? "Đơn hàng đã được xác nhận"
                                :   order.status === orderStatuses.PREPARED.value  ? "Đơn hàng đang được giao"
                                :   order.status === orderStatuses.DONE.value ? "Đơn hàng đã được hoàn tất"
                                :   order.status === orderStatuses.CANCELLED.value && "Đơn hàng đã bị hủy"
                                :   ""
                            }
                        </Text>
                    </View>
                </View>

                <View style={styles.summaryWrapper}>
                    <View><Text category={"p2"}>Payment Method</Text></View>
                    <View>
                        <Text category={"p2"}>
                            {   order ?
                                    order.paymentType === CHECKOUT_TYPE.PICKUP.value ? "Thanh toán tại cửa hàng"
                                :   order.paymentType === CHECKOUT_TYPE.COD.value ? "Thanh toán khi nhận hàng"
                                :   order.paymentType === CHECKOUT_TYPE.CREDITCARD.value && "Thanh toán qua ngân hàng"
                                :   ""                                
                            }
                        </Text>
                    </View>
                </View>

                <View style={[styles.summaryWrapper, {marginBottom: 20}]}>
                    <View><Text category={"p2"}>Payment Status</Text></View>
                    <View>
                        <Text category={"p2"}>
                            {   order ?
                                    order.cartCheckoutState === checkoutStates.pending.value ? "Chưa thanh toán"
                                :   order.cartCheckoutState === checkoutStates.completed.value && "Đã thanh toán"
                                :   ""                                
                            }
                        </Text>
                    </View>
                </View>

                <Divider/>

                <View style={styles.buttonWrapper}>
                    <View style={styles.summaryWrapper}>
                        <Button size={"small"} style={styles.buttonWidth} appearance={"outline"} onPress={() => handleGoToHome()}>
                            {props => (
                                <Text
                                    {...props}
                                    category="p1"
                                    style={[
                                        commonStyles.mainFont,
                                        {color: COLORS.SUB},
                                    ]}>
                                    Go to Home
                                </Text>
                            )}
                        </Button>
                        <Button size={"small"} style={styles.buttonWidth} onPress={() => handleViewDetail()}>
                            {props => (
                                <Text
                                    {...props}
                                    category="p1"
                                    style={[
                                        commonStyles.mainFont,
                                        commonStyles.lightColor,
                                    ]}>
                                    View Detail
                                </Text>
                            )}
                        </Button>
                    </View>
                </View>
            </View>
        </BaseScreen>
        :   null
    )
}

export default OrderCompleteScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        paddingHorizontal: 20,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 30,
        backgroundColor: COLORS.WHITE
    },
    divider: {
        marginTop: 30,
        marginBottom: 20
    },
    /********* Image ********/
    image: {
        height: 250,
        marginTop: 50
    },
    /******** Content **********/
    contentWrapper: {
        flexDirection: 'column',
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    /******** Summary *********/
    summaryWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 20,
    },
    /******** Button ***********/ 
    buttonWrapper: {
        marginTop: 30
    },
    buttonWidth: {
        width: "48%"
    }
})