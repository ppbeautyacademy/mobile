import { Button, Divider, List, Text, Icon as KittenIcon } from '@ui-kitten/components';
import React, { useContext, useEffect, useState } from 'react';
import { Alert, StyleSheet, View } from 'react-native';
import { checkoutStates, CHECKOUT_TYPE, getMoneyFormat, HTTP_STATUS_OK, messageSubjectTypes, messageTypes, numberWithCommas, orderStatuses, orderTypes } from '../../assets/constants';
import { COLORS } from '../../assets/theme/colors';
import BaseScreen from '../../component/BaseScreen';
import { ScreenIds } from '../../ScreenIds';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { ScrollView } from 'react-native-gesture-handler';
import moment from 'moment';
import FastImage from 'react-native-fast-image';
import { priceTypes } from '../Courses/CourseDetail';
import { commonStyles } from '../../assets/theme/styles';
import { EnterUsernameModal } from '../Account/ContactScreen';
import context from '../../../context/context';
import { cancelOrder } from '../../services/ServiceWorker';
import { Toast } from 'popup-ui';
import { translate } from '../../../config/i18n';

// TODO Translation
const OrderHistoryDetail = ({route, navigation}) => { 
    const {state, actions} = useContext(context);
    const {order} = route.params;

    useEffect(() => {
        if(!order) {
            navigation.navigate(ScreenIds.OrderHistoryScreen)
        }
    }, [])

    const renderItem = (item, idx) => {
        return(
            <View style={styles.orderItemContainer} key={idx}>
                <View style={{flexDirection: 'row', marginBottom: 10}}>
                    <FastImage 
                        source={{uri: item.mainImage?.imageUrl}}
                        style={{
                            width: 70,
                            height: 70,
                            borderRadius: 10,
                            marginRight: 10
                        }}
                    />
                    <View style={{flexDirection: "column"}}>
                        <Text category={"p1"} numberOfLines={1} style={item.status === orderStatuses.CANCELLED.value ? styles.lineThrough : {}}>{item.name}</Text>
                        <Text category={"p2"} appearance={"hint"} style={item.status === orderStatuses.CANCELLED.value ? styles.lineThrough : {}}>
                            {item.unitPrice ? numberWithCommas(item.unitPrice) : 0} x {item.quantity}
                        </Text>
                        <Text category={"p2"} appearance={"hint"} style={item.status === orderStatuses.CANCELLED.value ? styles.lineThrough : {}}>
                            {   order.type === orderTypes.product.value 
                            ?
                                (item.colorName && item.sizeName) 
                                ?  `${item.colorName - item.sizeName}`
                                :   item.colorName 
                                ?   `${item.colorName}`
                                :   item.sizeName
                                &&  `${item.sizeName}`
                            :
                                order.type === orderTypes.course.value 
                            ?
                                item.coursePriceType 
                                ?   (item.coursePriceType === priceTypes.generic.value || item.coursePriceType === priceTypes.onsite.value)
                                    ?   priceTypes.onsite.label
                                    :   priceTypes.online.label
                                :   ""
                            :   ""
                            }
                        </Text>
                    </View>
                </View>  
                <Divider style={styles.dividerSpacing}/>              
            </View>
        )
    }

    const handleConfirmCancelOrder = () => {
        actions.showAppPopup({
            isVisible: true,
            message: "Are you sure to cancel this order ?",
            okText: "Yes",
            cancelText: "No",
            onOkHandler: () => handleCancelOrder()
        })
    }

    const handleCancelOrder = async() => {
        actions.showWhiteAppSpinner(true);
        const cancelStatus = await cancelOrder(state.userInfo?.id, state.version, order.id).finally(() => actions.showWhiteAppSpinner(false));
        if(cancelStatus === HTTP_STATUS_OK) {
            navigation.navigate(ScreenIds.OrderHistoryScreen, {
                redirectToIndex: 4
            })
        } else {
            Toast.show({
                title: translate('common.errorTitle'),
                text: 'Lỗi hệ thống. Vui lòng thử lại sau',
                color: COLORS.ERROR_NOTIFY,
                timing: 3500,
            })
        }
    }

    const handleLeaveReview = () => {

    }

    //#region Support Form
    const [showEnterNameModal, setShowEnterNameModal] = useState(false);

    const showEnterNamePopup = () => {
        setShowEnterNameModal(true)
    }
    const hideEnterNamePopup = () => {
        setShowEnterNameModal(false)
    }
    //#endregion

    //#region Support Form
    const [showReviewModal, setShowReviewModal] = useState(false);

    const showReviewPopup = () => {
        setShowReviewModal(true)
    }
    const hideReviewPopup = () => {
        setShowReviewModal(false)
    }
    //#endregion

    //#region Icon
    const renderSupportIcon = props => (
        <KittenIcon {...props} name="paper-plane-outline" />
    )

    const renderCancelIcon = props => (
        <KittenIcon {...props} name="close-outline" />
    )

    //#endregion

    return(
        <BaseScreen title='Order Detail'>
            <EnterUsernameModal 
                visible={showEnterNameModal}
                onPressOutside={hideEnterNamePopup}
                subject={`${messageSubjectTypes.orderSupport} #${order.orderNumber}`}
                type={messageTypes.ticket}
            />
            <View style={(order.status === orderStatuses.NEW.value) ? styles.containerWithPadding : styles.container}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{
                        paddingBottom: 30,
                        backgroundColor: COLORS.LIGHT
                    }}
                >
                    <View 
                        style={[styles.statusWrapper,
                            order ?   
                                order.status === orderStatuses.NEW.value ? styles.newStatusBg
                            :   order.status === orderStatuses.CONFIRMED.value  ? styles.confirmStatusBg
                            :   order.status === orderStatuses.PREPARED.value  ? styles.prepareStatusBg
                            :   order.status === orderStatuses.DONE.value ? styles.doneStatusBg
                            :   order.status === orderStatuses.CANCELLED.value && styles.cancelledStatusBg
                            :   {}
                        ]}
                    >   
                        <View style={styles.statusContainer}>
                            <View style={{width: '80%'}}>
                                <Text style={{color: COLORS.WHITE}}>
                                    {   order ?   
                                            order.status === orderStatuses.NEW.value ? "Đơn hàng mới"
                                        :   order.status === orderStatuses.CONFIRMED.value  ? "Đơn hàng đã được xác nhận"
                                        :   order.status === orderStatuses.PREPARED.value  ? "Đơn hàng đang được giao"
                                        :   order.status === orderStatuses.DONE.value ? "Đơn hàng đã được hoàn tất"
                                        :   order.status === orderStatuses.CANCELLED.value && "Đơn hàng đã bị hủy"
                                        :   ""
                                    }
                                </Text>
                                <Text style={{color: COLORS.WHITE}} category={"label"} appearance="hint">
                                    {   order ?   
                                            order.status === orderStatuses.NEW.value ? "Đơn hàng của bạn đã được khởi tạo thành công và đang chờ được xác nhận"
                                        :   order.status === orderStatuses.CONFIRMED.value  ? "Đơn hàng của bạn đã được xác nhận. Chúng tôi sẽ sớm hoàn tất đơn hàng của bạn"
                                        :   order.status === orderStatuses.PREPARED.value  ? "Đơn hàng sẽ được giao sớm đến bạn. Vui lòng chờ nhận hàng"
                                        :   order.status === orderStatuses.DONE.value ? "Bạn có thể chia sẻ nhận xét và đánh giá của bạn nhằm nâng cao chất lượng dịch vụ của chúng tôi"
                                        :   order.status === orderStatuses.CANCELLED.value && "Chúng tôi rất lấy làm tiếc khi đơn hàng của bạn bị hủy"
                                        :   ""
                                    }
                                </Text>
                            </View>
                            <View style={{justifyContent: 'center', alignItems: "center", marginLeft: 15}}>
                                <Icon 
                                    name= {   
                                        order ?   
                                        order.status === orderStatuses.NEW.value ? "description"
                                    :   order.status === orderStatuses.CONFIRMED.value  ? "fact-check"
                                    :   order.status === orderStatuses.PREPARED.value  ? "local-shipping"
                                    :   order.status === orderStatuses.DONE.value ? "check-circle"
                                    :   order.status === orderStatuses.CANCELLED.value && "cancel"
                                    :   "settings"
                                    }
                                    color={COLORS.WHITE}
                                    size={32}
                                />
                            </View>
                        </View>
                    </View>
                    
                    {/* Order Basic Info */}
                    <View style={styles.sectionWrapper}>
                        <View style={styles.sectionContainer}>
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Order No</Text>
                                <Text  category={"p2"}>{order.orderNumber ? `#${order.orderNumber}` : ""}</Text>
                            </View>
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Created Date</Text>
                                <Text category={"p2"}>{order.dateFmt ? moment(order.dateFmt).format("yyyy-MM-DD HH:mm") : ""}</Text>
                            </View>
                        </View>
                    </View>
                    
                    {/* Order Billing Info */}
                    <View style={styles.sectionWrapper}>
                        <View style={styles.sectionContainer}>
                            <View style={{flexDirection: 'row'}}>
                                <Icon 
                                    name="info"
                                    color={COLORS.SUB}
                                    size={24}
                                    style={{marginRight: 10}}
                                />
                                <Text>Billing Information</Text>
                            </View>
                            <Divider style={styles.dividerSpacing}/>
                            <View style={styles.billingItem}>
                                <Icon 
                                    name="person"
                                    color={COLORS.BLACK_HALF_OPACITY}
                                    size={20}
                                    style={{marginRight: 15}}
                                />
                                <Text appearance={"hint"} category={"p2"}>{order.billingAddress && order.billingAddress.fullname}</Text>
                            </View>  
                            <View style={styles.billingItem}>
                                <Icon 
                                    name="mail"
                                    color={COLORS.BLACK_HALF_OPACITY}
                                    size={19}
                                    style={{marginRight: 15}}
                                />
                                <Text appearance={"hint"} category={"p2"}>{order.billingAddress && order.billingAddress.email}</Text>
                            </View>      
                            <View style={styles.billingItem}>
                                <Icon 
                                    name="phone"
                                    color={COLORS.BLACK_HALF_OPACITY}
                                    size={20}
                                    style={{marginRight: 15}}
                                />
                                <Text appearance={"hint"} category={"p2"}>{order.billingAddress && order.billingAddress.phone}</Text>
                            </View>   
                            {order.billingAddress && order.billingAddress.address ?
                                (<View style={styles.billingItem}>
                                    <Icon 
                                        name="home"
                                        color={COLORS.BLACK_HALF_OPACITY}
                                        size={20}
                                        style={{marginRight: 15}}
                                    />
                                    <Text style={{width: "85%"}} appearance={"hint"} category={"p2"}>{order.billingAddress.address}</Text>
                                </View>) :  null
                            }   
                        </View>
                    </View>
                    
                    {/* Order Payment Info */}
                    <View style={styles.sectionWrapper}>
                        <View style={styles.sectionContainer}>
                            <View style={{flexDirection: 'row'}}>
                                <Icon 
                                    name="payment"
                                    color={COLORS.SUB}
                                    size={24}
                                    style={{marginRight: 10}}
                                />
                                <Text>Payment</Text>
                            </View>
                            <Divider style={styles.dividerSpacing}/>
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Payment Method</Text>
                                <Text category={"p2"}>
                                    {   order ?
                                            order.paymentType === CHECKOUT_TYPE.PICKUP.value ? "Thanh toán tại cửa hàng"
                                        :   order.paymentType === CHECKOUT_TYPE.COD.value ? "Thanh toán khi nhận hàng"
                                        :   order.paymentType === CHECKOUT_TYPE.CREDITCARD.value && "Thanh toán qua ngân hàng"
                                        :   ""                                
                                    }
                                </Text>
                            </View>      
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Payment Status</Text>
                                <Text category={"p2"}>
                                    {   order ?
                                            order.cartCheckoutState === checkoutStates.pending.value ? "Chưa thanh toán"
                                        :   order.cartCheckoutState === checkoutStates.completed.value && "Đã thanh toán"
                                        :   ""                                
                                    }
                                </Text>
                            </View>       
                        </View>
                    </View>
                    
                    {/* Cart Info */}
                    <View style={styles.sectionWrapper}>
                        <View style={styles.sectionContainer}>
                            <View style={{flexDirection: 'row'}}>
                                <Icon 
                                    name="receipt"
                                    color={COLORS.SUB}
                                    size={24}
                                    style={{marginRight: 10}}
                                />
                                <Text>Cart Summary</Text>
                            </View>
                            <Divider style={styles.dividerSpacing}/>
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Items total</Text>
                                <Text category={"label"}>{order && order.totalPrice ? getMoneyFormat(order.totalPrice, order.currency) : "0"}</Text>
                            </View>     
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Discount</Text>
                                <Text category={"label"}>{order && order.discount ? getMoneyFormat(order.discount, order.currency) : "0"}</Text>
                            </View>  
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Shipping Fee</Text>
                                <Text category={"label"}>{order && order.shippingFee ? getMoneyFormat(order.shippingFee, order.currency) : "0"}</Text>
                            </View>  
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"label"}>Extra Fee</Text>
                                <Text category={"label"}>{order && order.extraFee ? getMoneyFormat(order.extraFee, order.currency) : "0"}</Text>
                            </View>  
                            <Divider style={{marginVertical: 10}}/>
                            <View style={styles.sectionHeader}>
                                <Text appearance={"hint"} category={"p2"}>Cart Total</Text>
                                <Text category={"p1"}>{order && order.totalCheckout ? getMoneyFormat(order.totalCheckout, order.currency) : ""}</Text>
                            </View>       
                        </View>
                    </View>
                    
                    {/* Order Items Info */}
                    <View style={styles.sectionWrapper}>
                        <View style={styles.sectionContainer}>
                            <View style={{flexDirection: 'row'}}>
                                <Icon 
                                    name="format-list-bulleted"
                                    color={COLORS.SUB}
                                    size={24}
                                    style={{marginRight: 10}}
                                />
                                <Text>Item Listing</Text>
                            </View>
                            <Divider style={styles.dividerSpacing}/>
                            {order && order.orderDetailList && 
                                order.orderDetailList.map((item, idx) => (
                                    renderItem(item, idx)
                                ))
                            }
                            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                                <Text>
                                    {order && order.orderDetailList &&
                                        `${order.orderDetailList.length} item(s) in total` 
                                    }
                                </Text>
                            </View>       
                        </View>
                    </View>

                    {/* Order Support*/}
                    <View style={styles.sectionWrapper}>
                        <View style={styles.sectionContainer}>
                            <View style={{flexDirection: 'row'}}>
                                <Icon 
                                    name="support-agent"
                                    color={COLORS.SUB}
                                    size={24}
                                    style={{marginRight: 10}}
                                />
                                <Text>Support</Text>
                            </View>
                            <Divider style={styles.dividerSpacing}/>
                            <View style={{marginVertical: 10}}>
                                <Button
                                    accessoryLeft={renderSupportIcon}
                                    onPress={() => showEnterNamePopup()}
                                >
                                {props => (
                                    <Text
                                        {...props}
                                        category="p2"
                                        style={[
                                            commonStyles.mainFont,
                                            commonStyles.lightColor,
                                        ]}>
                                        CONTACT SUPPORT
                                    </Text>
                                )}    
                                </Button>  
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>

            {   (order.status === orderStatuses.NEW.value) &&
                <View style={styles.actionWrapper}>
                    {/* To cancel order */}
                    {order.status === orderStatuses.NEW.value &&
                        <View style={styles.actionContainer}>
                            <Button
                                accessoryLeft={renderCancelIcon}
                                style={{backgroundColor: COLORS.ERROR_NOTIFY}}
                                onPress={() => handleConfirmCancelOrder()}
                            >
                            {props => (
                                <Text
                                    {...props}
                                    category="p2"
                                    style={[
                                        commonStyles.mainFont,
                                        commonStyles.lightColor,
                                    ]}>
                                    CANCEL ORDER
                                </Text>
                            )}    
                            </Button>  
                        </View>
                    }

                    {/* To leave a review */}
                    {/* TODO Next Version */}
                    {/* {order.status === orderStatuses.DONE.value &&
                        <View style={styles.actionContainer}>
                            <Button
                                accessoryLeft={renderCancelIcon}
                                status={"success"}
                                onPress={() => handleLeaveReview()}
                            >
                            {props => (
                                <Text
                                    {...props}
                                    category="p2"
                                    style={[
                                        commonStyles.mainFont,
                                        commonStyles.lightColor,
                                    ]}>
                                    CANCEL ORDER
                                </Text>
                            )}    
                            </Button>  
                        </View>
                    }                     */}
                </View>
            }
        </BaseScreen>
    )
}

export default OrderHistoryDetail;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.LIGHT
    },
    containerWithPadding: {
        flex: 1,
        backgroundColor: COLORS.LIGHT,
        marginBottom: 60
    },
    statusWrapper: {
        height: 100,
        marginBottom: 10
    },
    statusContainer: {
        flexDirection: "row",
        padding: 16
    },
    newStatusBg: {
        backgroundColor: "rgba(51, 102, 255, 0.9)"
    },
    confirmStatusBg: {
        backgroundColor: "rgb(51, 102, 255)"
    },
    prepareStatusBg: {
        backgroundColor: COLORS.SUB
    },
    doneStatusBg: {
        backgroundColor: "#76C62F"
    },
    cancelledStatusBg: {
        backgroundColor: "#DB646A"
    },
    sectionWrapper: {
        backgroundColor: COLORS.WHITE,
        marginBottom: 10
    },
    sectionContainer: {
        padding: 16
    },
    sectionHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    billingItem: {
        flexDirection: 'row', 
        marginBottom: 2
    },
    orderItemContainer: {
        minHeight: 70,
        paddingBottom: 10,

    },
    dividerSpacing: {
        marginVertical: 5
    },
    actionWrapper: {
        paddingHorizontal: 16,
        width: '100%'
    },
    actionContainer: {
        position: 'absolute',
        bottom: 8,
        alignSelf: 'center',
        width: '100%'
    },
    lineThrough: {
        textDecorationLine: 'line-through'
    }
})