import { useIsFocused } from '@react-navigation/native';
import { BottomNavigation, BottomNavigationTab, Layout, List, Text, ViewPager} from '@ui-kitten/components';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { ScrollView} from 'react-native-gesture-handler';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'
import context from '../../../context/context';
import { COLORS } from '../../assets/theme/colors';
import BaseScreen from '../../component/BaseScreen';
import { ScreenIds } from '../../ScreenIds';
import { getUserPageOrders } from '../../services/ServiceWorker';
import OrderItem, { displayTypes } from './OrderItem';

// TODO translation Screen
const OrderHistoryScreen = ({route, navigation}) => {
    const {state, actions} = useContext(context);

    const scrollViewRef = useRef(null);

    const redirect = route.params && route.params.redirectToIndex ? route.params.redirectToIndex : null;

    const [isLoaded, setIsLoaded] = useState(false);
    const [selectedIndex, setSelectedIndex] = useState(0);
    const [itemHolders, setItemHolders] = useState([
      {}, {}, {}, {}, {}
    ]);

    const [orders, setOrders] = useState({
      newOrder: [],
      confirmOrder: [],
      prepareOrder: [],
      doneOrder: [],
      cancelledOrder: []
    })

    //#region Load Orders
    const isFocused = useIsFocused();

    useEffect(async() => {
      if(redirect) {
        handleSetSelectedIndex(redirect);
        await loadMyOrders();
      } else {
        handleSetSelectedIndex(0);
        if(!isLoaded && isFocused) {            
          await loadMyOrders();
        }
      }
    }, [isFocused]);

    const loadMyOrders = async() => {
      const orderResponse = await getUserPageOrders(state.userInfo?.id, state.version);
      if(orderResponse && Object.keys(orderResponse).length > 0) {
        setOrders({
          newOrder: orderResponse.newOrder ? orderResponse.newOrder : [],
          confirmOrder: orderResponse.confirmOrder ? orderResponse.confirmOrder : [],
          prepareOrder: orderResponse.prepareOrder ? orderResponse.prepareOrder : [],
          doneOrder: orderResponse.doneOrder ? orderResponse.doneOrder : [],
          cancelledOrder: orderResponse.cancelledOrder ? orderResponse.cancelledOrder : [],
        })
        setIsLoaded(true);
      }
    }
    //#endregion

    //#region Render Content
    const renderOrderItem = ({ item, index }) => {
      return(
        <OrderItem 
          key={index}
          orderItem={item}
          displayType={displayTypes.orderHistory.value}
          onActionPress={() => handleOnViewDetail(item)}
        />
      )
    }

    const renderEmpty = () => {
      return(
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: '30%'}}>
          <View>
            <FastImage 
              source={require("../../assets/images/empty.png")}
              style={{
                width: 220,
                height: 130,
                marginBottom: 10
              }}
            />
            <Text style={{textAlign: "center"}}>Currently no orders</Text>
          </View>
        </View>
      )
    }

    const handleOnViewDetail = (order) => {
      if(order) {
        navigation.navigate(ScreenIds.OrderHistoryDetailScreen, {
          order: order,
        })
      }
    }

    const handleSetSelectedIndex = (index) => {
      if(index === 3 || index === 4) {
        scrollViewRef.current?.scrollToEnd({ animated: true });
      } else {
        scrollViewRef.current?.scrollTo({ x: 1, y: 0, animated: true })
      }

      setSelectedIndex(index)
    }
    //#endregion

    return(
      <BaseScreen title="Order History">
        <View>
          <ScrollView
            ref={scrollViewRef}
            horizontal
            showsHorizontalScrollIndicator={false}
            stickyHeaderIndices={[0]}
            contentContainerStyle={{
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}
          >
              <BottomNavigation
                  selectedIndex={selectedIndex}
                  onSelect={index => handleSetSelectedIndex(index)}
                  style={{
                      flexDirection: 'row'
                  }}>
                  <BottomNavigationTab
                      style={{width: 100}}
                      title={(evaProps) => <Text {...evaProps}>Mới</Text>}
                  />
                  <BottomNavigationTab
                      style={{width: 100}}
                      title={(evaProps) => <Text {...evaProps}>Đã Xác Nhận</Text>}
                  />
                  <BottomNavigationTab
                      style={{width: 100}}
                      title={(evaProps) => <Text {...evaProps}>Đang Giao</Text>}
                  />
                  <BottomNavigationTab
                      style={{width: 100}}
                      title={(evaProps) => <Text {...evaProps}>Hoàn Tất</Text>}
                  />
                  <BottomNavigationTab
                      style={{width: 100}}
                      title={(evaProps) => <Text {...evaProps}>Đã Hủy</Text>}                      
                  />
              </BottomNavigation>
          </ScrollView>
        </View>
        {
          isLoaded ?
          <ViewPager
            style={styles.tab}
            selectedIndex={selectedIndex}
            onSelect={index => handleSetSelectedIndex(index)}>
          <Layout
            style={styles.tab}
            level='2'>
            {orders.newOrder && orders.newOrder.length > 0 ?
              <List
                style={styles.container}
                data={orders.newOrder}
                renderItem={renderOrderItem}
              />
            : 
              renderEmpty()
            }
          </Layout>
          <Layout
            style={styles.tab}
            level='2'>
            {orders.confirmOrder && orders.confirmOrder.length > 0 ?
              <List
                style={styles.container}
                data={orders.confirmOrder}
                renderItem={renderOrderItem}
              />
            : 
              renderEmpty()
            }
          </Layout>
          <Layout
            style={styles.tab}
            level='2'>
            {orders.prepareOrder && orders.prepareOrder.length > 0 ?
              <List
                style={styles.container}
                data={orders.prepareOrder}
                renderItem={renderOrderItem}
              />
            : 
              renderEmpty()
            }
          </Layout>
          <Layout
            style={styles.tab}
            level='2'>
            {orders.doneOrder && orders.doneOrder.length > 0 ?
              <List
                style={styles.container}
                data={orders.doneOrder}
                renderItem={renderOrderItem}
              />
            : 
              renderEmpty()
            }
          </Layout>
          <Layout
            style={styles.tab}
            level='2'>
            {orders.cancelledOrder && orders.cancelledOrder.length > 0 ?
              <List
                style={styles.container}
                data={orders.cancelledOrder}
                renderItem={renderOrderItem}
              />
            : 
              renderEmpty()
            }
          </Layout>
        </ViewPager>
        :
          <React.Fragment>
            <View style={styles.container}>
              {itemHolders.map((item, idx) => (
                <View style={{marginBottom: 10, marginTop: idx === 0 ? 10 : 5}} key={idx}>
                  <SkeletonPlaceholder>
                    <View style={styles.cardHolderWrapper}></View>
                  </SkeletonPlaceholder>
                </View>
              ))}
            </View>
          </React.Fragment>
        }
      </BaseScreen>
    )
}

export default OrderHistoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:10,
    backgroundColor: '#F7F9FC'
  },
  cardHolderWrapper: {
    backgroundColor: COLORS.WHITE,
    height: 100,
    width: '100%'
  },
  tab: {
    flex: 1
  },
  /******** Navigation ***********/
  navigationWrapper: {
    flexDirection: 'row', 
    paddingTop: 10
  },
  navigationContainer: {
      width: 30,
      height: 30,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
  },
  navigationBackground: {
      backgroundColor: '#FDF3EB',
  },
});
