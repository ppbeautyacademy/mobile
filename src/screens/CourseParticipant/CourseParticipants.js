import {Button, Icon, List, ListItem} from '@ui-kitten/components'
import React from 'react'
import {View} from 'react-native'
import {translate} from '../../../config/i18n'
import BaseScreen from '../../component/BaseScreen'

const CourseParticipants = ({members}) => {
    const renderItemAccessory = props => (
        <Button size="tiny">{translate('course.followCap')}</Button>
    )

    const renderItemIcon = props => <Icon {...props} name="person" />

    const renderItem = ({item, index}) => (
        <ListItem
            key={index}
            title={`${item.fullname}`}
            description={`${item.roleName ? item.roleName : 'Học viên'}`}
            accessoryLeft={renderItemIcon}
            // accessoryRight={renderItemAccessory}
        />
    )

    return (
        <View style={{flex: 1, paddingHorizontal: 20}}>
            <List data={members} renderItem={renderItem} />
        </View>
    )
}

export default CourseParticipants
