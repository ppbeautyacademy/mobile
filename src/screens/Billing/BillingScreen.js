import React, { useContext, useEffect, useRef, useState } from 'react';
import { Alert, KeyboardAvoidingView, Platform, StyleSheet, View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import BaseScreen from '../../component/BaseScreen';
import {Button, Divider, Input, Radio, Text, Icon as KittenIcon} from '@ui-kitten/components'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../assets/theme/colors';
import { ScreenIds } from '../../ScreenIds';
import { Modalize } from 'react-native-modalize';
import { TouchableWithoutFeedback } from '@ui-kitten/components/devsupport';
import context from '../../../context/context';
import OrderContext from '../../../context/OrderContext';
import VNAddress from '../../component/address/VNAddress';
import ENAddress from '../../component/address/ENAddress';
import { checkoutStates, CHECKOUT_TYPE, EN_ADDRESS, LANGUAGE_CODES, orderTypes, STRIPE_APPLE_PAY_MERCHANT, VN_ADDRESS } from '../../assets/constants';
import { applyCouponCode, getStripePulicKey, removeItem, updateOrderAddress, updateOrderBillingInfo, updateOrderPaymentType } from '../../services/ServiceWorker';
import { Toast } from 'popup-ui';
import { translate } from '../../../config/i18n';
import { validEmail } from '../../utils/Validation';
import { useIsFocused } from '@react-navigation/core';
import PhoneInput from 'react-native-phone-number-input';
import { commonStyles } from '../../assets/theme/styles';
import Spinner from 'react-native-loading-spinner-overlay';
import OrderItem, { displayTypes } from '../Order/OrderItem';
import { formatMoney } from '../../component/money/MoneyUtils';
import { StripeProvider, useApplePay } from '@stripe/stripe-react-native';
import GooglePayForm from '../../component/payment/GooglePayForm';
import ApplePayForm from '../../component/payment/ApplePayForm';

// TODO Translation
const BillingScreen = ({navigation, route}) => {
    const {orderType} = route.params;
    const paymentProceed = route && route.params && route.params.paymentProceed ? route.params.paymentProceed : null

    const {state, actions} = useContext(context)
    const {orderState, orderActions} = useContext(OrderContext);

    const addressModalRef = useRef();

    const googlePayRef = useRef();
    const [showPayment, setShowPayment] = useState(false);

    //#region Reload Order After Payment
    const isFocused = useIsFocused();

    useEffect(async() => {
        if(isFocused) {
            await loadUserOrder();
            await fetchPublishableKey();

            if(paymentProceed) {
                await handleUpdateBilling()
            }
        }
    }, [isFocused]);
    //#endregion

    //#region Load Stripe public key
    const [publishableKey, setPublishableKey] = useState('')

    const fetchPublishableKey = async() => {
        const key = await getStripePulicKey(state.userInfo?.id, state.version)
        setPublishableKey(key);

        await googlePayRef.current?.initialize()
    }

    useEffect(async() => {
        await googlePayRef.current?.initialize()
    }, [showPayment])
    //#endregion

    //#region Customer Information
    const [billingAddress, setBillingAddress] = useState({});
    const [infoErrors, setInfoErrors] = useState({});
    const [addressErrors, setAddressErrors] = useState({});
    const [isLoaded, setIsLoaded] = useState(false)

    //#region Phone Input
    const [formattedValue, setFormattedValue] = useState("")
    const phoneInput = useRef(null)

    const formatPhone = (phone, phoneExtension) => {
        if (phone && phoneExtension && phoneExtension.dialCode) {
            if(phoneExtension.dialCode.length === 1) {
                if (
                    phoneExtension.dialCode ===
                    phone[phoneExtension.dialCode.length - 1]
                ) {
                    let phoneSpliced = phone.replace(
                        phone[phoneExtension.dialCode.length - 1],
                        '',
                    )
        
                    return phoneSpliced
                }
            } else if(phoneExtension.dialCode.length > 1 && phone.includes(phoneExtension.dialCode)) {
                let matchAll = true;
                for(let i = 0; i < phoneExtension.dialCode.length; i++) {
                    if(phone[i] !== phoneExtension.dialCode[i]) {
                        matchAll = false;
                    }
                }
    
                if(matchAll) {
                    let phoneSpliced = phone.slice(phoneExtension.dialCode.length);
    
                    return phoneSpliced;
                }
            }        
        }
    
        return ''
    }
    //#endregion
    

    //#region Load Order Billing
    const [order, setOrder] = useState({})
    const [note, setNote] = useState("")

    const loadUserOrder = async() => {
        let order = orderType === orderTypes.product.value ? orderState.productOrder : orderState.courseOrder;

        if(!order || (order.orderDetailList && order.orderDetailList.length < 1)) {
            if(orderType === orderTypes.product.value) {
                navigation.navigate(ScreenIds.Cart)
            } else {
                navigation.navigate(ScreenIds.Course)
            }
        }
        
        if(order?.billingAddress && order.billingAddress?.phone) {   
            setFormattedValue(`+${order.billingAddress.phone}`)
        }

        if(order.paymentType === CHECKOUT_TYPE.CREDITCARD.value && !order.paymentProceed && order.cartCheckoutState !== checkoutStates.completed.value) {  
            setShowPayment(true)
        } else {
            setShowPayment(false)
        }
        
        setOrders([order])
        setOrder(order)
        setBillingAddress(order.billingAddress ? {
            ...order.billingAddress,
            phone: order.billingAddress.phone ? formatPhone(order.billingAddress.phone, order.billingAddress.phoneExtension) : "",
            phoneExtensionBean : order.billingAddress.phoneExtension ? order.billingAddress.phoneExtension : {}
        } : {})
        setAddressBean(order.billingAddress ? order.billingAddress : {})
        setNote(order?.note)

        setIsLoaded(true)
    }
    //#endregion

    //#region Render Content
    const [orders, setOrders] = useState([])

    const renderOrderHeader = () => (
        <React.Fragment>
            <View style={{margin: 16}}>
                <Text>Before continue to proceed, please review your cart and order summary</Text>
            </View>
        </React.Fragment>
    )

    const renderOrderFooter = () => {
        return (
            <React.Fragment>
                <View style={styles.cartTotalContainer}>
                    <View style={styles.cardHeader}>
                        <View style={styles.cardIcon}>
                            <Icon name="receipt" color={COLORS.SUB} size={24}/>
                        </View>
                        <View style={styles.cardTitle}>
                            <Text category="p1">{translate('order.cartSummary')}</Text>
                        </View>
                    </View>
                    <Divider />
                    <View style={styles.summaryWrapper}>
                        <Text category="p2">
                            {translate('order.itemTotal')} :{' '}
                        </Text>
                        <Text category="p2">{order.totalPrice && formatMoney(order.totalPrice, order.currency)}</Text>
                    </View>

                    <View style={styles.summaryWrapper}>
                        <Text category="p2">
                            {translate('order.discount')} :{' '}
                        </Text>
                        <Text category="p2">{order.discount && formatMoney(order.discount, order.currency)}</Text>
                    </View>
                    <View style={styles.summaryWrapper}>
                        <Text category="p2">
                            {translate('order.shippingFee')} :{' '}
                        </Text>
                        <Text category="p2">{order.shippingFee && formatMoney(order.shippingFee, order.currency)}</Text>
                    </View>
                    <View style={styles.summaryWrapper}>
                        <Text category="p2">
                            {translate('order.extraFee')} :{' '}
                        </Text>
                        <Text category="p2">{order.extraFee && formatMoney(order.extraFee, order.currency)}</Text>
                    </View>
                    <Divider />
                    <View style={styles.totalWrapper}>
                        <Text category="h6">{translate('order.total')}</Text>
                        <Text category="h6">{order.totalCheckout && formatMoney(order.totalCheckout, order.currency)}</Text>
                    </View>
                </View>
            </React.Fragment>
        )
    }
    //#endregion

    //#region Item template
    const renderItem = (item, index) => (
        <OrderItem
            key={index}
            orderItem={item} 
            onRemoveItem={handleRemoveItem}
            displayType={displayTypes.orderCart.value}
            orderType={orderType}
        />
    )

    const handleRemoveItem = async(orderDetailId) => {
        actions.showAppSpinner(true)

        let data = {
            id: orderDetailId
        }

        if(orderType === orderTypes.product.value) {
            const order = await removeItem(state.userInfo?.id, orderState.productOrder?.id, data);

            if(order && order.id) {
                await orderActions.reloadProductOrder(order.id);
    
                if(!order.orderDetailList || order.orderDetailList.length < 1) {
                    navigation.navigate(ScreenIds.Cart)
                }

                setOrders(order ? [order] : [])
                setOrder(order)
                setBillingAddress(order?.billingAddress ? {
                    ...order.billingAddress,
                    phone: order.billingAddress.phone ? formatPhone(order.billingAddress.phone, order.billingAddress.phoneExtension) : "",
                    phoneExtensionBean:order.billingAddress.phoneExtension ? order.billingAddress.phoneExtension : {}
                } : {})
                setAddressBean(order.billingAddress ? order.billingAddress : {})
                setNote(order.note);
            }
        } else {
            const order = await removeItem(state.userInfo?.id, orderState.courseOrder?.id, data);

            if(order && order.id) {
                await orderActions.reloadCourseOrder(order.id);

                if(!order.orderDetailList || order.orderDetailList.length < 1) {
                    navigation.navigate(ScreenIds.Course)
                }

                setOrders(order ? [order] : [])
                setOrder(order)
                setBillingAddress(order?.billingAddress ? {
                    ...order.billingAddress,
                    phone: order.billingAddress.phone ? formatPhone(order.billingAddress.phone, orderbillingAddress.phoneExtension) : "",
                    phoneExtensionBean : order.billingAddress.phoneExtension ? order.billingAddress.phoneExtension : {}
                } : {})
                setAddressBean(order.billingAddress ? order.billingAddress : {})
                setNote(order?.note)
            }
        }

        actions.showAppSpinner(false)
    }
    //#endregion

    const handleUpdateBilling = async() => {
        let isError = false;

        let errors = {}
        if(!billingAddress.fullname || !billingAddress.phone || !billingAddress.email || (billingAddress.email && !validEmail(billingAddress.email))) {
            errors = {
                ...errors,
                fullname: !billingAddress.fullname,
                phone: !billingAddress.phone,
                email: !billingAddress.email || !validEmail(billingAddress.email)
            }

            isError = true;
        }

        if(billingAddress.phone) {
            if(!phoneInput.current?.isValidNumber(billingAddress.phone)) {
                errors = {
                    ...errors,
                    invalidPhone: true
                }

                isError = true
            }
        }

        if(!isError) {
            let toPostData = {
                id: billingAddress.id,
                fullname: billingAddress.fullname,
                email: billingAddress.email,
                phone: formattedValue?.replace(/'+'/g, ''),
                phoneExtensionBean: billingAddress.phoneExtensionBean ? billingAddress.phoneExtensionBean : {},
                address: billingAddress.address
            }

            const updateOrderBilling = await updateOrderBillingInfo(state.userInfo?.id, order.id, toPostData, note);
            if(updateOrderBilling && updateOrderBilling.id) {
                if(orderType === orderTypes.product.value) {
                    await orderActions.setProductOrder(updateOrderBilling)
                } else {
                    await orderActions.setCourseOrder(updateOrderBilling)
                }

                setOrder(updateOrderBilling)
                setBillingAddress(updateOrderBilling && updateOrderBilling.billingAddress ? {
                    ...updateOrderBilling.billingAddress,
                    phone: updateOrderBilling.billingAddress.phone ? formatPhone(updateOrderBilling.billingAddress.phone, updateOrderBilling.billingAddress.phoneExtension) : "",
                    phoneExtensionBean: updateOrderBilling.billingAddress?.phoneExtension ? updateOrderBilling.billingAddress?.phoneExtension : {}
                } : {})
                setAddressBean(updateOrderBilling.billingAddress ? updateOrderBilling.billingAddress : {})
                setNote(updateOrderBilling.note)

                if(updateOrderBilling.paymentType === CHECKOUT_TYPE.CREDITCARD.value && !updateOrderBilling.paymentProceed && updateOrderBilling.cartCheckoutState !== checkoutStates.completed.value) {
                    setShowPayment(true)
                } else {
                    navigation.navigate(ScreenIds.OrderCompleteScreen, {orderType: orderType})
                }
            }
        } else {
            setInfoErrors(errors)

            Toast.show({
                title: translate('common.errorTitle'),
                text: 'Please provide required information',
                color: '#DB646A',
                timing: 3500,
            })
        }
    }
    
    const handleUpdateAddress = async() => {
        let isError = false;
        let errors = {};

        if (state.version) {
            if (state.version === LANGUAGE_CODES.vi.value) {
                if (
                    !addressBean ||
                    !addressBean.street ||
                    !addressBean.refWard ||
                    !addressBean.refDistrict ||
                    !addressBean.refProvince
                ) {
                    errors = {
                        ...errors,
                        street: !addressBean.street,
                        refWard: !addressBean.refWard,
                        refDistrict: !addressBean.refDistrict,
                        refProvince: !addressBean.refProvince,
                    }

                    isError = true
                }
            } else {
                if (
                    !addressBean ||
                    !addressBean.street ||
                    !addressBean.city ||
                    !addressBean.refState ||
                    !addressBean.refCountry ||
                    !addressBean.zipCode
                ) {
                    errors = {
                        ...errors,
                        street: !addressBean.street,
                        city: !addressBean.city,
                        refState: !addressBean.refState,
                        refCountry: !addressBean.refCountry,
                        zipCode: !addressBean.zipCode,
                    }

                    isError = true
                }
            }
        }

        if(isError) {
            setAddressErrors(errors)
        } else {
            let data = {
                id: billingAddress.id,
                street : addressBean?.street,
                refWard : addressBean?.refWard,
                refDistrict : addressBean?.refDistrict,
                refProvince : addressBean?.refProvince,
                city : addressBean?.city,
                refState : addressBean?.refState,
                refCountry : addressBean?.refCountry,
                zipCode: addressBean?.zipCode,
            }

            const updateAddress = await updateOrderAddress(state.userInfo?.id, order.id, data);
            if(updateAddress && updateAddress.id) {
                setAddressErrors({})

                let billingUpdated = {
                    ...billingAddress,
                    street : updateAddress?.street,
                    refWard : updateAddress?.refWard,
                    refDistrict : updateAddress?.refDistrict,
                    refProvince : updateAddress?.refProvince,
                    city : updateAddress?.city,
                    refState : updateAddress?.refState,
                    refCountry : updateAddress?.refCountry,
                    zipCode: updateAddress?.zipCode,
                    address: updateAddress?.address
                }

                setBillingAddress(billingUpdated)

                if(orderType === orderTypes.product.value) {
                    orderActions.setProductOrder({
                        ...order,
                        billingAddress: billingUpdated
                    })
                } else {
                    orderActions.setCourseOrder({
                        ...order,
                        billingAddress: billingUpdated
                    })
                }

                Toast.show({
                    title: translate('common.successTitle'),
                    text: 'Delivery location updated',
                    color: '#5BAA22',
                    timing: 3500,
                })
            } else {
                Toast.show({
                    title: translate('common.errorTitle'),
                    text: 'Problem occurs. Please try again',
                    color: '#DB646A',
                    timing: 3500,
                })
            }
        }
    }
    //#endregion

    //#region Delivery Location
    const [addressBean, setAddressBean] = useState({})

    const showAddressModal = () => {
        addressModalRef.current?.open();
    }

    const onStreetChange = street => {
        setAddressBean({
            ...addressBean,
            street: street,
        })
    }

    const onCountryChange = country => {
        setAddressBean({
            ...addressBean,
            refCountry: country,
        })
    }

    const onStateChange = state => {
        setAddressBean({
            ...addressBean,
            refState: state,
        })
    }

    const onCityChange = city => {
        setAddressBean({
            ...addressBean,
            city: city,
        })
    }

    const onProvinceChange = province => {
        setAddressBean({
            ...addressBean,
            refProvince: province,
        })
    }

    const onDistrictChange = district => {
        setAddressBean({
            ...addressBean,
            refDistrict: district,
        })
    }

    const onWardChange = ward => {
        setAddressBean({
            ...addressBean,
            refWard: ward,
        })
    }

    const onZipCodeChange = zipCode => {
        setAddressBean({
            ...addressBean,
            zipCode: zipCode,
        })
    }

    const onFullNameChange = fullname => {
        setBillingAddress({
            ...billingAddress,
            fullname: fullname,
        })
    }

    const onEmailChange = email => {
        setBillingAddress({
            ...billingAddress,
            email: email,
        })
    }

    const onPhoneChange = phone => {
        setBillingAddress({
            ...billingAddress,
            phone: phone,
            phoneExtensionBean: {
                countryCode: phoneInput.current?.getCountryCode(),
                dialCode: phoneInput.current?.getCallingCode(),
            },
        })
    }
    //#endregion

    //#region Payment Type
    const onPaymentTypeChange = async(paymentType) => {
        const prevPaymentType = order.paymentType;

        setOrder({
            ...order,
            paymentType : paymentType
        })

        if(paymentType === CHECKOUT_TYPE.CREDITCARD.value) {
            setShowPayment(true)

            await googlePayRef.current?.initialize()
        } else {
            setShowPayment(false)
        }

        const updatePaymentType = await updateOrderPaymentType(state.userInfo?.id, order.id, paymentType);

        if(!updatePaymentType || !updatePaymentType.id) {
            setOrder({
                ...order,
                paymentType : prevPaymentType
            })

            if(prevPaymentType === CHECKOUT_TYPE.CREDITCARD.value) {
                setShowPayment(true)
    
                await googlePayRef.current?.initialize()
            } else {
                setShowPayment(false)
            }
        } else if(updatePaymentType.id) {
            if(orderType === orderTypes.product.value) {
                orderActions.setProductOrder(updatePaymentType)
            } else {
                orderActions.setCourseOrder(updatePaymentType)
            }
        }
    }
    //#endregion

    //#region Coupon
    const [couponCode, setCouponCode] = useState('');
    const [isApplying, setIsApplying] = useState(false);
    const [isApplied, setIsApplied] = useState(false);

    const CouponIcon = () => (
        <Icon name="money-off" size={20} color={COLORS.SUB}/>
    )

    const LoadingIndicator = props => (
        <View style={[props.style, commonStyles.center]}>
            <Spinner size="small" status="basic" />
        </View>
    )

    const applyCoupon = () => {
        setIsApplying(true)
    }

    const handleApplyCoupon = async() => {
        let isError = false;

        if(!couponCode) {
            isError = true;

            Toast.show({
                title: translate('common.errorTitle'),
                text: "Empty coupon code",
                color: COLORS.ERROR_NOTIFY,
                timing: 3500,
            })
        }

        if(!isError) {
            actions.showAppSpinner(true);
            setIsApplying(true)

            const applyStatus = await applyCouponCode(state.userInfo?.id, order.id, couponCode).finally(() => {
                setIsApplying(false);
                actions.showAppSpinner(false);
            });

            if(applyStatus && applyStatus.id) {
                Toast.show({
                    title: translate('common.successTitle'),
                    text: 'Order discounted successfully',
                    color: COLORS.SUCCESS_NOTIFY,
                    timing: 3500,
                })

                setOrder({
                    ...order,
                    discount: applyStatus.discount ? applyStatus.discount : 0,
                    totalCheckout: applyStatus.totalCheckout ? applyStatus.totalCheckout : 0,
                    couponCodeBean: applyStatus.couponCodeBean ? applyStatus.couponCodeBean : null
                })
            } else {
                setCouponCode("")
            }
        }
    }
  
    // #endregion

    //#region Payment
    const renderPaymentCardIcon = props => (
        <KittenIcon {...props} name="credit-card-outline" />
    )
    
    const handleCompletePayment = async() => {
        actions.showAppSpinner(true)
        if(orderType === orderTypes.product.value) {
            await orderActions.setProductOrder({
                ...order,
                paymentProceed: true
            })
        } else {
            await orderActions.setCourseOrder({
                ...order,
                paymentProceed: true
            })
        }

        setOrders([{
            ...order,
            paymentProceed: true
        }])
        setOrder({
            ...order,
            paymentProceed: true
        })
        
        setShowPayment(false)

        await handleUpdateBilling();
        actions.showAppSpinner(false)
    }

    const handleCheckoutWithCreditCard = async() => {
        navigation.navigate(ScreenIds.OrderCheckOutScreen, {orderType: orderType})
    }

    const { isApplePaySupported } = useApplePay();
    //#endregion

    return(
        !isLoaded ?
            <View style={{flex: 1, backgroundColor: COLORS.WHITE}}>
                <Spinner
                    visible={true}
                    color={COLORS.SUB}
                    overlayColor={COLORS.TRANSPARENT}
                    animation="fade"
                />   
            </View>
        :
            <BaseScreen title='Order Information'>
                <StripeProvider
                    publishableKey={publishableKey}
                    merchantIdentifier={STRIPE_APPLE_PAY_MERCHANT}
                >
                    <View style={styles.container}>
                        <ScrollView
                            keyboardShouldPersistTaps="handled"
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{
                                paddingBottom: 100
                            }}
                            nestedScrollEnabled
                        >  
                            {renderOrderHeader()}

                            <View style={[styles.cardWrapper]}>
                                {orders && orders.length > 0 &&
                                    orders.map((item, idx) => (
                                        renderItem(item, idx)
                                    ))
                                }
                            </View>

                            <View style={styles.cardWrapper}>
                                <View style={[styles.card]}>
                                    <View style={styles.cardHeader}>
                                        <View style={styles.cardIcon}>
                                            <Icon name="local-offer" color={COLORS.SUB} size={24}/>
                                        </View>
                                        <View style={styles.cardTitle}>
                                            <Text category="p1">{translate('order.coupon')}</Text>
                                        </View>
                                    </View>
                                    <Divider />
                                    <View style={styles.couponInputContainer}>
                                        <Input
                                            status="basic"
                                            placeholder={translate(
                                                'order.inputCouponPlaceholder',
                                            )}
                                            value={couponCode}
                                            onChangeText={nextValue => setCouponCode(nextValue)}
                                        />
                                    </View>
                                    <View style={styles.couponBtn}>
                                        <Button
                                            accessoryLeft={
                                                isApplying ? LoadingIndicator : CouponIcon
                                            }
                                            appearance="outline"
                                            onPress={() => (!isApplying ? handleApplyCoupon() : {})}
                                            size="small">
                                            {props => (
                                                <Text
                                                    {...props}
                                                    category="p1"
                                                    style={[
                                                        styles.mainFont,
                                                        styles.lightColor,
                                                    ]}>
                                                    {translate('order.applyCouponCap')}
                                                </Text>
                                            )}
                                        </Button>
                                    </View>
                                </View>
                            </View>
                            
                            <View style={styles.cardWrapper}>
                                {renderOrderFooter()}
                            </View>                      

                            <View style={styles.cardWrapper}>
                                <View style={styles.card}>
                                    <View style={styles.cardHeader}>
                                        <View style={styles.cardIcon}>
                                            <Icon name="contact-phone" color={COLORS.SUB} size={24}/>
                                        </View>
                                        <View style={styles.cardTitle}>
                                            <Text category="p1">Customer Information</Text>
                                        </View>
                                    </View>

                                    <Divider/>

                                    <KeyboardAvoidingView enabled>
                                        <View style={[styles.formContainer]}>
                                            <Input
                                                style={styles.formGroup}
                                                textStyle={styles.mainFont}
                                                value={billingAddress.fullname}
                                                status={infoErrors.fullname && 'danger'}
                                                label={'Full Name'}
                                                placeholder={'Input here'}
                                                onChangeText={fullname =>
                                                    onFullNameChange(fullname)
                                                }
                                            />
                                            <Input
                                                style={styles.formGroup}
                                                textStyle={styles.mainFont}
                                                value={billingAddress.email}
                                                status={infoErrors.email && 'danger'}
                                                label={'Email'}
                                                placeholder={'Input here'}
                                                onChangeText={email =>
                                                    onEmailChange(email)
                                                }
                                            />
                                            <Text style={{marginTop: 5}} category={'label'} appearance={'hint'}>
                                                {translate('common.phoneNumber')}
                                            </Text>
                                            <PhoneInput
                                                ref={phoneInput}
                                                defaultValue={billingAddress.phone}
                                                defaultCode={
                                                    billingAddress.phoneExtensionBean?.countryCode
                                                        ? billingAddress.phoneExtensionBean?.countryCode.toUpperCase()
                                                        : 'CA'
                                                }
                                                layout="first"
                                                placeholder="Enter phone number"
                                                onChangeText={text => {
                                                    onPhoneChange(text)
                                                }}
                                                onChangeFormattedText={text => {
                                                    setFormattedValue(text)
                                                }}
                                                containerStyle={commonStyles.customPhoneInputContainer}
                                                textContainerStyle={[styles.phoneText, commonStyles.p0]}
                                                textInputStyle={[
                                                    commonStyles.p0,
                                                    styles.phoneTextContainer,
                                                    styles.formGroup
                                                ]}
                                                flagButtonStyle={commonStyles.p0}
                                                codeTextStyle={[
                                                    commonStyles.p0,
                                                    commonStyles.m0,
                                                    styles.phoneCodeText,
                                                ]}
                                            />
                                            {infoErrors.phone && (
                                                <Text category={'label'} status={'danger'}>
                                                    {translate('errors.emptyPhoneNumber')}
                                                </Text>
                                            )}
                                            {infoErrors.invalidPhone && (
                                                <Text category={'label'} status={'danger'}>
                                                    {translate('errors.invalidPhoneNumber')}
                                                </Text>
                                            )}
                                        </View>
                                    </KeyboardAvoidingView>                        
                                </View>
                            </View>

                            {/* {orderType === orderTypes.product.value && */}
                                <View style={styles.cardWrapper}>
                                    <View style={[styles.card]}>
                                        <View style={styles.cardHeader}>
                                            <View style={styles.cardIcon}>
                                                <Icon name="place" color={COLORS.SUB} size={24}/>
                                            </View>
                                            <View style={styles.cardTitle}>
                                                <Text category="p1">Delivery Location</Text>
                                            </View>
                                        </View>
                                        <Divider />
                                        <TouchableWithoutFeedback
                                            onPress={() => showAddressModal()}
                                        >
                                            <View style={styles.optionWrapper}>
                                                <View style={{marginVertical: 10, width: '85%'}}>
                                                    <Text>{billingAddress?.address}</Text>
                                                </View>
                                                <View style={styles.radioBtn}>
                                                    <Icon name="chevron-right" color={COLORS.SUB} size={24} />
                                                </View>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                            {/* } */}

                            <View style={styles.cardWrapper}>
                                <View style={[styles.card]}
                                >
                                    <View style={styles.cardHeader}>
                                        <View style={styles.cardIcon}>
                                            <Icon name="notes" color={COLORS.SUB} size={24}/>
                                        </View>
                                        <View style={styles.cardTitle}>
                                            <Text category="p1">Note</Text>
                                        </View>
                                    </View>
                                    <Divider />
                                    <Input 
                                        style={styles.formGroup}
                                        textStyle={styles.mainFont}
                                        size="small"
                                        value={note}
                                        status={''}
                                        label={'Note'}
                                        multiline
                                        numberOfLines={3}
                                        placeholder={'Input here'}
                                        onChangeText={note =>
                                            setNote(note)
                                        }
                                    />
                                </View>
                            </View>
                            
                            <View style={styles.cardWrapper}>
                                <View style={[styles.card]}>
                                    <View style={styles.cardHeader}>
                                        <View style={styles.cardIcon}>
                                            <Icon name="payment" color={COLORS.SUB} size={24}/>
                                        </View>
                                        <View style={styles.cardTitle}>
                                            <Text category="p1">Payment Method</Text>
                                        </View>
                                    </View>
                                    <Divider />

                                    {
                                        order.paymentProceed ?
                                            <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 10}}>
                                                <View style={{alignSelf: 'center', marginRight: 10}}>
                                                    <Radio
                                                        status="success"
                                                        checked={true}
                                                    >
                                                    </Radio>
                                                </View>
                                                <Text>You have completed your payment</Text>
                                            </View>
                                        :
                                            <React.Fragment>
                                                {orderType === orderTypes.product.value &&
                                                    <React.Fragment>
                                                        <View style={styles.optionWrapper}>
                                                            <View style={{marginVertical: 10, width: "90%"}}>
                                                                <Text>Cash on delivery</Text>
                                                                <Text appearance="hint" category="label">Receive and pay at the time</Text>
                                                            </View>
                                                            <View style={styles.radioBtn}>
                                                                <Radio
                                                                    checked={order.paymentType === CHECKOUT_TYPE.COD.value}
                                                                    onChange={nextChecked => onPaymentTypeChange(CHECKOUT_TYPE.COD.value)}
                                                                >
                                                                </Radio>
                                                            </View>
                                                        </View>
                                                        <Divider />
                                                        <View style={styles.optionWrapper}>
                                                            <View style={{marginVertical: 10, width: "90%"}}>
                                                                <Text>Pickup and Pay at store</Text>
                                                                {state.version === LANGUAGE_CODES.vi.value &&
                                                                    <Text appearance="hint" category="label">{VN_ADDRESS}</Text>
                                                                }

                                                                {state.version === LANGUAGE_CODES.en.value &&
                                                                    <Text appearance="hint" category="label">{EN_ADDRESS}</Text>
                                                                }
                                                                
                                                            </View>
                                                            <View style={styles.radioBtn}>
                                                                <Radio
                                                                    checked={order.paymentType === CHECKOUT_TYPE.PICKUP.value}
                                                                    onChange={nextChecked => onPaymentTypeChange(CHECKOUT_TYPE.PICKUP.value)}
                                                                >
                                                                </Radio>
                                                            </View>
                                                        </View>
                                                        <Divider />
                                                    </React.Fragment>
                                                }

                                                {orderType === orderTypes.course.value &&
                                                    <React.Fragment>
                                                        <View style={styles.optionWrapper}>
                                                            <View style={{marginVertical: 10, width: "90%"}}>
                                                                <Text>Pay at store</Text>
                                                                {state.version === LANGUAGE_CODES.vi.value &&
                                                                    <Text appearance="hint" category="label">{VN_ADDRESS}</Text>
                                                                }

                                                                {state.version === LANGUAGE_CODES.en.value &&
                                                                    <Text appearance="hint" category="label">{EN_ADDRESS}</Text>
                                                                }
                                                            </View>
                                                            <View style={styles.radioBtn}>
                                                                <Radio
                                                                    checked={order.paymentType === CHECKOUT_TYPE.PICKUP.value}
                                                                    onChange={nextChecked => onPaymentTypeChange(CHECKOUT_TYPE.PICKUP.value)}
                                                                >
                                                                </Radio>
                                                            </View>
                                                        </View>
                                                        <Divider />
                                                    </React.Fragment>
                                                }
                                                
                                                
                                                <View style={styles.optionWrapper}>
                                                    <View style={{marginVertical: 10, width: "90%"}}>
                                                        <Text>Credit Card</Text>
                                                        <Text appearance="hint" category="label">Visa - Mastercard</Text>
                                                    </View>
                                                    <View style={styles.radioBtn}>
                                                        <Radio
                                                            checked={order.paymentType === CHECKOUT_TYPE.CREDITCARD.value}
                                                            onChange={nextChecked => onPaymentTypeChange(CHECKOUT_TYPE.CREDITCARD.value)}
                                                        >
                                                        </Radio>
                                                    </View>
                                                </View>
                                            </React.Fragment>
                                    }
                                </View>
                            </View>
                            
                            {showPayment ?
                                <View style={{padding: 20}}>
                                    <Button accessoryRight={renderPaymentCardIcon} style={{marginTop: 20}} onPress={() => handleCheckoutWithCreditCard()}>
                                        Pay with Stripe
                                    </Button>
                                    <View style={{marginVertical: 10}}>
                                        <Text style={{textAlign: "center"}}>or</Text>
                                    </View>
                                    <GooglePayForm 
                                        ref={googlePayRef}
                                        orderNumber={order?.orderNumber}
                                        amount={order?.totalCheckout}
                                        coupon={order?.couponCodeBean}
                                        onSuccessPayment={handleCompletePayment}
                                    />
                                    {isApplePaySupported &&
                                        <ApplePayForm 
                                            ref={googlePayRef}
                                            orderNumber={order?.orderNumber}
                                            amount={order?.totalCheckout}
                                            coupon={order?.couponCodeBean}
                                            onSuccessPayment={handleCompletePayment}
                                        />
                                    }
                                </View>
                            :
                                <Button style={{marginTop: 60, marginBottom: 30, marginHorizontal: 20}} onPress={() => handleUpdateBilling()}>
                                    SUBMIT ORDER
                                </Button>
                            }
                            
                            <View style={{marginVertical: 10, paddingHorizontal: 20}}>
                                <Text category={"label"} style={{textAlign: "center"}}>
                                    By submitting your order, you agree with our 
                                </Text>
                                <Text category={"label"} style={{textAlign: "center"}}><Text>Privacy Policy </Text>and<Text> Term and Conditions</Text></Text>
                            </View>
                        </ScrollView>
                    </View>
                </StripeProvider>

                <Modalize
                    ref={addressModalRef}
                    handleStyle={styles.handleModalStyle}
                    modalStyle={{
                        borderTopLeftRadius: 30,
                        borderTopRightRadius: 30,
                        backgroundColor: COLORS.WHITE,
                        paddingBottom: 30
                    }}
                    handlePosition="inside"
                    keyboardAvoidingBehavior="padding"
                    modalHeight={500}
                    disableScrollIfPossible={true}
                    keyboardAvoidingOffset={150}
                    onBackButtonPress={() => addressModalRef.current?.close()}
                    scrollViewProps={{ showsVerticalScrollIndicator:false, keyboardShouldPersistTaps: 'handled' }}
                >   
                    <View style={{marginVertical: 30, paddingHorizontal: 20}}>
                        {state.version === LANGUAGE_CODES.vi.value ? (
                            <VNAddress
                                addressBean={addressBean}
                                setStreet={onStreetChange}
                                setRefWard={onWardChange}
                                setRefDistrict={onDistrictChange}
                                setRefProvince={onProvinceChange}
                                errors={addressErrors}
                            />
                        ) : (
                            <ENAddress
                                addressBean={addressBean}
                                setStreet={onStreetChange}
                                setCity={onCityChange}
                                setRefState={onStateChange}
                                setRefCountry={onCountryChange}
                                setZipCode={onZipCodeChange}
                                errors={addressErrors}
                            />
                        )}

                        <View style={{marginVertical: 20}}>
                            <Button  onPress={() => handleUpdateAddress()}>CẬP NHẬT</Button>
                        </View>
                    </View>
                </Modalize>
            </BaseScreen>
    )
}

export default BillingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row', 
        paddingTop: 10
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /********* Card ********/
    cardWrapper: {
        paddingHorizontal: 16,
        paddingVertical: 5
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 10,
        justifyContent: 'space-between',
        width: '100%',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        padding: 16
    },
    cardHeader: {
        flexDirection: 'row'
    },
    cardIcon: {
        alignSelf: 'center', marginRight: 10
    },
    cardTitle: {
        paddingVertical: 10
    },
    optionWrapper: {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    radioBtn: {
        marginVertical: 10, 
        alignSelf: 'center'
    },
    /******* Form *******/
    formContainer: {
        marginVertical: 10
    },
    formGroup: {
        marginVertical: 5,
    },
    /********* Modal *********/
    handleModalStyle: {
        marginTop:30,
        backgroundColor:COLORS.SUB,
        width:80
    }, 
    /****** Generic ************/ 
    mainFont: {
        fontFamily: 'Prata-Regular'
    },
    lightColor: {
        color: COLORS.WHITE
    },
    // Phone
    phoneTextContainer: {
        color: "#222B45"
    },
    phoneText: {
        backgroundColor: "rgb(247, 249, 252)"
    },
    phoneCodeText: {
        fontSize: 16,
        paddingBottom: 1,
        fontFamily: 'Prata-Regular'
    },
     /******** Coupon *******/
    couponContainer: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 5,
        justifyContent: 'space-between',
        marginBottom: 15,
        width: '100%',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        padding: 10,
    },
    couponTitle: {
        paddingVertical: 10,
    },
    couponInputContainer: {
        paddingVertical: 10,
    },
    couponBtn: {
        paddingBottom: 15,
        paddingTop: 5,
    },

    mainFont: {
        fontFamily: 'Prata-Regular',
    },
    lightColor: {
        color: COLORS.SUB,
    },
     /******** Cart Summary **********/
     cartTotalContainer: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 5,
        justifyContent: 'space-between',
        width: '100%',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        padding: 10
    },
    cartTotalTitle: {
        paddingVertical: 10,
    },
    summaryWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 5,
    },
    totalWrapper: {
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
})

