import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet} from 'react-native'
import { COLORS } from '../../assets/theme/colors';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Text } from '@ui-kitten/components';

const AssignmentItem = ({
    index,
    name,
    description,
    color,
    onPress
}) => {
    return(
        <TouchableOpacity
            key={index}
            onPress={onPress}
            style={styles.container}
        >
            <View style={[styles.indexWrapper, {backgroundColor: color}]}>
                <Text style={styles.index}>{index}</Text>
            </View>

            <View style={styles.lessonWrapper}>
                <Text style={styles.label}>
                    {name}
                </Text>
                <Text style={styles.durationLabel}>
                    {description}
                </Text>
            </View>

            <View style={styles.viewButtonWrapper}>
                <TouchableOpacity
                    onPress={onPress}
                >
                    <View
                        style={[styles.viewButtonContainer, styles.viewButtonBackground]}
                    >
                        <Icon 
                            name="info"
                            size={20}
                            color={COLORS.SUB}
                        />
                    </View>
                </TouchableOpacity>
            </View>
           </TouchableOpacity>
    ) 
}

export default AssignmentItem;

const styles = StyleSheet.create({
    container: {
        flexDirection:"row",
        padding:10,
        marginHorizontal:20,
        borderRadius:20,
        alignItems:"center",
    },
    /****** Index *******/ 
    indexWrapper: {
        paddingVertical:5,
        paddingHorizontal:10,
        borderRadius:6
    },
    index: {
        fontSize:10,
    },
    /****** Lesson ********/
    lessonWrapper: {
        paddingLeft: 20
    },
    label: {
        color:"#345c74",
        fontSize:13,
        width:180
    },
    durationLabel: {
        color:"#f58084",
        fontSize:12
    },
    /******* Button *******/
    viewButtonWrapper: {
        position: 'absolute',
        right: 0,
        alignSelf: 'center'
    },
    viewButtonContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewButtonBackground: {
        backgroundColor: '#FDF3EB',
    },
})