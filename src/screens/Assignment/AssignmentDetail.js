import { useIsFocused } from '@react-navigation/core';
import { Text } from '@ui-kitten/components';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { StyleSheet, useWindowDimensions, View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { COLORS } from '../../assets/theme/colors';
import BaseScreen from '../../component/BaseScreen';
import { ScreenIds } from '../../ScreenIds';
import { getAssignmentDetail } from '../../services/ServiceWorker';
import Icon from 'react-native-vector-icons/MaterialIcons'
import RenderHtml from 'react-native-render-html';
import context from '../../../context/context';
import IframeRenderer, { iframeModel } from '@native-html/iframe-plugin';
import WebView from 'react-native-webview';
import CartAction from '../Cart/CartAction';
import { cartActionTypes } from '../../assets/constants';

const AssignmentDetail = ({navigation, route}) => {
    const { width } = useWindowDimensions();
    const { state } = useContext(context);
    const {assignmentId, participantId} = route.params;

    //#region LOAD ASSIGNMENT DETAIL
    const [assignment, setAssignment] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);

    const loadAssignmentDetail = async() => {
        const assignmentData = await getAssignmentDetail(assignmentId, participantId, state.userInfo?.id);

        if(assignmentData && assignmentData.id) {
            setAssignment(assignmentData);
            setIsLoaded(true);
        } else {
            navigation.navigate(ScreenIds.LessonScreen)
        }
    }

    const isFocused = useIsFocused();

    useEffect(async() => {
        if(!isLoaded && isFocused) {
            await loadAssignmentDetail()
        }
    }, [isFocused]);
    //#endregion  

    //#region RENDER HTML CONFIG
    const renderersProps = {
        img: {
          enableExperimentalPercentWidth: true
        },
        iframe: { scalesPageToFit: true, webViewProps: { /* Any prop you want to pass to iframe WebViews */ }}
    };

    const customHTMLElementModels = {
        iframe: iframeModel
    }

    const renderers = {
        iframe: IframeRenderer
    }
    //#endregion

    return(
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <View style={styles.navigationWrapper}>
                        <View style={{marginRight: 15}}>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate(ScreenIds.LessonScreen)
                                }>
                                <View
                                    style={[
                                        styles.navigationBackground,
                                        styles.navigationContainer,
                                    ]}>
                                    <Icon
                                        name="chevron-left"
                                        size={30}
                                        color={COLORS.SUB}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text category="h6">
                            {assignment.courseName} - {assignment.name}
                        </Text>
                    </View>
                    
                    {isLoaded && assignment.data &&
                        <View style={{flex: 1, marginTop: 20}}>
                            <RenderHtml
                                renderers={renderers}
                                WebView={WebView}
                                customHTMLElementModels={customHTMLElementModels}
                                contentWidth={width}
                                source={{ html : assignment.data}}
                                renderersProps={renderersProps}
                            />
                        </View>
                    }                    
                </ScrollView>
                <CartAction
                    type={cartActionTypes.assignmentDetail.value}
                    actionLabel={'NỘP BÀI TẬP'}
                    onActionPress={() => navigation.navigate(ScreenIds.SubmissionScreen, {participantId: participantId, assignmentId: assignmentId})}
                    actionEnabled
                />
            </View>
        </BaseScreen>
    )
}

export default AssignmentDetail;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingHorizontal: 20
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        paddingTop: 10,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
})