import React, { useContext, useRef, useState } from 'react';
import { Dimensions, Keyboard, KeyboardAvoidingView, StyleSheet, View } from 'react-native';
import { ScrollView, TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import context from '../../../context/context';
import { COLORS } from '../../assets/theme/colors';
import BaseScreen from '../../component/BaseScreen';
import { ScreenIds } from '../../ScreenIds';
import Icon from 'react-native-vector-icons/MaterialIcons'
import UploadImage from '../Account/component/UploadImage';
import FastImage from 'react-native-fast-image';
import { Button, ButtonGroup, Divider, Input, Text } from '@ui-kitten/components';
import { HTTP_STATUS_OK, uploadImageTypes } from '../../assets/constants';
import { updateSubmission } from '../../services/ServiceWorker';
import { submissionStatuses } from './CourseSubmissions';
import moment from 'moment';

const w = Dimensions.get("window").width;

const SubmissionScreen = ({navigation, route}) => {
    const {assignmentId, participantId} = route.params;
    const {state, actions} = useContext(context);

    //#region IMAGE PICKER
    const setShowImagePicker = useRef()

    const getShowPicker = setShowPicker => {
        setShowImagePicker.current = setShowPicker
    }
    const showImagePicker = () => {
        setShowImagePicker.current(true)
    }
    //#endregion

    //#region SUBMISSION
    const [imageUrls, setImageUrls] = useState([]);
    const [isImageLoaded, setIsImageLoaded] = useState(false);

    const [note, setNote] = useState('');

    const [submissionSummary, setSubmissionSummary] = useState({});
    const [isSubmitSuccess, setIsSubmitSuccess] = useState(false);

    const handleDisplayImage = (imageUrl) => {
        if(imageUrl) {
            let images = [...imageUrls];
            images.unshift(imageUrl);

            setImageUrls(images)
            setIsImageLoaded(true)
        }
    }

    const handleRemoveImageUploaded = (idx) => {
        let images = [...imageUrls];
        images.splice(idx, 1);

        setImageUrls(images);
        setIsImageLoaded(images.length > 0)
    }

    const handleUploadSubmission = async() => {
        actions.showAppSpinner(true)

        let data = {
            submittedImages: getImagesUploaded(imageUrls),
            note: note
        }
        const uploadSubmissionData = await updateSubmission(state.userInfo?.id, participantId, assignmentId, data);
        if(uploadSubmissionData && uploadSubmissionData.id) {
            setIsSubmitSuccess(true)
            setSubmissionSummary(uploadSubmissionData)
        }


        actions.showAppSpinner(false);
    }

    const getImagesUploaded = (imageUrls) => {
        if(imageUrls && Array.isArray(imageUrls)) {
            let returnedData = [];

            for(let i = 0; i < imageUrls.length; i++) {
                let item = {
                    imageUrl: imageUrls[i]
                }

                returnedData.push(item)
            }

            return returnedData;
        }
    }

    const renderSubmissionStatus = (status) => {
        if(status) {
            switch(status) {
                case submissionStatuses.NEW.value:
                    return submissionStatuses.NEW.label;
                case submissionStatuses.ACCEPTED.value:
                    return submissionStatuses.ACCEPTED.label;
                case submissionStatuses.DECLINED.value:
                    return submissionStatuses.DECLINED.label;
            }
        }
    }
    //#endregion

    return(
        <BaseScreen showHeader={false} onBackPress={() => {}}
            modals={
                <UploadImage
                    defaultImages={[]}
                    onImageChange={() => {}}
                    showAppPopup={actions.showAppPopup}
                    showAppSpinner={actions.showAppSpinner}
                    getShowPicker={getShowPicker}
                    uploadType={uploadImageTypes.submission.value}
                    displayImage={handleDisplayImage}
                />
            }
        >
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View style={{marginRight: 15}}>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.AssignmentDetail, {assignmentId: assignmentId, participantId: participantId})
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                {!isSubmitSuccess 
                ?
                    <React.Fragment>
                        {!isImageLoaded &&
                            <React.Fragment>
                                <View style={{padding: 20, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginTop: 50}}>
                                    <FastImage 
                                        source={require('../../assets/images/pick-image.png')}
                                        style={{
                                            height: 180,
                                            width: 250,
                                            alignSelf: 'center'
                                        }}
                                    />
                                    <Text style={{marginVertical: 20}}>Chọn ít nhất một bức hình để đăng tải</Text>
                                </View>

                                <View style={{paddingHorizontal: 20}}>
                                    <Button
                                        onPress={() => showImagePicker()}
                                    >CHỌN HÌNH ẢNH</Button>
                                </View>
                            </React.Fragment>
                        }
                        
                        {isImageLoaded && imageUrls.length > 0 &&
                            <React.Fragment>
                                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                                <View style={{height: 250, marginVertical: 20}}>
                                    <ScrollView
                                        horizontal
                                        showsHorizontalScrollIndicator={false}
                                    >                                   
                                        {
                                            imageUrls.map((image, idx) => (
                                                <View key={idx} 
                                                    style={{height:220, width: w, margin: 0}}
                                                >
                                                    <FastImage 
                                                        source={{uri: image}}
                                                        style={{
                                                            width: '100%',
                                                            height: 220
                                                        }}
                                                        resizeMode='contain'
                                                    />

                                                    <View style={{position: 'absolute', right: 50, top: 5, zIndex: 10}}>
                                                        <TouchableOpacity
                                                            onPress={() =>
                                                                handleRemoveImageUploaded(idx)
                                                            }>
                                                            <View
                                                                style={[
                                                                    styles.navigationBackground,
                                                                    styles.navigationContainer,
                                                                ]}>
                                                                <Icon
                                                                    name="highlight-off"
                                                                    size={20}
                                                                    color={COLORS.BLACK}
                                                                />
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            ))
                                        }
                                    </ScrollView>
                                </View>


                                <View style={{marginBottom: 20}}>
                                    <Button
                                        appearance="ghost"
                                        onPress={() => showImagePicker()}
                                    >
                                        Add more image
                                    </Button>
                                    <View style={{backgroundColor: COLORS.LIGHT, padding: 8, borderRadius: 5}}>
                                        <Text style={{textAlign: 'center'}}>You've uploaded {imageUrls.length} images</Text>
                                    </View>
                                </View>

                                </TouchableWithoutFeedback>


                                <Input
                                    value={note}
                                    label='Ghi chú'
                                    placeholder='Điền ghi chú của bạn'
                                    multiline
                                    onChangeText={nextValue => setNote(nextValue)}
                                />

                                <View style={{marginTop: 50}}>
                                    <Button
                                        onPress={() => handleUploadSubmission()}
                                    >GỬI BÀI</Button>
                                </View>
                            </React.Fragment>
                        }
                    </React.Fragment>
                :
                    <React.Fragment>
                        <View style={{padding: 10, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginTop: 50}}>
                            <FastImage 
                                source={require('../../assets/images/awaiting-to-assess.png')}
                                style={{
                                    height: 180,
                                    width: 280,
                                    alignSelf: 'center'
                                }}
                            />
                        </View>
                        <View style={{marginBottom: 10}}>
                            <Text style={{textAlign: 'center'}}>You've successfully submitted your assignment</Text>
                            <Text style={{textAlign: 'center'}} category="label" appearance="hint">Please wait for your lecturers to assess your submission</Text>
                        </View>
                        <Divider />
                        <View style={{marginVertical: 20}}>
                            <Text category="p2">Submission Summary</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{marginBottom: 5}}>
                                    <Text appearance="hint" category="label">Status</Text>
                                    <Text category="label">{renderSubmissionStatus(submissionSummary?.status)}</Text>
                                </View>

                                <View  style={{marginBottom: 5}}>
                                    <Text appearance="hint" category="label">Submitted on</Text>
                                    <Text category="label">{submissionSummary.date && moment(submissionSummary.date).format("MM-DD-yyyy HH:mm")}</Text>
                                </View>
                            </View>

                            <View style={{marginBottom: 5}}>
                                <Text appearance="hint" category="label">Your note</Text>
                                <Text category="label">{submissionSummary?.note ? submissionSummary?.note : 'No data given'}</Text>
                            </View> 
                        </View>
                    </React.Fragment>  
                }
            </View>
        </BaseScreen>
    )
} 

export default SubmissionScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingHorizontal: 20
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        paddingTop: 10,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: COLORS.LIGHT,
    },
})