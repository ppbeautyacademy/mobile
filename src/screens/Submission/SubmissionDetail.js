import React, { useContext, useState } from 'react';
import { Dimensions, FlatList, StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import BaseScreen from '../../component/BaseScreen';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../assets/theme/colors';
import { ScreenIds } from '../../ScreenIds';
import { Divider, Input, Select, SelectItem, Text } from '@ui-kitten/components';
import moment from 'moment';
import { acceptanceStatusOptions, submissionStatuses } from './CourseSubmissions';
import CartAction from '../Cart/CartAction';
import { cartActionTypes, HTTP_STATUS_OK } from '../../assets/constants';
import { submissionTypes } from '../Lesson/LessonListView';
import { selectParser } from '../../component/address/parser';
import { assessSubmission, deleteSubmission } from '../../services/ServiceWorker';
import context from '../../../context/context';
import { Toast } from 'popup-ui';
import { translate } from '../../../config/i18n';

const w = Dimensions.get("window").width;

const SubmissionDetail = ({navigation, route}) => {
    const {state} = useContext(context)

    const {submissionData, type, participantId} = route.params
    const {imagesList, date, note, comment, status, participantName, participantCode} = submissionData;

    const [lecturerComment, setLecturerComment] = useState(comment);
    const [lecturerAcceptance, setLecturerAcceptance] = useState(status);

    const renderSubmissionStatus = (status) => {
        if(status) {
            switch(status) {
                case submissionStatuses.NEW.value:
                    return submissionStatuses.NEW.label;
                case submissionStatuses.ACCEPTED.value:
                    return submissionStatuses.ACCEPTED.label;
                case submissionStatuses.DECLINED.value:
                    return submissionStatuses.DECLINED.label;
            }
        }
    }

    const renderAssessmentComment = (comment, status) => {
        if(type === submissionTypes.studentSubmission.value) {
            return(
                <Input 
                    value={lecturerComment}
                    onChangeText={(nextValue) => setLecturerComment(nextValue)}
                    multiline
                    placeholder={'Input comment here'}
                />
            )    
        } else {
            if(status === submissionStatuses.NEW.value) {
                return (<Text category="label">Awaiting for being assessed</Text>)
            }
    
            if(!comment) {
                return (<Text category="label">No comments for this assessment</Text>)
            }
    
            return (<Text category="label">{comment}</Text>)
        }
    }

    const renderAssessmentStatus = () => {
        if(type === submissionTypes.studentSubmission.value) {
            return(
                <View style={{marginBottom: 5}}>
                    <Select
                        style={styles.select}
                        label="Acceptance Status"
                        placeholder='Select a status'
                        value={lecturerAcceptance}
                        onSelect={index => handleOnAcceptanceChange(index)}>
                        {acceptanceStatusOptions &&
                            acceptanceStatusOptions.length > 0 &&
                            acceptanceStatusOptions.map((item, idx) => (
                                <SelectItem key={idx} title={item.label} />
                            ))}
                    </Select>
                </View>
            )
        }
    }

    const handleOnAcceptanceChange = (index) => {
        const acceptanceData = selectParser(index.row, acceptanceStatusOptions);
        if (acceptanceData) {
            setLecturerAcceptance(acceptanceData.value)
        }
    }

    const handleActionPress = () => {
        if(type === submissionTypes.studentSubmission.value) {
            handleUpdateAssessment();
        } else {
            handleDeleteSubmission();
        }
    }

    const handleDeleteSubmission = async() => {
        const deleteStatus = await deleteSubmission(submissionData.id);
        if(deleteStatus === HTTP_STATUS_OK) {
            navigation.navigate(ScreenIds.LessonScreen)
        }
    }

    const handleUpdateAssessment = async() => {
        let data = {
            id: submissionData.id,
            comment: lecturerComment,
            status: lecturerAcceptance
        }

        const assessmentStatus = await assessSubmission(state.userInfo?.id, participantId, data);
        if(assessmentStatus && assessmentStatus.id) {
            Toast.show({
                title: translate('common.successTitle'),
                text: 'Update assessment successfully',
                color: '#5BAA22',
                timing: 3500,
            })
        }
    }

    const renderActionLabel = () => {
        if(type === submissionTypes.studentSubmission.value) {
            return 'GỬI ĐÁNH GIÁ';
        } else {
            return 'XÓA BÀI NỘP';
        }
    }

    const renderActionStatus = () => {
        if(type === submissionTypes.studentSubmission.value) {
            return 'primary';
        } else {
            return 'danger';
        }
    }

    return(
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <View style={styles.container}>
                <View style={styles.navigationWrapper}>
                    <View style={{marginRight: 15}}>
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate(ScreenIds.LessonScreen)
                            }>
                            <View
                                style={[
                                    styles.navigationBackground,
                                    styles.navigationContainer,
                                ]}>
                                <Icon
                                    name="chevron-left"
                                    size={30}
                                    color={COLORS.SUB}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View>
                        {type === submissionTypes.mySubmission.value &&
                            <Text>Your submission</Text>
                        }

                        {type === submissionTypes.studentSubmission.value &&
                            <Text>Student's submission</Text>
                        }
                    </View>
                </View>
                <ScrollView nestedScrollEnabled showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        paddingBottom: 110
                    }}
                >
                    <View style={{height: 250, marginBottom: 15}}>
                        <ScrollView
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{
                                height: 250
                            }}
                        >
                            {imagesList && imagesList.length > 0 &&
                                imagesList.map((item, idx) => (
                                    <View key={idx} 
                                        style={{height:250, width: w, margin: 0}}
                                    >
                                        <FastImage 
                                            source={{uri: item}}
                                            style={{
                                                width: '100%',
                                                height: 250
                                            }}
                                            resizeMode='contain'
                                        />
                                    </View>
                                ))
                            }
                        </ScrollView>
                    </View>

                    <Divider />
                    
                    <View style={{marginVertical: 20}}>
                        {type === submissionTypes.studentSubmission.value &&
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={{marginBottom: 5}}>
                                    <Text appearance="hint" category="label">Name</Text>
                                    <Text category="label">{participantName}</Text>
                                </View>
    
                                <View  style={{marginBottom: 5, alignItems: 'flex-end'}}>
                                    <Text appearance="hint" category="label">Code</Text>
                                    <Text category="label">{participantCode}</Text>
                                </View>
                            </View>
                        }

                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{marginBottom: 5}}>
                                <Text appearance="hint" category="label">Status</Text>
                                <Text category="label">{renderSubmissionStatus(status)}</Text>
                            </View>

                            <View  style={{marginBottom: 5, alignItems: 'flex-end'}}>
                                <Text appearance="hint" category="label">Submitted on</Text>
                                <Text category="label">{date && moment(date).format("MM-DD-yyyy HH:mm")}</Text>
                            </View>
                        </View>

                        <View style={{marginBottom: 5}}>
                            <Text appearance="hint" category="label">Your note</Text>
                            <Text category="label">{note ? note : 'No data given'}</Text>
                        </View> 
                    </View>

                    <Divider />
                    
                    <View style={{marginVertical: 20}}>
                        {renderAssessmentStatus(status)}
                        <View style={{marginBottom: 5}}>
                            <Text appearance="hint" category="label">Assessor's comment</Text>
                            {renderAssessmentComment(comment, status)}
                        </View>
                    </View>
                </ScrollView>
                
                {
                    ((type === submissionTypes.mySubmission.value && status === submissionStatuses.NEW.value) 
                ||  (type === submissionTypes.studentSubmission.value)) 
                &&  <CartAction
                        type={cartActionTypes.submissionDetail.value}
                        actionLabel={renderActionLabel()}
                        onActionPress={() => handleActionPress()}
                        actionEnabled
                        status={renderActionStatus()}
                    />
                }
            </View>
        </BaseScreen>
    )
}

export default SubmissionDetail;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingHorizontal: 20
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        paddingTop: 10,
        alignItems: 'center'
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    select: {
        flex: 1,
        margin: 2,
    },
})