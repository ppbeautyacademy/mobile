import { Button, Icon, ListItem, Text } from '@ui-kitten/components';
import moment from 'moment';
import React from 'react';
import { SectionList, StyleSheet, View } from 'react-native';
import { COLORS } from '../../assets/theme/colors';
import { Badge } from 'react-native-elements'
import { ScreenIds } from '../../ScreenIds';
import { submissionTypes } from '../Lesson/LessonListView';

export const submissionStatuses = {
    NEW: {label: "Just Submitted", value: "NEW"},
    ACCEPTED: {label: "Submission Passed", value: "ACCEPTED"},
    DECLINED: {label: "Submission Declinded", value: "DECLINED"}
}

export const acceptanceStatusOptions = [
    submissionStatuses.NEW,
    submissionStatuses.ACCEPTED,
    submissionStatuses.DECLINED
]

const CourseSubmissions = ({navigation, submissions, type, participantId}) => {

    const renderSectionHeader = (title) => {
        return(
            <View style={styles.headerWrapper}>
                <Text style={styles.header} category="p2">{title}</Text>
            </View>
        )
    }

    const Item = ({ data }) => (
        type === submissionTypes.mySubmission.value 
        ?
            <ListItem
                title={evaProps => <Text {...evaProps}>Submitted on {data.date && moment(data.date).format('MM-DD-yyyy HH:mm')}</Text>}
                description={renderDescription(data)}
                accessoryLeft={renderItemIcon}
                accessoryRight={(evaProps) => renderItemAccessory(evaProps, data)}
            />
        :   type === submissionTypes.studentSubmission.value
        &&  
            <ListItem
                title={evaProps => <Text {...evaProps}>{data.participantName}</Text>}
                description={renderDescription(data)}
                accessoryLeft={renderItemIcon}
                accessoryRight={(evaProps) => renderItemAccessory(evaProps, data)}
            />
    );

    const renderDescription = ({status, date}) => {
        switch(status) {
            case submissionStatuses.NEW.value: 
                return(
                    <Text>
                        <Badge status="primary"/>
                        <Text category="label" appearance="hint"> New {type === submissionTypes.studentSubmission.value && date && `- ${moment(date).format('MM-DD-yyyy HH:mm')}`}</Text>
                    </Text>
                )
            case submissionStatuses.ACCEPTED.value:
                return(
                    <Text>
                        <Badge status="success"/>
                        <Text category="label" appearance="hint"> Accepted {type === submissionTypes.studentSubmission.value && date && `- ${moment(date).format('MM-DD-yyyy HH:mm')}`}</Text>
                    </Text>
                )
            case submissionStatuses.DECLINED.value:
                return(
                    <Text>
                        <Badge status="error"/>
                        <Text category="label" appearance="hint"> Declined {type === submissionTypes.studentSubmission.value && date && `- ${moment(date).format('MM-DD-yyyy HH:mm')}`}</Text>
                    </Text>
                )
        }
    }

    const renderItemAccessory = (props, data) => (
        <Button 
            size='tiny' 
            onPress={() => navigation.navigate(ScreenIds.SubmissionDetail, {
                submissionData: data,
                type: type,
                participantId: participantId
            })}
        >
            {type === submissionTypes.studentSubmission.value ? 'GRADE' : 'VIEW'}
        </Button>
      );
    
      const renderItemIcon = (props) => (
        <Icon {...props} name='file-text-outline'/>
    );

    return(
        <SectionList
            sections={submissions}
            keyExtractor={(item, index) => item + index}
            renderItem={({ item }) => <Item data={item} />}
            renderSectionHeader={({ section: { title } }) => (
                renderSectionHeader(title)
            )}
        />
    )
}

export default CourseSubmissions;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: COLORS.WHITE
    },
    headerWrapper: {
        backgroundColor: COLORS.LIGHT,
        padding: 5
    },
    header: {
        textAlign: 'center'
    }
});

