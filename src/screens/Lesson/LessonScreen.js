import React from 'react'
import {StyleSheet, View} from 'react-native'
import {translate} from '../../../config/i18n'
import BaseScreen from '../../component/BaseScreen'
import WebViewComp from '../../component/Webview'

const styles = StyleSheet.create({
    webViewContainer: {
        flex: 1,
    },
})

const LessonScreen = () => {
    return (
        <BaseScreen title={''}>
            <View style={styles.webViewContainer}>
                <WebViewComp uri="https://www.google.com" />
            </View>
        </BaseScreen>
    )
}

export default LessonScreen
