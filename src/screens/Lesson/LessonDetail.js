import React, { useContext, useEffect, useRef, useState } from 'react'
import {View, StyleSheet, Image, Dimensions, Alert, useWindowDimensions} from 'react-native'
import BaseScreen from '../../component/BaseScreen'
import Video from 'react-native-video';
import { Text } from '@ui-kitten/components';
import { COLORS } from '../../assets/theme/colors';
import WebView from 'react-native-webview';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import context from '../../../context/context';
import Icon from 'react-native-vector-icons/MaterialIcons'
import RenderHtml from 'react-native-render-html';
import { getLessonDetail } from '../../services/ServiceWorker';
import { ScreenIds } from '../../ScreenIds';
import { useIsFocused } from '@react-navigation/core';
import IframeRenderer, { iframeModel } from '@native-html/iframe-plugin';
import Spinner from 'react-native-loading-spinner-overlay';

const LessonDetail = ({navigation, route}) => {
    const {state} = useContext(context);
    const { width } = useWindowDimensions();

    const {lessonId, participantId} = route.params;

    //#region LESSON DETAIL
    const [lesson, setLesson] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);

    const loadLessonDetail = async() => {
        const lessonData = await getLessonDetail(lessonId, participantId, state.userInfo?.id);

        if(lessonData && lessonData.id) {
            setLesson(lessonData);
            setIsLoaded(true);
        } else {
            navigation.navigate(ScreenIds.LessonScreen)
        }
    }

    const isFocused = useIsFocused();

    useEffect(async() => {
        if(!isLoaded && isFocused) {
            await loadLessonDetail()
        }
    }, [isFocused]);
    //#endregion

    const renderersProps = {
        img: {
          enableExperimentalPercentWidth: true
        },
        iframe: { scalesPageToFit: true, webViewProps: { /* Any prop you want to pass to iframe WebViews */ }}
    };

    const customHTMLElementModels = {
        iframe: iframeModel
    }

    const renderers = {
        iframe: IframeRenderer
    }

    //#region Lesson Template
    const renderFrame = (title) => {
        return (
            title 
            ?   <View style={[styles.frame, {justifyContent: "center", alignSelf: "center"}]}>
                    <Text multiline numberOfLines={3} category={"h6"} style={{textAlign: "center", color: COLORS.WHITE}}>
                        {title}
                    </Text>
                </View>
            :   <View style={styles.frame}></View>
        )
    }
    //#endregion

    return(
        !isLoaded ?
        <View style={{flex: 1, backgroundColor: COLORS.WHITE}}>
            <Spinner
                visible={true}
                color={COLORS.SUB}
                overlayColor={COLORS.TRANSPARENT}
                animation="fade"
            />   
        </View>
        :
        <BaseScreen title={lesson.courseName}>
            <View style={styles.container}>
                {renderFrame(lesson.name)}
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        paddingBottom: 30
                    }}
                >          
                    {isLoaded && lesson.data &&
                        <View style={{flex: 1}}>
                            <RenderHtml
                                renderers={renderers}
                                WebView={WebView}
                                customHTMLElementModels={customHTMLElementModels}
                                contentWidth={width}
                                source={{ html : lesson.data}}
                                renderersProps={renderersProps}
                            />
                        </View>
                    }
                </ScrollView>
                {renderFrame("PP Beauty & Academy LTD")}
            </View>            
        </BaseScreen>
    )
}

export default LessonDetail;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
    frame: {
        // height: 45,
        minHeight: 45,
        width: "100%",
        backgroundColor: COLORS.SUB,
        flexGrow: 1
    },  
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        paddingTop: 10,
        paddingHorizontal: 20,
        marginBottom: 15
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
})