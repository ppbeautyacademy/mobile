import { useIsFocused } from '@react-navigation/core'
import {
    Text,
    BottomNavigation,
    BottomNavigationTab,
} from '@ui-kitten/components'
import React, {useContext, useEffect, useState} from 'react'
import {
    View,
    Image,
    ImageBackground,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import {Modalize} from 'react-native-modalize'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {translate} from '../../../config/i18n'
import context from '../../../context/context'
import {COLORS} from '../../assets/theme/colors'
import BaseScreen from '../../component/BaseScreen'
import {getRandomColor} from '../../component/color/ColorUtils'
import {ScreenIds} from '../../ScreenIds'
import { getAssignments, getCourseMembers, getLessons, getSubmissions } from '../../services/ServiceWorker'
import AssignmentItem from '../Assignment/AssignmentItem'
import CourseParticipants from '../CourseParticipant/CourseParticipants'
import CourseSubmissions from '../Submission/CourseSubmissions'
import LessonItem from './LessonItem'

export const submissionTypes = {
    mySubmission: {label: "My Submissions", value: "mySubmissions"},
    studentSubmission: {label: "Students Submissions", value: "studentSubmissions"}
}

const LessonListView = ({navigation, route}) => {
    const {state, actions} = useContext(context)
    const {courseId, name, participantId} = route.params;

    //#region MY COURSE TABS
    const [selectedIndex, setSelectedIndex] = useState(0)
    //#endregion

    //#region LOAD LESSONS
    const [lessons, setLessons] = useState([])
    const [areLessonsLoaded, setAreLessonsLoaded] = useState(false)

    const loadLessonsByCourse = async() => {
        let filter = {
            courseId: courseId
        }
        const lessonsData = await getLessons(filter, state.userInfo?.id, participantId);

        if(lessonsData && Array.isArray(lessonsData)) {
            setLessons(lessonsData);
            setAreLessonsLoaded(true)
        }
    }

    const isFocused = useIsFocused();

    useEffect(async() => {
        if(!areLessonsLoaded && isFocused) {
            await loadLessonsByCourse();
        }
    }, [isFocused]);
    // #endregion

    //#region COURSE ASSIGNMENTS
    const [assignments, setAssignments] = useState([]);
    const [areAssignmentsLoaded, setAreAssignmentsLoaded] = useState(false);

    const loadAssignmentsByCourse = async() => {
        const filter = {
            courseId: courseId
        }

        const assignmentData = await getAssignments(filter, participantId, state.userInfo?.id);
        if(assignmentData && Array.isArray(assignmentData)) {
            setAssignments(assignmentData);
            setAreAssignmentsLoaded(true);
        }
    }

    useEffect(async() => {
        if(!areAssignmentsLoaded && isFocused) {
            await loadAssignmentsByCourse();
        }
    }, [isFocused]);
    //#endregion

    //#region COURSE MEMBERS
    const [courseMembers, setCourseMembers] = useState([]);
    const [isCourseMemberLoaded, setIsCourseMemberLoaded] = useState(false);

    const loadCourseMember = async() => {
        const courseMembers = await getCourseMembers(courseId);
        if(courseMembers && Array.isArray(courseMembers)) {
            setCourseMembers(courseMembers);
            setIsCourseMemberLoaded(true);
        }
    }

    useEffect(async() => {
        if(!isCourseMemberLoaded && isFocused) {
            await loadCourseMember();
        }
    }, [isFocused]);
    //#endregion

    //#region MY COURSE SUBMISSIONS
    const [mySubmissions, setMySubmissions] = useState([]);
    const [isSubmissionLoaded, setIsSubmissionLoaded] = useState(false);

    const loadCourseSubmission = async() => {
        let filter = {
            courseId: courseId,
            participantId: participantId
        }
        const courseSubmissions = await getSubmissions(null, null, filter, true);
        if(courseSubmissions && Array.isArray(courseSubmissions)) {
            setMySubmissions(courseSubmissions);
            setIsSubmissionLoaded(true);
        }
    }

    useEffect(async() => {
        if(!isSubmissionLoaded && isFocused) {
            await loadCourseSubmission();
        }
    }, [isFocused]);
    //#endregion

    //#region STUDENTS' SUBMISSIONS
    const [studentSubmissions, setStudentSubmissions] = useState([]);
    const [isStudentSubmissionLoaded, setIsStudentSubmissionLoaded] = useState(false);

    const loadStudentSubmission = async() => {
        let filter = {
            courseId: courseId
        }
        const studentSubmissions = await getSubmissions(state.userInfo?.id, participantId, filter, true);
        if(studentSubmissions && Array.isArray(studentSubmissions)) {
            setStudentSubmissions(studentSubmissions);
            setIsSubmissionLoaded(true);
        }
    }

    useEffect(async() => {
        if(!isStudentSubmissionLoaded && isFocused) {
            await loadStudentSubmission();
        }
    }, [isFocused]);
    //#endregion

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <ImageBackground
                source={require('../../assets/images/lesson-bg.png')}
                style={styles.backgroundImage}>
                <View style={styles.container}>
                    <View style={styles.navigationWrapper}>
                        <View>
                            <TouchableOpacity
                                onPress={() =>
                                    navigation.navigate(ScreenIds.MyCourse)
                                }>
                                <View
                                    style={[
                                        styles.navigationBackground,
                                        styles.navigationContainer,
                                    ]}>
                                    <Icon
                                        name="chevron-left"
                                        size={30}
                                        color={COLORS.SUB}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <Image
                        source={require('../../assets/images/pp-logo.png')}
                        style={{
                            height: 50,
                            width: 50,
                            alignSelf: 'center',
                            marginTop: 10,
                            backgroundColor: COLORS.WHITE,
                            borderRadius: 50,
                        }}
                    />

                    <Text
                        style={{
                            color: '#FFF',
                            fontSize: 25,
                            width: 300,
                            alignSelf: 'center',
                            textAlign: 'center',
                        }}>
                        {name}
                    </Text>
                </View>

                <Modalize
                    handleStyle={styles.handleModalStyle}
                    modalStyle={{
                        borderTopLeftRadius: 30,
                        borderTopRightRadius: 30,
                    }}
                    alwaysOpen={500}
                    scrollViewProps={{showsVerticalScrollIndicator: false, showsHorizontalScrollIndicator: false}}>
                    
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        stickyHeaderIndices={[0]}
                        style={{
                            marginTop: 40
                        }}
                        contentContainerStyle={{
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}
                    >
                        <BottomNavigation
                            selectedIndex={selectedIndex}
                            onSelect={index => setSelectedIndex(index)}
                            style={{
                                flexDirection: 'row'
                            }}>
                            <BottomNavigationTab
                                style={{width: 100}}
                                title={translate('lesson.lessonsCap')}
                            />
                            <BottomNavigationTab
                                style={{width: 100}}
                                title={translate('lesson.assignmentsCap')}
                            />
                            <BottomNavigationTab
                                style={{width: 100}}
                                title={'ĐÃ NỘP'}
                            />
                            <BottomNavigationTab
                                style={{width: 100}}
                                title={translate('lesson.membersCap')}
                            />
                            <BottomNavigationTab
                                style={{width: 100}}
                                title={'CHẤM ĐIỂM'}
                            />
                        </BottomNavigation>
                    </ScrollView>
                    

                    {selectedIndex === 0 && (
                        <View>
                            {lessons &&
                                lessons.length > 0 &&
                                lessons.map((item, idx) => (
                                    <LessonItem
                                        key={idx}
                                        index={idx + 1}
                                        color={getRandomColor()}
                                        duration={item.duration}
                                        name={item.name}
                                        onPress={() =>
                                            navigation.navigate(
                                                ScreenIds.ChapterScreen,
                                                {lessonId: item.id, participantId: participantId},
                                            )
                                        }
                                    />
                                ))}
                        </View>
                    )}                    

                    {selectedIndex === 1 && (
                        <View>
                            {assignments &&
                                assignments.length > 0 &&
                                assignments.map((item, idx) => (
                                    <AssignmentItem
                                        key={idx}
                                        index={idx  + 1}
                                        color={getRandomColor()}
                                        description={item.description}
                                        name={item.name}
                                        onPress={() =>
                                            navigation.navigate(
                                                ScreenIds.AssignmentDetail,
                                                {assignmentId: item.id, participantId: participantId},
                                            )
                                        }
                                    />
                                ))}
                        </View>
                    )}

                    {selectedIndex === 2 && <CourseSubmissions navigation={navigation} submissions={mySubmissions} type={submissionTypes.mySubmission.value} participantId={participantId}/>}

                    {selectedIndex === 3 && <CourseParticipants members={courseMembers}/>}

                    {selectedIndex === 4 && <CourseSubmissions navigation={navigation} submissions={studentSubmissions} type={submissionTypes.studentSubmission.value} participantId={participantId}/>}
                </Modalize>
            </ImageBackground>
        </BaseScreen>
    )
}

export default LessonListView

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
    },
    /****** Image *****/
    backgroundImage: {
        width: '100%',
        height: '100%',
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 30,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /********* Modal *********/
    handleModalStyle: {
        marginTop: 30,
        backgroundColor: COLORS.WHITE,
        width: 80,
    },
})
