import React from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { COLORS } from '../../assets/theme/colors';
import {Text} from '@ui-kitten/components'
import { LANGUAGE_CODES } from '../../assets/constants';
import renderPrice from '../../component/money/PriceUtils';

const TopCourseItem = ({
    index,
    name,
    image,
    version,
    currency,
    coursePriceDisplay,
    onActionPress
}) => {
    return(
        <View style={styles.container}  key={index} >
            <View
                style={styles.cardContainer}
            >
                <View style={{height: 140}}>
                    <FastImage source={{uri: image}} style={styles.image}  resizeMode="contain"/>
                </View>
                <View 
                    style={styles.contentWrapper}
                    onStartShouldSetResponder={onActionPress}
                >
                    <Text style={styles.title}
                        category="p1"
                    >
                        {name}
                    </Text>
                    <Text appearance="hint" style={styles.price} category="label">
                        {version === LANGUAGE_CODES.en.value && `${currency} `}{renderPrice(coursePriceDisplay)}{version === LANGUAGE_CODES.vi.value && ` ${currency}`}
                    </Text>
                </View>
            </View>
        </View>
    )
}

export default TopCourseItem;

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        marginHorizontal: 10,
        height:220,
        width: 180,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
        borderRadius:20,

        backgroundColor: COLORS.WHITE
    },
    /******* Card *********/
    cardContainer: {
        borderRadius: 3
    },
    image: {
        height:120,
        paddingBottom: 10,
        borderTopLeftRadius:10,
        borderTopRightRadius:10
    },
    contentWrapper: {
        backgroundColor: COLORS.WHITE,
        height:50,
        paddingHorizontal: 8,
        paddingVertical: 6,
        zIndex: 5,
        marginTop: -10,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    title: {
        textAlign: 'center',
        minHeight: 40
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 1,
    },
    price: {
        paddingTop: 5,
        textAlign: 'center'
    },
    salePrice: {
        textDecorationLine: 'line-through',
    },
})  
