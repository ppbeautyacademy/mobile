import React, {useContext, useEffect, useRef, useState} from 'react'
import {
    Image,
    FlatList,
    StyleSheet,
    View,
    ImageBackground,
    Dimensions,
} from 'react-native'
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialIcons'
import BaseScreen from '../../component/BaseScreen'
import {SearchBar} from 'react-native-elements'
import {COLORS} from '../../assets/theme/colors'
import CartIcon from '../Cart/CartIcon'
import {Text} from '@ui-kitten/components'
import TopCourseItem from './TopCourseItem'
import context from '../../../context/context'
import {
    filterProductByPopularity,
    getTopCourses,
    removeFavorite,
    saveFavorite,
} from '../../services/ServiceWorker'
import {ScreenIds} from '../../ScreenIds'
import NewProductItem from './NewProductItem'
import FastImage from 'react-native-fast-image'
import {renderProductItem} from '../Products/ProductScreen'
import {LogBox} from 'react-native'
import {translate} from '../../../config/i18n'
import { HTTP_STATUS_OK } from '../../assets/constants'
import { useFocusEffect, useIsFocused } from '@react-navigation/core'
import { useScrollToTop } from '@react-navigation/native'

LogBox.ignoreLogs([
    'VirtualizedLists should never be nested inside plain ScrollViews with the same orientation because it can break windowing and other functionality - use another VirtualizedList-backed container instead.',
])

const HomeScreen = ({navigation}) => {
    const {state} = useContext(context)

    const containerRef = useRef(null);

    const isFocused = useIsFocused();

    useEffect(() => {
        containerRef.current?.scrollTo({x : 0})
    }, [isFocused]);

    //#region Search Section
    const [searchValue, setSearchValue] = useState('')
    //#endregion

    //#region Top Courses
    const [topCourses, setTopCourses] = useState([])

    const loadTopCourses = async () => {
        const topCourses = await getTopCourses(state.version)
        if (topCourses && Array.isArray(topCourses)) {
            setTopCourses(topCourses)
        }
    }

    const handleTopCourseAction = alias => {
        if (alias) {
            navigation.navigate(ScreenIds.CourseDetail, {
                alias: alias,
            })
        }
    }

    useEffect(async() => {
        await loadTopCourses()
    }, [])
    //#endregion

    //#region New Products
    const [newProducts, setNewProducts] = useState([])

    const loadNewProducts = async () => {
        const newProducts = await filterProductByPopularity(
            state.version,
            state.userInfo?.id,
            0,
        )
        if (newProducts && Array.isArray(newProducts)) {
            setNewProducts(newProducts)
        }
    }

    useEffect(async() => {
        await loadNewProducts()
    }, [])
    //#endregion

    //#region Top Products
    const [topProducts, setTopProducts] = useState([])

    const loadTopProducts = async () => {
        const topProducts = await filterProductByPopularity(
            state.version,
            state.userInfo?.id,
            1,
        )
        if (topProducts && Array.isArray(topProducts)) {
            setTopProducts(topProducts)
        }
    }

    const handleProductAction = alias => {
        if (alias) {
            navigation.navigate(ScreenIds.ProductDetail, {
                alias: alias,
            })
        }
    }

    useEffect(async() => {
        await loadTopProducts()
    }, [])
    //#endregion

    //#region Top Products Fav
    const addFav = async(productId) => {
        if (productId) {
            let data = {
                productId: productId,
            }

            const favoritePost = await saveFavorite(state.userInfo?.id, data)

            if (favoritePost && favoritePost.id) {
                const {productsFavorite} = favoritePost;

                if(productsFavorite && productsFavorite.id) {
                    let favTopProducts = [];

                    for(let i = 0; i < topProducts.length; i++) {
                        if(topProducts[i].id === productsFavorite.id) {
                            topProducts[i].liked = true;
                            topProducts[i].favoriteId = favoritePost.id;
                        }

                        favTopProducts.push(topProducts[i])
                    }

                    setTopProducts(favTopProducts)
                }
            }
        }
    }

    const removeFav = async(favoriteId, productId) => {
        if(favoriteId) {
            const favoriteDelete = await removeFavorite(favoriteId)

            if(favoriteDelete === HTTP_STATUS_OK) {
                if(productId) {
                    let favTopProducts = [];

                    for(let i = 0; i < topProducts.length; i++) {
                        if(topProducts[i].id === productId) {
                            topProducts[i].liked = false;
                            topProducts[i].favoriteId = null;
                        }

                        favTopProducts.push(topProducts[i])
                    }

                    setTopProducts(favTopProducts)
                }
            }
        }
    }
    //#endregion

    return (
        <BaseScreen showHeader={false} onBackPress={() => {}}>
            <ScrollView
                enabled={true}
                vertical={true}
                ref={containerRef}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    backgroundColor: COLORS.WHITE,
                    paddingBottom: 50,
                }}>
                <View style={{flex: 1, backgroundColor: COLORS.WHITE}}>
                    <View style={styles.header}>
                        <View
                            style={{
                                height: 40,
                                width: '80%',
                                paddingHorizontal: 20,
                            }}>
                            <Image
                                source={require('../../assets/logo/pp-gradient-align-logo.png')}
                                style={styles.logo}
                            />
                        </View>
                        <CartIcon
                            style={{marginRight: 15, marginTop: 5}}
                            navigation={navigation}
                        />
                    </View>

                    <View
                        style={{
                            marginTop: 25,
                            marginBottom: 10,
                            paddingHorizontal: 20,
                        }}>
                        <Text category="s1">
                            {translate('home.homeScreenTitle')}
                        </Text>
                    </View>

                    <View style={styles.searchContainer}>
                        <SearchBar
                            placeholder="Type Here..."
                            value={searchValue}
                            round
                            lightTheme
                            inputStyle={styles.searchBar}
                            containerStyle={styles.searchBarContainer}
                            inputContainerStyle={styles.searchBarInputContainer}
                            onFocus={() => navigation.navigate(ScreenIds.HomeSearchScreen)}
                        />
                    </View>
                    
                    {topCourses && topCourses.length > 0 && (
                        <View style={styles.sectionSpacing}>
                            <View>
                                <Text category="h5">
                                    {translate('home.courseTrainingSchedule')}
                                </Text>
                            </View>
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}>
                                {topCourses.map((item, idx) => (
                                    <View style={{paddingVertical: 3}} key={idx}>
                                        <TopCourseItem
                                            key={idx}
                                            index={idx}
                                            name={item.name}
                                            image={item.mainImage}
                                            version={item.version}
                                            currency={state.appCurrency}
                                            coursePriceDisplay={item.coursePriceDisplay}
                                            onActionPress={() => handleTopCourseAction(item.alias)}
                                        />
                                    </View>
                                ))}
                            </ScrollView>
                        </View>
                    )}

                    <View style={styles.sectionSpacing}>
                        <View style={styles.browseNewProductsContainer}>
                            <View
                                style={{
                                    paddingVertical: 20,
                                }}>
                                <Text style={styles.browseNewProductsTitle}>
                                    {translate('home.newArrivalsCap')}
                                </Text>
                                <Text category="label" style={{width: '90%'}}>
                                    {translate('home.newArrivalsQuote')}
                                </Text>

                                <TouchableOpacity
                                    onPress={() =>
                                        navigation.navigate(
                                            ScreenIds.Product,
                                        )
                                    }
                                    style={styles.browseNewProductsBtn}>
                                    <Text
                                        style={
                                            styles.browseNewProductBtnLabel
                                        }>
                                        {translate('home.shopNowCap')}
                                    </Text>

                                    <Icon
                                        name="double-arrow"
                                        color={COLORS.WHITE}
                                        size={26}
                                    />
                                </TouchableOpacity>
                            </View>
                            <FastImage
                                source={require('../../assets/images/new-arrival1.png')}
                                style={{
                                    width: 250,
                                    height: 120,
                                    marginLeft: -180,
                                }}
                            />
                        </View>

                        <View
                            style={{
                                marginTop: 10,
                            }}>
                            <Text category="h5">
                                {translate('home.newArrivals')}
                            </Text>
                        </View>
                    </View>

                    {newProducts && newProducts.length > 0 && (
                        <React.Fragment>
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}>
                                {newProducts.map((item, idx) => (
                                    <View style={{paddingVertical: 3}} key={idx}>
                                        <NewProductItem
                                            key={idx}
                                            index={idx}
                                            name={item.name}
                                            image={item.mainImage}
                                            version={state.version}
                                            currency={state.appCurrency}
                                            productPriceDisplay={item.productPriceDisplay}
                                            onActionPress={() => handleProductAction(item.alias)}
                                        />
                                    </View>
                                ))}
                            </ScrollView>
                        </React.Fragment>
                    )}

                    {topProducts && topProducts.length > 0 && (
                        <React.Fragment>
                            <View style={styles.sectionSpacing}>
                                <Text category="h5">
                                    {translate('home.suggestedByUs')}
                                </Text>
                            </View>
                            <View  style={{marginHorizontal: 15}}>
                                <FlatList
                                    key={'#'}
                                    nestedScrollEnabled
                                    showsVerticalScrollIndicator={false}
                                    horizontal={false}
                                    contentContainerStyle={{
                                        paddingTop: 20,
                                    }}
                                    numColumns={2}
                                    data={topProducts}
                                    renderItem={item =>
                                        renderProductItem(item, 2, navigation, false, state.version, state.appCurrency, addFav, removeFav, false, true)
                                    }
                                    keyExtractor={item => {
                                        return item.id
                                    }}
                                />
                            </View>
                        </React.Fragment>
                    )}
                </View>
            </ScrollView>
        </BaseScreen>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    /********* Logo **********/
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        backgroundColor: COLORS.WHITE
    },
    logo: {
        width: '100%',
        height: 50,
        maxHeight: 50,
    },

    /********* Search And Filter **********/
    searchContainer: {
        flexDirection: 'row',
        paddingHorizontal: 20,
    },
    searchBarInputContainer: {
        backgroundColor: COLORS.LIGHT,
        borderRadius: 10,
        height: 45,
    },
    searchBarContainer: {
        width: '100%',
        padding: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
    },
    searchBar: {
        height: 50,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    sortBtn: {
        marginLeft: 10,
        height: 45,
        width: 50,
        borderRadius: 10,
        backgroundColor: COLORS.LIGHT,
        justifyContent: 'center',
        alignItems: 'center',
    },

    /********* Top | Schedule Courses **********/
    courseContainer: {
        marginTop: 20,
        marginBottom: 20,
    },
    /********* New Arrival ************/
    browseNewProductsContainer: {
        flexDirection: 'row',
        backgroundColor: '#FFF2F2',
        borderRadius: 20,
        paddingVertical: 30,
        paddingLeft: 30,
        marginBottom: 10,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 1,
        // },
        // shadowOpacity: 0.20,
        // shadowRadius: 1.41,

        // elevation: 2,
    },
    browseNewProductsTitle: {
        color: '#345c74',
        width: 250,
        paddingRight: 100,
    },
    browseNewProductsBtn: {
        flexDirection: 'row',
        backgroundColor: '#f58084',
        alignItems: 'center',
        marginTop: 20,
        width: 150,
        paddingVertical: 5,
        borderRadius: 10,
        paddingHorizontal: 10,
    },
    browseNewProductBtnLabel: {
        color: COLORS.WHITE,
        fontSize: 14,
        marginRight: 3,
    },
    /******** Card *********/
    cardStyles: {
        backgroundColor: COLORS.WHITE,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    /********* Generic *************/
    sectionSpacing: {
        margin: 20,
    },
})
