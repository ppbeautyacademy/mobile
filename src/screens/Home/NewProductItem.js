import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { COLORS } from '../../assets/theme/colors';
import {Text} from '@ui-kitten/components'
import { LANGUAGE_CODES } from '../../assets/constants';
import renderPrice from '../../component/money/PriceUtils';

const gridWidth = Dimensions.get('window').width / 2 - 32

const NewProductItem = ({
    index,
    name,
    image,
    version,
    currency,
    productPriceDisplay,
    onActionPress
}) => {
    return(
        <View
            key={index}
            style={
                styles.cardExpand
            }>
            <FastImage
                source={{uri: image}}
                style={styles.cardImage}
            />

            <Text style={styles.title} category={"p2"} onPress={onActionPress}>
                {name}
            </Text>
            <View style={styles.priceContainer}>
                <Text appearance="hint" style={styles.price} category="label">
                    {version === LANGUAGE_CODES.en.value && `${currency} `}{renderPrice(productPriceDisplay)}{version === LANGUAGE_CODES.vi.value && ` ${currency}`}
                </Text>
            </View>
        </View>
    )
}

export default NewProductItem;

const styles = StyleSheet.create({
    container: {
        backgroundColor:COLORS.WHITE,
        height:200,
        width:180,
        borderRadius:20,
        marginTop:25,
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
        marginBottom: 5
    },
    /******** card **************/
    card: {
        height: 225,
        backgroundColor: COLORS.WHITE,
        width: gridWidth + 5,
        borderRadius: 10,
        marginBottom: 20,
        padding: 15
    },
    cardExpand: {
        // width: 220,
        marginBottom: 20,
        backgroundColor: COLORS.WHITE,
        borderRadius: 10,
        paddingHorizontal: 15,
        paddingVertical: 5
    },
    cardImage: {
        flex: 1,
        height: 150,
        width: 180,
        resizeMode: 'contain',
        paddingVertical: 5,
        marginTop: 3,
        borderRadius: 10
    },
    mediumMargin: {
        marginLeft: 14,
        marginRight: 10,
    },
    smallMargin: {
        marginLeft: 3,
        marginRight: 5,
    },
    // Detail
    title: {
        // fontSize: 12,
        marginTop: 10,
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 1,
    },
    price: {
        paddingTop: 5,
    },
    salePrice: {
        textDecorationLine: 'line-through',
    },
})  
