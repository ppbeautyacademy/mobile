import React, {useContext, useEffect} from 'react'
import {StyleSheet, TouchableOpacity, View} from 'react-native'
import Animated, {
    interpolate,
    runOnJS,
    useAnimatedStyle,
    useSharedValue,
    withDelay,
    withSequence,
    withSpring,
    withTiming,
} from 'react-native-reanimated'
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {Text} from '@ui-kitten/components'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'
import { COLORS } from '../../assets/theme/colors'
import { LANGUAGE_CODES } from '../../assets/constants'

const CartItem = ({
    version,
    currency,
    cartItem: {id, name, unitPrice, quantity, images, colorName, sizeName},
    index,
    onAdd,
    onDecrease,
    onRemove,
    deletedIndex,
}) => {
    const opacity = useSharedValue(0)
    const itemIsLoaded = useSharedValue(false)

    useEffect(() => {
        opacity.value = withDelay(
            index * 150,
            withSpring(
                1,
                {stiffness: 150, damping: 14},
                () => (itemIsLoaded.value = true),
            ),
        )
    }, [])

    const positionAnimStyle = useAnimatedStyle(() => {
        return {
            opacity: opacity.value,
            top: withDelay(
                (index - deletedIndex.value) * 80,
                withSpring(index * 115, {stiffness: 150, damping: 16}),
            ),
            transform: !itemIsLoaded.value
                ? [{translateY: interpolate(opacity.value, [0, 1], [50, 0])}]
                : [{scale: interpolate(opacity.value, [1, 0], [1, 0])}],
        }
    })

    return (
        <Animated.View style={[styles.container, positionAnimStyle]}>
            <FastImage style={styles.image} source={{uri: images && images[0] && images[0].image && images[0].image.imageUrl }} />
            <View style={styles.detailContainer}>
                <View style={styles.details}>
                    <Text category="p1">{name}</Text>
                    <Text category="label" appearance="hint">{version === LANGUAGE_CODES.en.value && `${currency} `}{unitPrice.toFixed(2)}{version === LANGUAGE_CODES.vi.value && ` ${currency}`}</Text>
                    <Text category="label">{colorName} - {sizeName}</Text>
                </View>
            </View>
            <View style={{position: 'absolute', right: 10, top: 10, zIndex: 10}}>
                <TouchableOpacity
                    style={styles.delete}
                    onPress={() => {
                        if (opacity.value === 1) {
                            deletedIndex.value = index
                            opacity.value = withSequence(
                                withTiming(1.05, {duration: 100}),
                                withTiming(0, {duration: 200}, () =>
                                    runOnJS(onRemove)(id),
                                ),
                            )
                        }
                    }}>
                    <Icon name="close" fill="white" />
                </TouchableOpacity>
            </View>

            <View style={{position: 'absolute', right: 10, bottom: 10, zIndex: 10}}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <TouchableWithoutFeedback
                        disabled={quantity === 1}
                        onPress={() => onDecrease(id, quantity)}
                    >
                        <Icon name="remove" color={quantity === 1 ? COLORS.GRAY_BD : COLORS.SUB}/>
                    </TouchableWithoutFeedback>

                    <Text style={{marginHorizontal: 8}}>{quantity}</Text>

                    <TouchableWithoutFeedback
                        onPress={() => onAdd(id, quantity)}
                    >
                        <Icon name="add" size={18} color={COLORS.SUB}/>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        </Animated.View>
    )
}

export default React.memo(CartItem)

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: 'white',
        flex: 1,
        height: 100,
        borderRadius: 16,
        marginBottom: 10,
        marginTop:10,
        paddingHorizontal: 15,
        paddingVertical: 10,
        justifyContent: 'space-between',
        position: 'absolute',
        top: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        
        elevation: 4
    },
    image: {
        height: '100%',
        width: '20%',
        borderRadius: 8,
        alignSelf: 'center'
    },
    detailContainer: {
        flexDirection: 'row',
        width: '80%',
    },
    details: {
        paddingLeft: 10,
        marginVertical: 10,
        justifyContent: 'space-between',
        width: '100%',
    },
    title: {
        fontFamily: 'Lato-Regular',
        fontSize: 16,
        color: `rgb(15, 5, 33)`,
    },
    total: {
        fontFamily: 'Lato-Black',
        fontSize: 18,
        color: `rgb(15, 5, 33)`,
        marginTop: 10,
    },
    /******* Button ***********/
    actionSection: {
        justifyContent: 'flex-end',
        width: '40%',
    },
    actionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 5,
        paddingBottom: 5,
    },
    quantity: {
        fontFamily: 'Lato-Black',
        fontSize: 16,
        color: `rgb(15, 5, 33)`,
    },
    delete: {
        height: 22,
        width: 22,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: `rgba(0, 0, 41, 0.15)`,
        borderRadius: 50,
    },
    /******* Generic *********/
    textBold: {
        fontWeight: '700',
    },
})
