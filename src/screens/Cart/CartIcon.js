import { Text } from '@ui-kitten/components';
import React, { memo, useContext, useEffect } from 'react';
import Animated, { interpolate, useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated';
import context from '../../../context/context';
import { COLORS } from '../../assets/theme/colors';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { StyleSheet, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ScreenIds } from '../../ScreenIds';
import OrderContext from '../../../context/OrderContext';

const CartIcon = ({navigation, style}) => {
    const {state} = useContext(context);
    const {orderState} = useContext(OrderContext);
  
    const numofAllItems = orderState.productOrder.orderDetailList && orderState.productOrder.orderDetailList.length > 0 ? orderState.productOrder.orderDetailList.length : 0;
    
    //#region Animation
    const opacity = useSharedValue(0);
  
    const badgeAnimationStyle = useAnimatedStyle(() => ({
      opacity: opacity.value,
      transform: [{scale: interpolate(opacity.value, [0, 1], [0, 1])}],
    }));
  
    useEffect(() => {
      if (numofAllItems) {
        opacity.value = withSpring(1, {
          stiffness: 400,
        });
      } else {
        opacity.value = 0;
      }
    }, [numofAllItems]);

    //#endregion
  
    return (
      <TouchableOpacity style={style} onPress={() => navigation.navigate(ScreenIds.Cart)}>
        <View>
            <View
                style={[styles.navigationBackground, styles.navigationContainer]}
            >
                <Icon 
                    name="shopping-bag"
                    size={22}
                    color={COLORS.SUB}
                />
            </View>
  
          {Boolean(numofAllItems) && (
            <Animated.View style={[styles.badge, badgeAnimationStyle]}>
              <Text style={styles.numOfItems}>{numofAllItems}</Text>
            </Animated.View>
          )}
        </View>
      </TouchableOpacity>
    );
};
  
export default memo(CartIcon);

const styles = StyleSheet.create({
    /******* Shopping Bag *******/
    navigationContainer: {
        width: 35,
        height: 35,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    }, 
    /******* Items Badge ********/ 
    badge: {
      position: 'absolute',
      right: 0,
      top: 0,
      backgroundColor: COLORS.ERROR,
      height: 16,
      width: 16,
      borderRadius: 50,
      justifyContent: 'center',
      alignItems: 'center',
      transform: [{scale: 0}],
    },
    numOfItems: {
      color: 'white',
      fontSize: 10,
    },
});