import React, {useMemo} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

const increaseColor = `rgb(79, 60, 230)`;
const decreaseColor = `rgba(0, 0, 41, 0.3)`;

const QuantityButton = ({decrease, increase, onPress}) => {
    return (
        <TouchableOpacity style={styles.btnStyle} onPress={onPress}>
          {increase &&
            <Icon name="add" color="rgb(251,236,234)" />
          }
          {decrease &&
            <Icon name="remove" color="rgb(251,236,234)" />
          }
        </TouchableOpacity>
    );
};

export default React.memo(QuantityButton);

const styles = StyleSheet.create({
  action: {
    height: 36,
    width: 36,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnStyle: {
    height: 22,
    width: 22,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: `#eaa196`,
    borderRadius: 50,
  },
});