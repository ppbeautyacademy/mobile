import React, {useContext, useEffect, useRef, useState} from 'react'
import {
    StyleSheet,
    View,
    Dimensions,
} from 'react-native'
import {BoxShadow} from 'react-native-shadow'
import {Button, Text as KittenText, Icon, Spinner} from '@ui-kitten/components'
import {cartActionTypes} from '../../assets/constants'
import {COLORS} from '../../assets/theme/colors'
import renderPrice from '../../component/money/PriceUtils'
import {translate} from '../../../config/i18n'
import OrderContext from '../../../context/OrderContext'
import { formatMoney } from '../../component/money/MoneyUtils'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

const shadowOpts = {
    width: Dimensions.get('window').width,
    height: 110,
    color: '#0f0521',
    border: 10,
    radius: 0,
    opacity: 0.12,
    x: 0,
    y: 25,
    style: {
        height: 110,
        width: Dimensions.get('window').width,
        position: 'absolute',
        bottom: 0,
    },
}

const paddingShadowOpts = {
    width: Dimensions.get('window').width,
    height: 100,
    color: '#0f0521',
    border: 10,
    radius: 0,
    opacity: 0.05,
    backgroundColor: COLORS.WHITE,
    x: 0,
    y: 25,
    style: {
        height: 100,
        width: Dimensions.get('window').width,
        position: 'absolute',
        bottom: 70,
    },
}

const priceShadowOpts = {
    width: Dimensions.get('window').width,
    color: '#0f0521',
    border: 10,
    radius: 0,
    opacity: 0.12,
    x: 0,
    y: 25,
    style: {
        width: Dimensions.get('window').width,
        position: 'absolute',
        bottom: 0,
    },
}

const CartAction = ({
    type, // to determine action and theme
    label,
    amount,
    currency, // display price
    priceSetting, // display price
    priceOptions,
    priceOptionContainerHeight,
    actionLabel,
    onActionPress,
    disabled,
    isPadding,
    status,
    isLoaded
}) => {
    const {orderState} = useContext(OrderContext)

    //#region ICON ANIMATION
    const checkoutIconRef = useRef()
    const shoppingBagIconRef = useRef()

    const renderCheckoutIcon = props => (
        <Icon
            {...props}
            ref={checkoutIconRef}
            animationConfig={{cycles: Infinity}}
            animation="pulse"
            name="credit-card-outline"
        />
    )

    const renderBagIcon = props => (
        <Icon
            {...props}
            ref={shoppingBagIconRef}
            animationConfig={{cycles: Infinity}}
            animation="pulse"
            name="shopping-bag-outline"
        />
    )

    useEffect(() => {
        if (type === cartActionTypes.order.value) {
            checkoutIconRef.current?.startAnimation()
        } else if (
            type === cartActionTypes.productDetail.value ||
            type === cartActionTypes.courseDetail.value
        ) {
            shoppingBagIconRef.current?.startAnimation()
        }
    }, [])
    //#endregion

    //#region COURSE - PRODUCT Price
    const [priceDisplay, setPriceDisplay] = useState(0)

    useEffect(async () => {
        if (
            type === cartActionTypes.productDetail.value ||
            type === cartActionTypes.courseDetail.value
        ) {
            if (priceSetting) {
                await renderPriceAmount()
            }
        }
    }, [priceSetting])

    const renderPriceAmount = async () => {
        if (priceSetting) {
            const priceAmount = await renderPrice(priceSetting)

            setPriceDisplay(priceAmount)
        }
    }
    //#endregion

    //#region TEMPLATE
    const renderBody = () => {
        // BUTTON CHECK OUT FOR ORDER SCREEN
        if (type === cartActionTypes.order.value) {
            return (
                <View style={[styles.checkoutContainer, {height: 110}]}>
                    <Button
                        accessoryLeft={renderCheckoutIcon}
                        style={styles.checkoutBtn}
                        size="small"
                        onPress={() => handleActionPress()}>
                        {props => (
                            <KittenText
                                {...props}
                                category="p1"
                                style={[styles.mainFont, styles.lightColor]}>
                                {translate('cart.proceedToCheckoutCap')}
                            </KittenText>
                        )}
                    </Button>
                </View>
            )
        }
        // ADD TO CART BUTTON FOR COURSE AND PRODUCT AND BILLING
        else if (
            (type === cartActionTypes.productDetail.value) ||
            (type === cartActionTypes.courseDetail.value)  ||
            (type === cartActionTypes.checkout.value)
        ) {
            return (
                <React.Fragment>
                    <View style={[styles.simpleActionContainer, type ===  cartActionTypes.productDetail.value ? {backgroundColor: COLORS.WHITE} : {} ,priceOptions ? {justifyContent: 'space-around'} : {}]}>
                        <View  style={[styles.actionDetails, !priceOptions ? {paddingVertical: 10} : {marginTop: 10}]}>
                            <KittenText category="h6">
                                {label}
                            </KittenText>
                            {!isLoaded ?
                                <SkeletonPlaceholder>
                                    <View style={{width: 100, height: 30}}></View>
                                </SkeletonPlaceholder>
                            :
                                <View
                                    style={{
                                        flexDirection: 'row'
                                    }}
                                >   
                                    <KittenText
                                    category="h6"
                                    style={[type !== cartActionTypes.checkout.value ? {paddingTop: 4} : {}]}
                                    >
                                        {type === cartActionTypes.checkout.value ? orderState.productOrder?.totalPrice : priceDisplay}
                                    </KittenText>
                                    <KittenText
                                        category="h6"
                                        style={{
                                            marginLeft: 3,
                                        }}>
                                        {currency}
                                    </KittenText>
                                </View>
                            }
                        </View>
                        {priceOptions && <View>{priceOptions}</View>}
                        <View
                            style={[
                                priceOptions
                                    ? {marginBottom: 15, marginTop: 5}
                                    : {},
                            ]}>
                            <Button
                                size="small"
                                accessoryLeft={renderBagIcon}
                                onPress={() => handleActionPress()}>
                                {props => (
                                    <KittenText
                                        {...props}
                                        category="p1"
                                        style={[styles.mainFont, {color: COLORS.WHITE}]}
                                    >
                                        {actionLabel}
                                    </KittenText>
                                )}
                            </Button>
                        </View>
                    </View>
                </React.Fragment>
            )
        }
        // BUTTON TO ORDER SCREEN
        else if(type === cartActionTypes.cart.value || type === cartActionTypes.course.value) {
            return(
                <React.Fragment>
                    <View style={styles.simpleActionContainer}>
                        <View style={[styles.actionDetails, {paddingVertical: 10}]}>
                            <KittenText>
                                Items Total : 
                            </KittenText>
                            <KittenText>
                                { type === cartActionTypes.course.value 
                                    ? orderState.courseOrder.totalPrice && formatMoney(orderState.courseOrder.totalPrice, currency)
                                    :   type === cartActionTypes.cart.value 
                                        &&   orderState.productOrder.totalPrice && formatMoney(orderState.productOrder.totalPrice, currency)}
                            </KittenText>
                        </View> 
                        <View>
                            <Button
                                size="small"
                                accessoryLeft={renderBagIcon}
                                onPress={() => handleActionPress()}
                                disabled={disabled}>
                                {props => (
                                    <KittenText
                                        {...props}
                                        category="p1"
                                        style={[
                                            styles.mainFont,
                                            {color: COLORS.WHITE},
                                        ]}>
                                        CHECK OUT
                                    </KittenText>
                                )}
                            </Button>
                        </View>
                    </View>
                </React.Fragment>
            )
        }
        // Assignment
        else if (type === cartActionTypes.assignmentDetail.value) {
            return (
                <View style={[styles.checkoutContainer, {height: 110, backgroundColor: COLORS.WHITE}]}>
                    <Button
                        style={styles.checkoutBtn}
                        size="small"
                        onPress={() => handleActionPress()}>
                        {props => (
                            <KittenText
                                {...props}
                                category="p1"
                                style={[styles.mainFont, styles.lightColor]}>
                                {actionLabel}
                            </KittenText>
                        )}
                    </Button>
                </View>
            )
        }
        // Submission
        else if(type === cartActionTypes.submissionDetail.value) {
            return (
                <View style={[styles.checkoutContainer, {height: 110, backgroundColor: COLORS.WHITE}]}>
                    <Button
                        style={styles.checkoutBtn}
                        size="small"
                        status={status}
                        onPress={() => handleActionPress()}>
                        {props => (
                            <KittenText
                                {...props}
                                category="p1"
                                style={[styles.mainFont, styles.lightColor]}>
                                {actionLabel}
                            </KittenText>
                        )}
                    </Button>
                </View>
            )
        }
    }

    const handleActionPress = async () => {
        if (onActionPress) {
            await onActionPress()
        }
    }
    //#endregion

    return (
        <BoxShadow
            setting={
                priceOptions
                    ? {
                          ...priceShadowOpts,
                          height: priceOptionContainerHeight
                              ? priceOptionContainerHeight
                              : 135,
                      }
                    : isPadding
                    ? {...paddingShadowOpts}
                    : {...shadowOpts}
            }>
            {renderBody()}
        </BoxShadow>
    )
}

export default CartAction

const styles = StyleSheet.create({
    /****** Action Layout ********/
    actionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 120,
        width: '100%',
        paddingHorizontal: 30,
        justifyContent: 'space-between',
        backgroundColor: COLORS.LIGHT,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    simpleActionContainer: {
        flex: 1,
        height: 200,
        width: '100%',
        paddingHorizontal: 40,
        backgroundColor: COLORS.LIGHT,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    /****** Action Content ********/
    actionEnabled: {
        height: '100%',
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: `rgb(79, 60, 230)`,
    },
    actionDisabled: {
        height: 70,
        flex: 1,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: `rgba(79, 60, 230, 0.2)`,
    },
    actionTitle: {
        fontFamily: 'Lato-Bold',
        fontSize: 18,
        color: 'white',
        marginLeft: 8,
    },
    actionDetails: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    label: {
        fontSize: 18,
    },
    price: {
        fontSize: 24,
    },
    /********* Order Checkout ***********/
    checkoutContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 80,
        width: '100%',
        paddingHorizontal: 30,
        backgroundColor: COLORS.LIGHT,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    checkoutBtn: {
        width: '100%',
    },
    mainFont: {
        fontFamily: 'Prata-Regular',
    },
    lightColor: {
        color: COLORS.WHITE,
    },
    /********* Detail **************/
    detailWrapper: {
        justifyContent: 'center',
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
})
