import {Button, Text} from '@ui-kitten/components'
import React, {useContext, useState, useCallback, useRef} from 'react'
import {StyleSheet, StatusBar, View, ScrollView} from 'react-native'
import FastImage from 'react-native-fast-image'
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler'
import {useSharedValue} from 'react-native-reanimated'
import context from '../../../context/context'
import {cartActionTypes, orderTypes} from '../../assets/constants'
import BaseScreen from '../../component/BaseScreen'
import {ScreenIds} from '../../ScreenIds'
import CartAction from './CartAction'
import CartItem from './CartItem'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../../assets/theme/colors'
import OrderContext from '../../../context/OrderContext'
import { removeItem, updateItemQuantity } from '../../services/ServiceWorker'
import {translate} from '../../../config/i18n'

const CartScreen = ({navigation}) => {
    const {state} = useContext(context);
    const {orderState, orderActions} = useContext(OrderContext);

    //#region Variables
    const data = orderState.productOrder && orderState.productOrder.orderDetailList ? orderState.productOrder.orderDetailList : [];
    const scrollHeight = useRef(data.length * 115)
    const deletedIndex = useSharedValue(0)
    //#endregion

    //#region Cart Functions
    const onPlusQuantity = async(orderDetailId, quantity) => {
        let updateQuantity = quantity + 1;
        let data = {
            id: orderDetailId,
            quantity: updateQuantity
        }

        const orderUpdate = await updateItemQuantity(state.userInfo?.id, orderState.productOrder?.id, data);
        if(orderUpdate && orderUpdate.id) {
            await orderActions.reloadProductOrder(orderUpdate.id)
        }
    }

    const onSubtractQuantity = async(orderDetailId, quantity) => {
        let updateQuantity = quantity - 1;
        let data = {
            id: orderDetailId,
            quantity: updateQuantity
        }

        const orderUpdate = await updateItemQuantity(state.userInfo?.id, orderState.productOrder?.id, data);
        if(orderUpdate && orderUpdate.id) {
            await orderActions.reloadProductOrder(orderUpdate.id)
        }
    }

    const onRemoveItem = async(orderDetailId) => {
        let data = {
            id: orderDetailId
        }
        const order = await removeItem(state.userInfo?.id, orderState.productOrder?.id, data);

        if(order && order.id) {
            await orderActions.reloadProductOrder(order.id)
        }
    }
    //#endregion

    return (
        <BaseScreen title='Cart'>
            <View style={styles.container}>
                <ScrollView
                    scrollEnabled={true}
                    contentContainerStyle={styles.scrollContainer}>
                    {data && data.length > 0 ? (
                        <View
                            style={{
                                height: scrollHeight.current,
                                marginBottom: 50,
                            }}>
                            {data.map((cartItem, index) => (
                                <CartItem
                                    key={cartItem.id}
                                    cartItem={cartItem}
                                    index={index}
                                    onAdd={onPlusQuantity}
                                    onDecrease={onSubtractQuantity}
                                    onRemove={onRemoveItem}
                                    deletedIndex={deletedIndex}
                                    currency={orderState.productOrder?.currency}
                                    version={orderState.productOrder?.version}
                                />
                            ))}
                        </View>
                    ) : (
                        <View>
                            <FastImage
                                source={require('../../assets/images/empty-cart.png')}
                                style={{
                                    height: 250,
                                    width: '100%',
                                    marginTop: 20,
                                }}
                            />
                            <View style={{alignItems: 'center'}}>
                                <Text category="s1">
                                    {translate('cart.cartEmptyMsg')}
                                </Text>
                            </View>
                            <View style={{margin: 20}}>
                                <Button
                                    onPress={() =>
                                        navigation.navigate(ScreenIds.Product)
                                    }
                                    appearance="outline">
                                    {translate('cart.shoppingNowCap')}
                                </Button>
                            </View>
                        </View>
                    )}
                </ScrollView>
                
                {data && data.length > 0 &&
                    <CartAction
                        type={cartActionTypes.checkout.value}
                        onActionPress={() => navigation.navigate(ScreenIds.OrderBillingScreen, {orderType: orderTypes.product.value})}
                        currency={state.appCurrency}
                        label={'Items Total'}
                        actionLabel={'PROCEED TO CHECKOUT'}
                        actionEnabled
                        isLoaded={true}
                    />
                }
            </View>
        </BaseScreen>
    )
}

export default CartScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
    scrollContainer: {
        paddingHorizontal: 20,
        paddingBottom: 100,
    },
    /******** Navigation ***********/
    navigationWrapper: {
        flexDirection: 'row',
        paddingTop: 10,
    },
    navigationContainer: {
        width: 30,
        height: 30,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navigationBackground: {
        backgroundColor: '#FDF3EB',
    },
    /******** generic *********/
    pH20: {
        paddingHorizontal: 20,
    },
    textBold: {
        fontWeight: '700',
    },
})
