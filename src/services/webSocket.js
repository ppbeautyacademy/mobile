import React from 'react'
import SockJS from "sockjs-client";
import Stomp from "webstomp-client";
import { getUsername, isAuthenticated, WEBSOCKET_BASE_URL, WEBSOCKET_PREFIX } from '../assets/constants';

let connected = false;
let socket ='';
let stompClient = '';

const useWebSocket = () => {
  const send = (destination, message, specificUser) => {
    if (stompClient && stompClient.connected) {
      let sendTo = '';

      if(destination) {
        sendTo = `${WEBSOCKET_PREFIX}/${destination}`;
      }

      if(specificUser) {
        sendTo += `/${specificUser}`
      }

      stompClient.send(`${sendTo}`, JSON.stringify(message), {});
    }
  }

  const connect = () => {
    socket = new SockJS(WEBSOCKET_BASE_URL);
    stompClient = Stomp.over(socket);
    
    stompClient.connect(
      isAuthenticated() ? {'username': `${getUsername()}`} : {} ,
      frame => {
        connected = true;
        stompClient.subscribe("/topic/subscribe", tick => {
        });        
      },
      error => {
        console.log(error);
        connected = false;
      }
    );
  }

  const disconnect = () => {
    if (stompClient) {
      stompClient.disconnect();
    }
    connected = false;
  }

  const tickleConnection = () => {
    connected ? disconnect() : connect();
  } 

  return {connect, disconnect, send, tickleConnection}
}

export default useWebSocket