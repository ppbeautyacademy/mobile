import axiosInstance from '../../config/AxiosConfig'
import context from '../../context/context'
import {API_BASE_URL, API_IMAGE_URL} from '../assets/constants'
import {rootNavigationRef} from '../Navigation'
import {ScreenIds} from '../ScreenIds'
import {clearAuthState, setUserAuth} from './AuthService'

export const handleUnauthorizedRequest = async () => {
    await clearAuthState()
    rootNavigationRef.current.navigate(ScreenIds.MainStack)
    rootNavigationRef.current.navigate(ScreenIds.AuthStack)
}

export const getNewToken = async refreshToken => {
    const response = await renewToken({refreshToken})
    if (response) {
        const {token, refreshToken, tokenType} = response
        setUserAuth({token, refreshToken, tokenType})
        return response
    }
    await handleUnauthorizedRequest()
    return null
}

//#region Address Options
export function getProvinces(simple) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/address/getProvinces`, {
            params: {simple},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getDistricts(simple, provinceId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/address/getDistricts`, {
            params: {simple, provinceId},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getWards(simple, districtId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/address/getWards`, {
            params: {simple, districtId},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getCountries(simple) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/address/getCountries`, {
            params: {simple},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getStates(simple, countryCode) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/address/getStates`, {
            params: {simple, countryCode},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getCities(simple, countryCode, stateCode) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/address/getCities`, {
            params: {simple, countryCode, stateCode},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getAddressFormatted(version, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/address/getAddressFormatted`, data, {
            params: {version}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}
//#endregion

//#region Account Management
export function renewToken(refreshToken) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/authenticate/renewToken`, refreshToken)
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function revokeRefreshToken(refreshToken) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/authenticate/revokeToken`, null, {
            params: {refreshToken}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function signIn(account) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/authenticate/getToken`, account)
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function registerUser(version, account) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/register`, account, {
            params: {version},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function forgetPassword(version, account) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/forget-password`, account, {
            params: {version},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function checkResetPasswordCode(version, username, resetPasswordCode) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/check-reset-code`, null, {
            params: {version, username, resetPasswordCode},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function resetPassword(version, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/reset-password`, data, {
            params: {version},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function changePassword(version, account) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/change-password`, account, {
            params: {version},
        })
        .then(res => res.data)
}

export function activateUser(userId, activateCode) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/user/active`, {
            params: {userId, activateCode},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function resendActivationCode(userId, version) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/user/resend-activation-code`, {
            params: {userId, version},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function updateUserProfile(
    version,
    account,
    returnInfo = true,
    byPassAddress = false,
) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/updateProfile`, account, {
            params: {version, returnInfo, byPassAddress},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function updateAddress(userId, version, data, isMobile = true) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/updateAddress`, data, {
            params: {userId, version, isMobile},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function deleteAddress(userId, addressId, isMobile = true) {
    return axiosInstance
        .delete(`${API_BASE_URL}/api/user/deleteAddress`, {
            params: {userId, id: addressId, isMobile},
        })
        .then(res => res.data)
        .catch(error => {
            console.log(error)
            return error
        })
}

export function updateMainAddress(userId, id) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/updateMainAddress`, null, {
            params: {userId, id},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function updateUserAvatar(userId, avatar) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/user/updateUserAvatar`, null, {
            params: {userId, avatar},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getUserInfo(userName) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/user/getUserByUsername`, {
            params: userName,
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

//#endregion

//#region Stripe Checkout
export function getStripePulicKey(userId, version, bypassVersion = true) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/payment/stripe/getPublicKey`, {
            params: {userId, version, bypassVersion},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getStripeToken(data, publicKey) {
    return axiosInstance
        .post(`https://api.stripe.com/v1/tokens`, data, {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: `Bearer ${publicKey}`,
            },
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function chargePayment(
    version,
    orderNumber,
    amount,
    data,
    coupon,
    bypassVersion = true,
) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/payment/stripe/completePayment`, data, {
            params: {version, orderNumber, amount, coupon, bypassVersion},
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getStripeClientSecret(userId, amount, orderNumber, coupon, request) {
    return axiosInstance
        .post(
            `${API_BASE_URL}/api/payment/stripe/createPaymentIntent`,
            request,
            {
                params: {userId, amount, orderNumber, coupon},
            },
        )
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function updateSuccessPayment(orderNumber, coupon, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/payment/stripe/updateSuccessPayment`, data, {
            params: {orderNumber, coupon}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function updateMobileSuccessPayment(orderNumber, couponCode) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/payment/stripe/mobile/updateSuccessPayment`, null, {
            params: {orderNumber, couponCode}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}
//#endregion

//#region IMAGE UPLOAD
export function uploadImageToServer(data) {
    return axiosInstance
        .post(`${API_IMAGE_URL}`, data)
        .then(res => res.data)
        .catch(error => console.log(error))
}
//#endregion

//#region COURSE
export function getCourseList(version, pageNumber, pageRows, userId) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/course/getAll`, null, {
            params: {version, pageNumber, pageRows, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getSelectedCourses(userId, version, orderId) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/course/getSelectedCourses`, null, {
            params: {userId, version, orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getCourseDetail(version, alias, type, userId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/course/getCourseDetail`, {
            params: {version, alias, type, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getCoursePrice(id, type) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/course/getCoursePrice`, {
            params: {id, type}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function searchCourses(version, filter, userId) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/course/searchCourses`, filter, { 
            params: {version, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getTopCourses(version) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/course/getTopCourses`, {
            params: {version}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region PRODUCT
export function getProductBundleList(version, pageNumber, pageRows, userId) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/product/getBundles`, null, {
            params: {version, pageNumber, pageRows, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getProductList(version, pageNumber, pageRows, userId) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/product/getAll`, null, {
            params: {version, pageNumber, pageRows, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getProductDetail(version, alias, colorId, sizeId, userId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/product/getProductDetail`, {
            params: {version, alias, colorId, sizeId, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getProductPrice(id, colorId, sizeId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/product/getProductPrice`, {
            params: {id, colorId, sizeId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function filterProductByKeywords(version, userId, keywords, isBundle = false) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/product/filterByKeywords`, {
            params: {keywords, version, userId, isBundle}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function getProductCategoryOptions(simple = true) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/product/getCategoryOptions`, {
            params: {simple}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function filterProductByCategory(version, userId, categoryId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/product/filterByCategory`, {
            params: {version, categoryId, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

// 0 - NEW | 1 - Top | 2 - Promoted | 3 - On Sale
export function filterProductByPopularity(version, userId, popular) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/product/filterByPopularity`, {
            params: {version, userId, popular}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

//#endregion REVIEW
export function saveComment(userId, pId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/comment/saveComment`, data, {
            params: {userId, pId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#region 

//#region FAVORITE
export function getUserFavorites(userId, version) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/favorite/getListUserFavorite`, {
            params: {userId, version}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function saveFavorite(userId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/favorite`, data, {
            params: {userId}
        })      
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function removeFavorite(id) {
    return axiosInstance
        .delete(`${API_BASE_URL}/api/favorite/deleteFavorite`, {
            params: {id}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region MESSAGE
export function saveMessage(userId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/message/saveMessage`, data, {
            params: {userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function replyMessage(userId, pId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/message/replyTicket`, data, {
            params: {userId, id: pId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function getPageMessages(userId, pageNumber, pageRows, type = "ticket") {
    return axiosInstance
        .post(`${API_BASE_URL}/api/message/getPageMessages`, null, {
            params: {userId, pageNumber, pageRows, type}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function getTicketDetail(userId, id) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/message/getTicketDetail`, {
            params: {userId, id}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function discardTicket(userId, id) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/message/discardTicket`, null, {
            params: {userId, id}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region Orders
export function getUserPageOrders(userId, version) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/getPageOrdersByStatus`, null, {
            params: {userId, version}
        })
        .then(res => res.data)
        .catch(error => console.log(error))
}

export function addToCart(userId, version, orderId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/updateUserOrder`, data, {
            params: {userId, version, orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function loadUserOrder(userId, version, orderId, includeShipment) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/order/loadUserOrder`, {
            params: {userId, orderId, version, includeShipment}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function cancelOrder(userId, version, id) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/cancelOrder`, null, {
            params: {userId, version, id}
        })
        .then(res => res.data).catch(error => console.log(error));
}

export function loadOrderPaymentCompleted(userId, orderNumber) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/order/loadCompleteOrderPayment`, {
            params: {userId, orderNumber}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function loadUserCompleteOrder(userId, version, orderId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/order/loadUserCompleteOrder`, {
            params: {userId, orderId, version}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function removeApplyCoupon(orderId) {
    return axiosInstance
        .put(`${API_BASE_URL}/api/order/removeApplyCoupon`, null, {
            params: {orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function updateItemQuantity(userId, orderId, orderDetail) {
    return axiosInstance
        .put(`${API_BASE_URL}/api/order/updateItemQuantity`, orderDetail, {
            params: {userId, orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function removeItem(userId, orderId, data) {
    return axiosInstance
        .put(`${API_BASE_URL}/api/order/removeItem`, data, {
            params: {userId, orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function updateOrderBillingInfo(userId, orderId, billingInfo, orderNote) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/updateOrderBilling`, billingInfo, {
            params: {userId, orderId, orderNote}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function updateOrderAddress(userId, orderId, billingInfo) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/updateOrderAddress`, billingInfo, {
            params: {userId, orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function updateOrderPaymentType(userId, orderId, type) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/updateOrderPaymentType`, null, {
            params: {userId, orderId, type}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function updateOrderLocation(userId, orderId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/updateOrderLocation`, data, {
            params: {userId, orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function getOrderItemDetails(userId, orderId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/order/getOrderItemDetails`, data, {
            params: {userId, orderId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region COUPON
export function applyCouponCode(userId, orderId, coupon) {
    return axiosInstance.post(`${API_BASE_URL}/api/order/applyCouponCode`, null, {
        params: {userId, orderId, coupon}
    }).then(res => res.data).catch(error => console.log(error));
}

//#endregion

//#region LESSONS
export function getLessons(filter, userId, participantId) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/lesson/getAll`, filter, {
            params: {userId, participantId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function getLessonDetail(id, participantId, userId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/lesson/getById`, {
            params: {id, participantId, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region ASSIGNMENTS
export function getAssignments(filter, participantId, userId) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/assignment/getAll`, filter, {
            params: {participantId, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function getAssignmentDetail(id, participantId, userId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/assignment/getById`, {
            params: {id, participantId, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region SUBMISSIONS
export function getSubmissions(userId, participantId, filter, isDto = false) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/submission/getAllSubmissions`, filter, {
            params: {userId, participantId, isDto}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function getSubmissonDetail(id) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/submission/getById`, {
            params: {id}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function updateSubmission(userId, participantId, assignmentId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/submission/updateSubmission`, data, {
            params: {userId, participantId, assignmentId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function deleteSubmission(submissionId) {
    return axiosInstance
        .delete(`${API_BASE_URL}/api/submission/deleteById`, {
            params: {submissionId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function assessSubmission(userId, participantId, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/submission/assessSubmission`, data, {
            params: {userId, participantId}
        })
        .then(res => res.data)
        .catch(error =>  console.log(error));
}
//#endregion

//#region COURSE MEMBERS
export function getCourseMembers(courseId) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/participant/getCourseParticipants`, {
            params: {courseId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region MY COURSES
export async function getMyCourses(userId, version) {
    return await axiosInstance
        .get(`${API_BASE_URL}/api/participant/getMyCourses`, {
            params: {userId, version}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region EXCHANGE COURSE
export function checkCourseCode(userId, version, code) {
    return axiosInstance
        .get(`${API_BASE_URL}/api/participant/checkCourseCode`, {
            params: {version, code, userId}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}

export function applyCourseCode(userId, version, code, data) {
    return axiosInstance
        .post(`${API_BASE_URL}/api/participant/applyCourseCode`, data, {
            params: {userId, version, code}
        })
        .then(res => res.data)
        .catch(error => console.log(error));
}
//#endregion

//#region SOCIAL AUTH
export function googleAuthenticate(data) {
    return axiosInstance.post(`${API_BASE_URL}/api/social-auth/googleLogin`, data)
    .then(res => res.data).catch(error => console.log(error));
}

export function facebookAuthenticate(data) {
    return axiosInstance.post(`${API_BASE_URL}/api/social-auth/facebookLogin`, data)
    .then(res => res.data).catch(error => console.log(error));
}

export function appleAuthenticate(data) {
    return axiosInstance.post(`${API_BASE_URL}/api/social-auth/appleLogin`, data)
    .then(res => res.data).catch(error => console.log(error));
}

export function getLinkedSocialAccounts(userId) {
    return axiosInstance.get(`${API_BASE_URL}/api/social-auth/getLinkedSocialAccounts`, {
        params: {userId}
    }).then(res => res.data).catch(error => console.log(error));
}

export function linkWithFacebook(userId, version, data) {
    return axiosInstance.post(`${API_BASE_URL}/api/social-auth/linkWithFacebook`, data, {
        params: {userId, version}
    }).then(res => res.data).catch(error => console.log(error));
}

export function linkWithGoogle(userId, version, data) {
    return axiosInstance.post(`${API_BASE_URL}/api/social-auth/linkWithGoogle`, data, {
        params: {userId, version}
    }).then(res => res.data).catch(error => console.log(error));
}

export function unlinkSocialAccount(userId, id, provider) {
    return axiosInstance.post(`${API_BASE_URL}/api/social-auth/unlinkSocialAccount`, null, {
        params: {userId, id, provider}
    }).then(res => res.data).catch(error => console.log(error));
}
//#endregion