import {clearStorage} from '../assets/constants'
import logService from './logService'
import {removeUserCredentials, setUserCredentials} from './secureData'

export const logout = async (resetStateFunc = () => {}) => {
    resetStateFunc()
    await clearStorage()
    await clearAuthState()
}

export async function setUserAuth(auth) {
    try {
        await setUserCredentials(auth)
    } catch (error) {
        logService.log('error setAuthState=', error)
    }
}

export async function clearAuthState() {
    logService.log('clear setAuthState=')
    try {
        await removeUserCredentials()
    } catch (error) {
        logService.log('error setAuthState=', error)
    }
}
