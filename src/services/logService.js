const isDebug = process.env.NODE_ENV === 'development';

const logServiceDebug = console;

const logServiceRelease = {
  log: () => {},
};

const logService = isDebug ? logServiceDebug : logServiceRelease;

const logWithIcon = (icon, name, payload) => {
  if (isDebug) {
    // eslint-disable-next-line no-console
    console.info(icon, name);
    // eslint-disable-next-line no-console
    console.tron.log({[icon]: name, ...payload});
  }
};

export const logNavi = ({name, params}) => {
  logWithIcon('🖥 ', name, params);
};

export const logRestful = (url, payload) => {
  logWithIcon('🚀', url, payload);
};

export const logNotification = (action, payload) => {
  logWithIcon('🔔', action, payload);
};

export default logService;
