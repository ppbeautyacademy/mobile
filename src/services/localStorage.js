import {LOCAL_STORAGE_KEY} from '../assets/constants'
import LocalStorage from '../storage/localStorage'
import logService from './logService'

export const getUserLanguage = async () => {
    let userLanguage = ''
    try {
        userLanguage = await LocalStorage.getItem(LOCAL_STORAGE_KEY.LANGUAGE)
    } catch (error) {
        logService.log(error)
    }
    return userLanguage
}

export const getUserCurreny = async () => {
    let userCurrency = ''
    try {
        userCurrency = await LocalStorage.getItem(LOCAL_STORAGE_KEY.CURRENCY)
    } catch (error) {
        logService.log(error)
    }
    return userCurrency
}
