import {useContext} from 'react'
import context from '../../context/context'
import {deleteAddress, getUserInfo} from './ServiceWorker'

export const useGetUserInfo = ({onSuccess, onFail, showSpinner = false}) => {
    const {state, actions} = useContext(context)

    const startGetInfo = async () => {
        if (showSpinner) {
            actions.showAppSpinner(true)
        }
        const userInfo = await getUserInfo({
            username: state.userInfo?.username,
        })
        if (userInfo) {
            onSuccess(userInfo)
        } else {
            onFail()
        }
        if (showSpinner) {
            actions.showAppSpinner(false)
        }
    }

    return {startGetInfo}
}

export const useDeleteUserAddress = ({
    onSuccess,
    onFail,
    showSpinner = false,
}) => {
    const {state, actions} = useContext(context)

    const startDelete = async addressId => {
        if (showSpinner) {
            actions.showAppSpinner(true)
        }
        const response = await deleteAddress(state.userInfo?.id, addressId)
        if (response) {
            onSuccess(response)
        } else {
            onFail()
        }
        if (showSpinner) {
            actions.showAppSpinner(false)
        }
    }

    return {startDelete}
}
