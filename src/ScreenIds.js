export const ScreenIds = {
    Home: 'HomeScreen',
    HomeSearchScreen: 'HomeSearchScreen',

    Course: 'CourseScreen',
    CourseDetail: 'CourseDetailScreen',
    CourseQuestion: 'CourseQuestion',

    MyCourse: 'MyCourseScreen',
    LessonScreen: 'LessonScreen',
    ChapterScreen: 'ChapterScreen',
    AssignmentDetail: 'AssignmentDetail',
    SubmissionScreen: 'SubmissionScreen',
    SubmissionDetail: 'SubmissionDetail',
    CourseExchange: 'CourseExchange',

    Product: 'ProductScreen',
    ProductDetail: 'ProductDetailScreen',
    ProductSearch: 'ProductSearchScreen',
    ProductComment: 'ProductCommentScreen',

    Cart: 'CartScreen',
    OrderScreen: 'OrderScreen',
    OrderBillingScreen: 'OrderBillingScreen',
    OrderCheckOutScreen: 'OrderCheckOutScreen',
    OrderCompleteScreen: 'OrderCompleteScreen',
    OrderHistoryScreen: "OrderHistoryScreen",
    OrderHistoryDetailScreen: "OrderHistoryDetailScreen",

    MainStack: 'MainStack',
    AuthStack: 'AuthStack',
    MainTab: 'MainTab',

    Login: 'Login',
    SignUp: 'SignUp',
    ForgetPassword: 'ForgetPassword',
    Activation: 'Activation',
    ProfileSetUp: 'ProfileSetUp',

    SplashScreen: 'SplashScreen',

    Account: 'Account',
    EditProfileScreen: 'EditProfileScreen',
    ChangePasswordScreen: 'ChangePasswordScreen',
    AddressScreen: 'AddressScreen',
    SocialConnectedScreen: 'SocialConnectedScreen',
    SupportScreen: 'SupportScreen',
    SupportDetailScreen: 'SupportDetailScreen',
    PolicyScreen: 'PolicyScreen',
    TermOfUseScreen: 'TermOfUseScreen',
    DisclaimerScreen: 'DisclaimerScreen',
    ReturnAndRefundScreen: 'ReturnAndRefundScreen',
    FavoriteScreen: 'FavoriteScreen',
    AboutScreen: 'AboutScreen',
    ContactScreen: 'ContactScreen',

    TestScreen: "TestScreen"
}
