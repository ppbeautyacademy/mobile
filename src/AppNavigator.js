import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import React from 'react'

import {ScreenIds} from './ScreenIds'
import HomeScreen from './screens/Home/HomeScreen'
import CourseScreen from './screens/Courses/CourseScreen'
import ProductScreen from './screens/Products/ProductScreen'
import AccountScreen from './screens/Account/AccountScreen'
import {setRootNavigationRef} from './Navigation'
import {translate} from '../config/i18n'
import {COLORS} from './assets/theme/colors'
import {BottomFabBar} from 'rn-wave-bottom-bar'
import {Icon as KittenIcon} from '@ui-kitten/components'
import LoginScreen from './screens/Authentication/LoginScreen'
import SignUpScreen from './screens/Authentication/SignUpScreen'
import ActivationScreen from './screens/Authentication/ActivationScreen'
import ProfileSetUpScreen from './screens/Authentication/ProfileSetUpScreen'
import ForgetPasswordScreen from './screens/Authentication/ForgetPasswordScreen'
import SplashScreen from './component/splashscreen/SplashScreen'
import AppWithAxios from '../context/AppWithAxios'
import EditProfileScreen from './screens/Account/EditProfileScreen'
import CourseDetail from './screens/Courses/CourseDetail'
import ProductDetailScreen from './screens/Products/ProductDetailScreen'
import CartScreen from './screens/Cart/CartScreen'
import ChangePasswordScreen from './screens/Account/ChangePasswordScreen'
import OrderHistoryScreen from './screens/Order/OrderHistoryScreen'
import BillingScreen from './screens/Billing/BillingScreen'
import CheckoutForm from './component/payment/CheckoutForm'
import MyCourseScreen from './screens/MyCourse/MyCourseScreen'
import LessonListView from './screens/Lesson/LessonListView'
import ClaimCourse from './screens/MyCourse/ClaimCourse'
import ProductSearchScreen from './screens/Products/ProductSearchSreen'
import ProductCommentScreen from './screens/Products/ProductComment'
import CourseQuestionScreen from './screens/Courses/CourseQuestion'
import PolicyScreen from './screens/Account/PolicyScreen'
import FavoriteScreen from './screens/Account/FavoriteScreen'
import LessonDetail from './screens/Lesson/LessonDetail'
import OrderCompleteScreen from './screens/Order/OrderCompleteScreen'
import HomeSearchScreen from './screens/Home/HomeSearchScreen'
import AssignmentDetail from './screens/Assignment/AssignmentDetail'
import SubmissionScreen from './screens/Submission/SubmissionScreen'
import SubmissionDetail from './screens/Submission/SubmissionDetail'
import TermOfUseScreen from './screens/Account/TermOfUseScreen'
import Disclaimer from './screens/Account/Disclaimer'
import ReturnAndRefund from './screens/Account/ReturnAndRefund'
import AboutScreen from './screens/Account/AboutScreen'
import ContactScreen from './screens/Account/ContactScreen'
import AddressScreen from './screens/Account/AddressScreen'
import SocialConnectedScreen from './screens/Account/SocialConnected'
import OrderHistoryDetail from './screens/Order/OrderHistoryDetail'
import SupportScreen from './screens/Account/SupportScreen/SupportScreen'
import SupportDetailScreen from './screens/Account/SupportScreen/SupportDetailScreen'
import GooglePayForm from './component/payment/GooglePayForm'

const RootStack = createStackNavigator()
const AppStack = createStackNavigator()
const MainTab = createBottomTabNavigator()
const AuthStack = createStackNavigator()

const AuthStackScreen = () => (
    <AuthStack.Navigator headerMode="none">
        <AuthStack.Screen name={ScreenIds.Login} component={LoginScreen} />
        <AuthStack.Screen name={ScreenIds.SignUp} component={SignUpScreen} />
        <AuthStack.Screen
            name={ScreenIds.Activation}
            component={ActivationScreen}
        />
        <AuthStack.Screen
            name={ScreenIds.ProfileSetUp}
            component={ProfileSetUpScreen}
        />
        <AuthStack.Screen
            name={ScreenIds.ForgetPassword}
            component={ForgetPasswordScreen}
        />
    </AuthStack.Navigator>
)

const createTabBarIcon =
    (name: string) =>
    ({focused, color, size}: {focused: boolean, color: string, size: number}) =>
        <KittenIcon style={{width:25, height: 25}} name={focused ? name : `${name}-outline`} fill={focused ? '#eaa196' : 'grey'} />

const MainTabNavigator = ({navigation}) => {
    setRootNavigationRef(navigation)

    return (
        <MainTab.Navigator
            headerMode="none"
            lazy
            tabBarOptions={{
                activeTintColor: COLORS.WHITE,
            }}
            tabBar={props => (
                <BottomFabBar
                    bottomBarContainerStyle={{
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        right: 0,
                    }}
                    color={COLORS.WHITE}
                    {...props}
                    isRtl={false}
                />
            )}>
            <MainTab.Screen
                name={ScreenIds.Home}
                component={HomeScreen}
                options={{
                    title: translate('common.home'),
                    tabBarIcon: createTabBarIcon('home'),
                }}
            />
            <MainTab.Screen
                name={ScreenIds.Course}
                component={CourseScreen}
                options={{
                    tabBarLabel: translate('common.course'),
                    tabBarIcon: createTabBarIcon('book'),
                }}
            />
            <MainTab.Screen
                name={ScreenIds.Product}
                component={ProductScreen}
                options={{
                    title: translate('common.product'),
                    tabBarIcon: createTabBarIcon('shopping-bag'),
                }}
            />
            <MainTab.Screen
                name={ScreenIds.MyCourse}
                component={MyCourseScreen}
                options={{
                    title: translate('myCourse'),
                    tabBarIcon: createTabBarIcon('folder'),
                }}
            />
            <MainTab.Screen
                name={ScreenIds.Account}
                component={AccountScreen}
                options={{
                    title: translate('common.account'),
                    tabBarIcon: createTabBarIcon('person'),
                }}
            />
        </MainTab.Navigator>
    )
}

const MainStackScreen = () => (
    <AppStack.Navigator headerMode="none">
        {/* Main Nav */}
        <AppStack.Screen
            name={ScreenIds.SplashScreen}
            component={SplashScreen}
        />
        <AppStack.Screen
            name={ScreenIds.MainTab}
            component={MainTabNavigator}
        />
        {/* End Main Nav */}

        {/* Home Screen */}
        <AppStack.Screen
            name={ScreenIds.HomeSearchScreen}
            component={HomeSearchScreen}
        />
        {/* End Home Screen */}

        {/* Account Screen */}
        <AppStack.Screen
            name={ScreenIds.PolicyScreen}
            component={PolicyScreen}
        />
        <AppStack.Screen
            name={ScreenIds.TermOfUseScreen}
            component={TermOfUseScreen}
        />
        <AppStack.Screen
            name={ScreenIds.ReturnAndRefundScreen}
            component={ReturnAndRefund}
        />
        <AppStack.Screen
            name={ScreenIds.DisclaimerScreen}
            component={Disclaimer}
        />
        <AppStack.Screen
            name={ScreenIds.AboutScreen}
            component={AboutScreen}
        />
        <AppStack.Screen
            name={ScreenIds.ContactScreen}
            component={ContactScreen}
        />

        <AppStack.Screen
            name={ScreenIds.EditProfileScreen}
            component={EditProfileScreen}
        />
        <AppStack.Screen
            name={ScreenIds.ChangePasswordScreen}
            component={ChangePasswordScreen}
        />
        <AppStack.Screen
            name={ScreenIds.AddressScreen}
            component={AddressScreen}
        /> 
        <AppStack.Screen
            name={ScreenIds.SocialConnectedScreen}
            component={SocialConnectedScreen}
        />     

        <AppStack.Screen
            name={ScreenIds.OrderHistoryScreen}
            component={OrderHistoryScreen}
        />
        <AppStack.Screen
            name={ScreenIds.OrderHistoryDetailScreen}
            component={OrderHistoryDetail}
        />
        <AppStack.Screen
            name={ScreenIds.FavoriteScreen}
            component={FavoriteScreen}
        />
        <AppStack.Screen
            name={ScreenIds.SupportScreen}
            component={SupportScreen}
        />
          <AppStack.Screen
            name={ScreenIds.SupportDetailScreen}
            component={SupportDetailScreen}
        />
        {/* End Account Screen */}
        
        <AppStack.Screen
            name={ScreenIds.CourseDetail}
            component={CourseDetail} 
        />
        <AppStack.Screen
            name={ScreenIds.CourseQuestion}
            component={CourseQuestionScreen} 
        />
        <AppStack.Screen
            name={ScreenIds.ProductDetail}
            component={ProductDetailScreen}
        />  
        <AppStack.Screen
            name={ScreenIds.ProductSearch}
            component={ProductSearchScreen}
        /> 
        <AppStack.Screen
            name={ScreenIds.ProductComment}
            component={ProductCommentScreen}
        /> 
        <AppStack.Screen
            name={ScreenIds.LessonScreen}
            component={LessonListView}
        />
        <AppStack.Screen
            name={ScreenIds.ChapterScreen}
            component={LessonDetail}
        />
        <AppStack.Screen
            name={ScreenIds.AssignmentDetail}
            component={AssignmentDetail}
        />
        <AppStack.Screen
            name={ScreenIds.SubmissionScreen}
            component={SubmissionScreen}
        />
        <AppStack.Screen
            name={ScreenIds.SubmissionDetail}
            component={SubmissionDetail}
        />
        <AppStack.Screen
            name={ScreenIds.Cart}
            component={CartScreen}
        />
        <AppStack.Screen
            name={ScreenIds.OrderBillingScreen}
            component={BillingScreen}
        />
        <AppStack.Screen
            name={ScreenIds.OrderCheckOutScreen}
            component={CheckoutForm}
        />
        <AppStack.Screen
            name={ScreenIds.OrderCompleteScreen}
            component={OrderCompleteScreen}
        />
       
        
        <AppStack.Screen 
            name={ScreenIds.CourseExchange}
            component={ClaimCourse}
        />
    </AppStack.Navigator>
)

const RootStackScreen = () => (
    <RootStack.Navigator mode="card" headerMode="none">
        <RootStack.Screen
            name={ScreenIds.MainStack}
            component={MainStackScreen}
        />
        <RootStack.Screen
            name={ScreenIds.AuthStack}
            component={AuthStackScreen}
        />
    </RootStack.Navigator>
)

const AppNavigator = props => {
    return (
        <AppWithAxios>
            <NavigationContainer>
                <RootStackScreen />
            </NavigationContainer>
        </AppWithAxios>
    )
}

export default AppNavigator
