import React from 'react';

export function filterMainImageToTop(images) {
    let filterImages = [];

    if(!images) {
        return filterImages;
    }

    for(let i = 0; i < images.length; i++) {
        if(images[i].main) {
            filterImages.unshift(images[i])
        } else {
            filterImages.push(images[i])
        }
    }

    return filterImages;
}