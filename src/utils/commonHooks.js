import {useEffect} from 'react';
import {InteractionManager} from 'react-native';

const useMount = func =>
  useEffect(() => {
    func && func();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

const useMountInteraction = func =>
  useEffect(() => {
    callAfterInteraction(func);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

function callAfterInteraction(func) {
  InteractionManager.runAfterInteractions(() => {
    func && func();
  });
}

export {callAfterInteraction, useMount, useMountInteraction};
