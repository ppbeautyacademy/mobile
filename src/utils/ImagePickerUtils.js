import {findIndex} from 'lodash';

export const IMAGE_MIME = {
  JPEG: 'image/jpeg',
  PNG: 'image/png',
  JPG: 'image/jpg',
};

const mapImagesUrisToUI = (imageUris, startIndex) => {
  if (!imageUris || imageUris?.length === 0 || !Array.isArray(imageUris)) {
    return [];
  }
  let itemId = startIndex;
  return imageUris.map(e => {
    return {id: itemId++, uri: e, url: e};
  });
};

const mapEditedImageToArrayImages = (id, editedImage, images) => {
  if (!images || !images.length) {
    return [];
  }

  if (!editedImage) {
    return [...images];
  }

  const newArr = [...images];
  const index = findIndex(images, {id});
  if (index !== -1) {
    newArr[index].uri = editedImage.uri;
  }

  return newArr;
};

const checkMime = image => {
  return (
    image.mime === IMAGE_MIME.JPEG ||
    image.mime === IMAGE_MIME.PNG ||
    image.mime === IMAGE_MIME.JPG
  );
};

function getLastPathComponent(path) {
  if (path == null || typeof path !== 'string') {
    return null;
  }

  //remove '/' at the end if needed
  const pathSeparator = '/';
  let lastComponent = path;
  const lastCharacter = path.substring(path.length - 1, 1);
  if (lastCharacter === pathSeparator) {
    lastComponent = lastComponent.substr(0, path.length - 2);
  }

  //
  const startIndex = path.lastIndexOf(pathSeparator) + 1;
  if (startIndex >= path.length) {
    return null;
  }

  lastComponent = lastComponent.substr(startIndex);
  return lastComponent;
}

function getImageSource(uri) {
  if (uri) {
    const imageName = getLastPathComponent(uri);
    imageSource = {uri: uri, name: imageName};
  }
  return imageSource;
}

const ImagePickerUtils = {
  mapImagesUrisToUI,
  mapEditedImageToArrayImages,
  checkMime,
  getImageSource,
  getLastPathComponent,
};

export default ImagePickerUtils;
