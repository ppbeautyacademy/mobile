export const getValidUrl = url => {
  let validUrl = null;

  if (url && (url.startsWith('http://') || url.startsWith('https://'))) {
    validUrl = url;
  }

  return validUrl;
};
