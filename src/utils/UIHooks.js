const useSyncList = (tabList, headerDiff) => {
    return event => {
        const {y} = event.nativeEvent.contentOffset

        for (const {ref, scrollY} of tabList) {
            const scrollPosition = scrollY.value ?? 0

            if (scrollPosition > headerDiff && y > headerDiff) {
                continue
            }

            ref.current?.scrollToOffset({
                offset: Math.min(y, headerDiff),
                animated: false,
            })
        }
    }
}

export {useSyncList}
