/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import * as eva from '@eva-design/eva'
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components'
import {EvaIconsPack} from '@ui-kitten/eva-icons'
import Toast, {ToastProvider} from 'react-native-fast-toast'
import {InAppNotificationProvider} from 'react-native-in-app-notification'

import {default as theme} from '../theme/app-theme.json' // <-- Import app theme
import {default as mapping} from '../theme/mapping.json'
import Context from '../context/context'
import GlobalState from '../context/GlobalState'
import AppNavigator from './AppNavigator'
import GlobalFont from '../config/GlobalFont'
import { Root } from 'popup-ui'
import OrderState from '../context/OrderState'

class App extends React.Component {
    constructor() {
        super()

        this.state = {
            theme: 'light',
        }
    }

    async componentDidMount() {
        GlobalFont.applyGlobal();
    }

    render() {
        return (
            <React.Fragment>
                <IconRegistry icons={EvaIconsPack} />
                <GlobalState>
                    <Context.Consumer>
                        {context => (
                            <ApplicationProvider
                                {...eva}
                                customMapping={mapping}
                                theme={{
                                    ...eva[context.state.theme],
                                    ...theme,
                                }}>
                                <OrderState>
                                    <ToastProvider>
                                        <InAppNotificationProvider
                                            closeInterval={3000}
                                            backgroundColour={'#fbecea'}>
                                                <Root>
                                                    <AppNavigator />
                                                </Root>
                                        </InAppNotificationProvider>
                                    </ToastProvider>
                                </OrderState>
                            </ApplicationProvider>
                        )}
                    </Context.Consumer>
                </GlobalState>
                <Toast ref={ref => (global['toast'] = ref)} />
            </React.Fragment>
        )
    }
}
export default App

// TODO TOAST PROVIDER
